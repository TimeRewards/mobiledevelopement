package com.timerewards.tr.DataModels;

import java.io.Serializable;

public class TimeSheetResponse implements Serializable {

    private String $id;
    private int UserID;
    private int EZStatusID;
    private int TimeActivityID;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public int getEZStatusID() {
        return EZStatusID;
    }

    public void setEZStatusID(int EZStatusID) {
        this.EZStatusID = EZStatusID;
    }

    public int getTimeActivityID() {
        return TimeActivityID;
    }

    public void setTimeActivityID(int timeActivityID) {
        TimeActivityID = timeActivityID;
    }
}
