package com.timerewards.tr.DataModels;

import java.io.Serializable;

public class EmployeeLogin implements Serializable {

    private String $id;
    private boolean IsValid;
    private int Userid;
    private String DisplayName;
    private String SiteCode;
    private String EmployeeSignature = "";
    private String ApproverSignature = "";

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public boolean isValid() {
        return IsValid;
    }

    public void setIsValid(boolean isValid) {
        IsValid = isValid;
    }

    public int getUserid() {
        return Userid;
    }

    public void setUserid(int userid) {
        Userid = userid;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public String getSiteCode() {
        return SiteCode;
    }

    public void setSiteCode(String siteCode) {
        SiteCode = siteCode;
    }

    public String getEmployeeSignature() {
        return EmployeeSignature;
    }

    public void setEmployeeSignature(String employeeSignature) {
        EmployeeSignature = employeeSignature;
    }

    public String getApproverSignature() {
        return ApproverSignature;
    }

    public void setApproverSignature(String approverSignature) {
        ApproverSignature = approverSignature;
    }
}
