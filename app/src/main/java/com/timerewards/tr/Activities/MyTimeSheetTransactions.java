package com.timerewards.tr.Activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Adapters.TimesheetDatesAdapter;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.DataModels.TimeSheetResponse;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MyTimeSheetTransactions extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private TimesheetDatesAdapter timesheetDatesAdapter;
    private ListView listView;
    private Button addTimesheet;
    private TextView startDate, endDate;
    private ImageView previousWeek, nextWeek;
    private Timesheet timesheet;

    private SimpleDateFormat simpleDateFormat;
    private SimpleDateFormat simpleDateFormatNotification = new SimpleDateFormat("yyyy-MM-dd");
    private Calendar calendar;
    private Calendar tempEndDate;
    private ArrayList<String> dates;
    private ArrayList<String> hoursArrayList = new ArrayList<>();
    private TableObject tableObject;
    private ArrayList<Timesheet> timesheetArrayListOLD;
    private ArrayList<Timesheet> timesheetArrayListNEW;
    private EmployeeLogin employeeLogin;
    private ArrayList<Timesheet> timesheetArrayListFromServer = new ArrayList<>();
    private GetOptionsDataFile getOptionsDataFile;
    private ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();
    private TextView textReason;
    private EditText etApproverSignature;
    private Button dialogOkButton;
    private Button dialogCancelButton;
    private TextView tvTotalHours;
    private int numOfRequests = 0;
    private int numOfItems = 0;
    private boolean toRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        toRefresh = false;
        tableObject = (TableObject) OfflineDataStorage.readObject(this, Constants.TIMESHEETS_FILE);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(this, Constants.GETOPTIONS_FILE);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(this, Constants.EMPLOYEE_LOGIN_FILE);
        if (getOptionsDataFile.isEmpSigRequiredforTS()) {
            if (employeeLogin.getEmployeeSignature() != null && employeeLogin.getEmployeeSignature().length() > 0) {

            } else {
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.dialog_set_emp_signature);
                dialog.setTitle("Set Your Signature");
                textReason = (TextView) dialog.findViewById(R.id.text_reason);
                textReason.setVisibility(View.GONE);
                etApproverSignature = (EditText) dialog.findViewById(R.id.et_dialog_approver_signature);
                dialogOkButton = (Button) dialog.findViewById(R.id.button_dialog_ok);
                dialogCancelButton = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                dialog.setCancelable(false);
                dialogOkButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (etApproverSignature.getText().toString().length() > 0) {
                            employeeLogin.setEmployeeSignature(etApproverSignature.getText().toString());
                            OfflineDataStorage.writeObject(MyTimeSheetTransactions.this, employeeLogin, Constants.EMPLOYEE_LOGIN_FILE);
                            dialog.dismiss();
                        } else {
                            etApproverSignature.setError("Can't be left blank!");
                        }
                    }
                });

                dialogCancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
                dialog.show();
            }
        }
        init();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Timesheets");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        listView.setAdapter(timesheetDatesAdapter);
        if (getIntent().hasExtra(Constants.RELOAD_LIST)) {
            getTimesheetFromServer();
        }
//        Toast.makeText(this, "No. of timesheets = " + tableObject.getTimesheetList().size(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        tableObject = (TableObject) OfflineDataStorage.readObject(this, Constants.TIMESHEETS_FILE);
        super.onResume();
        if (toRefresh) {
            timesheetDatesAdapter.changeData(dates);
            tvTotalHours.setText("Total Hours " + String.format("%.2f", (double) Math.round(timesheetDatesAdapter.getTotalHours() * 100) / 100));
        } else {
            toRefresh = true;
        }
    }

    private void init() {
        tvTotalHours = (TextView) findViewById(R.id.tv_total_hours);
        timesheetArrayListOLD = tableObject.getTimesheetList();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        listView = (ListView) findViewById(R.id.listView_dates);
        startDate = (TextView) findViewById(R.id.tv_start_date);
        endDate = (TextView) findViewById(R.id.tv_end_date);
        simpleDateFormat = new SimpleDateFormat("dd-MM");
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("dd-MMM");
        startDate.setText(simpleDateFormat.format(calendar.getTimeInMillis()));
        tempEndDate = calendar;
        calendar.add(Calendar.DATE, +6);
        endDate.setText(simpleDateFormat.format(calendar.getTimeInMillis()));
        calendar.add(Calendar.DATE, -6);
        setDates();
        addTimesheet = (Button) findViewById(R.id.button_add_timesheet);
        addTimesheet.setOnClickListener(this);
        previousWeek = (ImageView) findViewById(R.id.iv_previous_week);
        previousWeek.setOnClickListener(this);
        nextWeek = (ImageView) findViewById(R.id.iv_next_week);
        nextWeek.setOnClickListener(this);
    }

    private void setDates() {

        //if timesheet frequency = 1 then this is weekly
        //if timesheet frequency = 2 then this is semi-monthly
        //if timesheet frequency = 4 then this is bi-weekly

        Date date = new Date();
        Log.i("arsh", date.toString());
        dates = new ArrayList<>();
//        getOptionsDataFile.setLastDayOfWeek(5);
//        getOptionsDataFile.setTimeSheetFrequency(4);
        timesheetDatesAdapter = new TimesheetDatesAdapter(this, dates);
        Calendar calendarNotificationDate = Calendar.getInstance();  // if this activity is opened from notification then date is received.
//        calendarNotificationDate.setTimeZone(TimeZone.getTimeZone("UTC"));
//        simpleDateFormatNotification.setTimeZone(TimeZone.getTimeZone("UTC"));
        if (getIntent().hasExtra("date")) {
            try {
                long millis = simpleDateFormatNotification.parse(getIntent().getStringExtra("date")).getTime();
                calendarNotificationDate.setTimeInMillis(millis);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        tvTotalHours.setText("Total Hours " + String.valueOf(timesheetDatesAdapter.getTotalHours()));
        if (getOptionsDataFile.getTimeSheetFrequency() == 1) {
            int todaysPositionInWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
            if (getIntent().hasExtra("date")) {
                todaysPositionInWeek = calendarNotificationDate.get(Calendar.DAY_OF_WEEK);
                tempEndDate = calendarNotificationDate;
            }
            int lastDayOfWeek = (getOptionsDataFile.getLastDayOfWeek() % 7);
            if (todaysPositionInWeek == lastDayOfWeek) {

            }
            if (todaysPositionInWeek < lastDayOfWeek) {
                int days = lastDayOfWeek - todaysPositionInWeek;
                tempEndDate.add(Calendar.DATE, days);
            }
            if (todaysPositionInWeek > lastDayOfWeek) {
                int days = todaysPositionInWeek - lastDayOfWeek;
                tempEndDate.add(Calendar.DATE, 7 - days);
            }

            tempEndDate.add(Calendar.DATE, -6);
            startDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
            simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            simpleDateFormat = new SimpleDateFormat("dd-MMM");
            endDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
        }

        if (getOptionsDataFile.getTimeSheetFrequency() == 2) {
            Calendar todayDate = Calendar.getInstance();
            int todaysPositionInMonth = todayDate.get(Calendar.DAY_OF_MONTH);
            if (getIntent().hasExtra("date")) {
                todaysPositionInMonth = calendarNotificationDate.get(Calendar.DAY_OF_MONTH);
                todayDate = calendarNotificationDate;
            }
            if (todaysPositionInMonth <= 15) {
                Log.i("arsh", todayDate.toString());
                int days = 0 - todayDate.get(Calendar.DAY_OF_MONTH);
                todayDate.add(Calendar.DATE, days + 1);
                Log.i("arsh", "today date = " + todayDate.toString());
            } else {
                Log.i("arsh", todayDate.toString());
                int days = todaysPositionInMonth - 16;
                todayDate.add(Calendar.DATE, -days);
                Log.i("arsh", "today date = " + todayDate.toString());
            }

            int size = 0;
            if (todaysPositionInMonth <= 15) {
                size = 14;
            } else {
                size = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH) - 16;
            }
            dates = new ArrayList<String>();
            tempEndDate = todayDate;
            startDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
            simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            for (int i = 0; i < size; i++) {
                dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
                tempEndDate.add(Calendar.DATE, 1);
            }
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            simpleDateFormat = new SimpleDateFormat("dd-MMM");
            endDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
        }
        if (getOptionsDataFile.getTimeSheetFrequency() == 4) {

            Date biWeeklyEndDate = null;
            try {
                biWeeklyEndDate = new SimpleDateFormat("MM/dd/yyyy").parse(getOptionsDataFile.getBiWeeklyEndDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar biWeeklyEndDay = Calendar.getInstance();
            biWeeklyEndDay.setTime(biWeeklyEndDate);
            long millis = Calendar.getInstance().getTimeInMillis() - biWeeklyEndDate.getTime();
            if (getIntent().hasExtra("date")) {
                millis = calendarNotificationDate.getTimeInMillis() - biWeeklyEndDate.getTime();
            }
            int days = (int) TimeUnit.DAYS.convert(millis, TimeUnit.MILLISECONDS) % 14;
            tempEndDate = Calendar.getInstance();
            if (getIntent().hasExtra("date")) {
                tempEndDate = calendarNotificationDate;
            }
            tempEndDate.add(Calendar.DATE, 15 - days);
            tempEndDate.add(Calendar.DATE, -14);
            dates = new ArrayList<>();
            startDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
            simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            for (int i = 0; i < 13; i++) {
                dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
                tempEndDate.add(Calendar.DATE, 1);
            }
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            simpleDateFormat = new SimpleDateFormat("dd-MMM");
            endDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
            Log.i("arsh", String.valueOf(days));

        }

        timesheetDatesAdapter.changeData(dates);
        tvTotalHours.setText("Total Hours " + String.format("%.2f", (double) Math.round(timesheetDatesAdapter.getTotalHours() * 100) / 100));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_timesheet_transactions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.refresh) {
            if (Utilities.isOnline(this)) {
                syncTimesheet();
            } else {
                Utilities.showNoConnectionToast(this);
            }
            return true;
        }

        if (item.getItemId() == android.R.id.home) {
            goToParentActivity();
            return true;
        }

        if (item.getItemId() == R.id.submit_all) {
            if (Utilities.isOnline(this)) {
                submitAllTimeSheets();
            } else {
                Utilities.showNoConnectionToast(this);
            }
            return true;
        }

        if (item.getItemId() == R.id.delete_all) {
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(MyTimeSheetTransactions.this);

            alertDialogBuilder.setTitle("This will delete all your on-hold or locally saved data.");

            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                            deleteAllTimeSheets();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.show();

            return true;
        }
        if (item.getItemId() == R.id.timer) {
            goToTimerActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToTimerActivity() {
        Intent intent = new Intent(this, TimerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        Intent intent;

        switch (view.getId()) {
            case R.id.button_add_timesheet:
                intent = new Intent(this, NewTimeSheet.class);
                startActivity(intent);
//                finish();
                break;
            case R.id.iv_previous_week:
                showPreviousPeriod();
                break;
            case R.id.iv_next_week:
                showNextPeriod();
                break;
        }
    }

    private void showNextPeriod() {
        dates = new ArrayList<>();
        if (getOptionsDataFile.getTimeSheetFrequency() == 1) {
            calendar = tempEndDate;
            calendar.add(Calendar.DATE, +1);
            startDate.setText(simpleDateFormat.format(calendar.getTimeInMillis()));
            calendar.add(Calendar.DATE, +6);
            endDate.setText(simpleDateFormat.format(calendar.getTimeInMillis()));
            tempEndDate = calendar;
            tempEndDate.add(Calendar.DATE, -6);
            startDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
            simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            simpleDateFormat = new SimpleDateFormat("dd-MMM");
            endDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
        }

        if (getOptionsDataFile.getTimeSheetFrequency() == 2) {
            if (tempEndDate.get(Calendar.DAY_OF_MONTH) <= 15) {
                tempEndDate.add(Calendar.DATE, 1);
                Log.i("arsh", tempEndDate.toString());
            } else {
                tempEndDate.add(Calendar.DATE, 1);
                Log.i("arsh", tempEndDate.toString());
            }
            int positionInMonth = tempEndDate.get(Calendar.DAY_OF_MONTH);
            int size = 0;
            if (positionInMonth <= 15) {
                size = 14;
            } else {
                size = tempEndDate.getActualMaximum(Calendar.DAY_OF_MONTH) - 16;
            }
            startDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
            simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            for (int i = 0; i < size; i++) {
                dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
                tempEndDate.add(Calendar.DATE, 1);
            }
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            simpleDateFormat = new SimpleDateFormat("dd-MMM");
            endDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
        }
        if (getOptionsDataFile.getTimeSheetFrequency() == 4) {
            tempEndDate.add(Calendar.DATE, 1);
            startDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
            simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            for (int i = 0; i < 13; i++) {
                dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
                tempEndDate.add(Calendar.DATE, 1);
            }
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            simpleDateFormat = new SimpleDateFormat("dd-MMM");
            endDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
        }

        timesheetDatesAdapter.changeData(dates);
        tvTotalHours.setText("Total Hours " + String.format("%.2f", (double) Math.round(timesheetDatesAdapter.getTotalHours() * 100) / 100));

    }

    private void showPreviousPeriod() {
        dates = new ArrayList<>();
        if (getOptionsDataFile.getTimeSheetFrequency() == 1) {
            calendar = tempEndDate;
            calendar.add(Calendar.DATE, -7);
            endDate.setText(simpleDateFormat.format(calendar.getTimeInMillis()));
            calendar.add(Calendar.DATE, -6);
            startDate.setText(simpleDateFormat.format(calendar.getTimeInMillis()));
            calendar.add(Calendar.DATE, 6);
            tempEndDate = calendar;
            tempEndDate.add(Calendar.DATE, -6);
            startDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
            simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            tempEndDate.add(Calendar.DATE, 1);
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            simpleDateFormat = new SimpleDateFormat("dd-MMM");
            endDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
        }
        if (getOptionsDataFile.getTimeSheetFrequency() == 2) {
            if (tempEndDate.get(Calendar.DAY_OF_MONTH) <= 15) {
                tempEndDate.add(Calendar.MONTH, -1);
                tempEndDate.add(Calendar.DATE, 1);
                Log.i("arsh", tempEndDate.toString());
            } else {
                tempEndDate.add(Calendar.DATE, 1);
                tempEndDate.add(Calendar.MONTH, -1);
                Log.i("arsh", tempEndDate.toString());
            }
            int positionInMonth = tempEndDate.get(Calendar.DAY_OF_MONTH);
            int size = 0;
            if (positionInMonth <= 15) {
                size = 14;
            } else {
                size = tempEndDate.getActualMaximum(Calendar.DAY_OF_MONTH) - 16;
            }
            startDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
            simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            for (int i = 0; i < size; i++) {
                dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
                tempEndDate.add(Calendar.DATE, 1);
            }
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            simpleDateFormat = new SimpleDateFormat("dd-MMM");
            endDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
        }

        if (getOptionsDataFile.getTimeSheetFrequency() == 4) {
            tempEndDate.add(Calendar.DATE, -27);
            startDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
            simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            for (int i = 0; i < 13; i++) {
                dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
                tempEndDate.add(Calendar.DATE, 1);
            }
            dates.add(simpleDateFormat.format(tempEndDate.getTimeInMillis()).toString());
            simpleDateFormat = new SimpleDateFormat("dd-MMM");
            endDate.setText(simpleDateFormat.format(tempEndDate.getTimeInMillis()));
        }

        timesheetDatesAdapter.changeData(dates);
        tvTotalHours.setText("Total Hours " + String.format("%.2f", (double) Math.round(timesheetDatesAdapter.getTotalHours() * 100) / 100));
    }

    private void submitAllTimeSheets() {

        String submitDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").format(Calendar.getInstance().getTimeInMillis());

        timesheetArrayList = tableObject.getTimesheetList();
        for (int i = 0; i < timesheetArrayList.size(); i++) {
            final int temp = i;
            for (int j = 0; j < dates.size(); j++) {
                String stringDate = dates.get(j);

                if (stringDate.equalsIgnoreCase(Utilities.dateToLocalFormat(timesheetArrayList.get(i).getActivityDate()))) {
                    if (tableObject.getTimesheetList().get(i).getEZStatusID() == 0
                            || tableObject.getTimesheetList().get(i).getEZStatusID() == 1) {
                        timesheet = tableObject.getTimesheetList().get(i);
                        String displayname = null;
                        String activityDate = null;
                        String projectname = null;
                        String taskname = null;
                        String classname = null;
                        String starttime = null;
                        String endtime = null;
                        String actualhours = null;
                        String descrip = null;
                        String adminnotes = null;
                        String taskid = null;
                        String projectid = null;
                        String classrowid = null;
                        String billable = null;
                        String ezstatusid = null;
                        String istime = null;
                        String empsubdate = submitDate;
                        String empsignature = null;
                        String status = null;
                        String timeactivityid = null;
                        if (tableObject.getDisplayName() != null) {
                            displayname = employeeLogin.getDisplayName();
                        }
                        if (timesheet.getActivityDate() != null) {
                            activityDate = timesheet.getActivityDate();
                        }
                        if (timesheet.getProjectFullName() != null) {
                            projectname = timesheet.getProjectFullName();
                        }
                        if (timesheet.getTaskFullName() != null) {
                            taskname = timesheet.getTaskFullName();
                        }
                        if (timesheet.getClassFullName() != null) {
                            classname = timesheet.getClassFullName();
                        }
                        if (timesheet.getStartTime() != null) {
                            starttime = timesheet.getStartTime();
                        }
                        if (timesheet.getEndTime() != null) {
                            endtime = timesheet.getEndTime();
                        }

                        actualhours = "" + timesheet.getActualHours();

                        if (timesheet.getDescription() != null) {
                            descrip = timesheet.getDescription();
                        }
                        if (timesheet.getAdminNotes() != null) {
                            adminnotes = timesheet.getAdminNotes();
                        }
                        taskid = "" + timesheet.getTaskID();
                        projectid = "" + timesheet.getProjectID();
                        classrowid = "" + timesheet.getClassRowID();
                        billable = "" + timesheet.isBillable();
                        ezstatusid = "" + timesheet.getEZStatusID();
                        istime = "" + timesheet.isTime();
                        if (timesheet.getEmpSubDate() != null) {
                            empsubdate = timesheet.getEmpSubDate();
                        }
                        if (timesheet.getEmpSignature() != null) {
                            empsignature = timesheet.getEmpSignature();
                        }
                        if (timesheet.getStatus() != null) {
                            status = timesheet.getStatus();
                        }
                        timeactivityid = "" + timesheet.getTimeActivityID();

                        String url = Constants.SUBMIT_NEW_TIMESHEET_API;
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("userid", employeeLogin.getUserid());
                            jsonObject.put("sitecode", employeeLogin.getSiteCode());
                            jsonObject.put("EmployeeFullName", employeeLogin.getDisplayName());
                            jsonObject.put("ActivityDate", timesheet.getActivityDate());
                            jsonObject.put("ProjectFullName", timesheet.getProjectFullName());
                            jsonObject.put("TaskFullName", taskname);
                            jsonObject.put("ClassFullName", classname);
                            jsonObject.put("StartTime", starttime);
                            jsonObject.put("EndTime", endtime);
                            jsonObject.put("ActualHours", actualhours);
                            jsonObject.put("Description", descrip);
                            jsonObject.put("AdminNotes", adminnotes);
                            jsonObject.put("TaskID", taskid);
                            jsonObject.put("ProjectID", projectid);
                            jsonObject.put("ClassRowID", classrowid);
                            jsonObject.put("Billable", billable);
                            jsonObject.put("EZStatusID", ezstatusid);
                            jsonObject.put("IsTime", istime);
                            jsonObject.put("EmpSignature", empsignature);
                            jsonObject.put("EmpSubDate", empsubdate);
                            jsonObject.put("Status", status);
                            jsonObject.put("SourceRefID", 18);
                            jsonObject.put("TimeActivityID", timeactivityid);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.i("arsh", "url = " + url);
                        Log.i("arsh", "TimeSheet Position = " + i);
                        Log.i("arsh", "Display Name = " + displayname);
                        Log.i("arsh", "UserID = " + tableObject.getUserid());
                        Log.i("arsh", "Activity Date = " + activityDate);
                        Log.i("arsh", "task Name = " + taskname);
                        Log.i("arsh", "class Name = " + classname);
                        Log.i("arsh", "Project Name" + projectname);
                        Log.i("arsh", "start time = " + starttime);
                        Log.i("arsh", "end time = " + endtime);
                        Log.i("arsh", "Actual hours = " + actualhours);
                        Log.i("arsh", "Description = " + descrip);
                        Log.i("arsh", "Admin Notes = " + adminnotes);
                        Log.i("arsh", "task id = " + taskid);
                        Log.i("arsh", "Project id = " + projectid);
                        Log.i("arsh", "class row id = " + classrowid);
                        Log.i("arsh", "billable = " + billable);
                        Log.i("arsh", "EZ Status id = " + ezstatusid);
                        Log.i("arsh", "Is time = " + istime);
                        Log.i("arsh", "Emp sub date = " + empsubdate);
                        Log.i("arsh", "Emp Signature = " + empsignature);
                        Log.i("arsh", "status = " + status);
                        Log.i("arsh", "time activity id = " + timeactivityid);
                        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject
                                , new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                hideProgressDialog();
                                try {
                                    JSONObject jsonObject = new JSONObject(s);
                                    if (jsonObject.has("TimeActivityID")) {
                                        Log.i("arsh", "response = " + s);
                                        TimeSheetResponse timeSheetResponse = new GsonBuilder().create().fromJson(s.toString(), TimeSheetResponse.class);
                                        tableObject.getTimesheetList().get(temp).setTimeActivityID(timeSheetResponse.getTimeActivityID());
                                        tableObject.getTimesheetList().get(temp).setEZStatusID(timeSheetResponse.getEZStatusID());
                                        tableObject.getTimesheetList().get(temp).set$id("");
                                        numOfItems++;
                                        Log.i("arsh", "" + timeSheetResponse.getTimeActivityID());
                                        Toast.makeText(getBaseContext(), "Submitted Successfully", Toast.LENGTH_SHORT).show();
                                        OfflineDataStorage.writeObject(getBaseContext(), tableObject, Constants.TIMESHEETS_FILE);
                                    } else {
                                        if (jsonObject.has("ErrorMessage")) {
                                            Toast.makeText(getBaseContext(), jsonObject.getString("ErrorMessage"), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getBaseContext(), "Request Failed.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getBaseContext(), "Exception occured.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                Utilities.handleVolleyError(MyTimeSheetTransactions.this, volleyError);
                            }
                        });
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        showProgressDialog();
                        AppController.getInstance().addToRequestQueue(stringRequest);
                    }
                }

            }
        }

    }

    private void deleteAllTimeSheets() {
        timesheetArrayList = tableObject.getTimesheetList();
        for (int i = 0; i < timesheetArrayList.size(); i++) {
            final int temp = i;
            timesheet = timesheetArrayList.get(temp);

            for (int j = 0; j < dates.size(); j++) {
                String stringDate = dates.get(j);

                if (stringDate.equalsIgnoreCase(Utilities.dateToLocalFormat(timesheetArrayList.get(i).getActivityDate()))) {

                    if (timesheet.getEZStatusID() == 0 && timesheet.get$id().equalsIgnoreCase("new")) {
                        tableObject.getTimesheetList().get(i).setIsMarkedForDeletion(true);
                        tableObject.getTimesheetList().get(i).setHasDeletedOnServer(true);
                        OfflineDataStorage.writeObject(getBaseContext(), tableObject, Constants.TIMESHEETS_FILE);
                    } else {
                        if (timesheet.getEZStatusID() == 0 || timesheet.getEZStatusID() == 1) {
                            String url = Constants.DELETE_TIMESHEET_API;
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("userid", employeeLogin.getUserid());
                                jsonObject.put("sitecode", employeeLogin.getSiteCode());
                                jsonObject.put("timeActivityID", timesheet.getTimeActivityID());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//                            + "?timeActivityID=" + timesheet.getTimeActivityID()
//                            + "&UserID=" + employeeLogin.getUserid()
//                            + "&SiteCode=" + employeeLogin.getSiteCode();
                            Log.i("arsh", "url=" + url);
                            JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject
                                    , new Response.Listener<String>() {
                                @Override
                                public void onResponse(String s) {
                                    hideProgressDialog();
                                    Log.i("arsh", "response=" + s);
                                    if (s.equalsIgnoreCase("true")) {
                                        tableObject.getTimesheetList().get(temp).setIsMarkedForDeletion(true);
                                        tableObject.getTimesheetList().get(temp).setHasDeletedOnServer(true);
                                        OfflineDataStorage.writeObject(getBaseContext(), tableObject, Constants.TIMESHEETS_FILE);
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
                                    hideProgressDialog();
                                    Utilities.handleVolleyError(MyTimeSheetTransactions.this, volleyError);
                                }
                            });
                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            showProgressDialog();
                            AppController.getInstance().addToRequestQueue(stringRequest);
                        }
                    }

                }
            }
        }
        onResume();
    }

    private void syncTimesheet() {

        new AlertDialog.Builder(this)
                .setTitle("Confirm Sync!")
                .setMessage("Do you want to Sync Timesheets")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        getTimesheetFromServer();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void getTimesheetFromServer() {

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);

        String url = Constants.TIMESHEETS_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
            Log.e("synck date",sharedPreferences.getString(Constants.TIMESHEET_LAST_SYNC_DATE, ""));
            jsonObject.put("startdate", sharedPreferences.getString(Constants.TIMESHEET_LAST_SYNC_DATE, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        if (getIntent().hasExtra(Constants.RELOAD_LIST)) {
//            try {
//                url = url + "&startDate=" + URLEncoder.encode(sharedPreferences.getString(Constants.TIMESHEET_LAST_SYNC_DATE, ""), "UTF-8");
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//        }

        Utilities.showErrorLog(url);

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                timesheetArrayListFromServer = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(s, Timesheet[].class)));
                timesheetArrayListOLD = tableObject.getTimesheetList();
                MyTaskSyncTimeSheet myTaskSyncTimeSheet = new MyTaskSyncTimeSheet();
                myTaskSyncTimeSheet.execute();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(MyTimeSheetTransactions.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShowDialog.showDialog(this);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void showProgressDialog() {
        numOfRequests++;
        ShowDialog.showDialog(this);
    }

    private void hideProgressDialog() {
        numOfRequests--;
        if (numOfRequests < 1) {
            ShowDialog.hideDialog();
//            Toast.makeText(this, "Request Processed for " + numOfItems + " Items", Toast.LENGTH_SHORT).show();
            numOfRequests = 0;
            numOfItems = 0;
            Intent intent = new Intent(this, MyTimeSheetTransactions.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToParentActivity();
    }

    private void goToParentActivity() {
        if (MainActivity.isOpen == false) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        finish();
    }

    private class MyTaskSyncTimeSheet extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            SimpleDateFormat simpleDateFormatServer = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            for (int i = 0; i < timesheetArrayListFromServer.size(); i++) {
                boolean found = false;
                for (int j = 0; j < timesheetArrayListOLD.size(); j++) {
                    if (timesheetArrayListFromServer.get(i).getTimeActivityID() == timesheetArrayListOLD.get(j).getTimeActivityID()) {
                        if (timesheetArrayListFromServer.get(i).getUpdatedDate() == null) {
                            ///add this line by rajeev below single line
                            timesheetArrayListOLD.set(j, timesheetArrayListFromServer.get(i));
                            ///add this line by rajeev
                            found = true;
                            continue;
                        }
                        if (timesheetArrayListOLD.get(j).getUpdatedDate() == null) {
                            timesheetArrayListOLD.set(j, timesheetArrayListFromServer.get(i));
                            found = true;
                            continue;
                        }
                        if (timesheetArrayListFromServer.get(i).getUpdatedDate() != null
                                && timesheetArrayListOLD.get(j).getUpdatedDate() != null) {

                            try {
                                Date dateServerTimesheet = simpleDateFormatServer.parse(timesheetArrayListFromServer.get(i).getUpdatedDate());
                                Date dateLocalTimesheet = simpleDateFormatServer.parse(timesheetArrayListOLD.get(j).getUpdatedDate());
                                if (dateLocalTimesheet.before(dateServerTimesheet)) {
                                    timesheetArrayListOLD.set(j, timesheetArrayListFromServer.get(i));
                                    found = true;
                                    continue;
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
                if (!found) {
                    timesheetArrayListOLD.add(timesheetArrayListFromServer.get(i));
                    Log.e("size of olde",timesheetArrayListOLD.size()+"");
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowDialog.hideDialog();
            tableObject.setTimesheetList(timesheetArrayListOLD);
            OfflineDataStorage.writeObject(MyTimeSheetTransactions.this, tableObject, Constants.TIMESHEETS_FILE);
            Toast.makeText(MyTimeSheetTransactions.this, "Timesheets synced", Toast.LENGTH_SHORT).show();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Constants.TIMESHEET_LAST_SYNC_DATE, sdf.format(new Date(Calendar.getInstance().getTimeInMillis())));
            editor.apply();
            Intent intent = new Intent(MyTimeSheetTransactions.this, MyTimeSheetTransactions.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            timesheetDatesAdapter.changeData(dates);
//            startActivity(intent);
//            finish();

        }
    }
}