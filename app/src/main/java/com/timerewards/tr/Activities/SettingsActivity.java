package com.timerewards.tr.Activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.ExpenseItemModel;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.ShowDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar mToolbar;
    DatePickerDialog pickerDialog;
    SimpleDateFormat simpleDateFormat;
    EditText selectDate;
    boolean hasSelectedDate = false;
    TableObject tableObject;
    ExpenseDataFile expenseDataFile;
    Button buttonClearData;
    ArrayList<Timesheet> timesheetArrayListOld = new ArrayList<>();
    ArrayList<Timesheet> timesheetArrayListNEW = new ArrayList<>();
    ArrayList<Expense> expenseArrayListOld = new ArrayList<>();
    ArrayList<Expense> expenseArrayListNEW = new ArrayList<>();
    int deletedTimesheetCount = 0;
    int deletedExpenseCount = 0;
    Date thresholdDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        init();
    }

    private void init() {
        tableObject = (TableObject) OfflineDataStorage.readObject(this, Constants.TIMESHEETS_FILE);
        expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(this, Constants.EXPENSE_DATA_FILE);
        timesheetArrayListOld = tableObject.getTimesheetList();
        expenseArrayListOld = expenseDataFile.getExpense();
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        mToolbar.setTitle("Settings");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        selectDate = (EditText) findViewById(R.id.et_select_date);
        selectDate.setOnClickListener(this);
        buttonClearData = (Button) findViewById(R.id.button_clean_up_data);
        buttonClearData.setOnClickListener(this);
    }

    private void selectDate() {
        Calendar newCalendar = Calendar.getInstance();
        pickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                selectDate.setText(new SimpleDateFormat("dd-MMM-yyyy").format(newDate.getTime()));
                selectDate.setError(null);
                try {
                    thresholdDate = new SimpleDateFormat("dd-MMM-yyyy").parse(selectDate.getText().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                hasSelectedDate = true;
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        pickerDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_select_date:
                selectDate();
                break;
            case R.id.button_clean_up_data:
                if (hasSelectedDate) {
                    clearData();
                } else {
                    selectDate.setError("Please Select Date");
                    Toast.makeText(this, "Please Select Date", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void clearData() {

        new AlertDialog.Builder(this)
                .setTitle("Confirm Delete!")
                .setMessage("Are you sure you want to Delete?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        selectDate.setText("");
                        selectDate.setError(null);
                        MyTask myTask = new MyTask();
                        myTask.execute();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowDialog.showDialog(SettingsActivity.this);
        }

        @Override
        protected Void doInBackground(String... params) {
            Log.i("arsh", "My Name");
            for (int i = 0; i < timesheetArrayListOld.size(); i++) {
                if ((timesheetArrayListOld.get(i).getEZStatusID() == 0
                        || timesheetArrayListOld.get(i).getEZStatusID() == 1)) {
                    timesheetArrayListNEW.add(timesheetArrayListOld.get(i));
                    continue;
                }
                try {
                    Date date = simpleDateFormat.parse(timesheetArrayListOld.get(i).getActivityDate());
                    if (thresholdDate.after(date)) {
                        deletedTimesheetCount++;
                        Log.i("arsh", "after" + i);
                    } else {
                        timesheetArrayListNEW.add(timesheetArrayListOld.get(i));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            for (int i = 0; i < expenseArrayListOld.size(); i++) {

                Expense expense = expenseArrayListOld.get(i);
                ArrayList<ExpenseItemModel> expenseItemModelArrayList = expense.getExpenseItemList();
                ArrayList<ExpenseItemModel> expenseItemModelArrayListNEW = new ArrayList<>();
                for (int j = 0; j < expenseItemModelArrayList.size(); j++) {
                    if (expenseItemModelArrayList.get(j).getEZStatusID() == 0
                            || expenseItemModelArrayList.get(j).getEZStatusID() == 1) {
                        expenseItemModelArrayListNEW.add(expenseItemModelArrayList.get(j));
                        continue;
                    }

                    try {
                        Date date = simpleDateFormat.parse(expenseItemModelArrayList.get(j).getExpenseDate());
                        if (thresholdDate.after(date)) {
                            deletedExpenseCount++;
                            Log.i("arsh", "after" + i);
                        } else {
                            expenseItemModelArrayListNEW.add(expenseItemModelArrayList.get(j));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }
                Expense expenseNEW = new Expense();
                expenseNEW.setExpHeader(expense.getExpHeader());
                expenseNEW.setExpenseItemList(expenseItemModelArrayListNEW);
                expenseArrayListNEW.add(expenseNEW);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowDialog.hideDialog();
            tableObject.setTimesheetList(timesheetArrayListNEW);
            expenseDataFile.setExpense(expenseArrayListNEW);
            OfflineDataStorage.writeObject(SettingsActivity.this, tableObject, Constants.TIMESHEETS_FILE);
            OfflineDataStorage.writeObject(SettingsActivity.this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
            selectDate.setText("");
            Toast.makeText(SettingsActivity.this,
                    String.valueOf(deletedTimesheetCount)
                            + " Timesheets Deleted\n"
                            + String.valueOf(deletedExpenseCount)
                            + " Expenses Deleted", Toast.LENGTH_SHORT).show();
            selectDate.setError(null);
            deletedTimesheetCount = 0;
            deletedExpenseCount = 0;
            timesheetArrayListOld = tableObject.getTimesheetList();
            expenseArrayListOld = expenseDataFile.getExpense();
            timesheetArrayListNEW = new ArrayList<>();
            expenseArrayListNEW = new ArrayList<>();
            hasSelectedDate = false;
        }
    }

}
