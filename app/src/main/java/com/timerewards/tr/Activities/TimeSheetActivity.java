package com.timerewards.tr.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Adapters.TimeSheetAdapter;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.DataModels.TimeSheetResponse;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class TimeSheetActivity extends AppCompatActivity implements View.OnClickListener {

    EmployeeLogin employeeLogin;
    ProgressDialog progressDialog;
    int numOfRequests = 0;
    private TextView tvDate, tvAddTimesheet;
    private ListView listView;
    private TimeSheetAdapter timeSheetAdapter;
    private Intent intent;
    private String stringDate;
    private Toolbar toolbar;
    private TableObject tableObject;
    private ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();
    private Timesheet timesheet;
    private String strOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timesheets);
        tableObject = (TableObject) OfflineDataStorage.readObject(getBaseContext(), Constants.TIMESHEETS_FILE);
        timesheetArrayList = tableObject.getTimesheetList();
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(getBaseContext(), Constants.EMPLOYEE_LOGIN_FILE);
        intent = getIntent();
        stringDate = intent.getStringExtra("date");
        SimpleDateFormat sdfmt1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfmt2 = new SimpleDateFormat("dd-MMM-yyyy");
        java.util.Date dDate = null;
        try {
            dDate = sdfmt1.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        strOutput = sdfmt2.format(dDate);
        init();
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("TimeSheets");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        tvDate.setText(strOutput);
        listView.setAdapter(timeSheetAdapter);
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvAddTimesheet = (TextView) findViewById(R.id.tv_add_timehseet);
        tvAddTimesheet.setOnClickListener(this);
        tvDate = (TextView) findViewById(R.id.tv_date);
        listView = (ListView) findViewById(R.id.listView_dates);
        timeSheetAdapter = new TimeSheetAdapter(this, stringDate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_submit_delete_all, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.submit_all:
                if (Utilities.isOnline(this)) {
                    submitAllTimeSheets();
                } else {
                    Utilities.showNoConnectionToast(this);
                }

                break;
            case R.id.delete_all:
                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(TimeSheetActivity.this);

                alertDialogBuilder.setTitle("This will delete all your on-hold or locally saved data.");

                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                                deleteAllTimeSheets();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        });
                android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void submitAllTimeSheets() {

        String submitDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").format(Calendar.getInstance().getTimeInMillis());

        for (int i = 0; i < timesheetArrayList.size(); i++) {
            final int temp = i;
            if (stringDate.equalsIgnoreCase(timesheetArrayList.get(i).getActivityDate().substring(0, timesheetArrayList.get(i).getActivityDate().indexOf("T")))) {
                if (tableObject.getTimesheetList().get(i).getEZStatusID() == 0
                        || tableObject.getTimesheetList().get(i).getEZStatusID() == 1) {
                    if (Utilities.isOnline(getBaseContext())) {
                        timesheet = tableObject.getTimesheetList().get(i);
                        String displayname = null;
                        String activityDate = null;
                        String projectname = null;
                        String taskname = null;
                        String classname = null;
                        String starttime = null;
                        String endtime = null;
                        String actualhours = null;
                        String descrip = null;
                        String adminnotes = null;
                        String taskid = null;
                        String projectid = null;
                        String classrowid = null;
                        String billable = null;
                        String ezstatusid = null;
                        String istime = null;
                        String empsubdate = submitDate;
                        String empsignature = null;
                        String status = null;
                        String timeactivityid = null;
                        if (tableObject.getDisplayName() != null) {
                            displayname = employeeLogin.getDisplayName();
                        }
                        if (timesheet.getActivityDate() != null) {
                            activityDate = timesheet.getActivityDate();
                        }
                        if (timesheet.getProjectFullName() != null) {
                            projectname = timesheet.getProjectFullName();
                        }
                        if (timesheet.getTaskFullName() != null) {
                            taskname = timesheet.getTaskFullName();
                        }
                        if (timesheet.getClassFullName() != null) {
                            classname = timesheet.getClassFullName();
                        }
                        if (timesheet.getStartTime() != null) {
                            starttime = timesheet.getStartTime();
                        }
                        if (timesheet.getEndTime() != null) {
                            endtime = timesheet.getEndTime();
                        }

                        actualhours = "" + timesheet.getActualHours();

                        if (timesheet.getDescription() != null) {
                            descrip = timesheet.getDescription();
                        }
                        if (timesheet.getAdminNotes() != null) {
                            adminnotes = timesheet.getAdminNotes();
                        }
                        taskid = "" + timesheet.getTaskID();
                        projectid = "" + timesheet.getProjectID();
                        classrowid = "" + timesheet.getClassRowID();
                        billable = "" + timesheet.isBillable();
                        ezstatusid = "" + timesheet.getEZStatusID();
                        istime = "" + timesheet.isTime();
                        if (timesheet.getEmpSubDate() != null) {
                            empsubdate = timesheet.getEmpSubDate();
                        }
                        if (timesheet.getEmpSignature() != null) {
                            empsignature = timesheet.getEmpSignature();
                        }
                        if (timesheet.getStatus() != null) {
                            status = timesheet.getStatus();
                        }
                        timeactivityid = "" + timesheet.getTimeActivityID();

                        String url = Constants.SUBMIT_NEW_TIMESHEET_API;
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("userid", employeeLogin.getUserid());
                            jsonObject.put("sitecode", employeeLogin.getSiteCode());
                            jsonObject.put("EmployeeFullName", employeeLogin.getDisplayName());
                            jsonObject.put("ActivityDate", timesheet.getActivityDate());
                            jsonObject.put("ProjectFullName", timesheet.getProjectFullName());
                            jsonObject.put("TaskFullName", taskname);
                            jsonObject.put("ClassFullName", classname);
                            jsonObject.put("StartTime", starttime);
                            jsonObject.put("EndTime", endtime);
                            jsonObject.put("ActualHours", actualhours);
                            jsonObject.put("Description", descrip);
                            jsonObject.put("AdminNotes", adminnotes);
                            jsonObject.put("TaskID", taskid);
                            jsonObject.put("ProjectID", projectid);
                            jsonObject.put("ClassRowID", classrowid);
                            jsonObject.put("Billable", billable);
                            jsonObject.put("EZStatusID", ezstatusid);
                            jsonObject.put("IsTime", istime);
                            jsonObject.put("EmpSignature", empsignature);
                            jsonObject.put("EmpSubDate", empsubdate);
                            jsonObject.put("Status", status);
                            jsonObject.put("SourceRefID", 18);
                            jsonObject.put("TimeActivityID", timeactivityid);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.i("arsh", "url = " + url);
                        Log.i("arsh", "TimeSheet Position = " + i);
                        Log.i("arsh", "Display Name = " + displayname);
                        Log.i("arsh", "UserID = " + tableObject.getUserid());
                        Log.i("arsh", "Activity Date = " + activityDate);
                        Log.i("arsh", "task Name = " + taskname);
                        Log.i("arsh", "class Name = " + classname);
                        Log.i("arsh", "Project Name" + projectname);
                        Log.i("arsh", "start time = " + starttime);
                        Log.i("arsh", "end time = " + endtime);
                        Log.i("arsh", "Actual hours = " + actualhours);
                        Log.i("arsh", "Description = " + descrip);
                        Log.i("arsh", "Admin Notes = " + adminnotes);
                        Log.i("arsh", "task id = " + taskid);
                        Log.i("arsh", "Project id = " + projectid);
                        Log.i("arsh", "class row id = " + classrowid);
                        Log.i("arsh", "billable = " + billable);
                        Log.i("arsh", "EZ Status id = " + ezstatusid);
                        Log.i("arsh", "Is time = " + istime);
                        Log.i("arsh", "Emp sub date = " + empsubdate);
                        Log.i("arsh", "Emp Signature = " + empsignature);
                        Log.i("arsh", "status = " + status);
                        Log.i("arsh", "time activity id = " + timeactivityid);
                        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject
                                , new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                hideProgressDialog();
                                try {
                                    JSONObject jsonObject = new JSONObject(s);
                                    if (jsonObject.has("TimeActivityID")) {
                                        Log.i("arsh", "response = " + s);
                                        TimeSheetResponse timeSheetResponse = new GsonBuilder().create().fromJson(s.toString(), TimeSheetResponse.class);
                                        tableObject.getTimesheetList().get(temp).setTimeActivityID(timeSheetResponse.getTimeActivityID());
                                        tableObject.getTimesheetList().get(temp).setEZStatusID(timeSheetResponse.getEZStatusID());
                                        tableObject.getTimesheetList().get(temp).set$id("");
                                        Log.i("arsh", "" + timeSheetResponse.getTimeActivityID());
                                        Toast.makeText(getBaseContext(), "Submitted Successfully", Toast.LENGTH_SHORT).show();
                                        OfflineDataStorage.writeObject(getBaseContext(), tableObject, Constants.TIMESHEETS_FILE);
                                    } else {
                                        if (jsonObject.has("ErrorMessage")) {
                                            Toast.makeText(getBaseContext(), jsonObject.getString("ErrorMessage"), Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getBaseContext(), "Request Failed.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                } catch (JSONException e) {
                                    Toast.makeText(getBaseContext(), "Exception occured.", Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                Utilities.handleVolleyError(TimeSheetActivity.this, volleyError);
                            }
                        });
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        showProgressDialog();
                        AppController.getInstance().addToRequestQueue(stringRequest);
                    } else {
                        Utilities.showNoConnectionToast(TimeSheetActivity.this);
                    }
                }
            }
        }

    }

    private void deleteAllTimeSheets() {
        for (int i = 0; i < timesheetArrayList.size(); i++) {
            final int temp = i;
            timesheet = timesheetArrayList.get(temp);
            if (stringDate.equalsIgnoreCase(timesheetArrayList.get(i).getActivityDate().substring(0, timesheetArrayList.get(i).getActivityDate().indexOf("T")))) {
                if (timesheet.getEZStatusID() == 0 && timesheet.get$id().equalsIgnoreCase("new")) {
                    tableObject.getTimesheetList().get(i).setIsMarkedForDeletion(true);
                    tableObject.getTimesheetList().get(i).setHasDeletedOnServer(true);
                    OfflineDataStorage.writeObject(getBaseContext(), tableObject, Constants.TIMESHEETS_FILE);
                } else {
                    if (timesheet.getEZStatusID() == 0 || timesheet.getEZStatusID() == 1) {
                        String url = Constants.DELETE_TIMESHEET_API;
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("userid", employeeLogin.getUserid());
                            jsonObject.put("sitecode", employeeLogin.getSiteCode());
                            jsonObject.put("timeActivityID", timesheet.getTimeActivityID());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                            + "?timeActivityID=" + timesheet.getTimeActivityID()
//                            + "&UserID=" + employeeLogin.getUserid()
//                            + "&SiteCode=" + employeeLogin.getSiteCode();
                        Log.i("arsh", "url=" + url);
                        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject
                                , new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                hideProgressDialog();
                                Log.i("arsh", "response=" + s);
                                if (s.equalsIgnoreCase("true")) {
                                    tableObject.getTimesheetList().get(temp).setIsMarkedForDeletion(true);
                                    tableObject.getTimesheetList().get(temp).setHasDeletedOnServer(true);
                                    OfflineDataStorage.writeObject(getBaseContext(), tableObject, Constants.TIMESHEETS_FILE);
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                hideProgressDialog();
                                Utilities.handleVolleyError(TimeSheetActivity.this, volleyError);
                            }
                        });
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        showProgressDialog();
                        AppController.getInstance().addToRequestQueue(stringRequest);
                    }
                }
            }
        }
        timeSheetAdapter.changeData(stringDate);
    }

    private void showProgressDialog() {
        numOfRequests++;
        ShowDialog.showDialog(this);
    }

    private void hideProgressDialog() {
        numOfRequests--;
        if (numOfRequests < 1) {
            ShowDialog.hideDialog();
            numOfRequests = 0;
//            Intent intent = new Intent(this, MyTimeSheetTransactions.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);
//            finish();
            restartActivity();
        }
    }

    private void restartActivity() {
        Intent intent = new Intent(getBaseContext(), MyTimeSheetTransactions.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_add_timehseet:
                Intent intent = new Intent(TimeSheetActivity.this, NewTimeSheet.class);
                intent.putExtra(Constants.CURRENT_DATE, strOutput);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;
        }
    }

}