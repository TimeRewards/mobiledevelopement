package com.timerewards.tr.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.Controller.TimerService;
import com.timerewards.tr.DataFiles.AccountModel;
import com.timerewards.tr.DataFiles.AccountsDataFile;
import com.timerewards.tr.DataFiles.AssignmentModel;
import com.timerewards.tr.DataFiles.ClassesDataFile;
import com.timerewards.tr.DataFiles.ClassesModel;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.ExpenseItemModel;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.LeaveTaskModel;
import com.timerewards.tr.DataFiles.ProjectsDataFile;
import com.timerewards.tr.DataFiles.ProjectsModel;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.TaskDataFile;
import com.timerewards.tr.DataFiles.TasksModel;
import com.timerewards.tr.DataFiles.TimerDataFile;
import com.timerewards.tr.DataFiles.TimerModel;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    public static ArrayList<TimerModel> timerModelArrayList = new ArrayList<>();
    public static TimerDataFile timerDataFile = new TimerDataFile();
    public static boolean isOpen;
    public long time = 0;
    TableObject tableObject;
    EmployeeLogin employeeLogin;
    ClassesDataFile classesDataFile;
    ProjectsDataFile projectsDataFile;
    TaskDataFile taskDataFile;
    ExpenseDataFile expenseDataFile;
    AccountsDataFile accountsDataFile;
    ArrayList<Timesheet> timeSheetList = new ArrayList<>();
    ArrayList<ClassesModel> classesModelList = new ArrayList<>();
    ArrayList<ProjectsModel> projectsModelList = new ArrayList<>();
    ArrayList<TasksModel> tasksModelList = new ArrayList<>();
    ArrayList<AssignmentModel> assignmentModelArrayList = new ArrayList<>();
    ArrayList<LeaveTaskModel> leaveTaskModelList = new ArrayList<>();
    ArrayList<AccountModel> accountModelArrayList = new ArrayList<>();
    private Button button;
    private Toolbar mToolbar;
    private NavigationView mDrawer;
//    private int count = 0;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private int mSelectedId;
    private TextView tvUserName;
    private boolean backPressed = false;
    private GetOptionsDataFile getOptionsDataFile;
    private boolean classesSynced = false;
    private boolean projectsSynced = false;
    private boolean tasksSynced = false;
    private boolean timesheetSynced = false;
    private boolean expenseSheetsSynced = false;
    private boolean getOptionsSynced = false;
    private boolean acccountsSynced = false;
    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;
    private CardView cvTimesheet, cvExpenses, cvApprovals;
    private ArrayList<Timesheet> timesheetArrayListFromServer = new ArrayList<>();
    private ArrayList<Timesheet> timesheetArrayListOLD = new ArrayList<>();
    private ArrayList<Expense> expenseArrayListOLD = new ArrayList<>();
    private ArrayList<Expense> expenseArrayListFromServer = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isOpen = true;
        setContentView(R.layout.activity_main);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                Log.e("arsh", "onReceive MainActivity");
                time++;
                if (time == 9) {
                    time = 0;
                    saveOffline();
                }
            }
        };
        intentFilter = new IntentFilter("TimerService");
        registerReceiver(broadcastReceiver, intentFilter);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(MainActivity.this, Constants.GETOPTIONS_FILE);
        new MyOfflineReadTask().execute();
    }

    private void updateToken() {
        String url = "";
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
        if (sharedPreferences.getString(Constants.FCM_TOKEN, "").length() == 0) {
            return;
        }
        url = Constants.UPDATE_TOKEN_API;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("deviceid", "1");
            jsonObject.put("token", sharedPreferences.getString(Constants.FCM_TOKEN, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utilities.showErrorLog(jsonObject.toString());
        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utilities.showErrorLog(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void syncNewAssignments() {
        ShowDialog.showDialog(MainActivity.this, "Syncing Data");
        syncProjects();
        syncTasks();
    }

    private void saveOffline() {
        ArrayList<TimerModel> timerModelArrayList1 = new ArrayList<>();
        timerModelArrayList = TimerService.timerModelArrayList;
        for (TimerModel timerModel : timerModelArrayList) {
            timerModelArrayList1.add(timerModel);
        }
        TimerDataFile timerDataFile = new TimerDataFile();
        timerDataFile.setTimerModelArrayList(timerModelArrayList1);
        OfflineDataStorage.writeObject(this, timerDataFile, Constants.TIMER_DATA_FILE);
        TimerDataFile timerDataFile1 = (TimerDataFile) OfflineDataStorage.readObject(this, Constants.TIMER_DATA_FILE);
        Log.e("Size is : ", String.valueOf(timerDataFile1.getTimerModelArrayList().size()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        backPressed = false;
    }

    private void init() {
        cvTimesheet = (CardView) findViewById(R.id.card_view_timesheets);
        cvTimesheet.setOnClickListener(this);
        cvExpenses = (CardView) findViewById(R.id.card_view_expenses);
        cvExpenses.setOnClickListener(this);
        cvApprovals = (CardView) findViewById(R.id.card_view_approvals);
        cvApprovals.setOnClickListener(this);

//        switch (1){
        switch (getOptionsDataFile.getProduct()) {
            case 1:
                cvExpenses.setEnabled(false);
                cvExpenses.setVisibility(View.GONE);
                break;
            case 2:
                cvExpenses.setEnabled(false);
                cvExpenses.setVisibility(View.GONE);
                break;
            case 4:
                cvTimesheet.setEnabled(false);
                cvTimesheet.setVisibility(View.GONE);
                break;
            case 3:

                break;
        }

        switch (getOptionsDataFile.getIsApprover()) {
            case 0:
                cvApprovals.setVisibility(View.GONE);
                break;
            case 1:
                cvApprovals.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void navigate(int mSelectedId) {
        Intent intent = null;
        if (mSelectedId == R.id.navigation_item_timesheets) {
            mDrawerLayout.closeDrawer(GravityCompat.START);

            if (getOptionsDataFile.getProduct() == 1
                    || getOptionsDataFile.getProduct() == 2
                    || getOptionsDataFile.getProduct() == 3) {

                intent = new Intent(this, MyTimeSheetTransactions.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "You can't use TimeSheets.", Toast.LENGTH_SHORT).show();
            }

        }
        if (mSelectedId == R.id.navigation_item_expenses) {
            mDrawerLayout.closeDrawer(GravityCompat.START);

            if (getOptionsDataFile.getProduct() == 1
                    || getOptionsDataFile.getProduct() == 3
                    || getOptionsDataFile.getProduct() == 4) {

                intent = new Intent(this, ExpenseSheetActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "You can't use Expenses.", Toast.LENGTH_SHORT).show();
            }

        }
        if (mSelectedId == R.id.navigation_item_approvals) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            if (getOptionsDataFile.getIsApprover() == 1) {
                intent = new Intent(this, ApproveActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, "You can't use Approvals.", Toast.LENGTH_SHORT).show();
            }
        }
        if (mSelectedId == R.id.navigation_item_settings) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
//            Toast.makeText(this, "Setting", Toast.LENGTH_SHORT).show();
        }
        if (mSelectedId == R.id.navigation_item_logout) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            String gcmToken = sharedPreferences.getString(Constants.FCM_TOKEN, "");
            editor.clear();
            if (gcmToken.length() > 0) {
                editor.putString(Constants.FCM_TOKEN, gcmToken);
            }
            editor.commit();
            Intent intent1 = new Intent(this, LoginActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
            finish();
        }
//        if (mSelectedId == R.id.ic_share_token) {
//            mDrawerLayout.closeDrawer(GravityCompat.START);
//            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
//            String token = sharedPreferences.getString(Constants.FCM_TOKEN, "");
//            if (token.trim().length() > 0) {
//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, sharedPreferences.getString(Constants.FCM_TOKEN, ""));
//                sendIntent.setType("text/plain");
//                startActivity(Intent.createChooser(sendIntent, "Select an app"));
//            } else {
//                Toast.makeText(this, "token is yet to be generated", Toast.LENGTH_SHORT).show();
//            }
//        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        mSelectedId = menuItem.getItemId();
        navigate(mSelectedId);
        return true;
    }

    private void showDrawer() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    private void hideDrawer() {
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_sync) {
            syncAll();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (backPressed == true) {
                finish();
            } else {
                Toast.makeText(MainActivity.this, "Press back again to exit", Toast.LENGTH_SHORT).show();
                backPressed = true;
            }

        }

    }

    @Override
    public void onClick(View view) {
        Intent intent;

        int id = view.getId();
        switch (id) {
            case R.id.card_view_timesheets:
                intent = new Intent(this, MyTimeSheetTransactions.class);
                startActivity(intent);
                break;
            case R.id.card_view_expenses:
                intent = new Intent(this, ExpenseSheetActivity.class);
                startActivity(intent);
                break;
            case R.id.card_view_approvals:
                intent = new Intent(this, ApproveActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    private void syncAll() {
        if (Utilities.isOnline(this)) {
            ShowDialog.showDialog(MainActivity.this, "Syncing Data");
            syncProjects();
            syncClasses();
            syncTasks();
            syncGetOptions();
            syncAccounts();
            getTimesheetFromServer();
            getExpensesFromServer();
        } else {
            Utilities.showNoConnectionToast(this);
        }
    }

    private void syncClasses() {

        String url = Constants.CLASSES_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            if (s.contains("Classes")) {
                                JSONArray array = (JSONArray) new JSONObject(s.toString()).get("Classes");
                                classesModelList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array.toString(), ClassesModel[].class)));
                                classesModelList = generateNonClickableClasses(classesModelList);
                            } else {
                                classesModelList = new ArrayList<>();
                            }
                            ClassesModel classesModel = new ClassesModel();
                            classesModel.setClassName("None");
                            classesModel.setClassRowID(0);
                            classesModel.setIsClickable(true);
                            classesModel.setFullName("None");
                            classesModel.setSubLevel(0);
                            classesModel.set$id("0");
                            classesModelList.add(0, classesModel);
                            classesDataFile.setClassesModelList(classesModelList);
                            OfflineDataStorage.writeObject(getBaseContext(), classesDataFile, Constants.CLASSES_DATA_FILE);
                            classesDataFile = (ClassesDataFile) OfflineDataStorage.readObject(getBaseContext(), Constants.CLASSES_DATA_FILE);
                            Toast.makeText(getBaseContext(), "Synced " + getOptionsDataFile.getClassLex() + " (" + classesModelList.size() + ")", Toast.LENGTH_SHORT).show();
                            classesSynced = true;
                            checkAllSync();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
//                volleyError.printStackTrace();
                Utilities.handleVolleyError(MainActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private ArrayList<ClassesModel> generateNonClickableClasses(ArrayList<ClassesModel> classesModelList1) {

        ArrayList<ClassesModel> classesModelListNEW = new ArrayList<>();

        int parentSubLevel = 0;
        String parentName = "";
        int currentSubLevel = 0;
        String currentName = "";
        int subLevel = 0;

        for (int i = 0; i < classesModelList1.size(); i++) {
            parentSubLevel = currentSubLevel;
            parentName = currentName;
            currentSubLevel = classesModelList1.get(i).getSubLevel();
            currentName = classesModelList1.get(i).getFullName();
            if (currentSubLevel == 0) {
                classesModelListNEW.add(classesModelList1.get(i));
                continue;
            }

            if ((currentSubLevel - parentSubLevel == 0)
                    && currentName.substring(currentName.lastIndexOf(":"), currentName.length())
                    .equalsIgnoreCase(parentName.substring(parentName.lastIndexOf(":"), parentName.length()))) {

                classesModelListNEW.add(classesModelList1.get(i));
                continue;

            }

            if ((currentSubLevel - parentSubLevel == 1)
                    && currentName.substring(0, currentName.lastIndexOf(":")).equalsIgnoreCase(parentName)) {

                classesModelListNEW.add(classesModelList1.get(i));
                continue;

            }
            String[] names = currentName.split(":");
            for (int j = 0; j < names.length - 1; j++) {
                ClassesModel classesModel = new ClassesModel();
                classesModel.setClassName(names[j]);
                classesModel.setIsClickable(false);
                classesModel.setFullName("ERROR!!!");
                classesModel.setSubLevel(j);
                classesModelListNEW.add(classesModel);
            }
            classesModelListNEW.add(classesModelList1.get(i));

        }

        return classesModelListNEW;

    }

    private void syncProjects() {

        String url = Constants.PROJECTS_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONArray array = (JSONArray) new JSONObject(s.toString()).get("Projects");
                            projectsModelList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array.toString(), ProjectsModel[].class)));
                            ProjectsModel projectsModel = new ProjectsModel();
                            projectsModel.set$id("0");
                            projectsModel.setSubLevel(0);
                            projectsModel.setFullName("None");
                            projectsModel.setIsClickable(true);
                            projectsModel.setProjectName("None");
                            projectsModel.setParentID(0);
                            projectsModel.setEnabled(true);
                            projectsModel.setExpPrj(true);
                            projectsModelList.add(0, projectsModel);
                            projectsDataFile.setProjectsModelList(projectsModelList);
                            OfflineDataStorage.writeObject(getBaseContext(), projectsDataFile, Constants.PROJECTS_DATA_FILE);
                            Toast.makeText(getBaseContext(), "Synced " + getOptionsDataFile.getCustomerLex() + " : " + getOptionsDataFile.getProjectLex() + " (" + projectsModelList.size() + ")", Toast.LENGTH_SHORT).show();
                            projectsSynced = true;
                            if (getIntent().hasExtra(Constants.NEW_ASSIGNMENTS)) {
                                checkNewAssignmentsSynced();
                            } else {
                                checkAllSync();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
//                volleyError.printStackTrace();
                Utilities.handleVolleyError(MainActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void syncTasks() {

        String url = Constants.TASKS_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONArray array = (JSONArray) new JSONObject(s.toString()).get("Tasks");
                            tasksModelList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array.toString(), TasksModel[].class)));
                            tasksModelList = generateNonClickableTasks(tasksModelList);
                            TasksModel tasksModel = new TasksModel();
                            tasksModel.set$id("0");
                            tasksModel.setFullName("None");
                            tasksModel.setTaskName("None");
                            tasksModel.setIsClickable(true);
                            tasksModel.setTaskID(0);
                            tasksModelList.add(0, tasksModel);
                            taskDataFile.setTasksModelList(tasksModelList);

                            JSONArray array2 = (JSONArray) new JSONObject(s.toString()).get("Assignments");
                            assignmentModelArrayList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array2.toString(), AssignmentModel[].class)));
                            taskDataFile.setAssignmentModels(assignmentModelArrayList);

                            JSONArray array3 = (JSONArray) new JSONObject(s.toString()).get("LeaveTask");
                            leaveTaskModelList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array3.toString(), LeaveTaskModel[].class)));
                            LeaveTaskModel leaveTaskModel = new LeaveTaskModel();
                            leaveTaskModel.set$id("0");
                            leaveTaskModel.setFullName("None");
                            leaveTaskModel.setTaskID(0);
                            leaveTaskModel.setIsClickable(true);
                            leaveTaskModel.setSubLevel(0);
                            leaveTaskModel.setTaskName("None");
                            leaveTaskModelList.add(0, leaveTaskModel);
                            taskDataFile.setLeaveTaskModels(leaveTaskModelList);
                            OfflineDataStorage.writeObject(getBaseContext(), taskDataFile, Constants.TASKS_DATA_FILE);
                            Toast.makeText(getBaseContext(), "Synced " + getOptionsDataFile.getTaskLex() + " (" + tasksModelList.size() + ")", Toast.LENGTH_SHORT).show();
                            tasksSynced = true;
                            if (getIntent().hasExtra(Constants.NEW_ASSIGNMENTS)) {
                                checkNewAssignmentsSynced();
                            } else {
                                checkAllSync();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
//                volleyError.printStackTrace();
                Utilities.handleVolleyError(MainActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private ArrayList<TasksModel> generateNonClickableTasks(ArrayList<TasksModel> tasksModelList1) {

        ArrayList<TasksModel> tasksModelListNEW = new ArrayList<>();

        int parentSubLevel = 0;
        String parentName = "";
        int currentSubLevel = 0;
        String currentName = "";

        for (int i = 0; i < tasksModelList1.size(); i++) {
            parentSubLevel = currentSubLevel;
            parentName = currentName;
            currentSubLevel = tasksModelList1.get(i).getSubLevel();
            currentName = tasksModelList1.get(i).getFullName();
            if (currentSubLevel == 0) {
                tasksModelListNEW.add(tasksModelList1.get(i));
                continue;
            }

            if ((currentSubLevel - parentSubLevel == 0)
                    && currentName.substring(currentName.lastIndexOf(":"), currentName.length())
                    .equalsIgnoreCase(parentName.substring(parentName.lastIndexOf(":"), parentName.length()))) {

                tasksModelListNEW.add(tasksModelList1.get(i));
                continue;

            }

            if ((currentSubLevel - parentSubLevel == 1)
                    && currentName.substring(0, currentName.lastIndexOf(":")).equalsIgnoreCase(parentName)) {

                tasksModelListNEW.add(tasksModelList1.get(i));
                continue;

            }

            String[] names = currentName.split(":");
            for (int j = 0; j < names.length - 1; j++) {
                TasksModel tasksModel = new TasksModel();
                tasksModel.setTaskName(names[j]);
                tasksModel.setIsClickable(false);
                tasksModel.setFullName("ERROR!!!");
                tasksModel.setSubLevel(j);
                tasksModelListNEW.add(tasksModel);
            }
            tasksModelListNEW.add(tasksModelList1.get(i));

        }

        return tasksModelListNEW;

    }

    private void syncGetOptions() {

        String url = Constants.GET_OPTIONS_API;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
//                        Log.i("arsh", s.toString());
                        Gson gson = new Gson();
                        getOptionsDataFile = gson.fromJson(s.toString(), GetOptionsDataFile.class);
                        OfflineDataStorage.writeObject(getBaseContext(), getOptionsDataFile, Constants.GETOPTIONS_FILE);
                        getOptionsSynced = true;
                        checkAllSync();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                volleyError.printStackTrace();
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(MainActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void syncAccounts() {

        String url = Constants.ACCOUNTS_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONArray array = (JSONArray) new JSONObject(s.toString()).get("AccountsList");
                            accountModelArrayList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array.toString(), AccountModel[].class)));
                            accountModelArrayList = generateNonClickableAccounts(accountModelArrayList);
                            AccountModel accountModel = new AccountModel();
                            accountModel.set$id("0");
                            accountModel.setAccountid(0);
                            accountModel.setAccountname("None");
                            accountModel.setAssettype("Expense");
                            accountModel.setIsClickable(true);
                            accountModel.setIsDefaultGLAnItem(0);
                            accountModelArrayList.add(0, accountModel);
                            accountsDataFile.setAccountsList(accountModelArrayList);
                            OfflineDataStorage.writeObject(getBaseContext(), accountsDataFile, Constants.ACCOUNTS_DATA_FILE);
                            Toast.makeText(getBaseContext(), "Synced Accounts" + " (" + accountModelArrayList.size() + ")", Toast.LENGTH_SHORT).show();
                            acccountsSynced = true;
                            checkAllSync();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
//                volleyError.printStackTrace();
                Utilities.handleVolleyError(MainActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private ArrayList<AccountModel> generateNonClickableAccounts(ArrayList<AccountModel> accountModelArrayList1) {

        ArrayList<AccountModel> accountsModelListNEW = new ArrayList<>();

        int parentSubLevel = 0;
        String parentName = "";
        int currentSubLevel = 0;
        String currentName = "";
        int subLevel = 0;
        String assestType = "";

        for (int i = 0; i < accountModelArrayList1.size(); i++) {
            assestType = accountModelArrayList1.get(i).getAssettype();
            parentSubLevel = currentSubLevel;
            parentName = currentName;
            currentSubLevel = accountModelArrayList.get(i).getSubLevelLocal();
            currentName = accountModelArrayList1.get(i).getFullName();
            int counter = 0;
            for (int jj = 0; jj < currentName.length(); jj++) {
                if (currentName.charAt(jj) == ':') {
                    counter++;
                }
            }
            currentSubLevel = counter;
            if (currentSubLevel == 0) {
                accountsModelListNEW.add(accountModelArrayList1.get(i));
                continue;
            }

            if ((currentSubLevel - parentSubLevel == 0)
                    && currentName.substring(currentName.lastIndexOf(":"), currentName.length())
                    .equalsIgnoreCase(parentName.substring(parentName.lastIndexOf(":"), parentName.length()))) {

                accountsModelListNEW.add(accountModelArrayList1.get(i));
                continue;

            }

//            if ((currentSubLevel - parentSubLevel == 1)
//                    && currentName.substring(0, currentName.lastIndexOf(":")).equalsIgnoreCase(parentName)) {
            //// TODO: 5/10/16 this need to be fixed...
            if ((currentSubLevel - parentSubLevel == 1)) {
                accountModelArrayList1.get(i).setSubLevelLocal(counter);
                accountsModelListNEW.add(accountModelArrayList1.get(i));
                continue;

            }
            String[] names = currentName.split(":");
            for (int j = 0; j < names.length - 1; j++) {
                AccountModel accountModel = new AccountModel();
                accountModel.setAccountname(names[j]);
                accountModel.setIsClickable(false);
                accountModel.setFullName("ERROR!!!");
                accountModel.setSubLevelLocal(j);
                accountModel.setAssettype(assestType);
                accountsModelListNEW.add(accountModel);
            }
            accountsModelListNEW.add(accountModelArrayList1.get(i));
        }
        return accountsModelListNEW;

    }

    private void getTimesheetFromServer() {

        String url = Constants.TIMESHEETS_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                timesheetArrayListFromServer = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(s, Timesheet[].class)));
                timesheetArrayListOLD = tableObject.getTimesheetList();
                MyTaskSyncTimeSheet myTaskSyncTimeSheet = new MyTaskSyncTimeSheet();
                myTaskSyncTimeSheet.execute();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(MainActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void getExpensesFromServer() {

        String url = "";
        url = Constants.EXPENSES_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        ExpenseDataFile expenseDataFile = new GsonBuilder().create().fromJson(s, ExpenseDataFile.class);
                        expenseArrayListFromServer = expenseDataFile.getExpense();
                        MyTaskSyncExpenses myTaskSyncExpenses = new MyTaskSyncExpenses();
                        myTaskSyncExpenses.execute();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(MainActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void checkAllSync() {
        if (classesSynced
                && projectsSynced
                && tasksSynced
                && getOptionsSynced
                && timesheetSynced
                && expenseSheetsSynced
                && acccountsSynced) {
            ShowDialog.hideDialog();
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            editor.putString(Constants.TIMESHEET_LAST_SYNC_DATE, sdf.format(new Date(Calendar.getInstance().getTimeInMillis())));
            editor.putString(Constants.EXPENSE_LAST_SYNC_DATE, sdf.format(new Date(Calendar.getInstance().getTimeInMillis())));
            editor.apply();
            Toast.makeText(this, "Sync Completed", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    private void checkNewAssignmentsSynced() {
        if (projectsSynced
                && tasksSynced) {
            ShowDialog.hideDialog();
            Toast.makeText(this, "Your customer/projects and service items are now updated and you can enter your timesheets and/or expenses.", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("arsh", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("arsh", "onDestroy");
        saveOffline();
        unregisterReceiver(broadcastReceiver);
        isOpen = false;
    }

    private class MyTaskSyncTimeSheet extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            SimpleDateFormat simpleDateFormatServer = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            for (int i = 0; i < timesheetArrayListFromServer.size(); i++) {
                boolean found = false;
                for (int j = 0; j < timesheetArrayListOLD.size(); j++) {
                    if (timesheetArrayListFromServer.get(i).getTimeActivityID() == timesheetArrayListOLD.get(j).getTimeActivityID()) {
                        if (timesheetArrayListFromServer.get(i).getUpdatedDate() == null) {
                            ///add this line by rajeev below single line
                            timesheetArrayListOLD.set(j, timesheetArrayListFromServer.get(i));
                            ///add this line by rajeev
                            found = true;
                            continue;
                        }
                        if (timesheetArrayListOLD.get(j).getUpdatedDate() == null) {
                            timesheetArrayListOLD.set(j, timesheetArrayListFromServer.get(i));
                            found = true;
                            continue;
                        }
                        if (timesheetArrayListFromServer.get(i).getUpdatedDate() != null
                                && timesheetArrayListOLD.get(j).getUpdatedDate() != null) {

                            try {
                                Date dateServerTimesheet = simpleDateFormatServer.parse(timesheetArrayListFromServer.get(i).getUpdatedDate());
                                Date dateLocalTimesheet = simpleDateFormatServer.parse(timesheetArrayListOLD.get(j).getUpdatedDate());
                                if (dateLocalTimesheet.before(dateServerTimesheet)) {
                                    timesheetArrayListOLD.set(j, timesheetArrayListFromServer.get(i));
                                    found = true;
                                    continue;
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
                if (!found) {
                    timesheetArrayListOLD.add(timesheetArrayListFromServer.get(i));
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            tableObject.setTimesheetList(timesheetArrayListOLD);
            OfflineDataStorage.writeObject(MainActivity.this, tableObject, Constants.TIMESHEETS_FILE);
            timesheetSynced = true;
            Toast.makeText(getBaseContext(), "Synced Timesheets" + " (" + tableObject.getTimesheetList().size() + ")", Toast.LENGTH_SHORT).show();
            checkAllSync();
        }
    }

    private class MyTaskSyncExpenses extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            SimpleDateFormat simpleDateFormatServer = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            expenseArrayListOLD = expenseDataFile.getExpense();

            for (int i = 0; i < expenseArrayListFromServer.size(); i++) {
                boolean headerFound = false;
                for (int j = 0; j < expenseArrayListOLD.size(); j++) {
                    if (expenseArrayListFromServer.get(i).getExpHeader().getEmployeeExpenseID() ==
                            expenseArrayListOLD.get(j).getExpHeader().getEmployeeExpenseID()) {

                        headerFound = true;
                        if (expenseArrayListFromServer.get(i).getExpHeader().getUpdatedDate() == null) {

                        }
                        if (expenseArrayListOLD.get(j).getExpHeader().getUpdatedDate() == null) {
                            expenseArrayListOLD.get(j).setExpHeader(expenseArrayListFromServer.get(i).getExpHeader());
                        }
                        if (expenseArrayListOLD.get(j).getExpHeader().getUpdatedDate() != null
                                && expenseArrayListFromServer.get(i).getExpHeader().getUpdatedDate() != null) {
                            try {
                                Date dateServerExpense = simpleDateFormatServer.parse(expenseArrayListFromServer.get(i).getExpHeader().getUpdatedDate());
                                Date dateLocalExpense = simpleDateFormatServer.parse(expenseArrayListOLD.get(j).getExpHeader().getUpdatedDate());
                                if (dateLocalExpense.before(dateServerExpense)) {
                                    expenseArrayListOLD.get(j).setExpHeader(expenseArrayListFromServer.get(i).getExpHeader());
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        ArrayList<ExpenseItemModel> expenseItemModelArrayListServer = expenseArrayListFromServer.get(i).getExpenseItemList();
                        ArrayList<ExpenseItemModel> expenseItemModelArrayListOLD = expenseArrayListOLD.get(j).getExpenseItemList();

                        for (int k = 0; k < expenseItemModelArrayListServer.size(); k++) {
                            boolean itemFound = false;
                            for (int l = 0; l < expenseItemModelArrayListOLD.size(); l++) {
                                if (expenseItemModelArrayListServer.get(k).getEmployeeExpenseItemID() ==
                                        expenseItemModelArrayListOLD.get(l).getEmployeeExpenseItemID()) {
                                    itemFound = true;
                                    if (expenseItemModelArrayListServer.get(k).getUpdatedDate() == null) {
                                        continue;
                                    }
                                    if (expenseItemModelArrayListOLD.get(l).getUpdatedDate() == null) {
                                        expenseItemModelArrayListOLD.set(l, expenseItemModelArrayListServer.get(k));
                                        continue;
                                    }
                                    if (expenseItemModelArrayListOLD.get(l).getUpdatedDate() != null
                                            && expenseItemModelArrayListServer.get(k).getUpdatedDate() != null) {
                                        try {
                                            Date dateServerExpense = simpleDateFormatServer.parse(expenseItemModelArrayListServer.get(k).getUpdatedDate());
                                            Date dateLocalExpense = simpleDateFormatServer.parse(expenseItemModelArrayListOLD.get(l).getUpdatedDate());
                                            if (dateLocalExpense.before(dateServerExpense)) {
                                                expenseItemModelArrayListOLD.set(l, expenseItemModelArrayListServer.get(k));
                                            }
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                            if (!itemFound) {
                                expenseItemModelArrayListOLD.add(expenseItemModelArrayListServer.get(k));
                            }

                        }

                        expenseArrayListOLD.get(j).setExpenseItemList(expenseItemModelArrayListOLD);

                    }
                }

                if (!headerFound) {
                    expenseArrayListOLD.add(expenseArrayListFromServer.get(i));
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            expenseDataFile.setExpense(expenseArrayListOLD);
            OfflineDataStorage.writeObject(MainActivity.this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
            expenseSheetsSynced = true;
            Toast.makeText(getBaseContext(), "Synced Expenses" + " (" + expenseDataFile.getExpense().size() + ")", Toast.LENGTH_SHORT).show();
            checkAllSync();
        }
    }

    class MyOfflineReadTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle("Home");
            mToolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
            upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            mDrawer = (NavigationView) findViewById(R.id.main_drawer);
            ShowDialog.showDialog(MainActivity.this, "Please Wait");
        }

        @Override
        protected Void doInBackground(Void... params) {
            tableObject = (TableObject) OfflineDataStorage.readObject(MainActivity.this, Constants.TIMESHEETS_FILE);
            employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(MainActivity.this, Constants.EMPLOYEE_LOGIN_FILE);
            classesDataFile = (ClassesDataFile) OfflineDataStorage.readObject(MainActivity.this, Constants.CLASSES_DATA_FILE);
            projectsDataFile = (ProjectsDataFile) OfflineDataStorage.readObject(MainActivity.this, Constants.PROJECTS_DATA_FILE);
            taskDataFile = (TaskDataFile) OfflineDataStorage.readObject(MainActivity.this, Constants.TASKS_DATA_FILE);
            expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(MainActivity.this, Constants.EXPENSE_DATA_FILE);
            accountsDataFile = (AccountsDataFile) OfflineDataStorage.readObject(MainActivity.this, Constants.ACCOUNTS_DATA_FILE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            init();
            mDrawer.setNavigationItemSelectedListener(MainActivity.this);
            mDrawerToggle = new ActionBarDrawerToggle(MainActivity.this,
                    mDrawerLayout,
                    mToolbar,
                    R.string.drawer_open,
                    R.string.drawer_close);
            mDrawerLayout.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();
            navigate(mSelectedId);
            View view = mDrawer.getHeaderView(0);
            tvUserName = (TextView) view.findViewById(R.id.tv_user_name);
            tvUserName.setText(employeeLogin.getDisplayName());
            if (getIntent().hasExtra(Constants.NEW_ASSIGNMENTS)) {
                syncNewAssignments();
            }
            updateToken();
            ShowDialog.hideDialog();
        }
    }

}