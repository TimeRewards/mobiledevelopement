package com.timerewards.tr.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Check;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.AccountModel;
import com.timerewards.tr.DataFiles.AccountsDataFile;
import com.timerewards.tr.DataFiles.ApproveExpenseDataFile;
import com.timerewards.tr.DataFiles.ApproveTimesheetDataFile;
import com.timerewards.tr.DataFiles.AssignmentModel;
import com.timerewards.tr.DataFiles.ClassesDataFile;
import com.timerewards.tr.DataFiles.ClassesModel;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.ExpenseHeaderModel;
import com.timerewards.tr.DataFiles.ExpenseItemModel;
import com.timerewards.tr.DataFiles.ExpenseToApproveDataFile;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.LeaveTaskModel;
import com.timerewards.tr.DataFiles.ProjectsDataFile;
import com.timerewards.tr.DataFiles.ProjectsModel;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.TaskDataFile;
import com.timerewards.tr.DataFiles.TasksModel;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import static com.timerewards.tr.Controller.Constants.EXPENSE_APPROVE_DATA_FILE;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button;
    private EditText mUsername, mPassword;
    private String uname, pass;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private EmployeeLogin employeeLoginResult;
    private Toast toast;
    private ProjectsDataFile projectsDataFile;
    private AccountsDataFile accountsDataFile;
    private ArrayList<ProjectsModel> projectsModelList;
    private ArrayList<AccountModel> accountModelArrayList;
    private ClassesDataFile classesDataFile;
    private ArrayList<ClassesModel> classesModelList;
    private TaskDataFile taskDataFile;
    private ExpenseDataFile expenseDataFile;
    private ExpenseToApproveDataFile expenseToApproveDataFile;
    private Expense expenseModel;
    private ArrayList<TasksModel> tasksModelList;
    private ArrayList<AssignmentModel> assignmentModelArrayList;
    private ArrayList<LeaveTaskModel> leaveTaskModelList;
    private ArrayList<Expense> expenseModelList;
    private ArrayList<ExpenseHeaderModel> expenseHeaderModelList;
    private ArrayList<ExpenseHeaderModel> approveExpenseHeaderModelList;
    private ArrayList<ExpenseItemModel> expenseItemModelList;
    private ArrayList<ExpenseItemModel> approveExpenseItemModelList;
    private TableObject tableObject;
    private ArrayList<Timesheet> timeSheetList;
    private ArrayList<Timesheet> timeSheetApproveList;
    private GetOptionsDataFile getOptionsDataFile;
    private ApproveExpenseDataFile approveExpenseDataFile;
    private ApproveTimesheetDataFile approveTimesheetDataFile;
    private ExpenseHeaderModel expenseHeaderModel;
    private ExpenseItemModel expenseItemModel;
    private Intent intent;
    private boolean classesSynced = false;
    private boolean projectsSynced = false;
    private boolean tasksSynced = false;
    private boolean timesheetSynced = false;
    private boolean expenseSheetsSynced = false;
    private boolean getOptionsSynced = false;
    private boolean acccountsSynced = false;
    private boolean timesheetsForApproveSynced = false;
    private boolean expensesForApproveSynced = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkLogin();
        setContentView(R.layout.activity_login);
        button = (Button) findViewById(R.id.button_sign_in);
        button.setOnClickListener(this);
        mUsername = (EditText) findViewById(R.id.et_uname);
        mPassword = (EditText) findViewById(R.id.et_password);
        projectsModelList = new ArrayList<>();
        accountModelArrayList = new ArrayList<>();
        classesModelList = new ArrayList<>();
        tasksModelList = new ArrayList<>();
        expenseHeaderModelList = new ArrayList<>();
        expenseItemModelList = new ArrayList<>();
        expenseHeaderModel = new ExpenseHeaderModel();
        expenseItemModel = new ExpenseItemModel();
        tableObject = new TableObject();
        projectsDataFile = new ProjectsDataFile();
        accountsDataFile = new AccountsDataFile();
        classesDataFile = new ClassesDataFile();
        taskDataFile = new TaskDataFile();
        expenseDataFile = new ExpenseDataFile();
        expenseModel = new Expense();
        approveExpenseDataFile = new ApproveExpenseDataFile();
        approveTimesheetDataFile = new ApproveTimesheetDataFile();
        toast = Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button_sign_in) {

            if (Check.isNetworkAvailable(this)) {
                login();
            } else {
                toast.show();
            }
        }
    }

    private void checkLogin() {
        preferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
        if ("yes".equalsIgnoreCase(preferences.getString("session", "na"))) {
            intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void login() {
        ShowDialog.showDialog(LoginActivity.this, "Logging In");
        uname = mUsername.getText().toString();
        pass = mPassword.getText().toString();

        String url = "";
        url = Constants.LOGIN_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", uname);
            jsonObject.put("password", pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Log.i("timeRewards", "login url= " + url);

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                ShowDialog.hideDialog();
                s.toString();
                if (s.equalsIgnoreCase("\"not a valid user\"")) {
                    Toast.makeText(getBaseContext(), "Invalid Username or password", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getBaseContext(), "Login successful", Toast.LENGTH_SHORT).show();
                    employeeLoginResult = new GsonBuilder().create().fromJson(s.toString(), EmployeeLogin.class);
                    editor = preferences.edit();
                    editor.putBoolean("isValid", employeeLoginResult.isValid());
                    editor.putLong("Userid", employeeLoginResult.getUserid());
                    editor.putString("displayName", employeeLoginResult.getDisplayName());
                    editor.putString("$id", employeeLoginResult.get$id());
                    editor.apply();
                    tableObject.set$id(employeeLoginResult.get$id());
                    tableObject.setDisplayName(employeeLoginResult.getDisplayName());
                    tableObject.setUserid(employeeLoginResult.getUserid());
                    tableObject.setSiteCode(employeeLoginResult.getSiteCode());
                    OfflineDataStorage.writeObject(getBaseContext(), employeeLoginResult, Constants.EMPLOYEE_LOGIN_FILE);
//                    syncAll();
                    syncGetOptions();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(LoginActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 4, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void syncAll() {
        ShowDialog.showDialog(LoginActivity.this, "Syncing Data");
        syncProjects();
        syncClasses();
        syncTasks();
        syncTimeSheets();
//        syncGetOptions();
        syncExpensesList();
        syncAccounts();

        approveTimesheetDataFile = (ApproveTimesheetDataFile) OfflineDataStorage.readObject(this, Constants.TIMESHEETS_APPROVE_FILE);
        if (approveTimesheetDataFile == null) {
            approveTimesheetDataFile = new ApproveTimesheetDataFile();
            ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();
            approveTimesheetDataFile.setTimesheetArrayList(timesheetArrayList);
            OfflineDataStorage.writeObject(this, approveTimesheetDataFile, Constants.TIMESHEETS_APPROVE_FILE);
        }
        ExpenseDataFile approveExpenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(this, EXPENSE_APPROVE_DATA_FILE);
        if (approveExpenseDataFile == null) {
            approveExpenseDataFile = new ExpenseDataFile();
            ArrayList<Expense> expenseArrayList = new ArrayList<>();
            approveExpenseDataFile.setExpense(expenseArrayList);
            OfflineDataStorage.writeObject(this, approveExpenseDataFile, Constants.EXPENSE_APPROVE_DATA_FILE);
        }

    }

    private void syncClasses() {

        String url = Constants.CLASSES_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            if (s.contains("Classes")) {
                                JSONArray array = (JSONArray) new JSONObject(s.toString()).get("Classes");
                                classesModelList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array.toString(), ClassesModel[].class)));
                                classesModelList = generateNonClickableClasses(classesModelList);
                            } else {
                                classesModelList = new ArrayList<>();
                            }
                            ClassesModel classesModel = new ClassesModel();
                            classesModel.setClassName("None");
                            classesModel.setClassRowID(0);
                            classesModel.setIsClickable(true);
                            classesModel.setFullName("None");
                            classesModel.setSubLevel(0);
                            classesModel.set$id("0");
                            classesModelList.add(0, classesModel);
                            classesDataFile.setClassesModelList(classesModelList);
                            OfflineDataStorage.writeObject(getBaseContext(), classesDataFile, Constants.CLASSES_DATA_FILE);
                            classesDataFile = (ClassesDataFile) OfflineDataStorage.readObject(getBaseContext(), Constants.CLASSES_DATA_FILE);
                            Toast.makeText(getBaseContext(), "Synced " + getOptionsDataFile.getClassLex() + " (" + classesModelList.size() + ")", Toast.LENGTH_SHORT).show();
                            classesSynced = true;
                            checkAllSync();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(LoginActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private ArrayList<ClassesModel> generateNonClickableClasses(ArrayList<ClassesModel> classesModelList1) {

        ArrayList<ClassesModel> classesModelListNEW = new ArrayList<>();

        int parentSubLevel = 0;
        String parentName = "";
        int currentSubLevel = 0;
        String currentName = "";
        int subLevel = 0;

        for (int i = 0; i < classesModelList1.size(); i++) {
            parentSubLevel = currentSubLevel;
            parentName = currentName;
            currentSubLevel = classesModelList1.get(i).getSubLevel();
            currentName = classesModelList1.get(i).getFullName();
            if (currentSubLevel == 0) {
                classesModelListNEW.add(classesModelList1.get(i));
                continue;
            }

            if (currentName.lastIndexOf(":") == -1) {
                currentSubLevel = 0;
            }

            if ((currentSubLevel - parentSubLevel == 0)
                    && currentName.substring(currentName.lastIndexOf(":"), currentName.length())
                    .equalsIgnoreCase(parentName.substring(parentName.lastIndexOf(":"), parentName.length()))) {

                classesModelListNEW.add(classesModelList1.get(i));
                continue;

            }

            if ((currentSubLevel - parentSubLevel == 1)
                    && currentName.substring(0, currentName.lastIndexOf(":")).equalsIgnoreCase(parentName)) {

                classesModelListNEW.add(classesModelList1.get(i));
                continue;

            }
            String[] names = currentName.split(":");
            for (int j = 0; j < names.length - 1; j++) {
                ClassesModel classesModel = new ClassesModel();
                classesModel.setClassName(names[j]);
                classesModel.setIsClickable(false);
                classesModel.setFullName("ERROR!!!");
                classesModel.setSubLevel(j);
                classesModelListNEW.add(classesModel);
            }
            classesModelListNEW.add(classesModelList1.get(i));

        }

        return classesModelListNEW;

    }

    private void syncProjects() {

        String url = Constants.PROJECTS_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONArray array = (JSONArray) new JSONObject(s.toString()).get("Projects");
                            projectsModelList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array.toString(), ProjectsModel[].class)));
                            ProjectsModel projectsModel = new ProjectsModel();
                            projectsModel.set$id("0");
                            projectsModel.setSubLevel(0);
                            projectsModel.setFullName("None");
                            projectsModel.setProjectName("None");
                            projectsModel.setParentID(0);
                            projectsModel.setEnabled(true);
                            projectsModel.setIsClickable(true);
                            projectsModel.setExpPrj(true);
                            projectsModelList.add(0, projectsModel);
                            projectsDataFile.setProjectsModelList(projectsModelList);
                            OfflineDataStorage.writeObject(getBaseContext(), projectsDataFile, Constants.PROJECTS_DATA_FILE);
                            Toast.makeText(getBaseContext(), "Synced " + getOptionsDataFile.getCustomerLex() + " : " + getOptionsDataFile.getProjectLex() + " (" + projectsModelList.size() + ")", Toast.LENGTH_SHORT).show();
                            projectsSynced = true;
                            checkAllSync();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
//                volleyError.printStackTrace();
                Utilities.handleVolleyError(LoginActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void syncTasks() {

        String url = Constants.TASKS_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONArray array = (JSONArray) new JSONObject(s.toString()).get("Tasks");
                            tasksModelList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array.toString(), TasksModel[].class)));
                            tasksModelList = generateNonClickableTasks(tasksModelList);
                            TasksModel tasksModel = new TasksModel();
                            tasksModel.set$id("0");
                            tasksModel.setFullName("None");
                            tasksModel.setTaskName("None");
                            tasksModel.setIsClickable(true);
                            tasksModel.setTaskID(0);
                            tasksModelList.add(0, tasksModel);
                            taskDataFile.setTasksModelList(tasksModelList);

                            JSONArray array2 = (JSONArray) new JSONObject(s.toString()).get("Assignments");
                            assignmentModelArrayList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array2.toString(), AssignmentModel[].class)));
                            taskDataFile.setAssignmentModels(assignmentModelArrayList);

                            JSONArray array3 = (JSONArray) new JSONObject(s.toString()).get("LeaveTask");
                            leaveTaskModelList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array3.toString(), LeaveTaskModel[].class)));
                            LeaveTaskModel leaveTaskModel = new LeaveTaskModel();
                            leaveTaskModel.set$id("0");
                            leaveTaskModel.setFullName("None");
                            leaveTaskModel.setTaskID(0);
                            leaveTaskModel.setIsClickable(true);
                            leaveTaskModel.setSubLevel(0);
                            leaveTaskModel.setTaskName("None");
                            leaveTaskModelList.add(0, leaveTaskModel);
                            taskDataFile.setLeaveTaskModels(leaveTaskModelList);
                            OfflineDataStorage.writeObject(getBaseContext(), taskDataFile, Constants.TASKS_DATA_FILE);
                            Toast.makeText(getBaseContext(), "Synced " + getOptionsDataFile.getTaskLex() + " (" + tasksModelList.size() + ")", Toast.LENGTH_SHORT).show();
                            tasksSynced = true;
                            checkAllSync();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
//                volleyError.printStackTrace();
                Utilities.handleVolleyError(LoginActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private ArrayList<TasksModel> generateNonClickableTasks(ArrayList<TasksModel> tasksModelList1) {

        ArrayList<TasksModel> tasksModelListNEW = new ArrayList<>();

        int parentSubLevel = 0;
        String parentName = "";
        int currentSubLevel = 0;
        String currentName = "";

        for (int i = 0; i < tasksModelList1.size(); i++) {
            parentSubLevel = currentSubLevel;
            parentName = currentName;
            currentSubLevel = tasksModelList1.get(i).getSubLevel();
            currentName = tasksModelList1.get(i).getFullName();
            if (currentSubLevel == 0) {
                tasksModelListNEW.add(tasksModelList1.get(i));
                continue;
            }

            if ((currentSubLevel - parentSubLevel == 0)
                    && currentName.substring(currentName.lastIndexOf(":"), currentName.length())
                    .equalsIgnoreCase(parentName.substring(parentName.lastIndexOf(":"), parentName.length()))) {

                tasksModelListNEW.add(tasksModelList1.get(i));
                continue;

            }

            if ((currentSubLevel - parentSubLevel == 1)
                    && currentName.substring(0, currentName.lastIndexOf(":")).equalsIgnoreCase(parentName)) {
                tasksModelListNEW.add(tasksModelList1.get(i));
                continue;
            }

            String[] names = currentName.split(":");
            for (int j = 0; j < names.length - 1; j++) {
                TasksModel tasksModel = new TasksModel();
                tasksModel.setTaskName(names[j]);
                tasksModel.setIsClickable(false);
                tasksModel.setFullName("ERROR!!!");
                tasksModel.setSubLevel(j);
                tasksModelListNEW.add(tasksModel);
            }
            tasksModelListNEW.add(tasksModelList1.get(i));
        }
        return tasksModelListNEW;
    }

    private ArrayList<AccountModel> generateNonClickableAccounts(ArrayList<AccountModel> accountModelArrayList1) {

        ArrayList<AccountModel> accountsModelListNEW = new ArrayList<>();

        int parentSubLevel = 0;
        String parentName = "";
        int currentSubLevel = 0;
        String currentName = "";
        int subLevel = 0;
        String assestType = "";

        for (int i = 0; i < accountModelArrayList1.size(); i++) {
            assestType = accountModelArrayList1.get(i).getAssettype();
            parentSubLevel = currentSubLevel;
            parentName = currentName;
            currentSubLevel = accountModelArrayList.get(i).getSubLevelLocal();
            currentName = accountModelArrayList1.get(i).getFullName();
            int counter = 0;
            for (int jj = 0; jj < currentName.length(); jj++) {
                if (currentName.charAt(jj) == ':') {
                    counter++;
                }
            }
            currentSubLevel = counter;
            if (currentSubLevel == 0) {
                accountsModelListNEW.add(accountModelArrayList1.get(i));
                continue;
            }

            if ((currentSubLevel - parentSubLevel == 0)
                    && currentName.substring(currentName.lastIndexOf(":"), currentName.length())
                    .equalsIgnoreCase(parentName.substring(parentName.lastIndexOf(":"), parentName.length()))) {

                accountsModelListNEW.add(accountModelArrayList1.get(i));
                continue;

            }

//            if ((currentSubLevel - parentSubLevel == 1)
//                    && currentName.substring(0, currentName.lastIndexOf(":")).equalsIgnoreCase(parentName)) {
            //// TODO: 5/10/16 this need to be fixed...
            if ((currentSubLevel - parentSubLevel == 1)) {
                accountModelArrayList1.get(i).setSubLevelLocal(counter);
                accountsModelListNEW.add(accountModelArrayList1.get(i));
                continue;

            }
            String[] names = currentName.split(":");
            for (int j = 0; j < names.length - 1; j++) {
                AccountModel accountModel = new AccountModel();
                accountModel.setAccountname(names[j]);
                accountModel.setIsClickable(false);
                accountModel.setFullName("ERROR!!!");
                accountModel.setSubLevelLocal(j);
                accountModel.setAssettype(assestType);
                accountsModelListNEW.add(accountModel);
            }
            accountsModelListNEW.add(accountModelArrayList1.get(i));

        }

        return accountsModelListNEW;

    }

    private void syncTimeSheets() {

        String url = Constants.TIMESHEETS_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONArray array = new JSONArray(s.toString());
                            timeSheetList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array.toString(), Timesheet[].class)));
                            tableObject.setTimesheetList(timeSheetList);
                            OfflineDataStorage.writeObject(getBaseContext(), tableObject, Constants.TIMESHEETS_FILE);
                            Toast.makeText(getBaseContext(), "Synced Timesheets" + " (" + timeSheetList.size() + ")", Toast.LENGTH_SHORT).show();
                            timesheetSynced = true;
                            checkAllSync();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
//                volleyError.printStackTrace();
                Utilities.handleVolleyError(LoginActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void syncGetOptions() {

        String url = Constants.GET_OPTIONS_API;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
//                        Log.i("arsh", s.toString());
                        Gson gson = new Gson();
                        getOptionsDataFile = gson.fromJson(s.toString(), GetOptionsDataFile.class);
                        OfflineDataStorage.writeObject(getBaseContext(), getOptionsDataFile, Constants.GETOPTIONS_FILE);
//                        Toast.makeText(getBaseContext(), "GetOptionsSynced\nbillable" + getOptionsDataFile.getShowBillable(), Toast.LENGTH_SHORT).show();
                        getOptionsSynced = true;
//                        checkAllSync();
                        syncAll();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
//                volleyError.printStackTrace();
                Utilities.handleVolleyError(LoginActivity.this, volleyError);
            }
        });
        ShowDialog.showDialog(LoginActivity.this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void syncExpensesList() {

        String url = Constants.EXPENSES_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        expenseDataFile = new GsonBuilder().create().fromJson(s, ExpenseDataFile.class);
                        OfflineDataStorage.writeObject(getBaseContext(), expenseDataFile, Constants.EXPENSE_DATA_FILE);
                        Toast.makeText(getBaseContext(), "Synced Expenses" + " (" + expenseDataFile.getExpense().size() + ")", Toast.LENGTH_SHORT).show();
                        expenseSheetsSynced = true;
                        checkAllSync();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
//                volleyError.printStackTrace();
                Utilities.handleVolleyError(LoginActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void syncAccounts() {

        String url = Constants.ACCOUNTS_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            JSONArray array = (JSONArray) new JSONObject(s.toString()).get("AccountsList");
                            accountModelArrayList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array.toString(), AccountModel[].class)));
                            accountModelArrayList = generateNonClickableAccounts(accountModelArrayList);
                            AccountModel accountModel = new AccountModel();
                            accountModel.set$id("0");
                            accountModel.setAccountid(0);
                            accountModel.setAccountname("None");
                            accountModel.setAssettype("Expense");
                            accountModel.setIsClickable(true);
                            accountModel.setIsDefaultGLAnItem(0);
                            accountModelArrayList.add(0, accountModel);
                            accountsDataFile.setAccountsList(accountModelArrayList);
                            OfflineDataStorage.writeObject(getBaseContext(), accountsDataFile, Constants.ACCOUNTS_DATA_FILE);
                            Toast.makeText(getBaseContext(), "Synced Accounts" + " (" + accountModelArrayList.size() + ")", Toast.LENGTH_SHORT).show();
                            acccountsSynced = true;
                            checkAllSync();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
//                volleyError.printStackTrace();
                Utilities.handleVolleyError(LoginActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void checkAllSync() {
        if (classesSynced
                && projectsSynced
                && tasksSynced
                && timesheetSynced
                && getOptionsSynced
//                && timesheetsForApproveSynced
                && expenseSheetsSynced
//                && expensesForApproveSynced
                && acccountsSynced) {
            editor = preferences.edit();
            editor.putString("session", "yes");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            editor.putString(Constants.TIMESHEET_LAST_SYNC_DATE, sdf.format(new Date(Calendar.getInstance().getTimeInMillis())));
            editor.putString(Constants.EXPENSE_LAST_SYNC_DATE, sdf.format(new Date(Calendar.getInstance().getTimeInMillis())));
            editor.apply();
            intent = new Intent(this, MainActivity.class);
            Toast.makeText(this, "Sync Completed", Toast.LENGTH_SHORT).show();
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        ShowDialog.hideDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}