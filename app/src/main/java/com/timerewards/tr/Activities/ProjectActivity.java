package com.timerewards.tr.Activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.widget.Toast;

import com.timerewards.tr.Adapters.ProjectsAdapter;
import com.timerewards.tr.R;

public class ProjectActivity extends AppCompatActivity implements ProjectsAdapter.ProjectSelection {

    RecyclerView.LayoutManager layoutManager;
    Dialog dialog;
    private RecyclerView recyclerView;
    private ProjectsAdapter projectsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_list);
//        projectsAdapter = new ProjectsAdapter(ProjectActivity.this, 12, new ArrayList<>());
        layoutManager = new LinearLayoutManager(ProjectActivity.this);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_list);
        recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(projectsAdapter);
        recyclerView.scrollToPosition(25);
        dialog.show();

//        recyclerView.addOnItemTouchListener();
//        recyclerView.setSelection(5);
    }


    @Override
    public void setProject(int position) {
        Toast.makeText(ProjectActivity.this, "position = " + position, Toast.LENGTH_SHORT).show();
    }
}
