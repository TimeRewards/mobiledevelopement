package com.timerewards.tr.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.timerewards.tr.Adapters.ClassesAdapter;
import com.timerewards.tr.Adapters.LeaveTaskAdapter;
import com.timerewards.tr.Adapters.ProjectsAdapter;
import com.timerewards.tr.Adapters.TasksAdapter;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.AssignmentModel;
import com.timerewards.tr.DataFiles.ClassesDataFile;
import com.timerewards.tr.DataFiles.ClassesModel;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.LeaveTaskModel;
import com.timerewards.tr.DataFiles.ProjectsDataFile;
import com.timerewards.tr.DataFiles.ProjectsModel;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.TaskDataFile;
import com.timerewards.tr.DataFiles.TasksModel;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class EditTimesheet extends AppCompatActivity implements View.OnClickListener, ProjectsAdapter.ProjectSelection, ClassesAdapter.ClassSelection, TasksAdapter.TasksSelection, LeaveTaskAdapter.LeaveTasksSelection, TextWatcher {

    private Toolbar mToolbar;
    private EditText activity_date, description, start_time, end_time, et_hours;
    private CheckBox billable;
    private Button saveTimesheet;
    private TextView tvProjectName, tvTaskName, tvClassName, textEditTimesheet, toolbarTV;
    private TextView textProject, textTask, textClass, textStartTime, textEndTime;
    private Dialog dialog;

    private ProjectsDataFile projectsDataFile;
    private TaskDataFile taskDataFile;
    private ClassesDataFile classesDataFile;
    private GetOptionsDataFile getOptionsDataFile;
    private TableObject tableObject;
    private EmployeeLogin employeeLogin;

    private ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();
    private ArrayList<ProjectsModel> projectsModelArrayList = new ArrayList<>();
    private ArrayList<TasksModel> tasksModelArrayList = new ArrayList<>();
    private ArrayList<ClassesModel> classesModelArrayList = new ArrayList<>();
    private ArrayList<LeaveTaskModel> leaveTaskModelArrayList = new ArrayList<>();
    private ArrayList<AssignmentModel> assignmentModelArrayList = new ArrayList<>();
    private ArrayList<TasksModel> tasksFromAssignmentArrayList = new ArrayList<>();
    private ArrayList<TasksModel> tasksModelArrayListSearch = new ArrayList<>();
    private ArrayList<ClassesModel> classesModelArrayListSearch = new ArrayList<>();
    private ArrayList<ProjectsModel> projectsModelArrayListSearch = new ArrayList<>();

    private ProjectsModel selectedProjectsModel;
    private ClassesModel selectedClassesModel;
    private TasksModel selectedTaskModel;
    private LeaveTaskModel selectedLeaveTaskModel;

    private DatePickerDialog pickerDialog;
    private TimePickerDialog timePickerDialog;
    private SimpleDateFormat simpleDateFormat;

    private RecyclerView recyclerView;
    private ProjectsAdapter projectsAdapter;
    private TasksAdapter tasksAdapter;
    private LeaveTaskAdapter leaveTaskAdapter;
    private ClassesAdapter classesAdapter;

    private boolean isLeavesInTaskList = false;
    private boolean isTasksInTaskList = true;
    private boolean isTaskFromAssignment = false;

    private int projectPosition = 0;
    private int taskPosition = 0;
    private int classPosition = 0;

    private Intent intent;
    private Timesheet timesheetFromIntent;
    private int timesheetPosition;
    private String typeOfListInDialog;
    private EditText searchET;
    private ImageView searchIcon;
    String dialougeIdentifier="";
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_timesheet);
        preferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putString(Constants.DIALOUGE_IDEN,"").apply();
        dialougeIdentifier=preferences.getString(Constants.DIALOUGE_IDEN,"");
        init();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Timesheet");
        mToolbar.setTitleTextColor(Color.WHITE);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        start_time.clearFocus();
        end_time.clearFocus();
        putData();
    }

    private void init() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        intent = getIntent();
        timesheetPosition = intent.getIntExtra(Constants.TIMESHEET_POSITION, -1);
        tableObject = (TableObject) OfflineDataStorage.readObject(this, Constants.TIMESHEETS_FILE);
        timesheetFromIntent = tableObject.getTimesheetList().get(timesheetPosition);
        textEditTimesheet = (TextView) findViewById(R.id.tv_add_new_timesheet);
        textEditTimesheet.setText("Edit Timesheet");
        toolbarTV = (TextView) findViewById(R.id.toolbarTV);
        toolbarTV.setText("Edit Timesheet");
        projectsDataFile = (ProjectsDataFile) OfflineDataStorage.readObject(this, Constants.PROJECTS_DATA_FILE);
        taskDataFile = (TaskDataFile) OfflineDataStorage.readObject(this, Constants.TASKS_DATA_FILE);
        classesDataFile = (ClassesDataFile) OfflineDataStorage.readObject(this, Constants.CLASSES_DATA_FILE);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(this, Constants.EMPLOYEE_LOGIN_FILE);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(this, Constants.GETOPTIONS_FILE);

        timesheetArrayList = tableObject.getTimesheetList();
        projectsModelArrayList = projectsDataFile.getProjectsModelList();
        projectsModelArrayList = Utilities.filterBasedOnExpPrj(projectsModelArrayList);
        tasksModelArrayList = taskDataFile.getTasksModelList();
        assignmentModelArrayList = taskDataFile.getAssignmentModels();
        leaveTaskModelArrayList = taskDataFile.getLeaveTaskModels();
        classesModelArrayList = classesDataFile.getClassesModelList();

        classesModelArrayListSearch = classesDataFile.getClassesModelList();
        tasksModelArrayListSearch = taskDataFile.getTasksModelList();
        projectsModelArrayListSearch = (ArrayList<ProjectsModel>) projectsModelArrayList.clone();


        projectsAdapter = new ProjectsAdapter(this, 0, projectsModelArrayList);

        tasksAdapter = new TasksAdapter(this, 0, tasksModelArrayList);
        leaveTaskAdapter = new LeaveTaskAdapter(this, 0, leaveTaskModelArrayList);

        classesAdapter = new ClassesAdapter(this, 0, classesModelArrayList);

//        Toast.makeText(this, "" + tableObject.getTimesheetList().size(), Toast.LENGTH_SHORT).show();
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_list);
        recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        searchIcon = (ImageView) dialog.findViewById(R.id.iv_search);
        searchIcon.setImageResource(R.drawable.search_icon);
        searchET = (EditText) dialog.findViewById(R.id.et_search);
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchET.getText().clear();

            }
        });
        searchET.addTextChangedListener(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        textStartTime = (TextView) findViewById(R.id.text_start_time);
        textEndTime = (TextView) findViewById(R.id.text_end_time);
        textProject = (TextView) findViewById(R.id.text_project);
        textTask = (TextView) findViewById(R.id.text_task);
        textClass = (TextView) findViewById(R.id.text_class);
        tvProjectName = (TextView) findViewById(R.id.tv_project_name);
        tvProjectName.setText("Select " + getOptionsDataFile.getCustomerLex() + " : " + getOptionsDataFile.getProjectLex());
        tvProjectName.setOnClickListener(this);
        tvTaskName = (TextView) findViewById(R.id.tv_task_name);
        tvTaskName.setText("Select " + getOptionsDataFile.getTaskLex());
        tvTaskName.setOnClickListener(this);
        tvClassName = (TextView) findViewById(R.id.tv_class_name);
        tvClassName.setText("Select " + getOptionsDataFile.getClassLex());
        tvClassName.setOnClickListener(this);
        activity_date = (EditText) findViewById(R.id.et_activity_date);
        activity_date.setOnClickListener(this);
        start_time = (EditText) findViewById(R.id.et_start_time);
        start_time.setText(getOptionsDataFile.getStartTime());
        end_time = (EditText) findViewById(R.id.et_end_time);
        end_time.setText(getOptionsDataFile.getEndTime());
        et_hours = (EditText) findViewById(R.id.et_hours);
        if (getOptionsDataFile.getStartTime().length() > 0
                && getOptionsDataFile.getEndTime().length() > 0) {
            calculateHours();
        }

        start_time.setOnClickListener(this);

        end_time.setOnClickListener(this);

        et_hours.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() > 0
                        && start_time.getText().toString().length() > 0
                        && end_time.getText().toString().length() > 0) {
                    if (editable.toString().equalsIgnoreCase(".")) {
                        et_hours.setText("0.");
                        et_hours.setSelection(et_hours.getText().toString().length());
                    } else {
                        calculateEndTime();
                    }

                }
            }
        });

        billable = (CheckBox) findViewById(R.id.checkbox_billable);
        description = (EditText) findViewById(R.id.et_description);
        saveTimesheet = (Button) findViewById(R.id.button_save_timesheet);
        saveTimesheet.setOnClickListener(this);
    }

    private void putData() {

        textProject.setText(getOptionsDataFile.getCustomerLex() + " : " + getOptionsDataFile.getProjectLex());
        textTask.setText(getOptionsDataFile.getTaskLex());
        textClass.setText(getOptionsDataFile.getClassLex());

        if (getOptionsDataFile.isUseClassesInTS() == false) {
            textClass.setVisibility(View.GONE);
            tvClassName.setVisibility(View.GONE);
        }
        switch (getOptionsDataFile.getShowBillable()) {
            case 0:
                billable.setVisibility(View.GONE);
                break;
            case 1:
                billable.setChecked(false);
                break;
            case 2:
                billable.setChecked(true);
                break;
            case 3:

                break;
        }
        activity_date.setText(Utilities.dateToLocalFormat(timesheetFromIntent.getActivityDate()));
        start_time.setText(timesheetFromIntent.getStartTime());
        end_time.setText(timesheetFromIntent.getEndTime());
        if (getOptionsDataFile.getCanEnterFutureTime() == 0) {
            end_time.setEnabled(false);
        }
        if (getOptionsDataFile.getCanEnterFutureTime() == 1) {
            end_time.setEnabled(true);
        }
        if (getOptionsDataFile.isShowStartEndTime() == false) {
            textStartTime.setVisibility(View.GONE);
            textEndTime.setVisibility(View.GONE);
            start_time.setVisibility(View.GONE);
            end_time.setVisibility(View.GONE);
        }
        if (getOptionsDataFile.getShowLeave() == 0) {
            isLeavesInTaskList = false;
            isTasksInTaskList = true;
        } else {
            isLeavesInTaskList = true;
            isTasksInTaskList = false;
        }
//        et_hours.setText(String.valueOf(timesheetFromIntent.getActualHours()));
        et_hours.setText(String.format("%.2f", timesheetFromIntent.getActualHours()));
        description.setText(timesheetFromIntent.getDescription());

        for (int i = 0; i < projectsModelArrayList.size(); i++) {
            if (timesheetFromIntent.getProjectID() == projectsModelArrayList.get(i).getProjectID()) {
                projectPosition = i;
                selectedProjectsModel = projectsModelArrayList.get(projectPosition);
                projectsAdapter.changePosition(projectPosition);
                tvProjectName.setText(selectedProjectsModel.getFullName());
                break;
            }
        }

        for (int i = 0; i < classesModelArrayList.size(); i++) {
            if (timesheetFromIntent.getClassRowID() == classesModelArrayList.get(i).getClassRowID()) {
                classPosition = i;
                selectedClassesModel = classesModelArrayList.get(classPosition);
                classesAdapter.changePosition(classPosition);
                tvClassName.setText(selectedClassesModel.getFullName());
                break;
            }
        }

        if (timesheetFromIntent.isTime() == false) {
            isLeavesInTaskList = true;
            isTaskFromAssignment = false;
            isTasksInTaskList = false;
            for (int i = 0; i < leaveTaskModelArrayList.size(); i++) {
                if (timesheetFromIntent.getTaskID() == leaveTaskModelArrayList.get(i).getTaskID()) {
                    taskPosition = i;
                    selectedLeaveTaskModel = leaveTaskModelArrayList.get(taskPosition);
                    leaveTaskAdapter.changePosition(taskPosition);
                    tvTaskName.setText(selectedLeaveTaskModel.getFullName());
                }
            }
        } else {

            if (selectedProjectsModel.getProjectID() != 0) {
                for (int i = 0; i < assignmentModelArrayList.size(); i++) {
                    if (selectedProjectsModel.getProjectID() == assignmentModelArrayList.get(i).getProjectID()) {
                        if (assignmentModelArrayList.get(i).getTaskID() != 0) {
                            for (int j = 0; j < tasksModelArrayList.size(); j++) {
                                if (assignmentModelArrayList.get(i).getTaskID() == tasksModelArrayList.get(j).getTaskID()) {
                                    tasksFromAssignmentArrayList = new ArrayList<>();
                                    TasksModel tasksModel = new TasksModel();
                                    tasksModel.setTaskID(0);
                                    tasksModel.setIsClickable(true);
                                    tasksModel.setTaskName("None");
                                    tasksModel.setSubLevel(0);
                                    tasksModel.setFullName("None");
                                    tasksFromAssignmentArrayList.add(tasksModel);
                                    tasksFromAssignmentArrayList.add(tasksModelArrayList.get(j));
                                    isLeavesInTaskList = false;
                                    isTaskFromAssignment = true;
                                    isTasksInTaskList = false;
                                    taskPosition = 1;
                                    selectedTaskModel = tasksFromAssignmentArrayList.get(taskPosition);
                                    tasksAdapter = new TasksAdapter(this, taskPosition, tasksFromAssignmentArrayList);
                                    tvTaskName.setText(selectedTaskModel.getFullName());
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (!isTaskFromAssignment) {
                for (int i = 0; i < tasksModelArrayList.size(); i++) {
                    isLeavesInTaskList = false;
                    isTaskFromAssignment = false;
                    isTasksInTaskList = true;
                    if (timesheetFromIntent.getTaskID() == tasksModelArrayList.get(i).getTaskID()) {
                        taskPosition = i;
                        selectedTaskModel = tasksModelArrayList.get(taskPosition);
                        tasksAdapter.changePosition(taskPosition);
                        tvTaskName.setText(selectedTaskModel.getFullName());
                    }
                }
            }
        }

        if (isLeavesInTaskList) {
            billable.setVisibility(View.GONE);
        } else {
            billable.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_save_timesheet:
                saveData();
                break;
            case R.id.et_activity_date:
                selectDate();
                break;
            case R.id.et_start_time:
                selectTime(R.id.et_start_time);
                break;
            case R.id.et_end_time:
                selectTime(R.id.et_end_time);
                break;
            case R.id.tv_project_name:
                showProjectDialog();
                break;
            case R.id.tv_task_name:
                showTaskDialog();
                break;
            case R.id.tv_class_name:
                showClassDialog();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, MyTimeSheetTransactions.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void selectTime(int id) {
        final int temp = id;
        final Calendar newCalendar = Calendar.getInstance();

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
                try {
                    Date date = format.parse("" + hourOfDay + ":" + minute);
                    if (temp == R.id.et_start_time) {
                        start_time.setText(new SimpleDateFormat("HH:mm", Locale.US).format(date.getTime()));
                        calculateHours();
                    }
                    if (temp == R.id.et_end_time) {
                        end_time.setText(new SimpleDateFormat("HH:mm", Locale.US).format(date.getTime()));
                        calculateHours();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    private void selectDate() {
        Calendar newCalendar = Calendar.getInstance();
        pickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                activity_date.setText(simpleDateFormat.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        pickerDialog.show();
    }

    private void calculateHours() {
        Date dDate1 = null;
        SimpleDateFormat sdfmt1 = new SimpleDateFormat("HH:mm");
        try {
            dDate1 = sdfmt1.parse(start_time.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date dDate2 = null;
        try {
            dDate2 = sdfmt1.parse(end_time.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long timeinMillis = dDate2.getTime() - dDate1.getTime();
        double v = ((float) timeinMillis) / 3600000;
        et_hours.setText(String.format("%.2f", v));
    }

    private void calculateEndTime() {

        double  d=Double.parseDouble(et_hours.getText().toString());
        d=d*60;
        d=Math.ceil(d);

        long tempHoursInmillis = (long)(d*1000l*60l);

        Date dDate1 = null;
        SimpleDateFormat sdfmt1 = new SimpleDateFormat("HH:mm");
        try {
            dDate1 = sdfmt1.parse(start_time.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long endTimeInMillis = dDate1.getTime() + tempHoursInmillis;
        Date date = new Date(endTimeInMillis);
        end_time.setText(sdfmt1.format(date));

    }

    private void saveData() {
//        if (start_time.getText().length() == 0) {
//            Toast.makeText(EditTimesheet.this, "Start Time is Mandatory", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//        if (end_time.getText().length() == 0) {
//            Toast.makeText(EditTimesheet.this, "End Time is Mandatory", Toast.LENGTH_SHORT).show();
//            return;
//        }

        Timesheet timesheet = new Timesheet();
        timesheet.setEZStatusID(0);
        timesheet.set$id("new");
        timesheet.setDescription(description.getText().toString());
        timesheet.setActivityDate(Utilities.dateToServerFormat(activity_date.getText().toString()));
        timesheet.setBillable(billable.isChecked());
        if (et_hours.getText().toString().length() > 0) {
            timesheet.setActualHours(Float.parseFloat(et_hours.getText().toString()));
        }
        timesheet.setStartTime(start_time.getText().toString());
        timesheet.setEndTime(end_time.getText().toString());
        if (selectedClassesModel.getClassRowID() == 0) {
            timesheet.setClassRowID(0);
            timesheet.setClassFullName("None");
        } else {
            timesheet.setClassRowID(selectedClassesModel.getClassRowID());
            timesheet.setClassFullName(selectedClassesModel.getFullName());
        }
        if (selectedProjectsModel.getProjectID() == 0) {
            timesheet.setProjectID(0);
            timesheet.setProjectFullName("None");
        } else {
            timesheet.setProjectID(selectedProjectsModel.getProjectID());
            timesheet.setProjectFullName(selectedProjectsModel.getFullName());
        }

        if (isLeavesInTaskList == true) {
            timesheet.setIsTime(false);
            if (selectedLeaveTaskModel.getTaskID() == 0) {
                timesheet.setTaskFullName("None");
                timesheet.setTaskID(0);
            } else {
                timesheet.setTaskFullName(selectedLeaveTaskModel.getFullName());
                timesheet.setTaskID(selectedLeaveTaskModel.getTaskID());
            }
        }
        if (isTasksInTaskList == true) {
            timesheet.setIsTime(true);
            if (selectedTaskModel.getTaskID() == 0) {
                timesheet.setTaskID(0);
                timesheet.setTaskFullName("None");
            } else {
                timesheet.setTaskFullName(selectedTaskModel.getFullName());
                timesheet.setTaskID(selectedTaskModel.getTaskID());
            }
        }
        if (isTaskFromAssignment == true) {
            timesheet.setIsTime(true);
            if (selectedTaskModel.getTaskID() == 0) {
                timesheet.setTaskID(0);
                timesheet.setTaskFullName("None");
            } else {
                timesheet.setTaskFullName(selectedTaskModel.getFullName());
                timesheet.setTaskID(selectedTaskModel.getTaskID());
            }
        }
        timesheet.setEmployeeFullName(employeeLogin.getDisplayName());
        timesheet.setEmployeeID(employeeLogin.getUserid());
        timesheet.setTimeActivityID(timesheetFromIntent.getTimeActivityID());
        tableObject.getTimesheetList().set(timesheetPosition, timesheet);
        OfflineDataStorage.writeObject(this, tableObject, Constants.TIMESHEETS_FILE);
        Toast.makeText(this, "Timesheet Saved", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MyTimeSheetTransactions.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


//
//    private void showProjectDialog() {
//        recyclerView.setAdapter(projectsAdapter);
//        recyclerView.scrollToPosition(projectPosition);
//        dialog.show();
//    }



    private void showProjectDialog() {
        typeOfListInDialog = "projects";
        dialougeIdentifier=preferences.getString(Constants.DIALOUGE_IDEN,"");
        if(dialougeIdentifier.equalsIgnoreCase("projects"))
        {

        }else
        {
            searchET.setText("");
            preferences.edit().putString(Constants.DIALOUGE_IDEN,"projects").apply();
        }

        recyclerView.setAdapter(projectsAdapter);
        dialog.show();

        if (selectedProjectsModel != null) {
            for (int i = 0; i < projectsModelArrayList.size(); i++) {
                if (projectsModelArrayList.get(i).getProjectID() == projectPosition) {
                    recyclerView.scrollToPosition(i);
                }
            }
        }
    }


//    private void showTaskDialog() {
//
//        if (isLeavesInTaskList == true) {
//            recyclerView.setAdapter(leaveTaskAdapter);
//        }
//        if (isTaskFromAssignment == true) {
//            tasksAdapter = new TasksAdapter(this, taskPosition, tasksFromAssignmentArrayList);
//            recyclerView.setAdapter(tasksAdapter);
//        }
//        if (isTasksInTaskList == true) {
//            tasksAdapter = new TasksAdapter(this, taskPosition, tasksModelArrayList);
//            recyclerView.setAdapter(tasksAdapter);
//        }
//
//        recyclerView.scrollToPosition(taskPosition);
//        dialog.show();
//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//    }
//
//    private void showClassDialog() {
//        classesAdapter = new ClassesAdapter(this, classPosition, classesModelArrayList);
//        recyclerView.setAdapter(classesAdapter);
//        recyclerView.scrollToPosition(classPosition);
//        dialog.show();
//    }



    private void showTaskDialog() {
        typeOfListInDialog = "tasks";
        dialougeIdentifier=preferences.getString(Constants.DIALOUGE_IDEN,"");
        if(dialougeIdentifier.equalsIgnoreCase("tasks"))
        {

        }else
        {
            searchET.setText("");
            preferences.edit().putString(Constants.DIALOUGE_IDEN,"tasks").apply();
        }


        if (isLeavesInTaskList == true) {
            recyclerView.setAdapter(leaveTaskAdapter);
        }
        if (isTaskFromAssignment == true) {
            tasksAdapter = new TasksAdapter(this, taskPosition, tasksFromAssignmentArrayList);
            recyclerView.setAdapter(tasksAdapter);
        }
        if (isTasksInTaskList == true) {
            tasksAdapter = new TasksAdapter(this, taskPosition, tasksModelArrayList);
            recyclerView.setAdapter(tasksAdapter);
        }

        if (selectedTaskModel != null) {
            for (int i = 0; i < tasksModelArrayList.size(); i++) {
                if (tasksModelArrayList.get(i).getTaskID() == taskPosition) {
                    recyclerView.scrollToPosition(i);
                    break;
                }
            }
        }
        dialog.show();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void showClassDialog() {
        typeOfListInDialog = "class";
        dialougeIdentifier=preferences.getString(Constants.DIALOUGE_IDEN,"");
        if(dialougeIdentifier.equalsIgnoreCase("class"))
        {

        }else
        {
            searchET.setText("");
            preferences.edit().putString(Constants.DIALOUGE_IDEN,"class").apply();
        }


        classesAdapter = new ClassesAdapter(this, classPosition, classesModelArrayList);
        recyclerView.setAdapter(classesAdapter);
        if (selectedClassesModel != null) {
            for (int i = 0; i < classesModelArrayList.size(); i++) {
                if (classesModelArrayList.get(i).getClassRowID() == classPosition) {
                    recyclerView.scrollToPosition(i);
                    break;
                }
            }
        }
        dialog.show();
    }


//    @Override
//    public void setProject(int position) {
//        projectPosition = position;
//        selectedProjectsModel = projectsModelArrayList.get(projectPosition);
//        projectsAdapter.changePosition(projectPosition);
//        tvProjectName.setText(selectedProjectsModel.getFullName());
//        dialog.dismiss();
//
//        isTaskFromAssignment = false;
//        isTasksInTaskList = true;
//        isLeavesInTaskList = false;
//
//        if (projectPosition != 0) {
//            for (int i = 0; i < assignmentModelArrayList.size(); i++) {
//                if (selectedProjectsModel.getProjectID() == assignmentModelArrayList.get(i).getProjectID()
//                        && assignmentModelArrayList.get(i).getTaskID() != 0) {
//                    tasksFromAssignmentArrayList = new ArrayList<>();
//                    TasksModel tasksModel = new TasksModel();
//                    tasksModel.setTaskID(0);
//                    tasksModel.setTaskName("None");
//                    tasksModel.setFullName("None");
//                    tasksModel.setSubLevel(0);
//                    tasksModel.setUserID(0);
//                    tasksModel.setIsClickable(true);
//                    tasksFromAssignmentArrayList.add(tasksModel);
//                    for (int j = 0; j < tasksModelArrayList.size(); j++) {
//                        if (assignmentModelArrayList.get(i).getTaskID() == tasksModelArrayList.get(j).getTaskID()) {
//                            isTaskFromAssignment = true;
//                            isLeavesInTaskList = false;
//                            isTasksInTaskList = false;
//                            taskPosition = 1;
//                            tasksFromAssignmentArrayList.add(tasksModelArrayList.get(j));
//                            selectedTaskModel = tasksFromAssignmentArrayList.get(1);
//                            tvTaskName.setText(selectedTaskModel.getFullName());
//                        }
//                    }
//                }
//            }
//
//            if (isTaskFromAssignment == false) {
//                isTasksInTaskList = true;
//                isLeavesInTaskList = false;
//                tvTaskName.setText("None");
//                taskPosition = 0;
//            }
//
//        } else {
//
//            if (getOptionsDataFile.getShowLeave() == 1) {
//                isLeavesInTaskList = true;
//                isTaskFromAssignment = false;
//                isTasksInTaskList = false;
//                tvTaskName.setText("None");
//                taskPosition = 0;
//                selectedTaskModel = tasksModelArrayList.get(0);
//                selectedLeaveTaskModel = leaveTaskModelArrayList.get(0);
//            } else {
//                isLeavesInTaskList = false;
//                isTaskFromAssignment = false;
//                isTasksInTaskList = true;
//                tvTaskName.setText("None");
//                selectedTaskModel = tasksModelArrayList.get(0);
//                selectedLeaveTaskModel = leaveTaskModelArrayList.get(0);
//            }
//        }
//
//        if (projectPosition == 0) {
//            billable.setVisibility(View.GONE);
//        } else {
//            billable.setVisibility(View.VISIBLE);
//        }
//
//    }

//    @Override
//    public void setTask(int position) {
//        taskPosition = position;
//        if (isTaskFromAssignment == true) {
//            tasksAdapter.changePosition(taskPosition);
//            selectedTaskModel = tasksFromAssignmentArrayList.get(taskPosition);
//            tvTaskName.setText(selectedTaskModel.getFullName());
//        } else {
//            tasksAdapter.changePosition(taskPosition);
//            selectedTaskModel = tasksModelArrayList.get(taskPosition);
//            tvTaskName.setText(selectedTaskModel.getFullName());
//        }
//        dialog.dismiss();
//        billable.setVisibility(View.VISIBLE);
//    }


    @Override
    public void setProject(int position) {
        projectPosition = position;

        for (int i = 0; i < projectsModelArrayList.size(); i++) {
            if (projectsModelArrayList.get(i).getProjectID() == position) {
                selectedProjectsModel = projectsModelArrayList.get(i);
            }
        }
        projectsAdapter.changePosition(selectedProjectsModel.getProjectID());
        tvProjectName.setText(selectedProjectsModel.getFullName());
        dialog.dismiss();

        isTaskFromAssignment = false;
        isTasksInTaskList = true;
        isLeavesInTaskList = false;

        if (projectPosition != 0) {
            for (int i = 0; i < assignmentModelArrayList.size(); i++) {
                if (selectedProjectsModel.getProjectID() == assignmentModelArrayList.get(i).getProjectID()
                        && assignmentModelArrayList.get(i).getTaskID() != 0) {
                    tasksFromAssignmentArrayList = new ArrayList<>();
                    TasksModel tasksModel = new TasksModel();
                    tasksModel.setTaskID(0);
                    tasksModel.setTaskName("None");
                    tasksModel.setFullName("None");
                    tasksModel.setSubLevel(0);
                    tasksModel.setUserID(0);
                    tasksModel.setIsClickable(true);
                    tasksFromAssignmentArrayList.add(tasksModel);
                    for (int j = 0; j < tasksModelArrayList.size(); j++) {
                        if (assignmentModelArrayList.get(i).getTaskID() == tasksModelArrayList.get(j).getTaskID()) {
                            isTaskFromAssignment = true;
                            isLeavesInTaskList = false;
                            isTasksInTaskList = false;
                            taskPosition = 1;
                            tasksFromAssignmentArrayList.add(tasksModelArrayList.get(j));
                            selectedTaskModel = tasksFromAssignmentArrayList.get(1);
                            tvTaskName.setText(selectedTaskModel.getFullName());
                        }
                    }
                }
            }

            if (isTaskFromAssignment == false) {
                isTasksInTaskList = true;
                isLeavesInTaskList = false;
                tvTaskName.setText("None");
                taskPosition = 0;
            }

        } else {

            if (getOptionsDataFile.getShowLeave() == 1) {
                isLeavesInTaskList = true;
                isTaskFromAssignment = false;
                isTasksInTaskList = false;
                tvTaskName.setText("None");
                taskPosition = 0;
                selectedTaskModel = tasksModelArrayList.get(0);
                selectedLeaveTaskModel = leaveTaskModelArrayList.get(0);
            } else {
                isLeavesInTaskList = false;
                isTaskFromAssignment = false;
                isTasksInTaskList = true;
                tvTaskName.setText("None");
                selectedTaskModel = tasksModelArrayList.get(0);
                selectedLeaveTaskModel = leaveTaskModelArrayList.get(0);
            }
        }

        if (projectPosition == 0) {
            billable.setVisibility(View.GONE);
        } else {
            billable.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void setTask(int position) {
        taskPosition = position;
        if (isTaskFromAssignment == true) {

            for (int i = 0; i < tasksModelArrayList.size(); i++) {
                if (tasksModelArrayList.get(i).getTaskID() == position) {

                    selectedTaskModel = tasksModelArrayList.get(i);
                    tvTaskName.setText(selectedTaskModel.getFullName());
                }
            }
            tasksAdapter.changePosition(selectedTaskModel.getTaskID());

        } else {

            for (int i = 0; i < tasksModelArrayList.size(); i++) {
                if (tasksModelArrayList.get(i).getTaskID() == position) {
                    selectedTaskModel = tasksModelArrayList.get(i);
                    tvTaskName.setText(selectedTaskModel.getFullName());
                    break;
                }
            }
            tasksAdapter.changePosition(selectedTaskModel.getTaskID());
        }
        dialog.dismiss();
        billable.setVisibility(View.VISIBLE);
    }


    @Override
    public void setLeave(int position) {
        taskPosition = position;
        tasksAdapter.changePosition(taskPosition);
        selectedLeaveTaskModel = leaveTaskModelArrayList.get(taskPosition);
        tvTaskName.setText(selectedLeaveTaskModel.getFullName());
        dialog.dismiss();
        billable.setVisibility(View.GONE);
    }

//    @Override
//    public void setClass(int position) {
//        classPosition = position;
//        classesAdapter.changePosition(classPosition);
//        selectedClassesModel = classesModelArrayList.get(classPosition);
//        tvClassName.setText(selectedClassesModel.getFullName());
//        dialog.dismiss();
//    }

    @Override
    public void setClass(int position) {
        classPosition = position;
//        classesAdapter.changePosition(classPosition);
//        selectedClassesModel = classesModelArrayList.get(classPosition);
//        tvClassName.setText(selectedClassesModel.getFullName());
//        dialog.dismiss();


        for (int i = 0; i < classesModelArrayList.size(); i++) {
            if (classesModelArrayList.get(i).getClassRowID() == position) {
                selectedClassesModel = classesModelArrayList.get(i);
                tvClassName.setText(selectedClassesModel.getFullName());
                break;
            }

        }
        classesAdapter.changePosition(selectedClassesModel.getClassRowID());
        dialog.dismiss();

    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


        if(s.length()>0)
        {
            searchIcon.setImageResource(R.drawable.ic_clear_black_24dp);


        }else
        {
            searchIcon.setImageResource(R.drawable.search_icon);

        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        filterList(s.toString());
    }
    private void filterList(String stringToSearch) {
        if (stringToSearch.trim().length() == 0) {
            if (typeOfListInDialog.equalsIgnoreCase("tasks")) {
                tasksAdapter.changeData(tasksModelArrayList);
                return;
            }
            if (typeOfListInDialog.equalsIgnoreCase("projects")) {
                projectsAdapter.changeData(projectsModelArrayList);
                return;
            }
            if (typeOfListInDialog.equalsIgnoreCase("class")) {
                classesAdapter.changeData(classesModelArrayList);
                return;
            }
        }

        if (typeOfListInDialog.equalsIgnoreCase("tasks")) {
            tasksModelArrayListSearch = new ArrayList<>();
            ArrayList<TasksModel> tasksLevel = new ArrayList<>();

            for (int i = 0; i < tasksModelArrayList.size(); i++) {
                TasksModel tasksModel = tasksModelArrayList.get(i);
                tasksModel.setHasAdded(false);
                int subLevel = tasksModel.getSubLevel();
                if (subLevel == 0) {
                    tasksLevel.clear();
                    tasksLevel.add(0, tasksModel);
                }
                if (subLevel == 1) {
                    if (tasksModel.getFullName().contains(tasksLevel.get(subLevel - 1).getFullName())) {
                        tasksLevel.add(subLevel, tasksModel);
                    } else {
                        tasksLevel.clear();
                        String[] taskNames = tasksModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            tasksLevel.add(j, tasksModel);
                        }
                        tasksLevel.add(subLevel, tasksModel);
                    }
                }
                if (subLevel == 2) {
                    if (tasksModel.getFullName().contains(tasksLevel.get(subLevel - 1).getFullName())) {
                        tasksLevel.add(subLevel, tasksModel);
                    } else {
                        String[] taskNames = tasksModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            tasksLevel.add(j, tasksModel);
                        }
                        tasksLevel.add(subLevel, tasksModel);
                    }
                }
                if (subLevel == 3) {
                    if (tasksModel.getFullName().contains(tasksLevel.get(subLevel - 1).getFullName())) {
                        tasksLevel.add(subLevel, tasksModel);
                    } else {
                        String[] taskNames = tasksModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            tasksLevel.add(j, tasksModel);
                        }
                        tasksLevel.add(subLevel, tasksModel);
                    }
                }
                if (subLevel == 4) {
                    if (tasksModel.getFullName().contains(tasksLevel.get(subLevel - 1).getFullName())) {
                        tasksLevel.add(subLevel, tasksModel);
                    } else {
                        String[] taskNames = tasksModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            tasksLevel.add(j, tasksModel);
                        }
                        tasksLevel.add(subLevel, tasksModel);
                    }
                }
                if (subLevel == 5) {
                    if (tasksModel.getFullName().contains(tasksLevel.get(subLevel - 1).getFullName())) {
                        tasksLevel.add(subLevel, tasksModel);
                    } else {
                        String[] taskNames = tasksModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            tasksLevel.add(j, tasksModel);
                        }
                        tasksLevel.add(subLevel, tasksModel);
                    }
                }

                if (tasksModelArrayList.get(i).getTaskName().toLowerCase().contains(stringToSearch.toLowerCase())) {
                    int subLevelTemp = tasksModel.getSubLevel();
                    for (int j = 0; j <= subLevelTemp - 1; j++) {
                        if (tasksLevel.get(j).isHasAdded() == false) {
                            tasksModelArrayListSearch.add(tasksLevel.get(j));
                            tasksLevel.get(j).setHasAdded(true);
                        }
                    }
                    tasksModelArrayListSearch.add(tasksModel);
                    tasksModel.setHasAdded(true);
                    tasksLevel.add(subLevel, tasksModel);
                }
            }

            tasksAdapter.changeData(tasksModelArrayListSearch);
        }

        if (typeOfListInDialog.equalsIgnoreCase("projects")) {
            projectsModelArrayListSearch = new ArrayList<>();
            ArrayList<ProjectsModel> projectLevel = new ArrayList<>();

            for (int i = 0; i < projectsModelArrayList.size(); i++) {
                ProjectsModel projectsModel = projectsModelArrayList.get(i);
                projectsModel.setHasAdded(false);
                int subLevel = projectsModel.getSubLevel();
                if (subLevel == 0) {
                    projectLevel.clear();
                    projectLevel.add(0, projectsModel);
                }
                if (subLevel == 1) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        projectLevel.clear();
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 2) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.set(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 3) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 4) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 5) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }

                if (projectsModelArrayList.get(i).getProjectName().toLowerCase().contains(stringToSearch.toLowerCase())) {
                    int subLevelTemp = projectsModel.getSubLevel();
                    for (int j = 0; j <= subLevelTemp - 1; j++) {
                        if (projectLevel.get(j).isHasAdded() == false) {
                            projectsModelArrayListSearch.add(projectLevel.get(j));
                            projectLevel.get(j).setHasAdded(true);
                        }
                    }

                    projectsModelArrayListSearch.add(projectsModel);
                    projectsModel.setHasAdded(true);
                    projectLevel.add(subLevel, projectsModel);
                }
            }

            projectsAdapter.changeData(projectsModelArrayListSearch);
        }


        if (typeOfListInDialog.equalsIgnoreCase("class")) {

            classesModelArrayListSearch = new ArrayList<>();
            ArrayList<ClassesModel> classLevel = new ArrayList<>();

            for (int i = 0; i < classesModelArrayList.size(); i++) {
                ClassesModel classesModel = classesModelArrayList.get(i);
                classesModel.setHasAdded(false);
                int subLevel = classesModel.getSubLevel();
                if (subLevel == 0) {
                    classLevel.clear();
                    classLevel.add(0, classesModel);
                }
                if (subLevel == 1) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        classLevel.clear();
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 2) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.set(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 3) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 4) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 5) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }

                if (classesModelArrayList.get(i).getClassName().toLowerCase().contains(stringToSearch.toLowerCase())) {
                    int subLevelTemp = classesModel.getSubLevel();
                    for (int j = 0; j <= subLevelTemp - 1; j++) {
                        if (classLevel.get(j).isHasAdded() == false) {
                            classesModelArrayListSearch.add(classLevel.get(j));
                            classLevel.get(j).setHasAdded(true);
                        }
                    }

                    classesModelArrayListSearch.add(classesModel);
                    classesModel.setHasAdded(true);
                    classLevel.add(subLevel, classesModel);
                }
            }

            classesAdapter.changeData(classesModelArrayListSearch);
        }

    }
}