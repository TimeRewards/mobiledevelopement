package com.timerewards.tr.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.timerewards.tr.Adapters.TimeSheetApproveAdapter;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.ApproveTimesheetDataFile;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.TimeSlotModel;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class ApproveTimesheetActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 12;
    public static boolean isToLoadFromLocal = true;
    TextView tvDate;
    ListView listView;
    TimeSheetApproveAdapter timeSheetApproveAdapter;
    View view;
    Toolbar toolbar;
    EmployeeLogin employeeLogin;
    ApproveTimesheetDataFile approveTimesheetDataFile;
    ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();
    Intent intent;
    ArrayList<TimeSlotModel> timeSlotModelArrayList = new ArrayList<>();
    String rejectReason;
    GetOptionsDataFile getOptionsDataFile;
    long startMillis;
    long endMillis;
    private SimpleDateFormat simpleDateFormatNEW = new SimpleDateFormat("dd-MMM-yyyy");
    private TableObject tableObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isToLoadFromLocal = true;
        setContentView(R.layout.activity_timesheets);
        init();
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Timesheets for Approval");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvDate = (TextView) findViewById(R.id.tv_date);
        listView = (ListView) findViewById(R.id.listView_dates);
        TextView textView = (TextView) findViewById(R.id.tv_add_timehseet);
        textView.setVisibility(View.GONE);
        intent = getIntent();
        startMillis = intent.getLongExtra(Constants.START_MILLIS, 0);
        endMillis = intent.getLongExtra(Constants.END_MILLIS, 0);
        simpleDateFormatNEW.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date = simpleDateFormatNEW.format(new Date(startMillis))
                + " - "
                + simpleDateFormatNEW.format(new Date(endMillis));
        tvDate.setText(date);
        timeSheetApproveAdapter = new TimeSheetApproveAdapter(this);
        listView.setAdapter(timeSheetApproveAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isToLoadFromLocal) {
            new MyAsyncTask().execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_approve_all, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        if (item.getItemId() == R.id.approve_all) {
            if (Utilities.isOnline(this)) {
                acceptRejectTimeSheet(true);
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }
        if (item.getItemId() == R.id.reject_all) {
            if (Utilities.isOnline(this)) {
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.dialog_reject_reason);
                dialog.setTitle("Please Enter Reject Reason");
                final EditText etReason = (EditText) dialog.findViewById(R.id.et_dialog_reject_reason);
                Button buttonReject = (Button) dialog.findViewById(R.id.button_dialog_ok);
                Button buttonCancel = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                dialog.setCancelable(false);
                buttonReject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (etReason.getText().toString().length() > 0) {
                            rejectReason = etReason.getText().toString();
                            acceptRejectTimeSheet(false);
                            dialog.dismiss();
                        } else {
                            etReason.setError("Can't be left Blank");
                        }
                    }
                });
                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }
        return super.onOptionsItemSelected(item);

    }

    private void acceptRejectTimeSheet(final boolean accept) {

        String IDs = "";

        for (int i = 0; i < timesheetArrayList.size(); i++) {
            IDs = IDs + String.valueOf(timesheetArrayList.get(i).getTimeActivityID());
            if (i < timesheetArrayList.size() - 1) {
                IDs = IDs + ",";
            }
        }
        Log.i("arsh", IDs);
        String url = "";
        url = Constants.ACCEPT_REJECT_TIMESHEET_API;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("TimeActivityID", IDs);
            jsonObject.put("ApproveOrReject", accept);
            jsonObject.put("AppSignature", employeeLogin.getApproverSignature());
            if (accept == false) {
                jsonObject.put("AdminNotes", rejectReason);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("arsh", "url=" + url);
        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                ShowDialog.hideDialog();
                ApproveTimesheetLayer.isToLoadFromLocal = true;
                for (int i = timesheetArrayList.size() - 1; i >= 0; i--) {
                    approveTimesheetDataFile.getTimesheetArrayList().remove(timesheetArrayList.get(i).getPositionInList());
                }
                OfflineDataStorage.writeObject(ApproveTimesheetActivity.this, approveTimesheetDataFile, Constants.TIMESHEETS_APPROVE_FILE);
                Log.i("arsh", "response=" + s);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(ApproveTimesheetActivity.this, volleyError);
            }
        });
        ShowDialog.showDialog(ApproveTimesheetActivity.this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void getProperTimesheetsWithPositions() {
        intent = getIntent();
        if (intent.hasExtra(Constants.START_MILLIS) && intent.hasExtra(Constants.END_MILLIS)) {
            //getting all the timesheets in this timeslot
            timesheetArrayList.clear();
            timesheetArrayList = approveTimesheetDataFile.getTimesheetArrayList();
            ArrayList<Timesheet> timesheetArrayList1 = new ArrayList<>();
            for (int i = 0; i < timesheetArrayList.size(); i++) {
                Timesheet timesheet = timesheetArrayList.get(i);
                if (timesheet.getTimeInMillis() >= startMillis && timesheet.getTimeInMillis() <= endMillis) {
                    timesheet.setPositionInList(i);
                    timesheetArrayList1.add(timesheet);
                }
            }
            timesheetArrayList = new ArrayList<>();
            timesheetArrayList.addAll(timesheetArrayList1);
            timesheetArrayList1.clear();
            //if employee name is clicked
            if (intent.hasExtra(Constants.EMPLOYEE_NAME)) {
                String empName = intent.getStringExtra(Constants.EMPLOYEE_NAME);
                for (int i = 0; i < timesheetArrayList.size(); i++) {
                    Timesheet timesheet = timesheetArrayList.get(i);
                    if (empName.equalsIgnoreCase(timesheet.getEmployeeFullName())) {
                        timesheetArrayList1.add(timesheet);
                    }
                }
                timesheetArrayList = new ArrayList<>();
                timesheetArrayList.addAll(timesheetArrayList1);
                timesheetArrayList1.clear();
                //if project or task is selected
                if (intent.hasExtra(Constants.PROJECT_OR_TASK)) {
                    //checking if project is selected
                    if (intent.getStringExtra(Constants.PROJECT_OR_TASK).equalsIgnoreCase("project")) {
                        String projName = intent.getStringExtra(Constants.PROJECT_OR_TASK_NAME);
                        for (int i = 0; i < timesheetArrayList.size(); i++) {
                            if (projName.equalsIgnoreCase("timeoff")) {
                                //in case if timeoff selected
                                if (timesheetArrayList.get(i).isTime() == false) {
                                    Timesheet timesheet = timesheetArrayList.get(i);
                                    timesheetArrayList1.add(timesheet);
                                }
                            } else {
                                Timesheet timesheet = timesheetArrayList.get(i);
                                if (projName.equalsIgnoreCase(timesheet.getProjectFullName())) {
                                    timesheetArrayList1.add(timesheet);
                                }
                            }
                        }
                    }
                    //checking if task is selected
                    if (intent.getStringExtra(Constants.PROJECT_OR_TASK).equalsIgnoreCase("task")) {
                        String taskName = intent.getStringExtra(Constants.PROJECT_OR_TASK_NAME);
                        for (int i = 0; i < timesheetArrayList.size(); i++) {
                            if (taskName.equalsIgnoreCase("timeoff")) {
                                //in case if timeoff selected
                                if (timesheetArrayList.get(i).isTime() == false) {
                                    Timesheet timesheet = timesheetArrayList.get(i);
                                    timesheetArrayList1.add(timesheet);
                                }
                            } else {
                                Timesheet timesheet = timesheetArrayList.get(i);
                                if (taskName.equalsIgnoreCase(timesheet.getTaskFullName())) {
                                    timesheetArrayList1.add(timesheet);
                                }
                            }
                        }
                    }
                    timesheetArrayList = new ArrayList<>();
                    timesheetArrayList.addAll(timesheetArrayList1);
                    timesheetArrayList1.clear();
                }
            }
        }
    }

    class MyAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            ShowDialog.showDialog(ApproveTimesheetActivity.this);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(getBaseContext(), Constants.EMPLOYEE_LOGIN_FILE);
            tableObject = (TableObject) OfflineDataStorage.readObject(ApproveTimesheetActivity.this, Constants.TIMESHEETS_FILE);
            getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(ApproveTimesheetActivity.this, Constants.GETOPTIONS_FILE);
            approveTimesheetDataFile = (ApproveTimesheetDataFile) OfflineDataStorage.readObject(ApproveTimesheetActivity.this, Constants.TIMESHEETS_APPROVE_FILE);
            timeSlotModelArrayList = approveTimesheetDataFile.getTimeSlotModelArrayList();
            getProperTimesheetsWithPositions();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowDialog.hideDialog();
            isToLoadFromLocal = false;
            timeSheetApproveAdapter.changeData(timesheetArrayList);
        }
    }
}
