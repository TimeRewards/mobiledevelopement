package com.timerewards.tr.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.timerewards.tr.Adapters.TimerAdapter;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.Controller.TimerService;
import com.timerewards.tr.CustomViews.DividerItemDecoration;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.TimerDataFile;
import com.timerewards.tr.DataFiles.TimerModel;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.Utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TimerActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private TimerAdapter timerAdapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private ArrayList<TimerModel> timerModelArrayList = new ArrayList<>();
    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;
    private Intent serviceIntent;
    private Button btnAddTimer;
    private long time = 0;
    private EmployeeLogin employeeLogin;
    private GetOptionsDataFile getOptionsDataFile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(this, Constants.GETOPTIONS_FILE);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(this, Constants.EMPLOYEE_LOGIN_FILE);
        setContentView(R.layout.activity_timer);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e("arsh", "onReceive TimerActivity");
                updateTimers();
                time++;
                if (time == 10) {
                    time = 0;
                    saveOffline();
                }
            }
        };
        intentFilter = new IntentFilter("TimerService");
        registerReceiver(broadcastReceiver, intentFilter);
        init();
        updateTimers();
        serviceIntent = new Intent(this, TimerService.class);
        if (TimerService.isRunning == false) {
            TimerService.isRunning = true;
            startService(serviceIntent);
        }
    }

    private void saveOffline() {
        ArrayList<TimerModel> timerModelArrayList1 = new ArrayList<>();
        for (TimerModel timerModel : timerModelArrayList) {
            timerModelArrayList1.add(timerModel);
        }
        TimerDataFile timerDataFile = new TimerDataFile();
        timerDataFile.setTimerModelArrayList(timerModelArrayList1);
        OfflineDataStorage.writeObject(this, timerDataFile, Constants.TIMER_DATA_FILE);
        TimerDataFile timerDataFile1 = (TimerDataFile) OfflineDataStorage.readObject(this, Constants.TIMER_DATA_FILE);
        Log.e("Size is : ", String.valueOf(timerDataFile1.getTimerModelArrayList().size()));
    }

    private void pauseAllNsave() {
        for (int i = 0; i < TimerService.timerModelArrayList.size(); i++) {
            TimerService.timerModelArrayList.get(i).setRunning(false);
        }
        saveOffline();
        updateTimers();
        TableObject tableObject = (TableObject) OfflineDataStorage.readObject(this, Constants.TIMESHEETS_FILE);
        for (int i = 0; i < TimerService.timerModelArrayList.size(); i++) {
            Timesheet timesheet = null;
            if (TimerService.timerModelArrayList.get(i).getTimesheet() == null) {
                //save empty timesheet
                timesheet = new Timesheet();
                timesheet.setEZStatusID(0);
                timesheet.set$id("new");
                timesheet.setEmployeeFullName(employeeLogin.getDisplayName());
                timesheet.setEmployeeID(employeeLogin.getUserid());
                timesheet.setTimeActivityID(0);
                timesheet.setTaskID(0);
                timesheet.setProjectID(0);
                timesheet.setClassRowID(0);
                timesheet.setDescription("");
                timesheet.setProjectFullName("None");
                timesheet.setTaskFullName("None");
                timesheet.setClassFullName("None");
                switch (getOptionsDataFile.getShowBillable()) {
                    case 0:
                        timesheet.setBillable(false);
                        break;
                    case 1:
                        timesheet.setBillable(false);
                        break;
                    case 2:
                        timesheet.setBillable(true);
                        break;
                    case 3:
                        timesheet.setBillable(true);
                        break;
                }
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat simpleDateFormatOLD = new SimpleDateFormat("dd-MMM-yyyy");
                timesheet.setActivityDate(Utilities.dateToServerFormat(simpleDateFormatOLD.format(new Date(calendar.getTimeInMillis()))));


//                if (TimerService.timerModelArrayList.get(i).getMillis() < 1 * 60 * 60 * 1000) {
//                    timesheet.setActualHours(((float) getOptionsDataFile.getTimerInterval()) / 60);
//                } else {
//                    timesheet.setActualHours(TimerService.timerModelArrayList.get(i).getMillis() / (1000 * 60 * 60));
//                }

//
//
//                long valueincurrentmuli=((long)(finalValue*1000l*60l*60l));
//                Log.e("end time",TimerService.timerModelArrayList.get(i).getStartTime()+valueincurrentmuli+"");
//                Date end=new Date(TimerService.timerModelArrayList.get(i).getStartTime()+valueincurrentmuli);
//                timesheet.setEndTime(new SimpleDateFormat("HH:mm").format(end));

            } else {
                //save timesheet
                timesheet = TimerService.timerModelArrayList.get(i).getTimesheet();

            }
            long gettimefromtimer=TimerService.timerModelArrayList.get(i).getMillis();
            if (gettimefromtimer == 0){
                gettimefromtimer = 1000;
            }

            int timeInterval = getOptionsDataFile.getTimerInterval();
            double timerRan = Double.valueOf(gettimefromtimer/1000);
            double x = timerRan;
            double y = x/60;
            double z = y/timeInterval;
            double a= Math.ceil(z);
            double hours = (timeInterval*a)/60;
            String rounded_hours = String.format("%.2f", hours);
            String textToShow = "Timer ran for " + x + " seconds.\n";
            textToShow += "Timer ran for " + y + " minutes.\n";
            textToShow += "It is " + z + " times of timer value.\n";
            textToShow += "but we have to consider " + a + " times of timer value.\n";
            textToShow += "hours to count is " + hours + ".\n";
            textToShow += "after rounding off hours are " + rounded_hours + ".\n";
            for (int l = 0; l < 4; l++) {
                Toast.makeText(this, textToShow, Toast.LENGTH_LONG).show();
            }

//                Double d = Double.valueOf(gettimefromtimer);
//                double finalValue=0.0;
//                d=d/(1000*60);
//                if(d<getOptionsDataFile.getTimerInterval())
//                {
//                    finalValue= ((double) getOptionsDataFile.getTimerInterval()) / 60;
//                    timesheet.setActualHours(((float) getOptionsDataFile.getTimerInterval()) / 60);
//                }else
//                {
//                    Double ceilingvalue=Math.ceil(d/getOptionsDataFile.getTimerInterval());
//                    timesheet.setActualHours((float)(ceilingvalue*getOptionsDataFile.getTimerInterval())/60);
//                    finalValue=(double)(ceilingvalue*getOptionsDataFile.getTimerInterval())/60;
//                }

            timesheet.setActualHours((float) hours);

            Date startDate = new Date(TimerService.timerModelArrayList.get(i).getStartTime());
            timesheet.setStartTime(new SimpleDateFormat("HH:mm").format(startDate));
            Calendar calendar1  = Calendar.getInstance();
            calendar1.setTimeInMillis((long) (startDate.getTime() + hours*60*60*1000));
            Date endDate = new Date(calendar1.getTimeInMillis());
            timesheet.setEndTime(new SimpleDateFormat("HH:mm").format(endDate));
            //saving timesheet
            tableObject.getTimesheetList().add(timesheet);
            timerAdapter.notifyDataSetChanged();
        }
        TimerService.timerModelArrayList.clear();
        OfflineDataStorage.writeObject(this, tableObject, Constants.TIMESHEETS_FILE);
        Utilities.showImportantToast(this, "All Timers Saved.");
    }

    private void init() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Timers");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        timerAdapter = new TimerAdapter(this, timerModelArrayList);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable drawable = getResources().getDrawable(R.drawable.divider);
        dividerItemDecoration = new DividerItemDecoration(this, drawable, 16);
        recyclerView.setAdapter(timerAdapter);
        timerAdapter.changeData(timerModelArrayList);
        btnAddTimer = (Button) findViewById(R.id.button_add_timer);
        btnAddTimer.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        if (item.getItemId() == R.id.action_save_all) {
            pauseAllNsave();
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateTimers() {
        timerModelArrayList = TimerService.timerModelArrayList;
        timerAdapter.changeData(timerModelArrayList);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_add_timer:
                TimerService.addTimer();
                timerAdapter.notifyDataSetChanged();
                updateTimers();
                recyclerView.scrollToPosition(timerModelArrayList.size() - 1);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_timers, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
