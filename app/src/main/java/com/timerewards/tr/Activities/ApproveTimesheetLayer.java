package com.timerewards.tr.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Adapters.TimeSheetProjApproveLayerAdapter;
import com.timerewards.tr.Adapters.TimeSheetTasksApproveLayerAdapter;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.ApproveTimesheetDataFile;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.ProjectsInTimeSlotModel;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.TasksInTimeSlotModel;
import com.timerewards.tr.DataFiles.TimeSlotModel;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class ApproveTimesheetLayer extends AppCompatActivity implements View.OnClickListener, TimeSheetProjApproveLayerAdapter.Refresh {

    public static boolean isToLoadFromServer = true;
    public static boolean isToLoadFromLocal = false;
    private TextView tvDate;
    private ExpandableListView listViewProjects;
    private ExpandableListView listViewTasks;
    private TimeSheetProjApproveLayerAdapter projApproveLayerAdapter;
    private TimeSheetTasksApproveLayerAdapter tasksApproveLayerAdapter;
    private View view;
    private Toolbar toolbar;
    private EmployeeLogin employeeLogin;
    private ApproveTimesheetDataFile approveTimesheetDataFile;
    private int numOfRequests = 0;
    private TableObject tableObject;
    private Intent intent;
    private ArrayList<Timesheet> timeSheetApproveList = new ArrayList<>();
    private String rejectReason;
    private GetOptionsDataFile getOptionsDataFile;
    private Button dialogOkButton, dialogCancelButton;
    private EditText etApproverSignature;
    private TextView textReason;
    private long maxTimeMillis;
    private Calendar calendarStart;
    private Calendar calendarEnd;
    private ArrayList<TimeSlotModel> timeSlotModelArrayList = new ArrayList<>();
    private ArrayList<TimeSlotModel> timeSlotModelTasksArrayList = new ArrayList<>();
    private int daysToAdd = 0;
    private boolean toRefresh;
    private TextView tabProjects, tabTasks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approve_layer_timesheets);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(getBaseContext(), Constants.EMPLOYEE_LOGIN_FILE);
        tableObject = (TableObject) OfflineDataStorage.readObject(this, Constants.TIMESHEETS_FILE);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(this, Constants.GETOPTIONS_FILE);
        if (getOptionsDataFile.isAppSigRequiredforTS()) {
            if (employeeLogin.getApproverSignature() != null && employeeLogin.getApproverSignature().length() > 0) {

            } else {
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.dialog_set_emp_signature);
                dialog.setTitle("Set Your Signature");
                textReason = (TextView) dialog.findViewById(R.id.text_reason);
                etApproverSignature = (EditText) dialog.findViewById(R.id.et_dialog_approver_signature);
                dialogOkButton = (Button) dialog.findViewById(R.id.button_dialog_ok);
                dialogCancelButton = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                dialog.setCancelable(false);
                dialogOkButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (etApproverSignature.getText().toString().length() > 0) {
                            employeeLogin.setApproverSignature(etApproverSignature.getText().toString());
                            OfflineDataStorage.writeObject(ApproveTimesheetLayer.this, employeeLogin, Constants.EMPLOYEE_LOGIN_FILE);
                            dialog.dismiss();
                        } else {
                            etApproverSignature.setError("Can't be left blank!");
                        }
                    }
                });

                dialogCancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

                dialog.show();
            }
        }
        init();
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Timesheets for Approval");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        tvDate.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isToLoadFromServer) {
            approveTimesheetDataFile = new ApproveTimesheetDataFile();
            syncTimesheetApproveList();
        } else {
//            timeSlotModelArrayList = new ArrayList<>();
//            projApproveLayerAdapter.changeData(timeSlotModelArrayList);
//            tasksApproveLayerAdapter.changeData(timeSlotModelArrayList);
//            timeSlotModelArrayList = new ArrayList<>();
            if (isToLoadFromLocal) {
                new MyTask().execute();
            }
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvDate = (TextView) findViewById(R.id.tv_date);
        listViewProjects = (ExpandableListView) findViewById(R.id.listView_projects);
        listViewTasks = (ExpandableListView) findViewById(R.id.listView_tasks);
        listViewTasks.setVisibility(View.GONE);
        TextView textView = (TextView) findViewById(R.id.tv_add_timehseet);
        textView.setVisibility(View.GONE);
        projApproveLayerAdapter = new TimeSheetProjApproveLayerAdapter(this, timeSlotModelArrayList);
        tasksApproveLayerAdapter = new TimeSheetTasksApproveLayerAdapter(this, timeSlotModelArrayList);
        listViewProjects.setAdapter(projApproveLayerAdapter);
        listViewTasks.setAdapter(tasksApproveLayerAdapter);
        tabProjects = (TextView) findViewById(R.id.tv_project);
        tabProjects.setText("By " + getOptionsDataFile.getProjectLex());
        tabProjects.setOnClickListener(this);
        tabTasks = (TextView) findViewById(R.id.tv_task);
        tabTasks.setText("By " + getOptionsDataFile.getTaskLex());
        tabTasks.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_approve_reject_all, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        if (item.getItemId() == R.id.approve_all) {
            if (Utilities.isOnline(this)) {
                acceptRejectTimeSheet(true);
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }
        if (item.getItemId() == R.id.reject_all) {
            if (Utilities.isOnline(this)) {
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.dialog_reject_reason);
                dialog.setTitle("Please Enter Reject Reason");
                final EditText etReason = (EditText) dialog.findViewById(R.id.et_dialog_reject_reason);
                Button buttonReject = (Button) dialog.findViewById(R.id.button_dialog_ok);
                Button buttonCancel = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                dialog.setCancelable(false);
                buttonReject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (etReason.getText().toString().length() > 0) {
                            rejectReason = etReason.getText().toString();
                            acceptRejectTimeSheet(false);
                            dialog.dismiss();
                        } else {
                            etReason.setError("Can't be left Blank");
                        }
                    }
                });
                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }
        if (item.getItemId() == R.id.refresh) {
            if (Utilities.isOnline(this)) {
                syncTimesheetApproveList();
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }
        return super.onOptionsItemSelected(item);

    }

    private void syncTimesheetApproveList() {

        String url = Constants.TIMESHEETS_TO_APPROVE_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utilities.showErrorLog(url);

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        timeSheetApproveList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(s.toString().toString(), Timesheet[].class)));
                        ShowDialog.hideDialog();
                        Toast.makeText(ApproveTimesheetLayer.this, "Timesheets to approve (" + timeSheetApproveList.size() + ")", Toast.LENGTH_SHORT).show();
                        new MyTask().execute(s);
                        isToLoadFromServer = false;
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(ApproveTimesheetLayer.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShowDialog.showDialog(ApproveTimesheetLayer.this);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void generateTimeMillis(ArrayList<Timesheet> timeSheetApproveList) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        for (int i = 0; i < timeSheetApproveList.size(); i++) {
            try {
                Long time = simpleDateFormat.parse(timeSheetApproveList.get(i).getActivityDate()).getTime();
                timeSheetApproveList.get(i).setTimeInMillis(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_project:
                selectTab(1);
                break;
            case R.id.tv_task:
                selectTab(2);
                break;
        }
    }

    private void selectTab(int selectedTab) {

        if (selectedTab == 1) {
            //projects tab selected
            listViewProjects.setVisibility(View.VISIBLE);
            listViewTasks.setVisibility(View.GONE);
            tabProjects.setTypeface(null, Typeface.BOLD);
            tabTasks.setTypeface(null, Typeface.NORMAL);
            tabProjects.setTextColor(getResources().getColor(R.color.white_color));
            tabTasks.setTextColor(getResources().getColor(R.color.black_color));
            tabProjects.setBackgroundColor(getResources().getColor(R.color.second_color));
            tabTasks.setBackgroundColor(getResources().getColor(R.color.white_color));
        }
        if (selectedTab == 2) {
            //tasks tab selected
            listViewTasks.setVisibility(View.VISIBLE);
            listViewProjects.setVisibility(View.GONE);
            tabTasks.setTypeface(null, Typeface.BOLD);
            tabProjects.setTextColor(getResources().getColor(R.color.black_color));
            tabTasks.setTextColor(getResources().getColor(R.color.white_color));
            tabTasks.setBackgroundColor(getResources().getColor(R.color.second_color));
            tabProjects.setBackgroundColor(getResources().getColor(R.color.white_color));
            tabProjects.setTypeface(null, Typeface.NORMAL);
        }
    }

    @Override
    public void refresh() {
        onResume();
    }

    @Override
    public void setTimesheetList(ArrayList<Timesheet> arrayListTimesheet) {
        approveTimesheetDataFile.setTimesheetArrayList(arrayListTimesheet);
        new SaveOffline().execute();
    }

    @Override
    public ArrayList<Timesheet> getTimeSheetList() {
        approveTimesheetDataFile = (ApproveTimesheetDataFile) OfflineDataStorage.readObject(this, Constants.TIMESHEETS_APPROVE_FILE);
        return approveTimesheetDataFile.getTimesheetArrayList();
    }

    private void generateDataForLayer() {

        SimpleDateFormat sdfForTimeSlot = new SimpleDateFormat("dd-MMM-yyyy");
        sdfForTimeSlot.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (timeSheetApproveList.size() == 0) {
            return;
        }
        getOptionsDataFile.setTimeSheetFrequency(1);
        if (getOptionsDataFile.getTimeSheetFrequency() == 1) {
            daysToAdd = 7;  //weekly
            //setting end date
            calendarEnd = Calendar.getInstance();
            calendarEnd.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendarEnd.setTimeInMillis(timeSheetApproveList.get(0).getTimeInMillis());
            int todaysPositionInWeek = calendarEnd.get(Calendar.DAY_OF_WEEK);
            int lastDayOfWeek = (getOptionsDataFile.getLastDayOfWeek() % 7);
            if (todaysPositionInWeek == lastDayOfWeek) {

            }
            if (todaysPositionInWeek < lastDayOfWeek) {
                int days = lastDayOfWeek - todaysPositionInWeek;
                calendarEnd.add(Calendar.DATE, days);
            }
            if (todaysPositionInWeek > lastDayOfWeek) {
                int days = todaysPositionInWeek - lastDayOfWeek;
                calendarEnd.add(Calendar.DATE, 7 - days);
            }

            calendarEnd.get(Calendar.DAY_OF_MONTH);
            calendarEnd.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendarEnd.add(Calendar.DATE, 1);
            calendarEnd.set(Calendar.MINUTE, 0);
            calendarEnd.set(Calendar.HOUR, 0);
            calendarEnd.set(Calendar.SECOND, 0);
            calendarEnd.set(Calendar.AM_PM, 0);
            calendarEnd.setTimeInMillis(calendarEnd.getTimeInMillis() - 1);

            //setting start date
            calendarStart = Calendar.getInstance();
            calendarStart.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendarStart.setTimeInMillis(calendarEnd.getTimeInMillis());
            calendarStart.add(Calendar.DATE, -daysToAdd + 1);
            calendarStart.set(Calendar.MINUTE, 0);
            calendarStart.set(Calendar.HOUR, 0);
            calendarStart.set(Calendar.SECOND, 0);
            calendarStart.set(Calendar.MILLISECOND, 0);
            calendarStart.set(Calendar.AM_PM, 0);
        }
        if (getOptionsDataFile.getTimeSheetFrequency() == 4) {
            daysToAdd = 14;  //bi-weekly
            //setting end date
            Date biWeeklyEndDate = null;
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                biWeeklyEndDate = simpleDateFormat.parse(getOptionsDataFile.getBiWeeklyEndDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar biWeeklyEndDay = Calendar.getInstance();
            biWeeklyEndDay.setTime(biWeeklyEndDate);

            long millis = timeSheetApproveList.get(0).getTimeInMillis() - biWeeklyEndDate.getTime();
//            long millis = biWeeklyEndDate.getTime() - timeSheetApproveList.get(0).getTimeInMillis();
            int days = Math.abs((int) TimeUnit.DAYS.convert(millis, TimeUnit.MILLISECONDS) % 14);
            calendarEnd = Calendar.getInstance();
            calendarEnd.setTimeInMillis(timeSheetApproveList.get(0).getTimeInMillis());
            calendarEnd.add(Calendar.DATE, (14 - days));

            calendarEnd.get(Calendar.DAY_OF_MONTH);
            calendarEnd.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendarEnd.add(Calendar.DATE, 1);
            calendarEnd.set(Calendar.MINUTE, 0);
            calendarEnd.set(Calendar.HOUR, 0);
            calendarEnd.set(Calendar.SECOND, 0);
            calendarEnd.set(Calendar.AM_PM, 0);
            calendarEnd.setTimeInMillis(calendarEnd.getTimeInMillis() - 1);

            //setting start date
            calendarStart = Calendar.getInstance();
            calendarStart.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendarStart.setTimeInMillis(calendarEnd.getTimeInMillis());
            calendarStart.add(Calendar.DATE, -daysToAdd + 1);
            calendarStart.set(Calendar.MINUTE, 0);
            calendarStart.set(Calendar.HOUR, 0);
            calendarStart.set(Calendar.SECOND, 0);
            calendarStart.set(Calendar.MILLISECOND, 0);
            calendarStart.set(Calendar.AM_PM, 0);
        }

        if (getOptionsDataFile.getTimeSheetFrequency() == 2) {
            calendarEnd = Calendar.getInstance();
            calendarEnd.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendarEnd.setTimeInMillis(timeSheetApproveList.get(0).getTimeInMillis());
            int positionInMonth = calendarEnd.get(Calendar.DAY_OF_MONTH);
            int size = 0;
            if (positionInMonth <= 15) {
                size = 15 - positionInMonth;
            } else {
                size = calendarEnd.getActualMaximum(Calendar.DAY_OF_MONTH) - positionInMonth;
            }
            calendarEnd.set(Calendar.MINUTE, 0);
            calendarEnd.set(Calendar.HOUR, 0);
            calendarEnd.set(Calendar.SECOND, 0);
            calendarEnd.set(Calendar.MILLISECOND, 0);
            calendarEnd.set(Calendar.AM_PM, 0);
            calendarEnd.add(Calendar.DATE, size + 1);
            calendarEnd.setTimeInMillis(calendarEnd.getTimeInMillis() - 1);

            calendarStart = Calendar.getInstance();
            calendarStart.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendarStart.setTimeInMillis(calendarEnd.getTimeInMillis());
            int temp;
            if (calendarEnd.get(Calendar.DAY_OF_MONTH) >= 15) {
                temp = calendarStart.getActualMaximum(Calendar.DAY_OF_MONTH) - 16;
            } else {
                temp = 14;
            }
            calendarStart.add(Calendar.DATE, -temp);
            calendarStart.set(Calendar.MINUTE, 0);
            calendarStart.set(Calendar.HOUR, 0);
            calendarStart.set(Calendar.SECOND, 0);
            calendarStart.set(Calendar.MILLISECOND, 0);
            calendarStart.set(Calendar.AM_PM, 0);
        }

        Utilities.showErrorLog("start : " + String.valueOf(calendarStart.getTimeInMillis()));
        Utilities.showErrorLog("end : " + String.valueOf(calendarEnd.getTimeInMillis()));

        TimeSlotModel timeSlotModel = new TimeSlotModel();
        timeSlotModel.setToShowPeriod(true);
        //iterate on the list of timesheets
        for (int i = 0; i < timeSheetApproveList.size(); i++) {
            Timesheet timesheet = timeSheetApproveList.get(i);
            //to check if timesheet at position i comes in that slot...
            boolean toProceed = false;
            while (toProceed == false) {
                if (timesheet.getTimeInMillis() >= calendarStart.getTimeInMillis()
                        && timesheet.getTimeInMillis() <= calendarEnd.getTimeInMillis()) {
                    toProceed = true;
                    timeSlotModel.getTimesheetArrayList().add(timesheet);
                } else {
                    //adding timeslot to arrayList if size > 0
                    if (timeSlotModel.getTimesheetArrayList().size() > 0) {
                        timeSlotModel.setStartMillis(calendarStart.getTimeInMillis());
                        timeSlotModel.setEndMillis(calendarEnd.getTimeInMillis());
                        timeSlotModel.setStartDate(sdfForTimeSlot.format(new Date(calendarStart.getTimeInMillis())));
                        timeSlotModel.setEndDate(sdfForTimeSlot.format(new Date(calendarEnd.getTimeInMillis())));
                        Utilities.showErrorLog("start : " + timeSlotModel.getStartDate());
                        Utilities.showErrorLog("end   : " + timeSlotModel.getEndDate());
                        Utilities.showErrorLog("count : " + timeSlotModel.getTimesheetArrayList().size());

                        //counting total hours in the slot
                        double hours = 0.0;
                        for (int k = 0; k < timeSlotModel.getTimesheetArrayList().size(); k++) {
                            hours = hours + timeSlotModel.getTimesheetArrayList().get(k).getActualHours();
                        }
                        timeSlotModel.setToShowPeriod(true);
                        timeSlotModel.setCompleteTimeSlotHours(hours);
                        timeSlotModelArrayList.add(timeSlotModel);
                        timeSlotModel = new TimeSlotModel();
                    }
                    moveToNextSlot();
                }
            }
        }
        if (timeSlotModel.getTimesheetArrayList().size() > 0) {
            timeSlotModel.setStartMillis(calendarStart.getTimeInMillis());
            timeSlotModel.setEndMillis(calendarEnd.getTimeInMillis());
            timeSlotModel.setStartDate(sdfForTimeSlot.format(new Date(calendarStart.getTimeInMillis())));
            timeSlotModel.setEndDate(sdfForTimeSlot.format(new Date(calendarEnd.getTimeInMillis())));
            Utilities.showErrorLog("start : " + timeSlotModel.getStartDate());
            Utilities.showErrorLog("end   : " + timeSlotModel.getEndDate());
            Utilities.showErrorLog("count : " + timeSlotModel.getTimesheetArrayList().size());

            //counting total hours in the slot
            double hours = 0.0;
            for (int k = 0; k < timeSlotModel.getTimesheetArrayList().size(); k++) {
                hours = hours + timeSlotModel.getTimesheetArrayList().get(k).getActualHours();
            }
            timeSlotModel.setToShowPeriod(true);
            timeSlotModel.setCompleteTimeSlotHours(hours);

            timeSlotModelArrayList.add(timeSlotModel);
        }

        generateDifferentListsForEmployees();

    }

    private void generateDifferentListsForEmployees() {
        ArrayList<TimeSlotModel> timeSlotModelArrayListFinal = new ArrayList<>();
        for (int i = 0; i < timeSlotModelArrayList.size(); i++) {
            TimeSlotModel timeSlotModel = timeSlotModelArrayList.get(i);
            HashMap<String, TimeSlotModel> hashMap = new HashMap<>();
            for (int j = 0; j < timeSlotModel.getTimesheetArrayList().size(); j++) {
                Timesheet timesheet = timeSlotModel.getTimesheetArrayList().get(j);
                String empName = timesheet.getEmployeeFullName();
                if (hashMap.containsKey(empName)) {
                    hashMap.get(empName).getTimesheetArrayList().add(timesheet);
                } else {
                    ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();
                    timesheetArrayList.add(timesheet);
                    TimeSlotModel tsm = new TimeSlotModel();
                    tsm.setEmpName(empName);
                    if (j == 0) {
//                        tsm.setToShowPeriod(true);
                    }
                    tsm.setCompleteTimeSlotHours(timeSlotModel.getCompleteTimeSlotHours());
                    tsm.setStartMillis(timeSlotModel.getStartMillis());
                    tsm.setEndMillis(timeSlotModel.getEndMillis());
                    tsm.setStartDate(timeSlotModel.getStartDate());
                    tsm.setEndDate(timeSlotModel.getEndDate());
                    tsm.setTimesheetArrayList(timesheetArrayList);
                    hashMap.put(timesheet.getEmployeeFullName(), tsm);
                }
            }

//            ArrayList<TimeSlotModel> timeSlotModelArrayListNew = new ArrayList<>();
            Iterator it = hashMap.entrySet().iterator();
            ArrayList<String> names = new ArrayList<>();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                names.add((String) entry.getKey());
            }
            Collections.sort(names);
            for (int j = 0; j < names.size(); j++) {
                if (j == 0) {
                    hashMap.get(names.get(j)).setToShowPeriod(true);
                } else {
                    hashMap.get(names.get(j)).setToShowPeriod(false);
                }
                timeSlotModelArrayListFinal.add(hashMap.get(names.get(j)));
            }

        }
        timeSlotModelArrayList = timeSlotModelArrayListFinal;
        timeSlotModelTasksArrayList.addAll(timeSlotModelArrayList);

        Log.e("arsh", "different timeslots generated");
        generateProjectNamesAndAmounts();
        generateTaskNamesAndAmounts();
    }

    private void generateProjectNamesAndAmounts() {
        for (int i = 0; i < timeSlotModelArrayList.size(); i++) {
            HashMap<String, ProjectsInTimeSlotModel> hashMap = new HashMap();
            TimeSlotModel timeSlotModel = timeSlotModelArrayList.get(i);
            for (int j = 0; j < timeSlotModel.getTimesheetArrayList().size(); j++) {
                Timesheet timesheet = timeSlotModel.getTimesheetArrayList().get(j);
                String projName;
                if (timesheet.isTime()) {
                    projName = timesheet.getProjectFullName();
                } else {
                    projName = "TimeOff";
                }
                if (hashMap.containsKey(projName)) {
                    double amountPrevious = hashMap.get(projName).getHours();
                    double amountToAdd = timesheet.getActualHours();
                    hashMap.get(projName).setHours(amountPrevious + amountToAdd);
                    hashMap.get(projName).getTimesheetArrayList().add(timesheet);
                } else {
                    ProjectsInTimeSlotModel projectsInTimeSlotModel = new ProjectsInTimeSlotModel();
                    projectsInTimeSlotModel.setProjectName(projName);
                    projectsInTimeSlotModel.setHours(timesheet.getActualHours());
                    hashMap.put(projName, projectsInTimeSlotModel);
                    hashMap.get(projName).getTimesheetArrayList().add(timesheet);
                }
            }

            Iterator it = hashMap.entrySet().iterator();
            ArrayList<String> names = new ArrayList<>();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                names.add((String) entry.getKey());
            }
            Collections.sort(names);
            for (int j = 0; j < names.size(); j++) {
                timeSlotModel.getProjectsList().add(hashMap.get(names.get(j)));
            }

            double hours = 0.0;
            for (int j = 0; j < timeSlotModel.getProjectsList().size(); j++) {
                hours = hours + timeSlotModel.getProjectsList().get(j).getHours();
            }
            timeSlotModel.setHours(hours);
        }

        Log.e("arsh", "differentiating based on projects");

    }

    private void generateTaskNamesAndAmounts() {
        for (int i = 0; i < timeSlotModelArrayList.size(); i++) {
            HashMap<String, TasksInTimeSlotModel> hashMap = new HashMap();
            TimeSlotModel timeSlotModel = timeSlotModelArrayList.get(i);
            for (int j = 0; j < timeSlotModel.getTimesheetArrayList().size(); j++) {
                Timesheet timesheet = timeSlotModel.getTimesheetArrayList().get(j);
                String taskName;
                if (timesheet.isTime()) {
                    taskName = timesheet.getTaskFullName();
                } else {
                    taskName = "TimeOff";
                }
                if (hashMap.containsKey(taskName)) {
                    double amountPrevious = hashMap.get(taskName).getHours();
                    double amountToAdd = timesheet.getActualHours();
                    hashMap.get(taskName).setHours(amountPrevious + amountToAdd);
                    hashMap.get(taskName).getTimesheetArrayList().add(timesheet);
                } else {
                    TasksInTimeSlotModel tasksInTimeSlotModel = new TasksInTimeSlotModel();
                    tasksInTimeSlotModel.setTaskName(taskName);
                    tasksInTimeSlotModel.setHours(timesheet.getActualHours());
                    hashMap.put(taskName, tasksInTimeSlotModel);
                    hashMap.get(taskName).getTimesheetArrayList().add(timesheet);
                }
            }

            Iterator it = hashMap.entrySet().iterator();
            ArrayList<String> names = new ArrayList<>();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                names.add((String) entry.getKey());
            }
            Collections.sort(names);
            for (int j = 0; j < names.size(); j++) {
                timeSlotModel.getTasksList().add(hashMap.get(names.get(j)));
            }

            double hours = 0.0;
            for (int j = 0; j < timeSlotModel.getTasksList().size(); j++) {
                hours = hours + timeSlotModel.getTasksList().get(j).getHours();
            }
            timeSlotModel.setHours(hours);
        }

        Log.e("arsh", "differentiating based on projects");

    }

    private void moveToNextSlot() {
        if (getOptionsDataFile.getTimeSheetFrequency() == 1 || getOptionsDataFile.getTimeSheetFrequency() == 4) {
            long start = calendarStart.getTimeInMillis();
            long end = calendarEnd.getTimeInMillis();
            calendarStart.add(Calendar.DATE, -daysToAdd);
            calendarStart.set(Calendar.MINUTE, 0);
            calendarStart.set(Calendar.HOUR, 0);
            calendarStart.set(Calendar.SECOND, 0);
            calendarStart.set(Calendar.AM_PM, 0);
            calendarEnd.add(Calendar.DATE, -daysToAdd);
        } else {
            if (calendarStart.get(Calendar.DAY_OF_MONTH) < 15) {
                //means we were in first half of month
                calendarEnd.add(Calendar.DATE, -15);
                int days = calendarEnd.getActualMaximum(Calendar.DAY_OF_MONTH) - 16;
                calendarStart.add(Calendar.DATE, -days - 1);
            } else {
                //means we were in second half of month
                int days = calendarEnd.getActualMaximum(Calendar.DAY_OF_MONTH) - 16;
                calendarEnd.add(Calendar.DATE, -days - 1);
                calendarStart.add(Calendar.DATE, -15);
            }
        }

    }

    private void acceptRejectTimeSheet(final boolean accept) {

        String IDs = "";

        for (int i = 0; i < timeSheetApproveList.size(); i++) {
            IDs = IDs + String.valueOf(timeSheetApproveList.get(i).getTimeActivityID());
            if (i < timeSheetApproveList.size() - 1) {
                IDs = IDs + ",";
            }
        }
        Log.i("arsh", IDs);
        String url = "";
        url = Constants.ACCEPT_REJECT_TIMESHEET_API;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("TimeActivityID", IDs);
            jsonObject.put("ApproveOrReject", accept);
            jsonObject.put("AppSignature", employeeLogin.getApproverSignature());
            if (accept == false) {
                jsonObject.put("AdminNotes", rejectReason);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("arsh", "url=" + url);
        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                ShowDialog.hideDialog();
                ApproveTimesheetLayer.isToLoadFromServer = true;
                Log.i("arsh", "response=" + s);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(ApproveTimesheetLayer.this, volleyError);
            }
        });
        ShowDialog.showDialog(ApproveTimesheetLayer.this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    class MyTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            timeSlotModelArrayList.clear();
            ShowDialog.showDialog(ApproveTimesheetLayer.this, "Please Wait");
        }

        @Override
        protected Void doInBackground(String... params) {
            if (params != null && params.length > 0) {
                String s = params[0];
                JSONArray array = null;
                try {
                    array = new JSONArray(s.toString());
                    timeSheetApproveList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(array.toString(), Timesheet[].class)));
                    generateTimeMillis(timeSheetApproveList);
                    Collections.sort(timeSheetApproveList, Timesheet.TimeSheetComparator);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getBaseContext(), "Error Loading Timesheets to approve", Toast.LENGTH_SHORT).show();
                }
            } else {
                approveTimesheetDataFile = (ApproveTimesheetDataFile) OfflineDataStorage.readObject(getBaseContext(), Constants.TIMESHEETS_APPROVE_FILE);
                timeSheetApproveList = approveTimesheetDataFile.getTimesheetArrayList();
            }
            generateDataForLayer();
//            approveTimesheetDataFile.setTimeSlotModelArrayList(timeSlotModelArrayList);
            approveTimesheetDataFile.setTimesheetArrayList(timeSheetApproveList);
            OfflineDataStorage.writeObject(getBaseContext(), approveTimesheetDataFile, Constants.TIMESHEETS_APPROVE_FILE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowDialog.hideDialog();
            projApproveLayerAdapter.changeData(timeSlotModelArrayList, approveTimesheetDataFile.getTimesheetArrayList());
            tasksApproveLayerAdapter.changeData(timeSlotModelArrayList, approveTimesheetDataFile.getTimesheetArrayList());
            isToLoadFromLocal = false;
        }
    }

    class SaveOffline extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowDialog.showDialog(ApproveTimesheetLayer.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            OfflineDataStorage.writeObject(ApproveTimesheetLayer.this, approveTimesheetDataFile, Constants.TIMESHEETS_APPROVE_FILE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowDialog.hideDialog();
            isToLoadFromLocal = true;
            refresh();
        }
    }
}
