package com.timerewards.tr.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.ExpenseHeaderModel;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class NewExpenseSheet extends AppCompatActivity implements View.OnClickListener {

    Toolbar mToolbar;
    EditText et_report_date, et_reason_for_expense;
    Button buttonAddExpense;
    DatePickerDialog pickerDialog;
    SimpleDateFormat simpleDateFormatOLD;
    SimpleDateFormat simpleDateFormatNEW;

    ExpenseDataFile expenseDataFile;
    Expense expense;
    ExpenseHeaderModel expenseHeaderModel;
    ArrayList<Expense> expenseArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_expense);
        init();
    }

    private void init() {
        simpleDateFormatOLD = new SimpleDateFormat("dd-MMM-yyyy");
        simpleDateFormatNEW = new SimpleDateFormat("yyyy-MM-dd");
        expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(this, Constants.EXPENSE_DATA_FILE);
        expenseArrayList = expenseDataFile.getExpense();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add New Expense");
        mToolbar.setTitleTextColor(Color.WHITE);
        et_report_date = (EditText) findViewById(R.id.et_report_date);
        et_report_date.setOnClickListener(this);
        et_report_date.setText(simpleDateFormatOLD.format(Calendar.getInstance().getTimeInMillis()));
        et_reason_for_expense = (EditText) findViewById(R.id.et_reason_for_expense);
        buttonAddExpense = (Button) findViewById(R.id.button_add_expense);
        buttonAddExpense.setOnClickListener(this);
    }

    private void selectDate() {
        Calendar newCalendar = Calendar.getInstance();
        pickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                et_report_date.setText(simpleDateFormatOLD.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        pickerDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_report_date:
                selectDate();
                break;
            case R.id.button_add_expense:
                saveExpense();
                break;
        }
    }

    private void saveExpense() {
        if (et_report_date.getText().toString().length() > 0) {

            String resonforEXpenses=et_reason_for_expense.getText().toString();
            if(resonforEXpenses.equalsIgnoreCase(""))
            {
                resonforEXpenses="Reason for Expense";
            }
            if (resonforEXpenses.length() > 0) {
                expense = new Expense();
                expenseHeaderModel = new ExpenseHeaderModel();
                expenseHeaderModel.set$id("new");
                Date date;
                String tempDate = "";
                try {
                    date = simpleDateFormatOLD.parse(et_report_date.getText().toString());
                    tempDate = simpleDateFormatNEW.format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                expenseHeaderModel.setEntryDate(tempDate + "T00:00:00");
                expenseHeaderModel.setDescription(resonforEXpenses);
                expense.setExpHeader(expenseHeaderModel);
                expenseArrayList.add(expense);
                expenseDataFile.setExpense(expenseArrayList);
                OfflineDataStorage.writeObject(getBaseContext(), expenseDataFile, Constants.EXPENSE_DATA_FILE);
                Intent intent = new Intent(getBaseContext(), NewExpenseItemActivity.class);
                intent.putExtra(Constants.HEADER_POSITION, expenseArrayList.size() - 1);
                startActivity(intent);

            } else {
                Toast.makeText(getBaseContext(), "Reason is Mandatory", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(NewExpenseSheet.this, "Report Date is Mandatory", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}