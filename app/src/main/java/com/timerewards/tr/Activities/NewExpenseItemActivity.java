package com.timerewards.tr.Activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.timerewards.tr.Adapters.AccountsAdapter;
import com.timerewards.tr.Adapters.ClassesAdapter;
import com.timerewards.tr.Adapters.CreditCardAdapter;
import com.timerewards.tr.Adapters.ProjectsAdapter;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.AccountModel;
import com.timerewards.tr.DataFiles.AccountsDataFile;
import com.timerewards.tr.DataFiles.ClassesDataFile;
import com.timerewards.tr.DataFiles.ClassesModel;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.ExpenseItemModel;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.ProjectsDataFile;
import com.timerewards.tr.DataFiles.ProjectsModel;
import com.timerewards.tr.DataFiles.TasksModel;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.Utilities;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class NewExpenseItemActivity extends AppCompatActivity implements View.OnClickListener, AccountsAdapter.AccoutSelection, ProjectsAdapter.ProjectSelection, ClassesAdapter.ClassSelection, CreditCardAdapter.CreditCardSelection, TextWatcher {

    private static final int PICK_IMAGE = 100;
    private static final int CAPTURE_IMAGE = 200;
    EmployeeLogin employeeLogin;
    ArrayList<Expense> expenseArrayList = new ArrayList<>();
    ArrayList<ClassesModel> classesModelArrayList = new ArrayList<>();
    ArrayList<ProjectsModel> projectsModelArrayList = new ArrayList<>();
    ArrayList<ProjectsModel> projectsModelArrayListSearch = new ArrayList<>();
    ArrayList<AccountModel> accountModelArrayList = new ArrayList<>();
    ArrayList<AccountModel> accountModelArrayListSearch= new ArrayList<>();
    private ArrayList<ClassesModel> classesModelArrayListSearch = new ArrayList<>();
    ArrayList<AccountModel> creditCardModelArrayList = new ArrayList<>();
    SimpleDateFormat simpleDateFormatOLD;
    SimpleDateFormat simpleDateFormatNEW;
    AccountModel selectedAccountModel;
    ProjectsModel selectedProjectsModel;
    ClassesModel selectedClassModel;
    AccountModel selectedCreditCardModel;
    int projectPosition = 0;
    int accountPosition = 0;
    int creditCardPosition = 0;
    int classPosition = 0;
    String captured_image = "";
    private Toolbar mToolbar;
    private EditText etExpenseDate, etMerchant, etAmount, etDetails;
    private CheckBox checkBoxBillable;
    private RadioGroup radioGroupPaidBy;
    private RadioButton radioButtonPaidByMe, radioButtonPaidByCompany;
    private Button buttonUploadReceipt, buttonAddExpense, buttonFromCamera, buttonFromGallery;
    private LinearLayout llAccount, llNonAccount;
    private DatePickerDialog pickerDialog;
    private ImageView ivReceipt, ivRemoveReceipt;
    private TextView tvAccountName, tvProjectName, tvClassName, tvCreditCardName, textAccount, textProject, textClass, textCreditCard;
    private TextView textQuantity, textPrice;
    private EditText etQuantity, etPrice;
    private GetOptionsDataFile getOptionsDataFile;
    private ExpenseDataFile expenseDataFile;
    private AccountsDataFile accountsDataFile;
    private ClassesDataFile classesDataFile;
    private ProjectsDataFile projectsDataFile;
    private RecyclerView recyclerView;
    private ProjectsAdapter projectsAdapter;
    private ClassesAdapter classesAdapter;
    private CreditCardAdapter creditCardAdapter;
    private AccountsAdapter accountsAdapter;
    private ArrayList<String> projectNames = new ArrayList<>();
    private ArrayList<String> customerNames = new ArrayList<>();
    private ArrayList<String> accountNames = new ArrayList<>();
    private ArrayList<String> creditCardNames = new ArrayList<>();
    private int expenseHeaderPosition;
    private Intent intent;
    private Dialog dialog;
    private Bitmap bitmap = null;
    private Dialog dialogList;
    private EditText searchET;
    private ImageView searchIcon;
    private String typeOfListInDialog;
    private LinearLayout searchBar;
    String dialougeIdentifier="";
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_expense_item);
        preferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
        preferences.edit().putString(Constants.DIALOUGE_IDEN,"").apply();
        dialougeIdentifier=preferences.getString(Constants.DIALOUGE_IDEN,"");
        init();
        putData();
    }

    private void init() {

        simpleDateFormatOLD = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        simpleDateFormatNEW = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        intent = getIntent();
        expenseHeaderPosition = intent.getIntExtra(Constants.HEADER_POSITION, -1);
        //Initializing xml items
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
//         mToolbar.setTitle("Edit Reason for Expense");
        getSupportActionBar().setTitle("Edit Reason for Expense");
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ivReceipt = (ImageView) findViewById(R.id.iv_receipt);
        ivRemoveReceipt = (ImageView) findViewById(R.id.iv_remove_receipt);
        ivRemoveReceipt.setOnClickListener(this);
        ivRemoveReceipt.setVisibility(View.GONE);
        textAccount = (TextView) findViewById(R.id.text_account);
        tvAccountName = (TextView) findViewById(R.id.tv_select_account);
        tvAccountName.setOnClickListener(this);
        etExpenseDate = (EditText) findViewById(R.id.et_expense_date);
        etExpenseDate.setOnClickListener(this);
        etExpenseDate.setText(simpleDateFormatOLD.format(Calendar.getInstance().getTimeInMillis()));
        textQuantity = (TextView) findViewById(R.id.text_qty);
        textPrice = (TextView) findViewById(R.id.text_price);
        etQuantity = (EditText) findViewById(R.id.et_expense_qty);
        etPrice = (EditText) findViewById(R.id.et_expense_price);
        etPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() > 0) {
                    if (editable.toString().equalsIgnoreCase(".")) {
                        etPrice.setText("0.");
                        etPrice.setSelection(2);
                    } else {
                        if (etQuantity.getText().toString().length() > 0) {
                            Double price = new Double(Double.parseDouble(etPrice.getText().toString()));
                            Double qty = new Double(Double.parseDouble(etQuantity.getText().toString()));
                            etAmount.setText(String.format("%.2f", (double) Math.round(price * qty * 100) / 100));
                        }
                    }
                }
            }
        });
        etQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() > 0) {
                    if (editable.toString().equalsIgnoreCase(".")) {
                        etQuantity.setText("0.");
                        etQuantity.setSelection(2);
                    } else {
                        if (etPrice.getText().toString().length() > 0) {
                            Double price = new Double(Double.parseDouble(etPrice.getText().toString()));
                            Double qty = new Double(Double.parseDouble(etQuantity.getText().toString()));
                            etAmount.setText(String.format("%.2f", (double) Math.round(price * qty * 100) / 100));
                        }
                    }
                }
            }
        });
        textQuantity.setVisibility(View.GONE);
        textPrice.setVisibility(View.GONE);
        etQuantity.setVisibility(View.GONE);
        etPrice.setVisibility(View.GONE);
        etAmount = (EditText) findViewById(R.id.et_expense_amount);
        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() > 0) {
                    if (editable.toString().equalsIgnoreCase(".")) {
                        etAmount.setText("0.");
                        etAmount.setSelection(2);
                    }
                }
            }
        });
        etMerchant = (EditText) findViewById(R.id.et_expense_merchant);
        checkBoxBillable = (CheckBox) findViewById(R.id.checkbox_billable);
        checkBoxBillable.setChecked(false);
        textProject = (TextView) findViewById(R.id.text_project);
        tvProjectName = (TextView) findViewById(R.id.tv_select_project);
        tvProjectName.setOnClickListener(this);
        textClass = (TextView) findViewById(R.id.text_class);
        tvClassName = (TextView) findViewById(R.id.tv_select_class);
        tvClassName.setOnClickListener(this);
        radioGroupPaidBy = (RadioGroup) findViewById(R.id.radioGroupPaidBy);
        radioButtonPaidByMe = (RadioButton) findViewById(R.id.radio_paid_by_me);
        radioButtonPaidByCompany = (RadioButton) findViewById(R.id.radio_paid_by_company);
        radioButtonPaidByCompany.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    tvCreditCardName.setEnabled(true);
                    if(creditCardModelArrayList.size()>0) {

                        selectedCreditCardModel = creditCardModelArrayList.get(0);
                        tvCreditCardName.setText(selectedCreditCardModel.getAccountname());
                    }

                } else {
                    tvCreditCardName.setEnabled(false);
                }
            }
        });
        textCreditCard = (TextView) findViewById(R.id.text_credit_card);
        tvCreditCardName = (TextView) findViewById(R.id.tv_select_credit_card);
        tvCreditCardName.setOnClickListener(this);
        etDetails = (EditText) findViewById(R.id.et_expense_details);
        buttonUploadReceipt = (Button) findViewById(R.id.button_upload_receipt);
        buttonUploadReceipt.setOnClickListener(this);
        buttonAddExpense = (Button) findViewById(R.id.button_add_expense);
        buttonAddExpense.setOnClickListener(this);
        llAccount = (LinearLayout) findViewById(R.id.ll_account);
        llNonAccount = (LinearLayout) findViewById(R.id.ll_non_account);
        dialogList = new Dialog(this);
        dialogList.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogList.setContentView(R.layout.dialog_list);
        recyclerView = (RecyclerView) dialogList.findViewById(R.id.recycler_view);
        searchIcon = (ImageView) dialogList.findViewById(R.id.iv_search);
        searchBar = (LinearLayout) dialogList.findViewById(R.id.searchBar);
        searchIcon.setImageResource(R.drawable.search_icon);

        searchET = (EditText) dialogList.findViewById(R.id.et_search);
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchET.getText().clear();

            }
        });
        searchET.addTextChangedListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        //initializing Data files and Models
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(getBaseContext(), Constants.EMPLOYEE_LOGIN_FILE);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(getBaseContext(), Constants.GETOPTIONS_FILE);
        expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(getBaseContext(), Constants.EXPENSE_DATA_FILE);
        expenseArrayList = expenseDataFile.getExpense();
        classesDataFile = (ClassesDataFile) OfflineDataStorage.readObject(getBaseContext(), Constants.CLASSES_DATA_FILE);
        projectsDataFile = (ProjectsDataFile) OfflineDataStorage.readObject(getBaseContext(), Constants.PROJECTS_DATA_FILE);
        accountsDataFile = (AccountsDataFile) OfflineDataStorage.readObject(getBaseContext(), Constants.ACCOUNTS_DATA_FILE);
        classesModelArrayList = classesDataFile.getClassesModelList();
        classesModelArrayListSearch = classesDataFile.getClassesModelList();

        //getting projects list....
        for (int i = 0; i < projectsDataFile.getProjectsModelList().size(); i++) {
            if (projectsDataFile.getProjectsModelList().get(i).isExpPrj() != false) {
                projectsModelArrayList.add(projectsDataFile.getProjectsModelList().get(i));
                projectsModelArrayListSearch.add(projectsDataFile.getProjectsModelList().get(i));
            }
        }

        AccountModel accountModel = new AccountModel();
        accountModel.setAssettype(Constants.ASSEST_TYPE_CREDIT_CARD);
        accountModel.setAccountid(0);
        accountModel.setIsDefaultGLAnItem(0);
        accountModel.setAccountname("None");
        creditCardModelArrayList.add(accountModel);

        //getting account Model list
        for (int i = 0; i < accountsDataFile.getAccountsList().size(); i++) {
            if (accountsDataFile.getAccountsList().get(i).getAssettype().equalsIgnoreCase(Constants.ASSEST_TYPE_CREDIT_CARD)) {
                creditCardModelArrayList.add(accountsDataFile.getAccountsList().get(i));
            } else {
                accountModelArrayList.add(accountsDataFile.getAccountsList().get(i));
                accountModelArrayListSearch.add(accountsDataFile.getAccountsList().get(i));

            }
        }

        //Hiding/Showing Customer List according to getOptions prefs.....
        if (getOptionsDataFile.isUseClassesInExp() == false) {
            llNonAccount.setVisibility(View.GONE);
        }

        selectedClassModel = classesModelArrayList.get(0);

    }

    private void putData() {

        textProject.setText(getOptionsDataFile.getCustomerLex() + " : " + getOptionsDataFile.getProjectLex());
        textClass.setText(getOptionsDataFile.getClassLex());

        tvProjectName.setText("Select " + getOptionsDataFile.getCustomerLex() + " : " + getOptionsDataFile.getProjectLex());
        tvClassName.setText("Select " + getOptionsDataFile.getClassLex());
        tvAccountName.setText("Select Account");
        tvCreditCardName.setText("Select Credit Card");
        tvCreditCardName.setEnabled(false);

        accountsAdapter = new AccountsAdapter(this, accountPosition, accountModelArrayList);
        projectsAdapter = new ProjectsAdapter(this, projectPosition, projectsModelArrayList);
        classesAdapter = new ClassesAdapter(this, classPosition, classesModelArrayList);
        creditCardAdapter = new CreditCardAdapter(this, creditCardPosition, creditCardModelArrayList);
        selectedProjectsModel = projectsModelArrayList.get(0);
        selectedAccountModel = accountModelArrayList.get(0);

    }

    private void selectDate() {
        Calendar newCalendar = Calendar.getInstance();
        pickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etExpenseDate.setText(simpleDateFormatOLD.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        pickerDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tv_select_account:
                showAccountDialog();
                break;
            case R.id.tv_select_project:
                showProjectDialog();
                break;
            case R.id.tv_select_class:
                showClassDialog();
                break;
            case R.id.tv_select_credit_card:
                showCreditCardDialog();
                break;
            case R.id.et_expense_date:
                selectDate();
                break;
            case R.id.button_add_expense:
                saveData();
                break;
            case R.id.button_upload_receipt:
                if (Utilities.checkStoragePermission(this)){
                    selectSourceDialog();
                }
                break;
            case R.id.button_from_camera:
                selectFromCamera();
                dialog.dismiss();
                break;
            case R.id.button_from_gallery:
                selectFromGallery();
                dialog.dismiss();
                break;
            case R.id.iv_remove_receipt:
                bitmap = null;
                ivReceipt.setImageDrawable(null);
                ivRemoveReceipt.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean cameraPermission = false;
        boolean storagePermission = false;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED && permissions[i].equalsIgnoreCase(Manifest.permission.CAMERA)){
                cameraPermission = true;
            }
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED && permissions[i].equalsIgnoreCase(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                cameraPermission = true;
            }
        }

        if (cameraPermission && storagePermission){
            selectSourceDialog();
        }
    }

    private void selectFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private void selectFromCamera() {

        Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        captured_image = System.currentTimeMillis() + ".jpg";
        File file = new File(Environment.getExternalStorageDirectory(), captured_image);
        captured_image = file.getAbsolutePath();
        Uri outputFileUri = Uri.fromFile(file);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(cameraIntent, CAPTURE_IMAGE);
    }

    private void selectSourceDialog() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_chooser_dialog);
        dialog.show();

        buttonFromCamera = (Button) dialog.findViewById(R.id.button_from_camera);
        buttonFromCamera.setOnClickListener(this);
        buttonFromGallery = (Button) dialog.findViewById(R.id.button_from_gallery);
        buttonFromGallery.setOnClickListener(this);
    }

    private void saveData() {

        //Checking Mandatory Fields
        if (checkMandatoryFields() != true) {
            return;
        }
        ExpenseItemModel expenseItemModel = new ExpenseItemModel();
        expenseItemModel.set$id("new");
        expenseItemModel.setUserID(employeeLogin.getUserid());
        expenseItemModel.setEmployeeExpenseID(expenseDataFile.getExpense().get(expenseHeaderPosition).getExpHeader().getEmployeeExpenseID());
        expenseItemModel.setEmployeeExpenseItemID(0);
        expenseItemModel.setExpenseDate(Utilities.dateToServerFormat(etExpenseDate.getText().toString()));
        if (selectedAccountModel.isMustEnterQtyRate()) {
            expenseItemModel.setQty(Double.parseDouble(etQuantity.getText().toString()));
            expenseItemModel.setPrice(Double.parseDouble(etPrice.getText().toString()));
        } else {
            expenseItemModel.setQty(0.0d);
            expenseItemModel.setPrice(0.0d);
        }
        expenseItemModel.setExpenseAmount(Float.parseFloat(etAmount.getText().toString()));
        if (radioButtonPaidByMe.isChecked() == true) {
            expenseItemModel.setPaidBy(1);
            expenseItemModel.setIsReimbursable(true);
        } else {
            expenseItemModel.setPaidBy(2);
            expenseItemModel.setIsReimbursable(false);
        }
        if (checkBoxBillable.isChecked()) {
            expenseItemModel.setBillable(true);
        } else {
            expenseItemModel.setBillable(false);
        }
        expenseItemModel.setMerchant(etMerchant.getText().toString());
        expenseItemModel.setDescription(etDetails.getText().toString());

        expenseItemModel.setProjectID(selectedProjectsModel.getProjectID());
        if (selectedProjectsModel.getProjectID() == 0) {
            expenseItemModel.setProjectFullName("None");
        } else {
            expenseItemModel.setProjectFullName(selectedProjectsModel.getFullName());
        }
        expenseItemModel.setAccountID(selectedAccountModel.getAccountid());
        if (tvCreditCardName.isEnabled() == true) {
            if (selectedCreditCardModel.getAccountid() == 0) {
                expenseItemModel.setCCAccountName("None");
            } else {
                expenseItemModel.setCCAccountName(selectedCreditCardModel.getAccountname());
            }
        } else {
            expenseItemModel.setCCAccountName("None");
        }
        expenseItemModel.setEmployeeFullName(employeeLogin.getDisplayName());
        expenseItemModel.setAccountFullName(selectedAccountModel.getAccountname());
        if (getOptionsDataFile.isUseClassesInExp() == true) {
            expenseItemModel.setClassRowID(selectedClassModel.getClassRowID());
            if (selectedClassModel.getClassRowID() == 0) {
                expenseItemModel.setClassFullName("None");
            } else {
                expenseItemModel.setClassFullName(selectedClassModel.getFullName());
            }
        } else {
            expenseItemModel.setClassRowID(0);
            expenseItemModel.setClassFullName("None");
        }

        if (radioButtonPaidByMe.isChecked() == true) {
            expenseItemModel.setIsDefaultGLAnItem(true);
        } else {
            expenseItemModel.setIsDefaultGLAnItem(false);
        }
        if (selectedAccountModel.getIsDefaultGLAnItem() == 0) {
            expenseItemModel.setIsDefaultGLAnItem(false);
        } else {
            expenseItemModel.setIsDefaultGLAnItem(true);
        }

        expenseItemModel.setEZStatusID(0);

        if (bitmap != null) {
            if (Utilities.isExternalStorageReadable()) {
                SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_FILE, MODE_PRIVATE);
                int count = sharedPreferences.getInt(Constants.IMAGE_COUNTER, 0);
                String filename = "expenseReceipt" + String.valueOf(count) + ".jpg";
                try {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
                    File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                    File dest = new File(f, filename);
                    while (dest.exists()) {
                        count++;
                        filename = "expenseReceipt" + String.valueOf(count) + ".jpg";
                        f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                        dest = new File(f, filename);
                    }
                    dest.createNewFile();
                    FileOutputStream fileOutputStream = new FileOutputStream(dest);
                    fileOutputStream.write(bytes.toByteArray());
                    fileOutputStream.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                expenseItemModel.setReceiptName(filename);
                Toast.makeText(NewExpenseItemActivity.this, "Image Saved", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(Constants.IMAGE_COUNTER, ++count);
                editor.apply();
            } else {

            }
        }

//        Toast.makeText(NewExpenseItemActivity.this, "size =" + expenseDataFile.getExpense().size(), Toast.LENGTH_SHORT).show();
        expenseDataFile.getExpense().get(expenseHeaderPosition).getExpenseItemList().add(expenseItemModel);
//        Toast.makeText(NewExpenseItemActivity.this, "size =" + expenseDataFile.getExpense().size(), Toast.LENGTH_SHORT).show();
        OfflineDataStorage.writeObject(getBaseContext(), expenseDataFile, Constants.EXPENSE_DATA_FILE);
        Intent intent = new Intent(getBaseContext(), ExpenseSheetActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private boolean checkMandatoryFields() {

        if (accountPosition == 0) {
            Toast.makeText(getBaseContext(), "Account is Mandatory", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (selectedAccountModel.isMustEnterQtyRate()) {
            if (etQuantity.getText().toString().length() < 1) {
                Toast.makeText(getBaseContext(), "Quantity is Mandatory", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (etPrice.getText().toString().length() < 1) {
                Toast.makeText(getBaseContext(), "Price is Mandatory", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if (accountPosition == 0) {
            Toast.makeText(getBaseContext(), "Account is Mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (etAmount.getText().toString().length() == 0) {
                Toast.makeText(getBaseContext(), "Amount is Mandatory", Toast.LENGTH_SHORT).show();
                return false;
            } else {
                if (checkBoxBillable.isChecked() == true) {
                    if (projectPosition == 0) {
                        Toast.makeText(getBaseContext(), "Project is Mandatory", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    else
//                    {
//                        if (tvCreditCardName.isEnabled() == true && creditCardPosition == 0) {
//                            Toast.makeText(getBaseContext(), "Credit Card is Mandatory", Toast.LENGTH_SHORT).show();
//                            return false;
//                        }
//                        else
                        {
                            if (etExpenseDate.getText().toString().length() == 0) {
                                Toast.makeText(getBaseContext(), "Expense Date is Mandatory", Toast.LENGTH_SHORT).show();
                                return false;
                            } else {

                            }
//                        }
                    }
                }


            }


        }
        if (tvCreditCardName.isEnabled() == true) {
            if (selectedCreditCardModel == null) {
                Toast.makeText(getBaseContext(), "Please select credit card", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }

    private void showAccountDialog() {
        searchBar.setVisibility(View.VISIBLE);
        typeOfListInDialog="account";

        dialougeIdentifier=preferences.getString(Constants.DIALOUGE_IDEN,"");
        if(dialougeIdentifier.equalsIgnoreCase("account"))
        {

        }else
        {
            searchET.setText("");
            preferences.edit().putString(Constants.DIALOUGE_IDEN,"account").apply();
        }
//        recyclerView.setAdapter(accountsAdapter);
//        recyclerView.scrollToPosition(accountPosition);
//        dialogList.show();

        accountsAdapter = new AccountsAdapter(NewExpenseItemActivity.this, accountPosition, accountModelArrayList);
        recyclerView.setAdapter(accountsAdapter);
        if (selectedAccountModel != null) {
            for (int i = 0; i < accountModelArrayList.size(); i++) {
                if (accountModelArrayList.get(i).getAccountid() == accountPosition) {
                    recyclerView.scrollToPosition(i);
                    break;
                }
            }
        }
        dialogList.show();
    }



    private void showProjectDialog() {
//        typeOfListInDialog="projects";
//        recyclerView.setAdapter(projectsAdapter);
//        recyclerView.scrollToPosition(projectPosition);
//        dialogList.show();
        searchBar.setVisibility(View.VISIBLE);
        typeOfListInDialog = "projects";

        dialougeIdentifier=preferences.getString(Constants.DIALOUGE_IDEN,"");
        if(dialougeIdentifier.equalsIgnoreCase("projects"))
        {

        }else
        {
            searchET.setText("");
            preferences.edit().putString(Constants.DIALOUGE_IDEN,"projects").apply();
        }

        recyclerView.setAdapter(projectsAdapter);
        dialogList.show();
        if (selectedProjectsModel != null) {
            for (int i = 0; i < projectsModelArrayList.size(); i++) {
                if (projectsModelArrayList.get(i).getProjectID() == projectPosition) {
                    recyclerView.scrollToPosition(i);
                }
            }
        }
    }

//    private void showClassDialog() {
//        searchBar.setVisibility(View.VISIBLE);
//        recyclerView.setAdapter(classesAdapter);
//        recyclerView.scrollToPosition(classPosition);
//        dialogList.show();
//    }


    private void showClassDialog() {
        typeOfListInDialog = "class";

        dialougeIdentifier=preferences.getString(Constants.DIALOUGE_IDEN,"");
        if(dialougeIdentifier.equalsIgnoreCase("class"))
        {

        }else
        {
            searchET.setText("");
            preferences.edit().putString(Constants.DIALOUGE_IDEN,"class").apply();
        }

        classesAdapter = new ClassesAdapter(this, classPosition, classesModelArrayList);
        recyclerView.setAdapter(classesAdapter);
        if (selectedClassModel != null) {
            for (int i = 0; i < classesModelArrayList.size(); i++) {
                if (classesModelArrayList.get(i).getClassRowID() == classPosition) {
                    recyclerView.scrollToPosition(i);
                    break;
                }
            }
        }
        dialogList.show();
    }



    private void showCreditCardDialog() {
        searchBar.setVisibility(View.GONE);
        recyclerView.setAdapter(creditCardAdapter);
        recyclerView.scrollToPosition(creditCardPosition);
        dialogList.show();
    }

    @Override
    public void setAccount(int position) {
        accountPosition = position;

        for (int i = 0; i < accountModelArrayList.size(); i++) {
            if (accountModelArrayList.get(i).getAccountid() == position) {
                selectedAccountModel = accountModelArrayList.get(i);
                break;
            }
        }

        accountsAdapter.changePosition(selectedAccountModel.getAccountid());
        tvAccountName.setText(selectedAccountModel.getAccountname());
        dialogList.dismiss();
        if (selectedAccountModel.isMustEnterQtyRate()) {
            etAmount.setEnabled(false);
            textQuantity.setVisibility(View.VISIBLE);
            textPrice.setVisibility(View.VISIBLE);
            etQuantity.setVisibility(View.VISIBLE);
            etPrice.setVisibility(View.VISIBLE);
            if (selectedAccountModel.isLockDefaultRate()) {
                etPrice.setText(String.valueOf(selectedAccountModel.getDefaultRate()));
                etPrice.setEnabled(false);
            } else {
                etPrice.setText(String.valueOf(selectedAccountModel.getDefaultRate()));
                etPrice.setEnabled(true);
            }
        } else {
            etAmount.setEnabled(true);
            textQuantity.setVisibility(View.GONE);
            textPrice.setVisibility(View.GONE);
            etQuantity.setVisibility(View.GONE);
            etPrice.setVisibility(View.GONE);
        }
    }

//    @Override
//    public void setClass(int position) {
//        classPosition = position;
//        selectedClassModel = classesModelArrayList.get(classPosition);
//        classesAdapter.changePosition(classPosition);
//        tvClassName.setText(selectedClassModel.getFullName());
//        dialogList.dismiss();
//    }


    @Override
    public void setClass(int position) {
        classPosition = position;
//        classesAdapter.changePosition(classPosition);
//        selectedClassesModel = classesModelArrayList.get(classPosition);
//        tvClassName.setText(selectedClassesModel.getFullName());
//        dialog.dismiss();


        for (int i = 0; i < classesModelArrayList.size(); i++) {
            if (classesModelArrayList.get(i).getClassRowID() == position) {
                selectedClassModel = classesModelArrayList.get(i);
                tvClassName.setText(selectedClassModel.getFullName());
                break;
            }

        }
        classesAdapter.changePosition(selectedClassModel.getClassRowID());
        dialogList.dismiss();

    }


    @Override
    public void setCreditCard(int position) {
        creditCardPosition = position;
        selectedCreditCardModel = creditCardModelArrayList.get(creditCardPosition);
        creditCardAdapter.changePosition(creditCardPosition);
        tvCreditCardName.setText(selectedCreditCardModel.getAccountname());
        dialogList.dismiss();
    }

    @Override
    public void setProject(int position) {
        projectPosition = position;
//        selectedProjectsModel = projectsModelArrayList.get(projectPosition);
//        projectsAdapter.changePosition(projectPosition);
//        tvProjectName.setText(selectedProjectsModel.getFullName());
//        dialogList.dismiss();


        for (int i = 0; i < projectsModelArrayList.size(); i++) {
            if (projectsModelArrayList.get(i).getProjectID() == position) {
                selectedProjectsModel = projectsModelArrayList.get(i);
            }
        }
        projectsAdapter.changePosition(selectedProjectsModel.getProjectID());
        tvProjectName.setText(selectedProjectsModel.getFullName());
        dialogList.dismiss();


        if (projectPosition == 0) {
            checkBoxBillable.setChecked(false);
        } else {
            checkBoxBillable.setChecked(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap small = Bitmap.createScaledBitmap(bitmap, 500, 500, true);
            ivReceipt.setImageBitmap(small);
            ivRemoveReceipt.setVisibility(View.VISIBLE);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        }
        if (requestCode == CAPTURE_IMAGE && resultCode == RESULT_OK) {
            bitmap = BitmapFactory.decodeFile(captured_image);
            bitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, true);
            ivReceipt.setImageBitmap(bitmap);
            ivRemoveReceipt.setVisibility(View.VISIBLE);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if(s.length()>0)
        {
            searchIcon.setImageResource(R.drawable.ic_clear_black_24dp);


        }else
        {
            searchIcon.setImageResource(R.drawable.search_icon);

        }

    }

    @Override
    public void afterTextChanged(Editable s) {

        filterList(s.toString());

    }
    private void filterList(String stringToSearch) {
        if (stringToSearch.trim().length() == 0) {
            if (typeOfListInDialog.equalsIgnoreCase("account")) {
                accountsAdapter.changeData(accountModelArrayList);
                return;
            }
            if (typeOfListInDialog.equalsIgnoreCase("projects")) {
                projectsAdapter.changeData(projectsModelArrayList);
                return;
            }
            if (typeOfListInDialog.equalsIgnoreCase("class")) {
                classesAdapter.changeData(classesModelArrayList);
                return;
            }


        }

        if (typeOfListInDialog.equalsIgnoreCase("account")) {
            accountModelArrayListSearch = new ArrayList<>();
            ArrayList<AccountModel> accountLevels = new ArrayList<>();

            for (int i = 0; i < accountModelArrayList.size(); i++) {
                AccountModel accountModel = accountModelArrayList.get(i);
                accountModel.setHasAdded(false);
                int subLevel = accountModel.getSubLevelLocal();
                if (subLevel == 0) {
                    accountLevels.clear();
                    accountLevels.add(0, accountModel);
                }
                if (subLevel == 1) {
                    if (accountModel.getFullName().contains(accountLevels.get(subLevel - 1).getFullName())) {
                        accountLevels.add(subLevel, accountModel);
                    } else {
                        accountLevels.clear();
                        String[] taskNames = accountModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            accountLevels.add(j, accountModel);
                        }
                        accountLevels.add(subLevel, accountModel);
                    }
                }
                if (subLevel == 2) {
                    if (accountModel.getFullName().contains(accountLevels.get(subLevel - 1).getFullName())) {
                        accountLevels.add(subLevel, accountModel);
                    } else {
                        String[] taskNames = accountModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            accountLevels.add(j, accountModel);
                        }
                        accountLevels.add(subLevel, accountModel);
                    }
                }
                if (subLevel == 3) {
                    if (accountModel.getFullName().contains(accountLevels.get(subLevel - 1).getFullName())) {
                        accountLevels.add(subLevel, accountModel);
                    } else {
                        String[] taskNames = accountModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            accountLevels.add(j, accountModel);
                        }
                        accountLevels.add(subLevel, accountModel);
                    }
                }
                if (subLevel == 4) {
                    if (accountModel.getFullName().contains(accountLevels.get(subLevel - 1).getFullName())) {
                        accountLevels.add(subLevel, accountModel);
                    } else {
                        String[] taskNames = accountModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            accountLevels.add(j, accountModel);
                        }
                        accountLevels.add(subLevel, accountModel);
                    }
                }
                if (subLevel == 5) {
                    if (accountModel.getFullName().contains(accountLevels.get(subLevel - 1).getFullName())) {
                        accountLevels.add(subLevel, accountModel);
                    } else {
                        String[] taskNames = accountModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            accountLevels.add(j, accountModel);
                        }
                        accountLevels.add(subLevel, accountModel);
                    }
                }

                if (accountModelArrayList.get(i).getAccountname().toLowerCase().contains(stringToSearch.toLowerCase())) {
                    int subLevelTemp = accountModel.getSubLevelLocal();
                    for (int j = 0; j <= subLevelTemp - 1; j++) {
                        if (accountLevels.get(j).isHasAdded() == false) {
                            accountModelArrayListSearch.add(accountLevels.get(j));
                            accountLevels.get(j).setHasAdded(true);
                        }
                    }
                    accountModelArrayListSearch.add(accountModel);
                    accountModel.setHasAdded(true);
                    accountLevels.add(subLevel, accountModel);
                }
            }

            accountsAdapter.changeData(accountModelArrayListSearch);
        }

        if (typeOfListInDialog.equalsIgnoreCase("projects")) {
            projectsModelArrayListSearch = new ArrayList<>();
            ArrayList<ProjectsModel> projectLevel = new ArrayList<>();

            for (int i = 0; i < projectsModelArrayList.size(); i++) {
                ProjectsModel projectsModel = projectsModelArrayList.get(i);
                projectsModel.setHasAdded(false);
                int subLevel = projectsModel.getSubLevel();
                if (subLevel == 0) {
                    projectLevel.clear();
                    projectLevel.add(0, projectsModel);
                }
                if (subLevel == 1) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        projectLevel.clear();
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 2) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.set(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 3) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 4) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 5) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }

                if (projectsModelArrayList.get(i).getProjectName().toLowerCase().contains(stringToSearch.toLowerCase())) {
                    int subLevelTemp = projectsModel.getSubLevel();
                    for (int j = 0; j <= subLevelTemp - 1; j++) {
                        if (projectLevel.get(j).isHasAdded() == false) {
                            projectsModelArrayListSearch.add(projectLevel.get(j));
                            projectLevel.get(j).setHasAdded(true);
                        }
                    }

                    projectsModelArrayListSearch.add(projectsModel);
                    projectsModel.setHasAdded(true);
                    projectLevel.add(subLevel, projectsModel);
                }
            }

            projectsAdapter.changeData(projectsModelArrayListSearch);
        }

        if (typeOfListInDialog.equalsIgnoreCase("class")) {

            classesModelArrayListSearch = new ArrayList<>();
            ArrayList<ClassesModel> classLevel = new ArrayList<>();

            for (int i = 0; i < classesModelArrayList.size(); i++) {
                ClassesModel classesModel = classesModelArrayList.get(i);
                classesModel.setHasAdded(false);
                int subLevel = classesModel.getSubLevel();
                if (subLevel == 0) {
                    classLevel.clear();
                    classLevel.add(0, classesModel);
                }
                if (subLevel == 1) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        classLevel.clear();
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 2) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.set(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 3) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 4) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 5) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }

                if (classesModelArrayList.get(i).getClassName().toLowerCase().contains(stringToSearch.toLowerCase())) {
                    int subLevelTemp = classesModel.getSubLevel();
                    for (int j = 0; j <= subLevelTemp - 1; j++) {
                        if (classLevel.get(j).isHasAdded() == false) {
                            classesModelArrayListSearch.add(classLevel.get(j));
                            classLevel.get(j).setHasAdded(true);
                        }
                    }

                    classesModelArrayListSearch.add(classesModel);
                    classesModel.setHasAdded(true);
                    classLevel.add(subLevel, classesModel);
                }
            }

            classesAdapter.changeData(classesModelArrayListSearch);
        }

    }



}