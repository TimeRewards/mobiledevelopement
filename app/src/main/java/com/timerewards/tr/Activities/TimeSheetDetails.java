package com.timerewards.tr.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.DataModels.TimeSheetResponse;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeSheetDetails extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private Intent intent;
    private int timesheetPosition;
    private Timesheet timesheet;
    private TableObject tableObject;
    private TextView mCustomer, mProject, mTask, mDate, mHours, mStatus, mDescription, mBillable, mSubmit, mDelete, mDuplicate, tvAdminNotes;
    private LinearLayout llAdminNotes, llClass;
    private ImageView mEdit;
    private String date;
    private EmployeeLogin employeeLogin;
    private GetOptionsDataFile getOptionsDataFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timesheet_detail);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(getBaseContext(), Constants.EMPLOYEE_LOGIN_FILE);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(this, Constants.GETOPTIONS_FILE);
        intent = getIntent();
        timesheetPosition = intent.getIntExtra("id", -1);
        init();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Timesheet Details");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        setDataIntoFields();
    }

    private void init() {
        tableObject = (TableObject) OfflineDataStorage.readObject(this, Constants.TIMESHEETS_FILE);
        timesheet = tableObject.getTimesheetList().get(timesheetPosition);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mEdit = (ImageView) findViewById(R.id.iv_edit);
        mEdit.setOnClickListener(this);
        mCustomer = (TextView) findViewById(R.id.tv_customer);
        mProject = (TextView) findViewById(R.id.tv_project);
        mTask = (TextView) findViewById(R.id.tv_task);
        mDate = (TextView) findViewById(R.id.tv_date);
        mHours = (TextView) findViewById(R.id.tv_hours);
        mBillable = (TextView) findViewById(R.id.tv_billable);
        mStatus = (TextView) findViewById(R.id.tv_status);
        mDescription = (TextView) findViewById(R.id.tv_description);
        mSubmit = (TextView) findViewById(R.id.tv_submit);
        mSubmit.setOnClickListener(this);
        mDelete = (TextView) findViewById(R.id.tv_delete);
        mDelete.setOnClickListener(this);
        mDuplicate = (TextView) findViewById(R.id.tv_duplicate);
        llAdminNotes = (LinearLayout) findViewById(R.id.ll_admin_notes);
        tvAdminNotes = (TextView) findViewById(R.id.tv_admin_notes);
        llClass = (LinearLayout) findViewById(R.id.ll_class);
        if (getOptionsDataFile.isUseClassesInTS()) {
            llClass.setVisibility(View.VISIBLE);
        } else {
            llClass.setVisibility(View.GONE);
        }
        mDuplicate.setOnClickListener(this);
    }

    private void setDataIntoFields() {
        mDate.setText(Utilities.dateToLocalFormat(timesheet.getActivityDate()));
        mCustomer.setText(timesheet.getClassFullName());
        mProject.setText(timesheet.getProjectFullName());
        mTask.setText(timesheet.getTaskFullName());
        mHours.setText(String.format("%.2f", (double) Math.round(timesheet.getActualHours() * 100) / 100));
        if (timesheet.isBillable()) {
            mBillable.setText("Billable");
        } else {
            mBillable.setText("Not Billable");
        }

        mStatus.setText(Utilities.intToStatus(timesheet.getEZStatusID()));
        if (timesheet.getTimeActivityID() == 0 && timesheet.get$id().equalsIgnoreCase("new")) {
            mStatus.setText("Saved Locally");
        }
        mDescription.setText(timesheet.getDescription());
        if (timesheet.getEZStatusID() == 1) {
            tvAdminNotes.setText(timesheet.getAdminNotes());
        } else {
            llAdminNotes.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            startTimeSheetActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_delete:
                if (timesheet.getEZStatusID() == 0 || timesheet.getEZStatusID() == 1) {
                    new AlertDialog.Builder(this)
                            .setTitle("Confirm Delete!")
                            .setMessage("Are you sure you want to Delete?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    deleteTimesheet();
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                } else {
                    Toast.makeText(getBaseContext(), "Can't be Deleted", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.iv_edit:
                if (timesheet.getEZStatusID() == 0 || timesheet.getEZStatusID() == 1) {
                    Intent intent1 = new Intent(this, EditTimesheet.class);
                    intent1.putExtra(Constants.TIMESHEET_POSITION, timesheetPosition);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent1);
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "Can't be Edited", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_duplicate:
                Intent intent1 = new Intent(this, DuplicateTimesheetActivity.class);
                intent1.putExtra(Constants.TIMESHEET_POSITION, timesheetPosition);
                intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent1);
                finish();
                break;
            case R.id.tv_submit:
                submitTimeSheetOnServer();
        }
    }

    private void submitTimeSheetOnServer() {

        String submitDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").format(Calendar.getInstance().getTimeInMillis());

        if (tableObject.getTimesheetList().get(timesheetPosition).getEZStatusID() == 0
                || tableObject.getTimesheetList().get(timesheetPosition).getEZStatusID() == 1) {
            if (Utilities.isOnline(getBaseContext())) {
                timesheet = tableObject.getTimesheetList().get(timesheetPosition);
                String displayname = null;
                String activityDate = null;
                String projectname = null;
                String taskname = null;
                String classname = null;
                String starttime = null;
                String endtime = null;
                String actualhours = null;
                String descrip = null;
                String adminnotes = null;
                String taskid = null;
                String projectid = null;
                String classrowid = null;
                String billable = null;
                String ezstatusid = null;
                String istime = null;
                String empsubdate = submitDate;
                String empsignature = null;
                String status = null;
                String timeactivityid = null;

                if (tableObject.getDisplayName() != null) {
                    displayname = employeeLogin.getDisplayName();
                }
                if (timesheet.getActivityDate() != null) {
                    activityDate = timesheet.getActivityDate();
                }
                if (timesheet.getProjectFullName() != null) {
                    projectname = timesheet.getProjectFullName();
                }
                if (timesheet.getTaskFullName() != null) {
                    taskname = timesheet.getTaskFullName();
                }
                if (timesheet.getClassFullName() != null) {
                    classname = timesheet.getClassFullName();
                }
                if (timesheet.getStartTime() != null) {
                    starttime = timesheet.getStartTime();
                }
                if (timesheet.getEndTime() != null) {
                    endtime = timesheet.getEndTime();
                }

                actualhours = "" + timesheet.getActualHours();

                if (timesheet.getDescription() != null) {
                    descrip = timesheet.getDescription();
                }
                if (timesheet.getAdminNotes() != null) {
                    adminnotes = timesheet.getAdminNotes();
                }
                taskid = "" + timesheet.getTaskID();
                projectid = "" + timesheet.getProjectID();
                classrowid = "" + timesheet.getClassRowID();
                billable = "" + timesheet.isBillable();
                ezstatusid = "" + timesheet.getEZStatusID();
                istime = "" + timesheet.isTime();
                if (timesheet.getEmpSubDate() != null) {
                    empsubdate = timesheet.getEmpSubDate();
                }
                if (timesheet.getEmpSignature() != null) {
                    empsignature = timesheet.getEmpSignature();
                }
                if (timesheet.getStatus() != null) {
                    status = timesheet.getStatus();
                }
                timeactivityid = "" + timesheet.getTimeActivityID();
                String url = Constants.SUBMIT_NEW_TIMESHEET_API;
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("userid", employeeLogin.getUserid());
                    jsonObject.put("sitecode", employeeLogin.getSiteCode());
                    jsonObject.put("EmployeeFullName", employeeLogin.getDisplayName());
                    jsonObject.put("ActivityDate", timesheet.getActivityDate());
                    jsonObject.put("ProjectFullName", timesheet.getProjectFullName());
                    jsonObject.put("TaskFullName", taskname);
                    jsonObject.put("ClassFullName", classname);
                    jsonObject.put("StartTime", starttime);
                    jsonObject.put("EndTime", endtime);
                    jsonObject.put("ActualHours", actualhours);
                    jsonObject.put("Description", descrip);
                    jsonObject.put("AdminNotes", adminnotes);
                    jsonObject.put("TaskID", taskid);
                    jsonObject.put("ProjectID", projectid);
                    jsonObject.put("ClassRowID", classrowid);
                    jsonObject.put("Billable", billable);
                    jsonObject.put("EZStatusID", ezstatusid);
                    jsonObject.put("IsTime", istime);
                    jsonObject.put("EmpSignature", empsignature);
                    jsonObject.put("EmpSubDate", empsubdate);
                    jsonObject.put("Status", status);
                    jsonObject.put("SourceRefID", 18);
                    jsonObject.put("TimeActivityID", timeactivityid);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i("arsh", "url = " + url);
                Log.i("arsh", "TimeSheet Position = " + timesheetPosition);
                Log.i("arsh", "Display Name = " + employeeLogin.getDisplayName());
                Log.i("arsh", "UserID = " + tableObject.getUserid());
                Log.i("arsh", "Activity Date = " + activityDate);
                Log.i("arsh", "task Name = " + taskname);
                Log.i("arsh", "class Name = " + classname);
                Log.i("arsh", "Project Name" + projectname);
                Log.i("arsh", "start time = " + starttime);
                Log.i("arsh", "end time = " + endtime);
                Log.i("arsh", "Actual hours = " + actualhours);
                Log.i("arsh", "Description = " + descrip);
                Log.i("arsh", "Admin Notes = " + adminnotes);
                Log.i("arsh", "task id = " + taskid);
                Log.i("arsh", "Project id = " + projectid);
                Log.i("arsh", "class row id = " + classrowid);
                Log.i("arsh", "billable = " + billable);
                Log.i("arsh", "EZ Status id = " + ezstatusid);
                Log.i("arsh", "Is time = " + istime);
                Log.i("arsh", "Emp sub date = " + empsubdate);
                Log.i("arsh", "Emp Signature = " + empsignature);
                Log.i("arsh", "status = " + status);
                Log.i("arsh", "time activity id = " + timeactivityid);
                Utilities.showErrorLog(jsonObject.toString());
                JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject
                        , new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        ShowDialog.hideDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.has("TimeActivityID")) {
                                Log.i("arsh", "response = " + s);
                                TimeSheetResponse timeSheetResponse = new GsonBuilder().create().fromJson(s.toString(), TimeSheetResponse.class);
                                tableObject.getTimesheetList().get(timesheetPosition).setTimeActivityID(timeSheetResponse.getTimeActivityID());
                                tableObject.getTimesheetList().get(timesheetPosition).setEZStatusID(timeSheetResponse.getEZStatusID());
                                tableObject.getTimesheetList().get(timesheetPosition).set$id("");
                                Log.i("arsh", "" + timeSheetResponse.getTimeActivityID());
                                Toast.makeText(getBaseContext(), "Submitted Successfully", Toast.LENGTH_SHORT).show();
                                OfflineDataStorage.writeObject(getBaseContext(), tableObject, Constants.TIMESHEETS_FILE);
                                startActivityAfterSubmit();
                            } else {
                                if (jsonObject.has("ErrorMessage")) {
                                    Toast.makeText(getBaseContext(), jsonObject.getString("ErrorMessage"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getBaseContext(), "Request Failed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getBaseContext(), "Exception occured.", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        ShowDialog.hideDialog();
                        Toast.makeText(getBaseContext(), "Timesheet submit failed", Toast.LENGTH_SHORT).show();
                    }
                });
                ShowDialog.showDialog(TimeSheetDetails.this);
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);
            } else {
                Utilities.showNoConnectionToast(TimeSheetDetails.this);
            }
        } else {
            Toast.makeText(getBaseContext(), "Already Submitted", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteTimesheet() {
        if (timesheet.getEZStatusID() == 0 && timesheet.get$id().equalsIgnoreCase("new")) {
            tableObject.getTimesheetList().remove(timesheetPosition);
            OfflineDataStorage.writeObject(getBaseContext(), tableObject, Constants.TIMESHEETS_FILE);
            startActivityAfterDelete();

        } else {
            if (timesheet.getEZStatusID() == 0 || timesheet.getEZStatusID() == 1) {
                if (Utilities.isOnline(getBaseContext())) {
                    String url = Constants.DELETE_TIMESHEET_API;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("userid", employeeLogin.getUserid());
                        jsonObject.put("sitecode", employeeLogin.getSiteCode());
                        jsonObject.put("timeActivityID", timesheet.getTimeActivityID());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                            + "?timeActivityID=" + timesheet.getTimeActivityID()
//                            + "&UserID=" + employeeLogin.getUserid()
//                            + "&SiteCode=" + employeeLogin.getSiteCode();
                    Log.i("arsh", "url=" + url);
                    JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject
                            , new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            ShowDialog.hideDialog();
                            Log.i("arsh", "response=" + s);
                            if (s.equalsIgnoreCase("true")) {
                                tableObject.getTimesheetList().remove(timesheetPosition);
                                Toast.makeText(getBaseContext(), "Timesheet Delete failed", Toast.LENGTH_SHORT).show();
                                OfflineDataStorage.writeObject(getBaseContext(), tableObject, Constants.TIMESHEETS_FILE);
                                startActivityAfterDelete();
                            } else {
                                Toast.makeText(getBaseContext(), "Timesheet Delete failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            ShowDialog.hideDialog();
                            Utilities.handleVolleyError(TimeSheetDetails.this, volleyError);
                        }
                    });
                    ShowDialog.showDialog(TimeSheetDetails.this);
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                } else {
                    Utilities.showNoConnectionToast(TimeSheetDetails.this);
                }
            }
        }
    }

    private void startActivityAfterDelete() {
        Intent intent = new Intent(getBaseContext(), MyTimeSheetTransactions.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void startActivityAfterSubmit() {
        Intent intent = new Intent(getBaseContext(), TimeSheetDetails.class);
        intent.putExtra("id", timesheetPosition);       //timesheet position in the array of timesheets stored offline.
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void startTimeSheetActivity() {
        Intent intent = new Intent(getBaseContext(), TimeSheetActivity.class);
        intent.putExtra("date", timesheet.getActivityDate().substring(0, timesheet.getActivityDate().indexOf("T")));       //timesheet position in the array of timesheets stored offline.
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        startTimeSheetActivity();
        super.onBackPressed();
    }
}