package com.timerewards.tr.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.timerewards.tr.Adapters.ClassesAdapter;
import com.timerewards.tr.Adapters.LeaveTaskAdapter;
import com.timerewards.tr.Adapters.ProjectsAdapter;
import com.timerewards.tr.Adapters.TasksAdapter;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.ApproveTimesheetDataFile;
import com.timerewards.tr.DataFiles.AssignmentModel;
import com.timerewards.tr.DataFiles.ClassesDataFile;
import com.timerewards.tr.DataFiles.ClassesModel;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.LeaveTaskModel;
import com.timerewards.tr.DataFiles.ProjectsDataFile;
import com.timerewards.tr.DataFiles.ProjectsModel;
import com.timerewards.tr.DataFiles.TaskDataFile;
import com.timerewards.tr.DataFiles.TasksModel;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class EditTimesheetInApprovals extends AppCompatActivity implements View.OnClickListener, ProjectsAdapter.ProjectSelection, ClassesAdapter.ClassSelection, TasksAdapter.TasksSelection, LeaveTaskAdapter.LeaveTasksSelection, TextWatcher {

    private Toolbar mToolbar;
    private EditText activity_date, description, start_time, end_time, et_hours, et_admin_notes;
    private TextView text_admin_notes;
    private CheckBox billable;
    private Button saveTimesheet;
    private TextView tvProjectName, tvTaskName, tvClassName, textEditTimesheet, toolbarTV;
    private TextView textProject, textTask, textClass, textStartTime, textEndTime;
    private Dialog dialog;

    private ProjectsDataFile projectsDataFile;
    private TaskDataFile taskDataFile;
    private ClassesDataFile classesDataFile;
    private GetOptionsDataFile getOptionsDataFile;
    private ApproveTimesheetDataFile approveTimesheetDataFile;
    private EmployeeLogin employeeLogin;

    private ArrayList<ProjectsModel> projectsModelArrayList = new ArrayList<>();
    private ArrayList<TasksModel> tasksModelArrayList = new ArrayList<>();
    private ArrayList<ClassesModel> classesModelArrayList = new ArrayList<>();
    private ArrayList<LeaveTaskModel> leaveTaskModelArrayList = new ArrayList<>();
    private ArrayList<AssignmentModel> assignmentModelArrayList = new ArrayList<>();
    private ArrayList<TasksModel> tasksFromAssignmentArrayList = new ArrayList<>();
    private ArrayList<TasksModel> tasksModelArrayListSearch = new ArrayList<>();
    private ArrayList<ClassesModel> classesModelArrayListSearch = new ArrayList<>();
    private ArrayList<ProjectsModel> projectsModelArrayListSearch = new ArrayList<>();

    private ProjectsModel selectedProjectsModel;
    private ClassesModel selectedClassesModel;
    private TasksModel selectedTaskModel;
    private LeaveTaskModel selectedLeaveTaskModel;

    private DatePickerDialog pickerDialog;
    private TimePickerDialog timePickerDialog;
    private SimpleDateFormat simpleDateFormat;

    private RecyclerView recyclerView;
    private ProjectsAdapter projectsAdapter;
    private TasksAdapter tasksAdapter;
    private LeaveTaskAdapter leaveTaskAdapter;
    private ClassesAdapter classesAdapter;

    private boolean isLeavesInTaskList = false;
    private boolean isTasksInTaskList = true;
    private boolean isTaskFromAssignment = false;

    private int projectPosition = 0;
    private int taskPosition = 0;
    private int classPosition = 0;

    private Intent intent;
    private int timesheetPosition;
    private Timesheet timesheetOld;
    private String typeOfListInDialog;
    private EditText searchET;
    private ImageView searchIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_timesheet);
        new MyAsyncTask().execute();
    }

    private void init() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        intent = getIntent();
        textEditTimesheet = (TextView) findViewById(R.id.tv_add_new_timesheet);
        textEditTimesheet.setText("Edit Timesheet");
        toolbarTV = (TextView) findViewById(R.id.toolbarTV);
        toolbarTV.setText("Edit Timesheet");

        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_list);
        recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        searchIcon = (ImageView) dialog.findViewById(R.id.iv_search);
        searchIcon.setImageResource(R.drawable.search_icon);

        searchET = (EditText) dialog.findViewById(R.id.et_search);
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchET.getText().clear();

            }
        });
        searchET.addTextChangedListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        textStartTime = (TextView) findViewById(R.id.text_start_time);
        textEndTime = (TextView) findViewById(R.id.text_end_time);
        textProject = (TextView) findViewById(R.id.text_project);
        textTask = (TextView) findViewById(R.id.text_task);
        textClass = (TextView) findViewById(R.id.text_class);
        tvProjectName = (TextView) findViewById(R.id.tv_project_name);
        tvProjectName.setText("Select " + getOptionsDataFile.getCustomerLex() + " : " + getOptionsDataFile.getProjectLex());
        tvProjectName.setOnClickListener(this);
        tvProjectName.setEnabled(false);
        tvTaskName = (TextView) findViewById(R.id.tv_task_name);
        tvTaskName.setText("Select " + getOptionsDataFile.getTaskLex());
        tvTaskName.setOnClickListener(this);
        tvTaskName.setEnabled(false);
        tvClassName = (TextView) findViewById(R.id.tv_class_name);
        tvClassName.setText("Select " + getOptionsDataFile.getClassLex());
        tvClassName.setOnClickListener(this);
        activity_date = (EditText) findViewById(R.id.et_activity_date);
        activity_date.setOnClickListener(this);
        start_time = (EditText) findViewById(R.id.et_start_time);
        start_time.setText(getOptionsDataFile.getStartTime());
        end_time = (EditText) findViewById(R.id.et_end_time);
        end_time.setText(getOptionsDataFile.getEndTime());
        et_admin_notes = (EditText) findViewById(R.id.et_admin_notes);
        et_admin_notes.setVisibility(View.VISIBLE);
        text_admin_notes = (TextView) findViewById(R.id.text_admin_notes);
        text_admin_notes.setVisibility(View.VISIBLE);
        et_hours = (EditText) findViewById(R.id.et_hours);
        if (getOptionsDataFile.getStartTime().length() > 0
                && getOptionsDataFile.getEndTime().length() > 0) {
            calculateHours();
        }

        start_time.setOnClickListener(this);

        end_time.setOnClickListener(this);

        et_hours.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() > 0
                        && start_time.getText().toString().length() > 0
                        && end_time.getText().toString().length() > 0) {
                    if (editable.toString().equalsIgnoreCase(".")) {
                        et_hours.setText("0.");
                        et_hours.setSelection(et_hours.getText().toString().length());
                    } else {
                        calculateEndTime();
                    }

                }
            }
        });

        billable = (CheckBox) findViewById(R.id.checkbox_billable);
        description = (EditText) findViewById(R.id.et_description);
        saveTimesheet = (Button) findViewById(R.id.button_save_timesheet);
        saveTimesheet.setText("Update On Server");
        saveTimesheet.setOnClickListener(this);
    }

    private void putData() {

        textProject.setText(getOptionsDataFile.getCustomerLex() + " : " + getOptionsDataFile.getProjectLex());
        textTask.setText(getOptionsDataFile.getTaskLex());
        textClass.setText(getOptionsDataFile.getClassLex());

        if (getOptionsDataFile.isUseClassesInTS() == false) {
            textClass.setVisibility(View.GONE);
            tvClassName.setVisibility(View.GONE);
        }
        switch (getOptionsDataFile.getShowBillable()) {
            case 0:
                billable.setVisibility(View.GONE);
                break;
            case 1:
                billable.setChecked(false);
                break;
            case 2:
                billable.setChecked(true);
                break;
            case 3:

                break;
        }

        billable.setChecked(timesheetOld.isBillable());
        activity_date.setText(Utilities.dateToLocalFormat(timesheetOld.getActivityDate()));
        start_time.setText(timesheetOld.getStartTime());
        end_time.setText(timesheetOld.getEndTime());
        if (getOptionsDataFile.getCanEnterFutureTime() == 0) {
            end_time.setEnabled(false);
        }
        if (getOptionsDataFile.getCanEnterFutureTime() == 1) {
            end_time.setEnabled(true);
        }
        if (getOptionsDataFile.isShowStartEndTime() == false) {
            textStartTime.setVisibility(View.GONE);
            textEndTime.setVisibility(View.GONE);
            start_time.setVisibility(View.GONE);
            end_time.setVisibility(View.GONE);
        }
        if (getOptionsDataFile.getShowLeave() == 0) {
            isLeavesInTaskList = false;
            isTasksInTaskList = true;
        } else {
            isLeavesInTaskList = true;
            isTasksInTaskList = false;
        }
        et_hours.setText(String.valueOf(timesheetOld.getActualHours()));
        description.setText(timesheetOld.getDescription());
        if (timesheetOld.getAdminNotes() != null) {
            et_admin_notes.setText(timesheetOld.getAdminNotes());
        }

        for (int i = 0; i < projectsModelArrayList.size(); i++) {
            if (timesheetOld.getProjectID() == projectsModelArrayList.get(i).getProjectID()) {
                projectPosition = i;
                selectedProjectsModel = projectsModelArrayList.get(projectPosition);
                projectsAdapter.changePosition(projectPosition);
                tvProjectName.setText(selectedProjectsModel.getFullName());
                break;
            }
        }

        for (int i = 0; i < classesModelArrayList.size(); i++) {
            if (timesheetOld.getClassRowID() == classesModelArrayList.get(i).getClassRowID()) {
                classPosition = i;
                selectedClassesModel = classesModelArrayList.get(classPosition);
                classesAdapter.changePosition(classPosition);
                tvClassName.setText(selectedClassesModel.getFullName());
                break;
            }
        }

        if (timesheetOld.isTime() == false) {
            isLeavesInTaskList = true;
            isTaskFromAssignment = false;
            isTasksInTaskList = false;
            for (int i = 0; i < leaveTaskModelArrayList.size(); i++) {
                if (timesheetOld.getTaskID() == leaveTaskModelArrayList.get(i).getTaskID()) {
                    taskPosition = i;
                    selectedLeaveTaskModel = leaveTaskModelArrayList.get(taskPosition);
                    leaveTaskAdapter.changePosition(taskPosition);
                    tvTaskName.setText(selectedLeaveTaskModel.getFullName());
                }
            }
        } else {
            if (selectedProjectsModel.getProjectID() != 0) {
                for (int i = 0; i < assignmentModelArrayList.size(); i++) {
                    if (selectedProjectsModel.getProjectID() == assignmentModelArrayList.get(i).getProjectID()) {
                        if (assignmentModelArrayList.get(i).getTaskID() != 0) {
                            for (int j = 0; j < tasksModelArrayList.size(); j++) {
                                if (assignmentModelArrayList.get(i).getTaskID() == tasksModelArrayList.get(j).getTaskID()) {
                                    tasksFromAssignmentArrayList = new ArrayList<>();
                                    TasksModel tasksModel = new TasksModel();
                                    tasksModel.setTaskID(0);
                                    tasksModel.setIsClickable(true);
                                    tasksModel.setTaskName("None");
                                    tasksModel.setSubLevel(0);
                                    tasksModel.setFullName("None");
                                    tasksFromAssignmentArrayList.add(tasksModel);
                                    tasksFromAssignmentArrayList.add(tasksModelArrayList.get(j));
                                    isLeavesInTaskList = false;
                                    isTaskFromAssignment = true;
                                    isTasksInTaskList = false;
                                    taskPosition = 1;
                                    selectedTaskModel = tasksFromAssignmentArrayList.get(taskPosition);
                                    tasksAdapter = new TasksAdapter(this, taskPosition, tasksFromAssignmentArrayList);
                                    tvTaskName.setText(selectedTaskModel.getFullName());
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (!isTaskFromAssignment) {
                for (int i = 0; i < tasksModelArrayList.size(); i++) {
                    isLeavesInTaskList = false;
                    isTaskFromAssignment = false;
                    isTasksInTaskList = true;
                    if (timesheetOld.getTaskID() == tasksModelArrayList.get(i).getTaskID()) {
                        taskPosition = i;
                        selectedTaskModel = tasksModelArrayList.get(taskPosition);
                        tasksAdapter.changePosition(taskPosition);
                        tvTaskName.setText(selectedTaskModel.getFullName());
                    }
                }
            }
        }

        if (isLeavesInTaskList) {
            billable.setVisibility(View.GONE);
        } else {
            billable.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_save_timesheet:
                saveData();
                break;
            case R.id.et_activity_date:
                selectDate();
                break;
            case R.id.et_start_time:
                selectTime(R.id.et_start_time);
                break;
            case R.id.et_end_time:
                selectTime(R.id.et_end_time);
                break;
            case R.id.tv_project_name:
                showProjectDialog();
                break;
            case R.id.tv_task_name:
                showTaskDialog();
                break;
            case R.id.tv_class_name:
                showClassDialog();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void selectTime(int id) {
        final int temp = id;
        final Calendar newCalendar = Calendar.getInstance();

        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.US);
                try {
                    Date date = format.parse("" + hourOfDay + ":" + minute);
                    if (temp == R.id.et_start_time) {
                        start_time.setText(new SimpleDateFormat("HH:mm", Locale.US).format(date.getTime()));
                        if (start_time.getText().toString().length() > 0 && end_time.getText().toString().length() > 0) {
                            calculateHours();
                        }
                    }
                    if (temp == R.id.et_end_time) {
                        end_time.setText(new SimpleDateFormat("HH:mm", Locale.US).format(date.getTime()));
                        if (timesheetOld.getStartTime() != null && timesheetOld.getEndTime() != null) {
                            calculateHours();
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    private void selectDate() {
        Calendar newCalendar = Calendar.getInstance();
        pickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                activity_date.setText(simpleDateFormat.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        pickerDialog.show();
    }

    private void calculateHours() {
        Date dDate1 = null;
        SimpleDateFormat sdfmt1 = new SimpleDateFormat("HH:mm");
        try {
            dDate1 = sdfmt1.parse(start_time.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date dDate2 = null;
        try {
            dDate2 = sdfmt1.parse(end_time.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long timeinMillis = dDate2.getTime() - dDate1.getTime();
        double v = ((float) timeinMillis) / 3600000;
        et_hours.setText(String.format("%.2f", v));
    }

    private void calculateEndTime() {
        long tempHoursInmillis = (long) Math.round(Double.parseDouble(et_hours.getText().toString()) * 1000 * 60 * 60);

        Date dDate1 = null;
        SimpleDateFormat sdfmt1 = new SimpleDateFormat("HH:mm");
        try {
            dDate1 = sdfmt1.parse(start_time.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long endTimeInMillis = dDate1.getTime() + tempHoursInmillis;
        Date date = new Date(endTimeInMillis);
        end_time.setText(sdfmt1.format(date));

    }

    private void saveData() {

        Timesheet timesheetNew = new Timesheet();
        timesheetNew.setEZStatusID(0);
        timesheetNew.set$id("new");
        timesheetNew.setDescription(description.getText().toString());
        timesheetNew.setActivityDate(Utilities.dateToServerFormat(activity_date.getText().toString()));
        timesheetNew.setBillable(billable.isChecked());
        if (et_hours.getText().toString().length() > 0) {
            timesheetNew.setActualHours(Float.parseFloat(et_hours.getText().toString()));
        }
        timesheetNew.setStartTime(start_time.getText().toString());
        timesheetNew.setEndTime(end_time.getText().toString());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            Date date = sdf.parse(activity_date.getText().toString());
            timesheetNew.setTimeInMillis(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (selectedClassesModel.getClassRowID() == 0) {
            timesheetNew.setClassRowID(0);
            timesheetNew.setClassFullName("None");
        } else {
            timesheetNew.setClassRowID(selectedClassesModel.getClassRowID());
            timesheetNew.setClassFullName(selectedClassesModel.getFullName());
        }
        if (selectedProjectsModel.getProjectID() == 0) {
            timesheetNew.setProjectID(0);
            timesheetNew.setProjectFullName("None");
        } else {
            timesheetNew.setProjectID(selectedProjectsModel.getProjectID());
            timesheetNew.setProjectFullName(selectedProjectsModel.getFullName());
        }

        if (selectedTaskModel != null) {
            if (isLeavesInTaskList == true) {
                timesheetNew.setIsTime(false);
                if (selectedLeaveTaskModel.getTaskID() == 0) {
                    timesheetNew.setTaskFullName("None");
                    timesheetNew.setTaskID(0);
                } else {
                    timesheetNew.setTaskFullName(selectedLeaveTaskModel.getFullName());
                    timesheetNew.setTaskID(selectedLeaveTaskModel.getTaskID());
                }
            }
            if (isTasksInTaskList == true) {
                timesheetNew.setIsTime(true);
                if (selectedTaskModel.getTaskID() == 0) {
                    timesheetNew.setTaskID(0);
                    timesheetNew.setTaskFullName("None");
                } else {
                    timesheetNew.setTaskFullName(selectedTaskModel.getFullName());
                    timesheetNew.setTaskID(selectedTaskModel.getTaskID());
                }
            }
            if (isTaskFromAssignment == true) {
                timesheetNew.setIsTime(true);
                if (selectedTaskModel.getTaskID() == 0) {
                    timesheetNew.setTaskID(0);
                    timesheetNew.setTaskFullName("None");
                } else {
                    timesheetNew.setTaskFullName(selectedTaskModel.getFullName());
                    timesheetNew.setTaskID(selectedTaskModel.getTaskID());
                }
            } else {
                timesheetNew.setTaskFullName("None");
                timesheetNew.setTaskID(0);
            }
        }
        timesheetNew.setAdminNotes(et_admin_notes.getText().toString());
        timesheetNew.setUpdatedBy(employeeLogin.getDisplayName());
        timesheetNew.setEmployeeID(employeeLogin.getUserid());
        timesheetNew.setEmployeeFullName(timesheetOld.getEmployeeFullName());
        timesheetNew.setUpdatedDate(Utilities.dateToServerFormat(simpleDateFormat.format(Calendar.getInstance().getTime())));
        updateTimesheetOnServer(timesheetNew);
    }

    private void updateTimesheetOnServer(final Timesheet timesheetNew) {
        String url = "";
        url = Constants.SUBMIT_NEW_TIMESHEET_API;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("timeActivityID", timesheetOld.getTimeActivityID());
            jsonObject.put("EZstatusid", String.valueOf("2"));
            jsonObject.put("UpdatedBy", timesheetNew.getUpdatedBy());
            jsonObject.put("UpdatedDate", timesheetNew.getUpdatedDate());
            jsonObject.put("sourceeRefID", 18);
            //checking if billable changed
            if (timesheetOld.isBillable() == timesheetNew.isBillable()) {
                //skip
            } else {
//                url = url + "&Billable=" + String.valueOf(timesheetNew.isBillable());
                jsonObject.put("Billable", timesheetNew.isBillable());
            }

            //checking if admin notes changed
//            if (et_admin_notes.getText().toString().trim().length() == 0) {
            if (timesheetOld.getAdminNotes() != null && timesheetOld.getAdminNotes().equalsIgnoreCase(timesheetNew.getAdminNotes())) {
                //skip
            } else {
//                url = url + "&AdminNotes=" + URLEncoder.encode(String.valueOf(et_admin_notes.getText().toString()), "UTF-8");
                jsonObject.put("AdminNotes", et_admin_notes.getText().toString());
            }

            //checking if description changed
            if (timesheetOld.getDescription().equalsIgnoreCase(description.getText().toString())) {
                //skip
            } else {
//                url = url + "&Description=" + URLEncoder.encode(String.valueOf(description.getText().toString()), "UTF-8");
                jsonObject.put("Description", description.getText().toString());
            }

            //checking if ActualHours changed
            if (timesheetOld.getActualHours() == timesheetNew.getActualHours()) {
                //skip
            } else {
//                url = url + "&ActualHours=" + URLEncoder.encode(String.valueOf(timesheetNew.getActualHours()), "UTF-8");
//                url = url + "&BillableHours=" + URLEncoder.encode(String.valueOf(timesheetNew.getActualHours()), "UTF-8");
                jsonObject.put("ActualHours", timesheetNew.getActualHours());
                jsonObject.put("BillableHours", timesheetNew.getActualHours());
            }

            //checking if ActivityDate changed
            if (timesheetOld.getActivityDate().equalsIgnoreCase(timesheetNew.getActivityDate())) {
                //skip
            } else {
//                url = url + "&ActivityDate=" + URLEncoder.encode(String.valueOf(timesheetNew.getActivityDate()), "UTF-8");
                jsonObject.put("ActivityDate", timesheetNew.getActivityDate());
            }

            //checking if Class/Customer changed
            if (timesheetOld.getClassRowID() == timesheetNew.getClassRowID()) {
                //skip
            } else {
//                url = url + "&ClassRowID=" + URLEncoder.encode(String.valueOf(timesheetNew.getClassRowID()), "UTF-8");
//                url = url + "&ClassFullName=" + URLEncoder.encode(String.valueOf(timesheetNew.getClassFullName()), "UTF-8");
                jsonObject.put("ClassRowID", timesheetNew.getClassRowID());
                jsonObject.put("ClassFullName", timesheetNew.getClassFullName());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utilities.showErrorLog(url);

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ShowDialog.hideDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("EZStatusID")) {
                        Utilities.showImportantToast(EditTimesheetInApprovals.this, "Update on server");
                        //replace the old with new one
                        approveTimesheetDataFile.getTimesheetArrayList().set(timesheetPosition, timesheetNew);
                        OfflineDataStorage.writeObject(EditTimesheetInApprovals.this, approveTimesheetDataFile, Constants.TIMESHEETS_APPROVE_FILE);
                        finish();
                    }
                    if (jsonObject.has("ErrorMessage")) {
                        Toast.makeText(EditTimesheetInApprovals.this, jsonObject.getString("ErrorMessage"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(EditTimesheetInApprovals.this, error);
            }
        });
        ShowDialog.showDialog(EditTimesheetInApprovals.this, "Please wait");
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    private void showProjectDialog() {
        typeOfListInDialog = "projects";
        recyclerView.setAdapter(projectsAdapter);
        dialog.show();
        if (selectedProjectsModel != null) {
            for (int i = 0; i < projectsModelArrayList.size(); i++) {
                if (projectsModelArrayList.get(i).getProjectID() == projectPosition) {
                    recyclerView.scrollToPosition(i);
                }
            }
        }
    }

    private void showTaskDialog() {

        if (isLeavesInTaskList == true) {
            recyclerView.setAdapter(leaveTaskAdapter);
        }
        if (isTaskFromAssignment == true) {
            tasksAdapter = new TasksAdapter(this, taskPosition, tasksFromAssignmentArrayList);
            recyclerView.setAdapter(tasksAdapter);
        }
        if (isTasksInTaskList == true) {
            tasksAdapter = new TasksAdapter(this, taskPosition, tasksModelArrayList);
            recyclerView.setAdapter(tasksAdapter);
        }

        recyclerView.scrollToPosition(taskPosition);
        dialog.show();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

//    private void showClassDialog() {
//        classesAdapter = new ClassesAdapter(this, classPosition, classesModelArrayList);
//        recyclerView.setAdapter(classesAdapter);
//        recyclerView.scrollToPosition(classPosition);
//        dialog.show();
//    }

    private void showClassDialog() {
        typeOfListInDialog = "class";

        classesAdapter = new ClassesAdapter(this, classPosition, classesModelArrayList);
        recyclerView.setAdapter(classesAdapter);
        if (selectedClassesModel != null) {
            for (int i = 0; i < classesModelArrayList.size(); i++) {
                if (classesModelArrayList.get(i).getClassRowID() == classPosition) {
                    recyclerView.scrollToPosition(i);
                }
            }
        }
        dialog.show();
    }

    @Override
    public void setProject(int position) {
        projectPosition = position;

        for (int i = 0; i < projectsModelArrayList.size(); i++) {
            if (projectsModelArrayList.get(i).getProjectID() == position) {
                selectedProjectsModel = projectsModelArrayList.get(i);
            }
        }
        projectsAdapter.changePosition(selectedProjectsModel.getProjectID());
        tvProjectName.setText(selectedProjectsModel.getFullName());
        dialog.dismiss();

        isTaskFromAssignment = false;
        isTasksInTaskList = true;
        isLeavesInTaskList = false;

        if (projectPosition != 0) {
            for (int i = 0; i < assignmentModelArrayList.size(); i++) {
                if (selectedProjectsModel.getProjectID() == assignmentModelArrayList.get(i).getProjectID()
                        && assignmentModelArrayList.get(i).getTaskID() != 0) {
                    tasksFromAssignmentArrayList = new ArrayList<>();
                    TasksModel tasksModel = new TasksModel();
                    tasksModel.setTaskID(0);
                    tasksModel.setTaskName("None");
                    tasksModel.setFullName("None");
                    tasksModel.setSubLevel(0);
                    tasksModel.setUserID(0);
                    tasksModel.setIsClickable(true);
                    tasksFromAssignmentArrayList.add(tasksModel);
                    for (int j = 0; j < tasksModelArrayList.size(); j++) {
                        if (assignmentModelArrayList.get(i).getTaskID() == tasksModelArrayList.get(j).getTaskID()) {
                            isTaskFromAssignment = true;
                            isLeavesInTaskList = false;
                            isTasksInTaskList = false;
                            taskPosition = 1;
                            tasksFromAssignmentArrayList.add(tasksModelArrayList.get(j));
                            selectedTaskModel = tasksFromAssignmentArrayList.get(1);
                            tvTaskName.setText(selectedTaskModel.getFullName());
                        }
                    }
                }
            }

            if (isTaskFromAssignment == false) {
                isTasksInTaskList = true;
                isLeavesInTaskList = false;
                tvTaskName.setText("None");
                taskPosition = 0;
            }

        } else {

            if (getOptionsDataFile.getShowLeave() == 1) {
                isLeavesInTaskList = true;
                isTaskFromAssignment = false;
                isTasksInTaskList = false;
                tvTaskName.setText("None");
                taskPosition = 0;
                selectedTaskModel = tasksModelArrayList.get(0);
                selectedLeaveTaskModel = leaveTaskModelArrayList.get(0);
            } else {
                isLeavesInTaskList = false;
                isTaskFromAssignment = false;
                isTasksInTaskList = true;
                tvTaskName.setText("None");
                selectedTaskModel = tasksModelArrayList.get(0);
                selectedLeaveTaskModel = leaveTaskModelArrayList.get(0);
            }
        }

        if (projectPosition == 0) {
            billable.setVisibility(View.GONE);
        } else {
            billable.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void setTask(int position) {
        taskPosition = position;
        if (isTaskFromAssignment == true) {

            for (int i = 0; i < tasksModelArrayList.size(); i++) {
                if (tasksModelArrayList.get(i).getTaskID() == position) {

                    selectedTaskModel = tasksModelArrayList.get(i);
                    tvTaskName.setText(selectedTaskModel.getFullName());
                }
            }
            tasksAdapter.changePosition(selectedTaskModel.getTaskID());

//            tasksAdapter.changePosition(taskPosition);
//            selectedTaskModel = tasksFromAssignmentArrayList.get(taskPosition);
//            tvTaskName.setText(selectedTaskModel.getFullName());
        } else {
//            tasksAdapter.changePosition(taskPosition);
//            selectedTaskModel = tasksModelArrayList.get(taskPosition);
//            tvTaskName.setText(selectedTaskModel.getFullName());

            for (int i = 0; i < tasksModelArrayList.size(); i++) {
                if (tasksModelArrayList.get(i).getTaskID() == position) {
                    selectedTaskModel = tasksModelArrayList.get(i);
                    tvTaskName.setText(selectedTaskModel.getFullName());
                }
            }
            tasksAdapter.changePosition(selectedTaskModel.getTaskID());
        }
        dialog.dismiss();
        billable.setVisibility(View.VISIBLE);
    }

    @Override
    public void setLeave(int position) {
        taskPosition = position;
        tasksAdapter.changePosition(taskPosition);
        selectedLeaveTaskModel = leaveTaskModelArrayList.get(taskPosition);
        tvTaskName.setText(selectedLeaveTaskModel.getFullName());
        dialog.dismiss();
        billable.setVisibility(View.GONE);
    }

    @Override
    public void setClass(int position) {
        classPosition = position;
//        classesAdapter.changePosition(classPosition);
//        selectedClassesModel = classesModelArrayList.get(classPosition);
//        tvClassName.setText(selectedClassesModel.getFullName());
//        dialog.dismiss();


        for (int i = 0; i < classesModelArrayList.size(); i++) {
            if (classesModelArrayList.get(i).getClassRowID() == position) {
                selectedClassesModel = classesModelArrayList.get(i);
                tvClassName.setText(selectedClassesModel.getFullName());
            }
            classesAdapter.changePosition(selectedClassesModel.getClassRowID());
        }
        dialog.dismiss();

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if(s.length()>0)
        {
            searchIcon.setImageResource(R.drawable.ic_clear_black_24dp);


        }else
        {
            searchIcon.setImageResource(R.drawable.search_icon);

        }

    }

    @Override
    public void afterTextChanged(Editable s) {
        filterList(s.toString());
    }

    private void filterList(String stringToSearch) {
        if (stringToSearch.trim().length() == 0) {
            if (typeOfListInDialog.equalsIgnoreCase("tasks")) {
                tasksAdapter.changeData(tasksModelArrayList);
                return;
            }
            if (typeOfListInDialog.equalsIgnoreCase("projects")) {
                projectsAdapter.changeData(projectsModelArrayList);
                return;
            }
            if (typeOfListInDialog.equalsIgnoreCase("class")) {
                tasksAdapter.changeData(tasksModelArrayList);
                return;
            }
        }

        if (typeOfListInDialog.equalsIgnoreCase("tasks")) {
            tasksModelArrayListSearch = new ArrayList<>();
            ArrayList<TasksModel> tasksLevel = new ArrayList<>();

            for (int i = 0; i < tasksModelArrayList.size(); i++) {
                TasksModel tasksModel = tasksModelArrayList.get(i);
                tasksModel.setHasAdded(false);
                int subLevel = tasksModel.getSubLevel();
                if (subLevel == 0) {
                    tasksLevel.clear();
                    tasksLevel.add(0, tasksModel);
                }
                if (subLevel == 1) {
                    if (tasksModel.getFullName().contains(tasksLevel.get(subLevel - 1).getFullName())) {
                        tasksLevel.add(subLevel, tasksModel);
                    } else {
                        tasksLevel.clear();
                        String[] taskNames = tasksModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            tasksLevel.add(j, tasksModel);
                        }
                        tasksLevel.add(subLevel, tasksModel);
                    }
                }
                if (subLevel == 2) {
                    if (tasksModel.getFullName().contains(tasksLevel.get(subLevel - 1).getFullName())) {
                        tasksLevel.add(subLevel, tasksModel);
                    } else {
                        String[] taskNames = tasksModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            tasksLevel.add(j, tasksModel);
                        }
                        tasksLevel.add(subLevel, tasksModel);
                    }
                }
                if (subLevel == 3) {
                    if (tasksModel.getFullName().contains(tasksLevel.get(subLevel - 1).getFullName())) {
                        tasksLevel.add(subLevel, tasksModel);
                    } else {
                        String[] taskNames = tasksModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            tasksLevel.add(j, tasksModel);
                        }
                        tasksLevel.add(subLevel, tasksModel);
                    }
                }
                if (subLevel == 4) {
                    if (tasksModel.getFullName().contains(tasksLevel.get(subLevel - 1).getFullName())) {
                        tasksLevel.add(subLevel, tasksModel);
                    } else {
                        String[] taskNames = tasksModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            tasksLevel.add(j, tasksModel);
                        }
                        tasksLevel.add(subLevel, tasksModel);
                    }
                }
                if (subLevel == 5) {
                    if (tasksModel.getFullName().contains(tasksLevel.get(subLevel - 1).getFullName())) {
                        tasksLevel.add(subLevel, tasksModel);
                    } else {
                        String[] taskNames = tasksModel.getFullName().split(":");
                        int count = taskNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            TasksModel tempModel = new TasksModel();
                            tempModel.setSubLevel(j);
                            tempModel.setTaskName(taskNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(taskNames[j]);
                            tasksLevel.add(j, tasksModel);
                        }
                        tasksLevel.add(subLevel, tasksModel);
                    }
                }

                if (tasksModelArrayList.get(i).getTaskName().toLowerCase().contains(stringToSearch.toLowerCase())) {
                    int subLevelTemp = tasksModel.getSubLevel();
                    for (int j = 0; j <= subLevelTemp - 1; j++) {
                        if (tasksLevel.get(j).isHasAdded() == false) {
                            tasksModelArrayListSearch.add(tasksLevel.get(j));
                            tasksLevel.get(j).setHasAdded(true);
                        }
                    }
                    tasksModelArrayListSearch.add(tasksModel);
                    tasksModel.setHasAdded(true);
                    tasksLevel.add(subLevel, tasksModel);
                }
            }

            tasksAdapter.changeData(tasksModelArrayListSearch);
        }

        if (typeOfListInDialog.equalsIgnoreCase("projects")) {
            projectsModelArrayListSearch = new ArrayList<>();
            ArrayList<ProjectsModel> projectLevel = new ArrayList<>();

            for (int i = 0; i < projectsModelArrayList.size(); i++) {
                ProjectsModel projectsModel = projectsModelArrayList.get(i);
                projectsModel.setHasAdded(false);
                int subLevel = projectsModel.getSubLevel();
                if (subLevel == 0) {
                    projectLevel.clear();
                    projectLevel.add(0, projectsModel);
                }
                if (subLevel == 1) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        projectLevel.clear();
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 2) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.set(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 3) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 4) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }
                if (subLevel == 5) {
                    if (projectsModel.getFullName().contains(projectLevel.get(subLevel - 1).getFullName())) {
                        projectLevel.add(subLevel, projectsModel);
                    } else {
                        String[] projectNames = projectsModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ProjectsModel tempModel = new ProjectsModel();
                            tempModel.setSubLevel(j);
                            tempModel.setProjectName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            projectLevel.add(j, projectsModel);
                        }
                        projectLevel.add(subLevel, projectsModel);
                    }
                }

                if (projectsModelArrayList.get(i).getProjectName().toLowerCase().contains(stringToSearch.toLowerCase())) {
                    int subLevelTemp = projectsModel.getSubLevel();
                    for (int j = 0; j <= subLevelTemp - 1; j++) {
                        if (projectLevel.get(j).isHasAdded() == false) {
                            projectsModelArrayListSearch.add(projectLevel.get(j));
                            projectLevel.get(j).setHasAdded(true);
                        }
                    }

                    projectsModelArrayListSearch.add(projectsModel);
                    projectsModel.setHasAdded(true);
                    projectLevel.add(subLevel, projectsModel);
                }
            }

            projectsAdapter.changeData(projectsModelArrayListSearch);
        }


        if (typeOfListInDialog.equalsIgnoreCase("class")) {

            classesModelArrayListSearch = new ArrayList<>();
            ArrayList<ClassesModel> classLevel = new ArrayList<>();

            for (int i = 0; i < classesModelArrayList.size(); i++) {
                ClassesModel classesModel = classesModelArrayList.get(i);
                classesModel.setHasAdded(false);
                int subLevel = classesModel.getSubLevel();
                if (subLevel == 0) {
                    classLevel.clear();
                    classLevel.add(0, classesModel);
                }
                if (subLevel == 1) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        classLevel.clear();
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 2) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.set(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 3) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 4) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }
                if (subLevel == 5) {
                    if (classesModel.getFullName().contains(classLevel.get(subLevel - 1).getFullName())) {
                        classLevel.add(subLevel, classesModel);
                    } else {
                        String[] projectNames = classesModel.getFullName().split(":");
                        int count = projectNames.length;
                        for (int j = 0; j < count - 1; j++) {
                            ClassesModel tempModel = new ClassesModel();
                            tempModel.setSubLevel(j);
                            tempModel.setClassName(projectNames[j]);
                            tempModel.setIsClickable(false);
                            tempModel.setFullName(projectNames[j]);
                            classLevel.add(j, classesModel);
                        }
                        classLevel.add(subLevel, classesModel);
                    }
                }

                if (classesModelArrayList.get(i).getClassName().toLowerCase().contains(stringToSearch.toLowerCase())) {
                    int subLevelTemp = classesModel.getSubLevel();
                    for (int j = 0; j <= subLevelTemp - 1; j++) {
                        if (classLevel.get(j).isHasAdded() == false) {
                            classesModelArrayListSearch.add(classLevel.get(j));
                            classLevel.get(j).setHasAdded(true);
                        }
                    }

                    classesModelArrayListSearch.add(classesModel);
                    classesModel.setHasAdded(true);
                    classLevel.add(subLevel, classesModel);
                }
            }

            classesAdapter.changeData(classesModelArrayListSearch);
        }

    }

    class MyAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowDialog.showDialog(EditTimesheetInApprovals.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            approveTimesheetDataFile = (ApproveTimesheetDataFile) OfflineDataStorage.readObject(EditTimesheetInApprovals.this, Constants.TIMESHEETS_APPROVE_FILE);
            projectsDataFile = (ProjectsDataFile) OfflineDataStorage.readObject(EditTimesheetInApprovals.this, Constants.PROJECTS_DATA_FILE);
            taskDataFile = (TaskDataFile) OfflineDataStorage.readObject(EditTimesheetInApprovals.this, Constants.TASKS_DATA_FILE);
            classesDataFile = (ClassesDataFile) OfflineDataStorage.readObject(EditTimesheetInApprovals.this, Constants.CLASSES_DATA_FILE);
            employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(EditTimesheetInApprovals.this, Constants.EMPLOYEE_LOGIN_FILE);
            getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(EditTimesheetInApprovals.this, Constants.GETOPTIONS_FILE);
            projectsModelArrayList = projectsDataFile.getProjectsModelList();
            projectsModelArrayList = Utilities.filterBasedOnExpPrj(projectsModelArrayList);
            tasksModelArrayList = taskDataFile.getTasksModelList();
            tasksModelArrayListSearch= taskDataFile.getTasksModelList();
            assignmentModelArrayList = taskDataFile.getAssignmentModels();
            leaveTaskModelArrayList = taskDataFile.getLeaveTaskModels();
            classesModelArrayList = classesDataFile.getClassesModelList();
            classesModelArrayListSearch = classesDataFile.getClassesModelList();

            projectsAdapter = new ProjectsAdapter(EditTimesheetInApprovals.this, 0, projectsModelArrayList);

            tasksAdapter = new TasksAdapter(EditTimesheetInApprovals.this, 0, tasksModelArrayList);
            leaveTaskAdapter = new LeaveTaskAdapter(EditTimesheetInApprovals.this, 0, leaveTaskModelArrayList);

            classesAdapter = new ClassesAdapter(EditTimesheetInApprovals.this, 0, classesModelArrayList);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowDialog.hideDialog();
            init();
            intent = getIntent();
            timesheetPosition = intent.getIntExtra(Constants.TIMESHEET_POSITION, -1);
            timesheetOld = approveTimesheetDataFile.getTimesheetArrayList().get(timesheetPosition);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Edit Timesheet");
            mToolbar.setTitleTextColor(Color.WHITE);
            final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
            upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            start_time.clearFocus();
            end_time.clearFocus();
            putData();
        }
    }

}