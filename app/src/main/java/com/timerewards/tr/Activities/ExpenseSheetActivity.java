package com.timerewards.tr.Activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Adapters.ExpenseSheetAdapter;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.ExpenseHeaderSubmitResponse;
import com.timerewards.tr.DataFiles.ExpenseItemModel;
import com.timerewards.tr.DataFiles.ExpenseItemSubmitResponse;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.PhotoUploadResponse;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.PhotoMultipartRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

public class ExpenseSheetActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvDate, tvAddNewExpense;
    ListView listView;
    ExpenseSheetAdapter expenseSheetAdapter;
    Intent intent;
    Toolbar toolbar;
    ExpenseDataFile expenseDataFile;
    ArrayList<Expense> expenseArrayListFromServer = new ArrayList<>();
    ArrayList<Expense> expenseArrayListOLD = new ArrayList<>();
    EmployeeLogin employeeLogin;
    TextView textReason;
    EditText etApproverSignature;
    Button dialogOkButton;
    Button dialogCancelButton;
    int numOfRequests = 0;
    int numOfItems = 0;
    GetOptionsDataFile getOptionsDataFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timesheets);
        expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(ExpenseSheetActivity.this, Constants.EXPENSE_DATA_FILE);
        generateTimeMillis();
        OfflineDataStorage.writeObject(this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
        Collections.sort(expenseDataFile.getExpense(), Expense.ExpenseComparator);
        OfflineDataStorage.writeObject(this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(this, Constants.EMPLOYEE_LOGIN_FILE);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(this, Constants.GETOPTIONS_FILE);
        if (getOptionsDataFile.isEmpSigRequiredforExp()) {
            if (employeeLogin.getEmployeeSignature() != null && employeeLogin.getEmployeeSignature().length() > 0) {

            } else {
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.dialog_set_emp_signature);
                dialog.setTitle("Set Your Signature");
                textReason = (TextView) dialog.findViewById(R.id.text_reason);
                textReason.setVisibility(View.GONE);
                etApproverSignature = (EditText) dialog.findViewById(R.id.et_dialog_approver_signature);
                dialogOkButton = (Button) dialog.findViewById(R.id.button_dialog_ok);
                dialogCancelButton = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                dialog.setCancelable(false);
                dialogOkButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (etApproverSignature.getText().toString().length() > 0) {
                            employeeLogin.setEmployeeSignature(etApproverSignature.getText().toString());
                            OfflineDataStorage.writeObject(ExpenseSheetActivity.this, employeeLogin, Constants.EMPLOYEE_LOGIN_FILE);
                            dialog.dismiss();
                        } else {
                            etApproverSignature.setError("Can't be left blank!");
                        }
                    }
                });

                dialogCancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

                dialog.show();
            }
        }
        init();
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Expenses");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        listView.setAdapter(expenseSheetAdapter);
        if (getIntent().hasExtra(Constants.RELOAD_LIST)) {
            getExpensesFromServer();
        }
    }

    private void generateTimeMillis() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        for (int i = 0; i < expenseDataFile.getExpense().size(); i++) {
            try {
                Long time = simpleDateFormat.parse(expenseDataFile.getExpense().get(i).getExpHeader().getEntryDate()).getTime();
                expenseDataFile.getExpense().get(i).getExpHeader().setTimeInMillis(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvAddNewExpense = (TextView) findViewById(R.id.tv_add_timehseet);
        tvAddNewExpense.setText("+Add New Expense");
        tvAddNewExpense.setOnClickListener(this);
        tvDate = (TextView) findViewById(R.id.tv_date);
        tvDate.setVisibility(View.GONE);
        listView = (ListView) findViewById(R.id.listView_dates);
        expenseSheetAdapter = new ExpenseSheetAdapter(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_submit_delete_all_with_refresh, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            goToParentActivity();
        }
        if (item.getItemId() == R.id.refresh) {
            if (Utilities.isOnline(this)) {
                syncExpenses();
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }
        if (item.getItemId() == R.id.submit_all) {
            numOfRequests = 0;
            if (Utilities.isOnline(this)) {
                for (int i = 0; i < expenseDataFile.getExpense().size(); i++) {
                    Expense expense = expenseDataFile.getExpense().get(i);
                    if (expense.getExpenseItemList().size() > 0) {
                        submitWholeExpense(i);
                    }
                }
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }
        if (item.getItemId() == R.id.delete_all) {

            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(ExpenseSheetActivity.this);

            alertDialogBuilder.setTitle("This will delete all your on-hold or locally saved data.");

            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                            numOfRequests = 0;
                            for (int i = 0; i < expenseDataFile.getExpense().size(); i++) {
                                if (expenseDataFile.getExpense().get(i).getExpenseItemList().size() > 0) {
                                    deleteWholeExpense(i);
                                }
                            }
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.show();


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_add_timehseet:
                Intent intent = new Intent(ExpenseSheetActivity.this, NewExpenseSheet.class);
                startActivity(intent);
                break;
        }
    }

    private void submitWholeExpense(final int expenseSheetPosition) {
        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().getEmployeeExpenseID() == 0) {
            if (Utilities.isOnline(this)) {
                submitOnServer(expenseSheetPosition);
            } else {
                Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else {
            submitAllExpenseItems(expenseSheetPosition);
        }
    }

    private void deleteWholeExpense(final int expenseSheetPosition) {
        ArrayList<ExpenseItemModel> expenseItemModelArrayList = expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList();
        for (int i = 0; i < expenseItemModelArrayList.size(); i++) {
            if (!(expenseItemModelArrayList.get(i).isHasDeletedInDevice() || expenseItemModelArrayList.get(i).isMarkedForDeletion())) {
                if (expenseItemModelArrayList.get(i).getEZStatusID() == 0 || expenseItemModelArrayList.get(i).getEZStatusID() == 1) {
                    if (expenseItemModelArrayList.get(i).getEmployeeExpenseItemID() == 0) {
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(i).setHasDeletedInDevice(true);
                        OfflineDataStorage.writeObject(this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                        continue;
                    } else {
                        deleteExpenseItem(expenseSheetPosition, i);
                    }
                }
            }
        }
    }

    private void submitAllExpenseItems(final int expenseSheetPosition) {
        ArrayList<ExpenseItemModel> expenseItemModelArrayList = expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList();
        for (int i = 0; i < expenseItemModelArrayList.size(); i++) {
            if (Utilities.isOnline(this)) {
                if (!(expenseItemModelArrayList.get(i).isHasDeletedInDevice() || expenseItemModelArrayList.get(i).isMarkedForDeletion())) {
                    if (expenseItemModelArrayList.get(i).getEZStatusID() == 0 || expenseItemModelArrayList.get(i).getEZStatusID() == 1) {
                        submitExpenseItemOnServer(expenseSheetPosition, i);
                    }
                }
            } else {
                Utilities.showNoConnectionToast(ExpenseSheetActivity.this);
            }
        }
    }

    private void submitOnServer(final int expenseSheetPosition) {

        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(ExpenseSheetActivity.this, Constants.EMPLOYEE_LOGIN_FILE);

        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().getEmployeeExpenseID() == 0) {
            Expense expense = expenseDataFile.getExpense().get(expenseSheetPosition);
            if (expense.getExpHeader().getEmployeeExpenseID() == 0) {
                String url = null;
                if (Utilities.isOnline(ExpenseSheetActivity.this)) {
                    url = Constants.SUBMIT_EXPENSE_HEADER_API;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("UserId", employeeLogin.getUserid());
                        jsonObject.put("SiteCode", employeeLogin.getSiteCode());
                        jsonObject.put("EmployeeExpenseID", 0);
                        jsonObject.put("ExpenseReason", expense.getExpHeader().getDescription());
                        jsonObject.put("ExpenseDate", expense.getExpHeader().getEntryDate());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            Log.i("arsh", "response=" + s);
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.has("EmployeeExpenseID")) {
                                    ExpenseHeaderSubmitResponse expenseHeaderSubmitResponse = new GsonBuilder().create().fromJson(s.toString(), ExpenseHeaderSubmitResponse.class);
                                    expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().setEmployeeExpenseID(expenseHeaderSubmitResponse.getEmployeeExpenseID());
                                    for (int i = 0; i < expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().size(); i++) {
                                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(i).setEmployeeExpenseID(expenseHeaderSubmitResponse.getEmployeeExpenseID());
                                    }
                                    expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().set$id("");
                                    OfflineDataStorage.writeObject(ExpenseSheetActivity.this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                                    submitAllExpenseItems(expenseSheetPosition);
                                } else {
                                    Toast.makeText(ExpenseSheetActivity.this, "Request Failed", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(ExpenseSheetActivity.this, "Request Failed", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                            hideProgressDialog();

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            hideProgressDialog();
                            Utilities.handleVolleyError(ExpenseSheetActivity.this, volleyError);
                        }
                    });
                    showProgressDialog();
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                } else {
                    Utilities.showNoConnectionToast(ExpenseSheetActivity.this);
                }
            }
        }
    }

    private void submitExpenseItemOnServer(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(ExpenseSheetActivity.this, Constants.EMPLOYEE_LOGIN_FILE);
        String submitDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").format(Calendar.getInstance().getTimeInMillis());
        Expense expense = expenseDataFile.getExpense().get(expenseSheetPosition);
        String url = null;
        JSONObject jsonObject = new JSONObject();
        url = Constants.SUBMIT_EXPENSE_ITEM_API;
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("EmployeeExpenseID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseID());
            jsonObject.put("EmployeeExpenseItemID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID());
            jsonObject.put("ExpenseDate", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getExpenseDate());
            jsonObject.put("ExpenseAmount", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getExpenseAmount());
            jsonObject.put("PaidBy", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getPaidBy());
            jsonObject.put("IsReimbursable", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isReimbursable());
            jsonObject.put("Billable", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isBillable());
            jsonObject.put("Merchant", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getMerchant());
            jsonObject.put("Description", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getDescription());
            jsonObject.put("ProjectID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getProjectID());
            jsonObject.put("AccountID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getAccountID());
            jsonObject.put("ClassRowID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getClassRowID());
            jsonObject.put("EmployeeFullNme", employeeLogin.getDisplayName());
            jsonObject.put("CCAccountName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getCCAccountName());
            jsonObject.put("ProjectFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getProjectFullName());
            jsonObject.put("AccountFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getAccountFullName());
            jsonObject.put("ClassFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getClassFullName());
            jsonObject.put("SourceRefID", "18");
            jsonObject.put("EmpSubDate", submitDate);
            jsonObject.put("Qty", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getQty());
            jsonObject.put("Price", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getPrice());
            jsonObject.put("EmpSignature", employeeLogin.getEmployeeSignature());
            jsonObject.put("IsDefaultGLAnItem", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isDefaultGLAnItem());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("arsh", "url = " + url);

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("arsh", "response=" + s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has("EmployeeExpenseItemID")) {
                        numOfItems++;
                        ExpenseItemSubmitResponse expenseItemSubmitResponse = new GsonBuilder().create().fromJson(s.toString(), ExpenseItemSubmitResponse.class);
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setEmployeeExpenseItemID(expenseItemSubmitResponse.getEmployeeExpenseItemID());
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setEZStatusID(expenseItemSubmitResponse.getEZStatusID());
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).set$id("");
                        OfflineDataStorage.writeObject(ExpenseSheetActivity.this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getReceiptName().length() > 0
                                && expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isHasReceiptUploadedOnServer() == false) {
                            uploadImage(expenseSheetPosition, expenseItemPosition);
                        } else {
//                                Toast.makeText(ExpenseSheetActivity.this, "Submitted successfully", Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                            if(jsonObject.has("ErrorMessage")){
//                                Toast.makeText(getBaseContext(), jsonObject.getString("ErrorMessage"), Toast.LENGTH_SHORT).show();
//                            }
                    }
                } catch (JSONException e) {
                    Toast.makeText(ExpenseSheetActivity.this, "Exception Occured", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Utilities.handleVolleyError(ExpenseSheetActivity.this, volleyError);
                hideProgressDialog();
            }
        });
        showProgressDialog();
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void uploadImage(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(ExpenseSheetActivity.this, Constants.EMPLOYEE_LOGIN_FILE);
        String url = "";

        url = Constants.UPLOAD_EXPENSE_RECEIPTS_API;
        String filename = expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getReceiptName();
        File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(f, filename);

        if (file.exists()) {

            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
            entityBuilder.addPart("file", new FileBody(file));
            entityBuilder.addTextBody("userid", String.valueOf(employeeLogin.getUserid()));
            entityBuilder.addTextBody("sitecode", String.valueOf(employeeLogin.getSiteCode()));
            entityBuilder.addTextBody("employeeexpenseitemid", String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID()));
            entityBuilder.setLaxMode().setBoundary("xx").setCharset(null);

            Log.e("file", file.getName());
            PhotoMultipartRequest multipartRequest = new PhotoMultipartRequest(url, entityBuilder, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    hideProgressDialog();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("uploaded")) {
                            PhotoUploadResponse photoUploadResponse = new GsonBuilder().create().fromJson(response, PhotoUploadResponse.class);
                            if (photoUploadResponse.getUploaded().equalsIgnoreCase("true")) {
                            } else {
                                Toast.makeText(ExpenseSheetActivity.this, "Receipt not uploaded", Toast.LENGTH_SHORT).show();
                            }
                            Log.e("response", response);
                        }
                    } catch (JSONException e) {
                        Toast.makeText(ExpenseSheetActivity.this, "Exception Occured", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressDialog();
                    Utilities.handleVolleyError(getBaseContext(), error);
                }
            });
            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    100000000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            try {
                Log.e("sdf", multipartRequest.getBodyContentType());
                Log.e("gbh", multipartRequest.getUrl());
                Log.e("fd", multipartRequest.getBody()[0] + "");

                for (Map.Entry e : multipartRequest.getHeaders().entrySet()) {
                    Log.e("key", e.getKey() + "");
                    Log.e("value", e.getValue() + "");
                }

            } catch (AuthFailureError authFailureError) {
                authFailureError.printStackTrace();
            }
            showProgressDialog();
            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getRequestQueue().add(multipartRequest);
        } else {
            Toast.makeText(ExpenseSheetActivity.this, "Receipt Image Not found", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteExpenseItem(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(ExpenseSheetActivity.this, Constants.EMPLOYEE_LOGIN_FILE);
        if (Utilities.isOnline(ExpenseSheetActivity.this)) {
            String url = null;
            url = Constants.DELETE_EXPENSE_ITEM_API;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("userid", employeeLogin.getUserid());
                jsonObject.put("sitecode", employeeLogin.getSiteCode());
                jsonObject.put("EmployeeExpenseID", "0");
                jsonObject.put("EmployeeExpenseItemID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    Log.i("arsh", "response=" + s);
                    if (s.equalsIgnoreCase("true")) {
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setHasDeletedInDevice(true);
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setIsMarkedForDeletion(true);
                        OfflineDataStorage.writeObject(ExpenseSheetActivity.this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                        numOfItems++;
                    } else {
                    }
                    hideProgressDialog();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Utilities.handleVolleyError(ExpenseSheetActivity.this, volleyError);
                    hideProgressDialog();
                }
            });

            showProgressDialog();
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        } else {
            Utilities.showNoConnectionToast(ExpenseSheetActivity.this);
        }
    }

    private void showProgressDialog() {
        numOfRequests++;
        ShowDialog.showDialog(ExpenseSheetActivity.this);
    }

    private void hideProgressDialog() {
        numOfRequests--;
        if (numOfRequests < 1) {
            ShowDialog.hideDialog();
//            Toast.makeText(this, "Request processed for " + numOfItems + " Items", Toast.LENGTH_SHORT).show();
            numOfItems = 0;
            numOfRequests = 0;
            Intent intent = new Intent(this, ExpenseSheetActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    private void syncExpenses() {
        new AlertDialog.Builder(this)
                .setTitle("Confirm Sync!")
                .setMessage("Do you want to Sync Expenses")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        getExpensesFromServer();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void getExpensesFromServer() {

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);

        String url = "";
        url = Constants.EXPENSES_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            if (getIntent().hasExtra(Constants.RELOAD_LIST)) {
                jsonObject.put("startDate", sharedPreferences.getString(Constants.EXPENSE_LAST_SYNC_DATE, ""));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utilities.showErrorLog(url);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                ExpenseDataFile expenseDataFile = new GsonBuilder().create().fromJson(s, ExpenseDataFile.class);
                expenseArrayListFromServer = expenseDataFile.getExpense();
                MyTask myTask = new MyTask();
                myTask.execute();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(ExpenseSheetActivity.this, volleyError);
            }
        });
        ShowDialog.showDialog(ExpenseSheetActivity.this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToParentActivity();
    }

    private void goToParentActivity() {
        if (MainActivity.isOpen == false) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        finish();
    }

    private class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            SimpleDateFormat simpleDateFormatServer = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            expenseArrayListOLD = expenseDataFile.getExpense();

            for (int i = 0; i < expenseArrayListFromServer.size(); i++) {
                boolean headerFound = false;
                for (int j = 0; j < expenseArrayListOLD.size(); j++) {
                    if (expenseArrayListFromServer.get(i).getExpHeader().getEmployeeExpenseID() ==
                            expenseArrayListOLD.get(j).getExpHeader().getEmployeeExpenseID()) {

                        headerFound = true;
                        if (expenseArrayListFromServer.get(i).getExpHeader().getUpdatedDate() == null) {

                        }
                        if (expenseArrayListOLD.get(j).getExpHeader().getUpdatedDate() == null) {
                            expenseArrayListOLD.get(j).setExpHeader(expenseArrayListFromServer.get(i).getExpHeader());
                        }
                        if (expenseArrayListOLD.get(j).getExpHeader().getUpdatedDate() != null
                                && expenseArrayListFromServer.get(i).getExpHeader().getUpdatedDate() != null) {
                            try {
                                Date dateServerExpense = simpleDateFormatServer.parse(expenseArrayListFromServer.get(i).getExpHeader().getUpdatedDate());
                                Date dateLocalExpense = simpleDateFormatServer.parse(expenseArrayListOLD.get(j).getExpHeader().getUpdatedDate());
                                if (dateLocalExpense.before(dateServerExpense)) {
                                    expenseArrayListOLD.get(j).setExpHeader(expenseArrayListFromServer.get(i).getExpHeader());
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                        ArrayList<ExpenseItemModel> expenseItemModelArrayListServer = expenseArrayListFromServer.get(i).getExpenseItemList();
                        ArrayList<ExpenseItemModel> expenseItemModelArrayListOLD = expenseArrayListOLD.get(j).getExpenseItemList();

                        for (int k = 0; k < expenseItemModelArrayListServer.size(); k++) {
                            boolean itemFound = false;
                            for (int l = 0; l < expenseItemModelArrayListOLD.size(); l++) {
                                if (expenseItemModelArrayListServer.get(k).getEmployeeExpenseItemID() ==
                                        expenseItemModelArrayListOLD.get(l).getEmployeeExpenseItemID()) {
                                    itemFound = true;
                                    if (expenseItemModelArrayListServer.get(k).getUpdatedDate() == null) {
                                        continue;
                                    }
                                    if (expenseItemModelArrayListOLD.get(l).getUpdatedDate() == null) {
                                        expenseItemModelArrayListOLD.set(l, expenseItemModelArrayListServer.get(k));
                                        continue;
                                    }
                                    if (expenseItemModelArrayListOLD.get(l).getUpdatedDate() != null
                                            && expenseItemModelArrayListServer.get(k).getUpdatedDate() != null) {
                                        try {
                                            Date dateServerExpense = simpleDateFormatServer.parse(expenseItemModelArrayListServer.get(k).getUpdatedDate());
                                            Date dateLocalExpense = simpleDateFormatServer.parse(expenseItemModelArrayListOLD.get(l).getUpdatedDate());
                                            if (dateLocalExpense.before(dateServerExpense)) {
                                                expenseItemModelArrayListOLD.set(l, expenseItemModelArrayListServer.get(k));
                                            }
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                            if (!itemFound) {
                                expenseItemModelArrayListOLD.add(expenseItemModelArrayListServer.get(k));
                            }

                        }

                        expenseArrayListOLD.get(j).setExpenseItemList(expenseItemModelArrayListOLD);

                    }
                }

                if (!headerFound) {
                    expenseArrayListOLD.add(expenseArrayListFromServer.get(i));
                }

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            expenseDataFile.setExpense(expenseArrayListOLD);
            OfflineDataStorage.writeObject(ExpenseSheetActivity.this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Constants.EXPENSE_LAST_SYNC_DATE, sdf.format(new Date(Calendar.getInstance().getTimeInMillis())));
            editor.apply();
            Toast.makeText(ExpenseSheetActivity.this, "Expenses synced", Toast.LENGTH_SHORT).show();
            expenseSheetAdapter.notifyDataSetChanged();
            ShowDialog.hideDialog();
            expenseSheetAdapter = new ExpenseSheetAdapter(ExpenseSheetActivity.this);
            listView.setAdapter(expenseSheetAdapter);
        }
    }
}