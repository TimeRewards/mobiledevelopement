package com.timerewards.tr.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.ApproveTimesheetDataFile;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class ApproveTimesheetDetails extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private Intent intent;
    private String date;
    private int timesheetPosition;
    private Timesheet timesheet;
    private ApproveTimesheetDataFile approveTimesheetDataFile;
    private TextView mCustomer, mProject, mTask, mDate, mHours, mStatus, mDescription, mBillable, mAccept, mReject, mEmployeeName;
    private EmployeeLogin employeeLogin;
    private ImageView ivEdit;
    private String rejectReason;
    private GetOptionsDataFile getOptionsDataFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approve_timesheet_detail);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(this, Constants.GETOPTIONS_FILE);
        init();
        intent = getIntent();
        timesheetPosition = intent.getIntExtra(Constants.TIMESHEET_POSITION, -1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Timesheet Details");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mCustomer = (TextView) findViewById(R.id.tv_customer);
        mProject = (TextView) findViewById(R.id.tv_project);
        mTask = (TextView) findViewById(R.id.tv_task);
        mDate = (TextView) findViewById(R.id.tv_date);
        mHours = (TextView) findViewById(R.id.tv_hours);
        mBillable = (TextView) findViewById(R.id.tv_billable);
        mStatus = (TextView) findViewById(R.id.tv_status);
        mDescription = (TextView) findViewById(R.id.tv_description);
        mAccept = (TextView) findViewById(R.id.tv_accept_timesheet);
        mAccept.setOnClickListener(this);
        mReject = (TextView) findViewById(R.id.tv_reject_timesheet);
        mReject.setOnClickListener(this);
        mEmployeeName = (TextView) findViewById(R.id.tv_employee_name);
        ivEdit = (ImageView) findViewById(R.id.iv_edit);
        ivEdit.setOnClickListener(this);
        if (getOptionsDataFile.isCanApproverEditTS()) {
            ivEdit.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new MyAsyncTask().execute();
    }

    private void setDataIntoFields() {
        mDate.setText(Utilities.dateToLocalFormat(timesheet.getActivityDate()));
        mCustomer.setText(timesheet.getClassFullName());
        mProject.setText(timesheet.getProjectFullName());
        mTask.setText(timesheet.getTaskFullName());
        int ii = timesheet.getTaskID();
        mHours.setText(String.valueOf(timesheet.getActualHours()));
        if (timesheet.isBillable()) {
            mBillable.setText("Billable");
        } else {
            mBillable.setText("Not Billable");
        }
        mStatus.setText(Utilities.intToStatus(timesheet.getEZStatusID()));
        mEmployeeName.setText("Employee Name : " + timesheet.getEmployeeFullName());
        mDescription.setText(timesheet.getDescription());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        final Intent intent = new Intent(this, MyTimeSheetTransactions.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        switch (view.getId()) {
            case R.id.iv_edit:
                Intent i = new Intent(this, EditTimesheetInApprovals.class);
                i.putExtra(Constants.TIMESHEET_POSITION, timesheetPosition);
                startActivity(i);
                finish();
                break;
            case R.id.tv_accept_timesheet:
                if (Utilities.isOnline(this)) {
                    acceptRejectTimeSheet(timesheetPosition, true);
                } else {
                    Utilities.showNoConnectionToast(this);
                }
                break;
            case R.id.tv_reject_timesheet:
                if (Utilities.isOnline(this)) {
                    final Dialog dialog = new Dialog(this);
                    dialog.setContentView(R.layout.dialog_reject_reason);
                    dialog.setTitle("Please Enter Reject Reason");
                    final EditText etReason = (EditText) dialog.findViewById(R.id.et_dialog_reject_reason);
                    Button buttonReject = (Button) dialog.findViewById(R.id.button_dialog_ok);
                    Button buttonCancel = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                    dialog.setCancelable(false);
                    buttonReject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (etReason.getText().toString().length() > 0) {
                                rejectReason = etReason.getText().toString();
                                acceptRejectTimeSheet(timesheetPosition, false);
                                dialog.dismiss();
                            } else {
                                etReason.setError("Can't be left Blank");
                            }
                        }
                    });
                    buttonCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();

                    break;
                } else {
                    Utilities.showNoConnectionToast(this);
                }
        }
    }

    private void acceptRejectTimeSheet(final int positionInList, final boolean accept) {
        if (Utilities.isOnline(getBaseContext())) {
            String url = "";
            url = Constants.ACCEPT_REJECT_TIMESHEET_API;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("userid", employeeLogin.getUserid());
                jsonObject.put("sitecode", employeeLogin.getSiteCode());
                jsonObject.put("TimeActivityID", timesheet.getTimeActivityID());
                jsonObject.put("ApproveOrReject", accept);
                jsonObject.put("AppSignature", employeeLogin.getApproverSignature());
                if (accept == false) {
                    jsonObject.put("AdminNotes", rejectReason);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.i("arsh", "url=" + url);
            JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    ShowDialog.hideDialog();
                    Log.i("arsh", "response=" + s);
                    if (timesheetPosition != -1) {
                        ApproveTimesheetLayer.isToLoadFromLocal = true;
                        ApproveTimesheetActivity.isToLoadFromLocal = true;
                        approveTimesheetDataFile.getTimesheetArrayList().remove(timesheetPosition);
                        OfflineDataStorage.writeObject(ApproveTimesheetDetails.this, approveTimesheetDataFile, Constants.TIMESHEETS_APPROVE_FILE);
                        finish();
                    } else {
                        Toast.makeText(ApproveTimesheetDetails.this, "No Timesheet Approve/Rejected.", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Utilities.handleVolleyError(ApproveTimesheetDetails.this, volleyError);
                }
            });
            ShowDialog.showDialog(ApproveTimesheetDetails.this);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        } else {
            Toast.makeText(ApproveTimesheetDetails.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    class MyAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            ShowDialog.showDialog(ApproveTimesheetDetails.this);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(getBaseContext(), Constants.EMPLOYEE_LOGIN_FILE);
            approveTimesheetDataFile = (ApproveTimesheetDataFile) OfflineDataStorage.readObject(ApproveTimesheetDetails.this, Constants.TIMESHEETS_APPROVE_FILE);
            timesheet = approveTimesheetDataFile.getTimesheetArrayList().get(timesheetPosition);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowDialog.hideDialog();
            setDataIntoFields();
        }
    }
}
