package com.timerewards.tr.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.timerewards.tr.Adapters.ExpenseItemsToApproveAdapter;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class ApproveExpensesDetails extends AppCompatActivity implements View.OnClickListener, ExpenseItemsToApproveAdapter.Refresh {

    public static boolean toRefreshFromLocal = false;
    private Toolbar toolbar;
    private Intent intent;
    private int expenseSheetPosition;
    private Expense expense;
    private ExpenseDataFile expenseDataFile;
    private TextView mDate, mAmount, mReason, mEmployeeName, mExpenseId;
    private float amount = 0.0f;
    private ListView listView;
    private Button buttonAddExpenseItem;
    private ExpenseItemsToApproveAdapter expenseItemsToApproveAdapter;
    private EmployeeLogin employeeLogin;
    private String rejectReason;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toRefreshFromLocal = true;
        setContentView(R.layout.expense_sheet_details);
        intent = getIntent();
        init();
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(this, Constants.EMPLOYEE_LOGIN_FILE);
        expenseSheetPosition = intent.getIntExtra("id", -1);
        listView = (ListView) findViewById(R.id.listView_expenseSheets);
        expenseItemsToApproveAdapter = new ExpenseItemsToApproveAdapter(this, expenseSheetPosition);
        listView.setAdapter(expenseItemsToApproveAdapter);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Expense Details");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (toRefreshFromLocal) {
            new MyTask().execute();
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDate = (TextView) findViewById(R.id.tv_expense_date);
        mAmount = (TextView) findViewById(R.id.tv_timesheet_hours);
        mReason = (TextView) findViewById(R.id.tv_expense_reason);
        buttonAddExpenseItem = (Button) findViewById(R.id.button_add_expense);
        buttonAddExpenseItem.setVisibility(View.GONE);
        mEmployeeName = (TextView) findViewById(R.id.tv_employee_name);
        mExpenseId = (TextView) findViewById(R.id.tv_expense_sheet_id);
    }

    private void setDataIntoFields() {
        mDate.setText(Utilities.dateToLocalFormat(expense.getExpHeader().getEntryDate()));
        mReason.setText(expense.getExpHeader().getDescription());
        amount = 0.0f;
        for (int z = 0; z < expense.getExpenseItemList().size(); z++) {
            if (expense.getExpenseItemList().get(z).isMarkedForAcceptRejectByUser() == false) {
                amount += expense.getExpenseItemList().get(z).getExpenseAmount();
            }
        }
        mAmount.setText(String.format("%.2f", (double) Math.round(amount * 100) / 100));
        mEmployeeName.setText("Employee Name : " + expense.getExpenseItemList().get(0).getEmployeeFullName());
        if (expense.getExpenseItemList().size() > 0) {
            mExpenseId.setText("Expense ID : " + String.valueOf(expense.getExpenseItemList().get(0).getEmployeeExpenseID()));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_approve_all, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        if (item.getItemId() == R.id.approve_all) {
            if (Utilities.isOnline(this)) {
                approveRejectAll(true);
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }

        if (item.getItemId() == R.id.reject_all) {
            if (Utilities.isOnline(this)) {
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.dialog_reject_reason);
                dialog.setTitle("Please Enter Reject Reason");
                final EditText etReason = (EditText) dialog.findViewById(R.id.et_dialog_reject_reason);
                Button buttonReject = (Button) dialog.findViewById(R.id.button_dialog_ok);
                Button buttonCancel = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                dialog.setCancelable(false);
                buttonReject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (etReason.getText().toString().length() > 0) {
                            rejectReason = etReason.getText().toString();
                            approveRejectAll(false);
                            dialog.dismiss();
                        } else {
                            etReason.setError("Can't be left Blank");
                        }
                    }
                });
                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_add_expense:
                Intent intent = new Intent(getBaseContext(), NewExpenseItemActivity.class);
                intent.putExtra(Constants.HEADER_POSITION, expenseSheetPosition);
                startActivity(intent);
                break;
        }
    }

    private void approveRejectAll(boolean b) {

        String url = "";
        url = Constants.ACCEPT_REJECT_EXPENSE_API;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("EmployeeExpenseID", String.valueOf(expense.getExpHeader().getEmployeeExpenseID()));
            jsonObject.put("EmployeeExpenseItemID", "0");
            jsonObject.put("ApproveOrReject", b);
            jsonObject.put("AppSignature", employeeLogin.getApproverSignature());
            if (b == false) {
                jsonObject.put("AdminNotes", rejectReason);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("arsh", "url=" + url);
        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                ShowDialog.hideDialog();
                expenseDataFile.getExpense().remove(expenseSheetPosition);
                new MyOfflineSaveTaskNFinish().execute();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(ApproveExpensesDetails.this, volleyError);
            }
        });
        ShowDialog.showDialog(ApproveExpensesDetails.this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void refresh(int expenseItemPosition) {
        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().remove(expenseItemPosition);
        expenseItemsToApproveAdapter.changeData(expenseDataFile);
        ApproveExpensesActivity.isToLoadFromLocal = true;
        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().size() == 0) {
            expenseDataFile.getExpense().remove(expenseSheetPosition);
            new MyOfflineSaveTaskNFinish().execute();
        } else {
            new MyOfflineSaveTask().execute();
        }
    }

    class MyTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowDialog.showDialog(ApproveExpensesDetails.this);
        }

        @Override
        protected Void doInBackground(String... params) {
            expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(ApproveExpensesDetails.this, Constants.EXPENSE_APPROVE_DATA_FILE);
            expense = expenseDataFile.getExpense().get(expenseSheetPosition);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            toRefreshFromLocal = false;
            ShowDialog.hideDialog();
            setDataIntoFields();
            expenseItemsToApproveAdapter.changeData(expenseDataFile);
        }
    }

    class MyOfflineSaveTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowDialog.showDialog(ApproveExpensesDetails.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            OfflineDataStorage.writeObject(ApproveExpensesDetails.this, expenseDataFile, Constants.EXPENSE_APPROVE_DATA_FILE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowDialog.hideDialog();
            ApproveExpensesActivity.isToLoadFromLocal = true;
            amount = 0.0f;
            for (int z = 0; z < expense.getExpenseItemList().size(); z++) {
                if (expense.getExpenseItemList().get(z).isMarkedForAcceptRejectByUser() == false) {
                    amount += expense.getExpenseItemList().get(z).getExpenseAmount();
                }
            }
            mAmount.setText(String.format("%.2f", (double) Math.round(amount * 100) / 100));
        }
    }

    class MyOfflineSaveTaskNFinish extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowDialog.showDialog(ApproveExpensesDetails.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            OfflineDataStorage.writeObject(ApproveExpensesDetails.this, expenseDataFile, Constants.EXPENSE_APPROVE_DATA_FILE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowDialog.hideDialog();
            ApproveExpensesActivity.isToLoadFromLocal = true;
            finish();
        }
    }
}