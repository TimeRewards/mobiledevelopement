package com.timerewards.tr.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Adapters.ExpenseItemsAdapter;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.ExpenseHeaderSubmitResponse;
import com.timerewards.tr.DataFiles.ExpenseItemModel;
import com.timerewards.tr.DataFiles.ExpenseItemSubmitResponse;
import com.timerewards.tr.DataFiles.PhotoUploadResponse;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.PhotoMultipartRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class ExpenseSheetDetails extends AppCompatActivity implements View.OnClickListener {

    ExpenseItemsAdapter expenseItemsAdapter;
    int numOfRequests = 0;
    int numOfItems = 0;
    private Toolbar toolbar;
    private Intent intent;
    private int expenseSheetPosition;
    private Expense expense;
    private ExpenseDataFile expenseDataFile;
    private TextView mDate, mAmount, mReason, mEmployeeName, mExpenseSheetID;
    private float amount = 0.0f;
    private ListView listView;
    private Button buttonAddExpenseItem;
    private EmployeeLogin employeeLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expense_sheet_details);
        intent = getIntent();
        expenseSheetPosition = intent.getIntExtra(Constants.HEADER_POSITION, -1);
        listView = (ListView) findViewById(R.id.listView_expenseSheets);
        expenseItemsAdapter = new ExpenseItemsAdapter(this, expenseSheetPosition);
        listView.setAdapter(expenseItemsAdapter);
        init();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Expense Details");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        setDataIntoFields();
    }

    private void init() {
        expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(this, Constants.EXPENSE_DATA_FILE);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(this, Constants.EMPLOYEE_LOGIN_FILE);
        expense = expenseDataFile.getExpense().get(expenseSheetPosition);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDate = (TextView) findViewById(R.id.tv_expense_date);
        mAmount = (TextView) findViewById(R.id.tv_timesheet_hours);
        mReason = (TextView) findViewById(R.id.tv_expense_reason);
        mEmployeeName = (TextView) findViewById(R.id.tv_employee_name);
        mExpenseSheetID = (TextView) findViewById(R.id.tv_expense_sheet_id);
        buttonAddExpenseItem = (Button) findViewById(R.id.button_add_expense);
        buttonAddExpenseItem.setOnClickListener(this);
    }

    private void setDataIntoFields() {
        mDate.setText(Utilities.dateToLocalFormat(expense.getExpHeader().getEntryDate()));
        mReason.setText(expense.getExpHeader().getDescription());

        //displaying employee full name in expense sheet details header
        if (expense.getExpenseItemList().size() > 0) {
            if (expense.getExpenseItemList().get(0).getEmployeeFullName() != null) {
                mEmployeeName.setText("Employee Name : " + expense.getExpenseItemList().get(0).getEmployeeFullName());
            }
        }

        //displaying expense id in details header
        mExpenseSheetID.setText("Expense ID : " + String.valueOf(expense.getExpHeader().getEmployeeExpenseID()));


        for (int z = 0; z < expense.getExpenseItemList().size(); z++) {
            if (expense.getExpenseItemList().get(z).isMarkedForDeletion() != true) {
                amount += expense.getExpenseItemList().get(z).getExpenseAmount();
            }
        }
        mAmount.setText(String.format("%.2f", (double) Math.round(amount * 100) / 100));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            gotoParentActivity();
        }
        if (item.getItemId() == R.id.submit_all) {
            numOfRequests = 0;
            if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().getEmployeeExpenseID() == 0) {
                if (Utilities.isOnline(this)) {
                    submitOnServer(expenseSheetPosition);
                } else {
                    Toast.makeText(ExpenseSheetDetails.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            } else {
                submitAllExpenseItems();
            }
        }
        if (item.getItemId() == R.id.delete_all) {
            numOfRequests = 0;
            ArrayList<ExpenseItemModel> expenseItemModelArrayList = expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList();
            for (int i = 0; i < expenseItemModelArrayList.size(); i++) {
                if (!(expenseItemModelArrayList.get(i).isHasDeletedInDevice() || expenseItemModelArrayList.get(i).isMarkedForDeletion())) {
                    if (expenseItemModelArrayList.get(i).getEZStatusID() == 0 || expenseItemModelArrayList.get(i).getEZStatusID() == 1) {
                        if (expenseItemModelArrayList.get(i).getEmployeeExpenseItemID() == 0) {
                            expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(i).setHasDeletedInDevice(true);
                            OfflineDataStorage.writeObject(this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                            continue;
                        } else {
                            deleteExpenseItem(expenseSheetPosition, i);
                        }
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void submitAllExpenseItems() {
        ArrayList<ExpenseItemModel> expenseItemModelArrayList = expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList();
        for (int i = 0; i < expenseItemModelArrayList.size(); i++) {
            if (Utilities.isOnline(this)) {
                if (!(expenseItemModelArrayList.get(i).isHasDeletedInDevice() || expenseItemModelArrayList.get(i).isMarkedForDeletion())) {
                    if (expenseItemModelArrayList.get(i).getEZStatusID() == 0 || expenseItemModelArrayList.get(i).getEZStatusID() == 1) {
                        submitExpenseItemOnServer(expenseSheetPosition, i);
                    }
                }
            } else {
                Toast.makeText(ExpenseSheetDetails.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_submit_delete_all, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        gotoParentActivity();
    }

    private void gotoParentActivity() {
        Intent intent = new Intent(this, ExpenseSheetActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_add_expense:
                Intent intent = new Intent(getBaseContext(), NewExpenseItemActivity.class);
                intent.putExtra(Constants.HEADER_POSITION, expenseSheetPosition);
                startActivity(intent);
                break;
        }
    }

    private void submitOnServer(final int expenseSheetPosition) {

        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(ExpenseSheetDetails.this, Constants.EMPLOYEE_LOGIN_FILE);

//        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().getEmployeeExpenseID() == 0) {
        Expense expense = expenseDataFile.getExpense().get(expenseSheetPosition);
        if (expense.getExpHeader().getEmployeeExpenseID() == 0) {
            String url = null;
            if (Utilities.isOnline(ExpenseSheetDetails.this)) {
                url = Constants.SUBMIT_EXPENSE_HEADER_API;
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("UserId", employeeLogin.getUserid());
                    jsonObject.put("SiteCode", employeeLogin.getSiteCode());
                    jsonObject.put("EmployeeExpenseID", 0);
                    jsonObject.put("ExpenseReason", expense.getExpHeader().getDescription());
                    jsonObject.put("ExpenseDate", expense.getExpHeader().getEntryDate());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.i("arsh", "response=" + s);
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.has("EmployeeExpenseID")) {
                                ExpenseHeaderSubmitResponse expenseHeaderSubmitResponse = new GsonBuilder().create().fromJson(s.toString(), ExpenseHeaderSubmitResponse.class);
                                expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().setEmployeeExpenseID(expenseHeaderSubmitResponse.getEmployeeExpenseID());
                                for (int i = 0; i < expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().size(); i++) {
                                    expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(i).setEmployeeExpenseID(expenseHeaderSubmitResponse.getEmployeeExpenseID());
                                }
                                expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().set$id("");
                                OfflineDataStorage.writeObject(ExpenseSheetDetails.this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                                submitAllExpenseItems();
                            } else {
//                                    if (jsonObject.has("ErrorMessage")) {
//                                        Toast.makeText(getBaseContext(), jsonObject.getString("ErrorMessage"), Toast.LENGTH_SHORT).show();
//                                    }
                            }
                        } catch (JSONException e) {
                            Toast.makeText(ExpenseSheetDetails.this, "Request Failed", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                        hideProgressDialog();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        hideProgressDialog();
                        Utilities.handleVolleyError(ExpenseSheetDetails.this, volleyError);
                    }
                });
                showProgressDialog();
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);
            } else {
                Utilities.showNoConnectionToast(ExpenseSheetDetails.this);
            }
        }
//        }
    }

    private void submitExpenseItemOnServer(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(ExpenseSheetDetails.this, Constants.EMPLOYEE_LOGIN_FILE);
        String submitDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").format(Calendar.getInstance().getTimeInMillis());

//        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).get$id().equalsIgnoreCase("new")) {
        Expense expense = expenseDataFile.getExpense().get(expenseSheetPosition);
        String url = null;
        JSONObject jsonObject = new JSONObject();
        url = Constants.SUBMIT_EXPENSE_ITEM_API;
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("EmployeeExpenseID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseID());
            jsonObject.put("EmployeeExpenseItemID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID());
            jsonObject.put("ExpenseDate", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getExpenseDate());
            jsonObject.put("ExpenseAmount", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getExpenseAmount());
            jsonObject.put("PaidBy", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getPaidBy());
            jsonObject.put("IsReimbursable", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isReimbursable());
            jsonObject.put("Billable", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isBillable());
            jsonObject.put("Merchant", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getMerchant());
            jsonObject.put("Description", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getDescription());
            jsonObject.put("ProjectID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getProjectID());
            jsonObject.put("AccountID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getAccountID());
            jsonObject.put("ClassRowID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getClassRowID());
            jsonObject.put("EmployeeFullNme", employeeLogin.getDisplayName());
            jsonObject.put("CCAccountName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getCCAccountName());
            jsonObject.put("ProjectFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getProjectFullName());
            jsonObject.put("AccountFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getAccountFullName());
            jsonObject.put("ClassFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getClassFullName());
            jsonObject.put("SourceRefID", "18");
            jsonObject.put("EmpSubDate", submitDate);
            jsonObject.put("Qty", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getQty());
            jsonObject.put("Price", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getPrice());
            jsonObject.put("EmpSignature", employeeLogin.getEmployeeSignature());
            jsonObject.put("IsDefaultGLAnItem", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isDefaultGLAnItem());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("arsh", "url = " + url);

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("arsh", "response=" + s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has("EmployeeExpenseItemID")) {
                        numOfItems++;
                        ExpenseItemSubmitResponse expenseItemSubmitResponse = new GsonBuilder().create().fromJson(s.toString(), ExpenseItemSubmitResponse.class);
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setEmployeeExpenseItemID(expenseItemSubmitResponse.getEmployeeExpenseItemID());
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setEZStatusID(expenseItemSubmitResponse.getEZStatusID());
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).set$id("");
                        OfflineDataStorage.writeObject(ExpenseSheetDetails.this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getReceiptName().length() > 0
                                && expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isHasReceiptUploadedOnServer() == false) {
                            uploadImage(expenseSheetPosition, expenseItemPosition);
                        } else {
//                                Toast.makeText(ExpenseSheetDetails.this, "Submitted successfully", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(ExpenseSheetDetails.this, "Request Failed", Toast.LENGTH_SHORT).show();
                    }
                    hideProgressDialog();
                } catch (JSONException e) {
                    Toast.makeText(ExpenseSheetDetails.this, "Exception Occured", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Utilities.handleVolleyError(ExpenseSheetDetails.this, volleyError);
                hideProgressDialog();
            }
        });
        showProgressDialog();
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
//        }
    }

    private void uploadImage(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(ExpenseSheetDetails.this, Constants.EMPLOYEE_LOGIN_FILE);
        String url = "";

        try {
            url = Constants.UPLOAD_EXPENSE_RECEIPTS_API;
            JSONObject jsonObject = new JSONObject();
//            try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("EmployeeExpenseItemID", employeeLogin.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String filename = expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getReceiptName();
        File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(f, filename);

        if (file.exists()) {

            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
            entityBuilder.addPart("file", new FileBody(file));
            entityBuilder.addTextBody("userid", String.valueOf(employeeLogin.getUserid()));
            entityBuilder.addTextBody("sitecode", String.valueOf(employeeLogin.getSiteCode()));
            entityBuilder.addTextBody("employeeexpenseitemid", String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID()));
            entityBuilder.setLaxMode().setBoundary("xx").setCharset(null);

            Log.e("file", file.getName());
            PhotoMultipartRequest multipartRequest = new PhotoMultipartRequest(url, entityBuilder, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    hideProgressDialog();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("uploaded")) {
                            PhotoUploadResponse photoUploadResponse = new GsonBuilder().create().fromJson(response, PhotoUploadResponse.class);
                            if (photoUploadResponse.getUploaded().equalsIgnoreCase("true")) {
//                                Toast.makeText(ExpenseSheetDetails.this, "Submitted successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ExpenseSheetDetails.this, "Receipt not uploaded", Toast.LENGTH_SHORT).show();
                            }
                            Log.e("response", response);
                        }
                    } catch (JSONException e) {
                        Toast.makeText(ExpenseSheetDetails.this, "Exception Occured", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressDialog();
                    Utilities.handleVolleyError(ExpenseSheetDetails.this, error);
                }
            });
            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    100000000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            try {
                Log.e("sdf", multipartRequest.getBodyContentType());
                Log.e("gbh", multipartRequest.getUrl());
                Log.e("fd", multipartRequest.getBody()[0] + "");

                for (Map.Entry e : multipartRequest.getHeaders().entrySet()) {
                    Log.e("key", e.getKey() + "");
                    Log.e("value", e.getValue() + "");
                }

            } catch (AuthFailureError authFailureError) {
                authFailureError.printStackTrace();
            }
            showProgressDialog();
            AppController.getRequestQueue().add(multipartRequest);
        } else {
            Toast.makeText(ExpenseSheetDetails.this, "Receipt Image Not found", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteExpenseItem(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(ExpenseSheetDetails.this, Constants.EMPLOYEE_LOGIN_FILE);
        if (Utilities.isOnline(ExpenseSheetDetails.this)) {
            String url = null;
            url = Constants.DELETE_EXPENSE_ITEM_API;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("userid", employeeLogin.getUserid());
                jsonObject.put("sitecode", employeeLogin.getSiteCode());
                jsonObject.put("EmployeeExpenseID", "0");
                jsonObject.put("EmployeeExpenseItemID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    Log.i("arsh", "response=" + s);
                    if (s.equalsIgnoreCase("true")) {
//                        Toast.makeText(ExpenseSheetDetails.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setHasDeletedInDevice(true);
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setIsMarkedForDeletion(true);
                        OfflineDataStorage.writeObject(ExpenseSheetDetails.this, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                        numOfItems++;
                    } else {
                    }
                    hideProgressDialog();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Utilities.handleVolleyError(ExpenseSheetDetails.this, volleyError);
                }
            });

            showProgressDialog();
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        } else {
            Utilities.showNoConnectionToast(ExpenseSheetDetails.this);
        }
    }

    private void showProgressDialog() {
        numOfRequests++;
        ShowDialog.showDialog(ExpenseSheetDetails.this);
    }

    private void hideProgressDialog() {
        numOfRequests--;
        if (numOfRequests < 1) {
            ShowDialog.hideDialog();
            Toast.makeText(this, "Request processed for " + numOfItems + " Items", Toast.LENGTH_SHORT).show();
            numOfRequests = 0;
            numOfItems = 0;
            Intent intent = new Intent(this, ExpenseSheetDetails.class);
            intent.putExtra(Constants.HEADER_POSITION, expenseSheetPosition);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }
}