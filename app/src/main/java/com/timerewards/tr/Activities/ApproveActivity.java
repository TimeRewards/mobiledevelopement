package com.timerewards.tr.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;

public class ApproveActivity extends AppCompatActivity implements View.OnClickListener {

    CardView approveTimesheet, approveExpenses;
    Toolbar mToolbar;
    GetOptionsDataFile getOptionsDataFile;
    EmployeeLogin employeeLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(this, Constants.GETOPTIONS_FILE);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(this, Constants.EMPLOYEE_LOGIN_FILE);

        setContentView(R.layout.activity_approve);
        init();
    }

    private void init() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Approve");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        approveTimesheet = (CardView) findViewById(R.id.card_view_approve_timesheets);
        approveTimesheet.setOnClickListener(this);
        approveExpenses = (CardView) findViewById(R.id.card_view_approve_expenses);
        approveExpenses.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.card_view_approve_timesheets:
                Intent intent = new Intent(this, ApproveTimesheetLayer.class);
                ApproveTimesheetLayer.isToLoadFromServer = true;
                intent.putExtra(Constants.RELOAD_LIST, true);
                startActivity(intent);
                break;
            case R.id.card_view_approve_expenses:
                Intent intent1 = new Intent(this, ApproveExpensesActivity.class);
                intent1.putExtra(Constants.RELOAD_LIST, true);
                startActivity(intent1);
                break;
        }
    }

}