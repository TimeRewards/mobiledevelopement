package com.timerewards.tr.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Adapters.ExpenseSheetApproveAdapter;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.ApproveRejectExpenseResponseModel;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

import static com.timerewards.tr.Controller.Constants.EXPENSE_APPROVE_DATA_FILE;

public class ApproveExpensesActivity extends AppCompatActivity implements ExpenseSheetApproveAdapter.Refresh {

    public static boolean isToLoadFromServer = true;
    public static boolean isToLoadFromLocal = false;
    TextView tvDate;
    ListView listView;
    ExpenseSheetApproveAdapter expenseSheetApproveAdapter;
    View view;
    Toolbar toolbar;
    ExpenseDataFile expenseDataFile;
    ArrayList<Expense> expensesArrayList = new ArrayList<>();
    EmployeeLogin employeeLogin;
    int numOfRequests = 0;
    TableObject tableObject;
    GetOptionsDataFile getOptionsDataFile;
    Intent intent;
    String rejectReason;
    Button dialogOkButton, dialogCancelButton;
    EditText etApproverSignature;
    TextView textReason;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isToLoadFromServer = true;
        setContentView(R.layout.activity_timesheets);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(getBaseContext(), Constants.EMPLOYEE_LOGIN_FILE);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(this, Constants.GETOPTIONS_FILE);
        if (getOptionsDataFile.isAppSigRequiredforExp()) {
            if (employeeLogin.getApproverSignature() != null && employeeLogin.getApproverSignature().length() > 0) {

            } else {
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.dialog_set_emp_signature);
                dialog.setTitle("Set Your Signature");
                textReason = (TextView) dialog.findViewById(R.id.text_reason);
                etApproverSignature = (EditText) dialog.findViewById(R.id.et_dialog_approver_signature);
                dialogOkButton = (Button) dialog.findViewById(R.id.button_dialog_ok);
                dialogCancelButton = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                dialog.setCancelable(false);
                dialogOkButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (etApproverSignature.getText().toString().length() > 0) {
                            employeeLogin.setApproverSignature(etApproverSignature.getText().toString());
                            OfflineDataStorage.writeObject(ApproveExpensesActivity.this, employeeLogin, Constants.EMPLOYEE_LOGIN_FILE);
                            dialog.dismiss();
                        } else {
                            etApproverSignature.setError("Can't be left blank!");
                        }
                    }
                });

                dialogCancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

                dialog.show();
            }
        }
        init();
        intent = getIntent();
        tableObject = (TableObject) OfflineDataStorage.readObject(this, Constants.TIMESHEETS_FILE);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Expenses for Approval");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        tvDate.setVisibility(View.GONE);
        listView.setAdapter(expenseSheetApproveAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isToLoadFromServer) {
            syncExpenseApproveList();
        } else {
            if (isToLoadFromLocal) {
                new MyTask().execute();
            }
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvDate = (TextView) findViewById(R.id.tv_date);
        listView = (ListView) findViewById(R.id.listView_dates);
        TextView textView = (TextView) findViewById(R.id.tv_add_timehseet);
        textView.setVisibility(View.GONE);
        expenseSheetApproveAdapter = new ExpenseSheetApproveAdapter(this);
    }

    private void generateTimeMillis() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        for (int i = 0; i < expenseDataFile.getExpense().size(); i++) {
            try {
                Long time = simpleDateFormat.parse(expenseDataFile.getExpense().get(i).getExpHeader().getEntryDate()).getTime();
                expenseDataFile.getExpense().get(i).getExpHeader().setTimeInMillis(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_approve_reject_all, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        if (item.getItemId() == R.id.approve_all) {
            if (Utilities.isOnline(this)) {
                acceptRejectAllExpenses(true);
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }
        if (item.getItemId() == R.id.reject_all) {
            if (Utilities.isOnline(this)) {
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.dialog_reject_reason);
                dialog.setTitle("Please Enter Reject Reason");
                final EditText etReason = (EditText) dialog.findViewById(R.id.et_dialog_reject_reason);
                Button buttonReject = (Button) dialog.findViewById(R.id.button_dialog_ok);
                Button buttonCancel = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                dialog.setCancelable(false);
                buttonReject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (etReason.getText().toString().length() > 0) {
                            rejectReason = etReason.getText().toString();
                            acceptRejectAllExpenses(false);
                            dialog.dismiss();
                        } else {
                            etReason.setError("Can't be left Blank");
                        }
                    }
                });
                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }
        if (item.getItemId() == R.id.refresh) {
            if (Utilities.isOnline(this)) {
                syncExpenseApproveList();
            } else {
                Utilities.showNoConnectionToast(this);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void acceptRejectAllExpenses(final boolean accept) {

        String IDs = "";
        expensesArrayList = expenseDataFile.getExpense();
        for (int i = 0; i < expensesArrayList.size(); i++) {
            if (!expensesArrayList.get(i).isMarkedOnServer()) {
                if (!(expensesArrayList.get(i).isAcceptedByUser() || expensesArrayList.get(i).isRejectedByUser())) {
                    IDs = IDs + expensesArrayList.get(i).getExpHeader().getEmployeeExpenseID() + ",";
                }
            }
        }

        if (IDs.length() > 2) {
            IDs = IDs.substring(0, IDs.length() - 1);
        }

//        for (int i = 0; i < expensesArrayList.size(); i++) {
//            if (!expensesArrayList.get(i).isMarkedOnServer()) {
//                if (!(expensesArrayList.get(i).isAcceptedByUser() || expensesArrayList.get(i).isRejectedByUser())) {
        String url = "";
        url = Constants.ACCEPT_REJECT_EXPENSE_API;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
            jsonObject.put("EmployeeExpenseID", IDs);
            jsonObject.put("EmployeeExpenseItemID", "");
            jsonObject.put("ApproveOrReject", accept);
            jsonObject.put("AppSignature", employeeLogin.getApproverSignature());
            if (accept == false) {
                jsonObject.put("AdminNotes", rejectReason);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("arsh", "url=" + url);

        if (Utilities.isOnline(getBaseContext())) {
            JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    ApproveRejectExpenseResponseModel responseModel = new GsonBuilder().create().fromJson(s.toString(), ApproveRejectExpenseResponseModel.class);
                    if (responseModel.getEmployeeExpenseItems().size() > 0) {
//                                    for (int i = 0; i < responseModel.getEmployeeExpenseItems().size(); i++) {
//                                        for (int j = 0; j < expensesArrayList.get(i).getExpenseItemList().size(); j++) {
//                                            if (responseModel.getEmployeeExpenseItems().get(i).getEmployeeExpenseID() == expensesArrayList.get(i).getExpenseItemList().get(j).getEmployeeExpenseID()) {
//                                                expensesArrayList.get(i).getExpenseItemList().get(j).setMarkedApprovedRejectedOnServer(true);
//                                                if (accept) {
//                                                    expensesArrayList.get(i).getExpenseItemList().get(j).setAcceptedByUser(true);
//                                                } else {
//                                                    expensesArrayList.get(i).getExpenseItemList().get(j).setRejectedByUser(true);
//                                                }
//                                                expensesArrayList.get(i).getExpenseItemList().get(j).setMarkedForAcceptRejectByUser(true);
//                                            }
//                                        }
//                                    }
                        OfflineDataStorage.writeObject(getBaseContext(), expenseDataFile, Constants.EXPENSE_APPROVE_DATA_FILE);
                        restartActivity();
                    } else {
//                        Toast.makeText(getBaseContext(), "No Expense Approved or Rejected", Toast.LENGTH_SHORT).show();
                    }
                    hideProgressDialog();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    hideProgressDialog();
                    Utilities.handleVolleyError(ApproveExpensesActivity.this, volleyError);
//                                Toast.makeText(getBaseContext(), "Error Submitting on Server", Toast.LENGTH_SHORT).show();
                }
            });
            showProgressDialog();
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 4, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        } else {
            Toast.makeText(getBaseContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgressDialog() {
        numOfRequests++;
        ShowDialog.showDialog(ApproveExpensesActivity.this);
    }

    private void hideProgressDialog() {
        numOfRequests--;
        if (numOfRequests == 0) {
            ShowDialog.hideDialog();
            restartActivity();
        }
    }

    private void restartActivity() {
        Intent intent = new Intent(getBaseContext(), ApproveExpensesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Constants.RELOAD_LIST, true);
        startActivity(intent);
        finish();
    }

    private void syncExpenseApproveList() {

        String url = Constants.EXPENSES_TO_APPROVE_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", tableObject.getUserid());
            jsonObject.put("sitecode", tableObject.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utilities.showErrorLog(url);

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST,
                url, jsonObject,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        expenseDataFile = new GsonBuilder().create().fromJson(s.toString(), ExpenseDataFile.class);
                        ShowDialog.hideDialog();
                        new MyTask().execute(s);
                        Toast.makeText(getBaseContext(), "Expenses to approve (" + expenseDataFile.getExpense().size() + ")", Toast.LENGTH_SHORT).show();
                        isToLoadFromServer = false;
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(ApproveExpensesActivity.this, volleyError);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShowDialog.showDialog(ApproveExpensesActivity.this);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void refresh(int expenseSheetPosition) {
        expenseDataFile.getExpense().remove(expenseSheetPosition);
        expenseSheetApproveAdapter.changeData(expenseDataFile);
        new MyOfflineSaveTask().execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToParentActivity();
    }

    private void goToParentActivity() {
        Intent intent = new Intent(this, ApproveActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    class MyTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowDialog.showDialog(ApproveExpensesActivity.this);
        }

        @Override
        protected Void doInBackground(String... params) {
            if (params != null && params.length > 0 && params[0] != null) {
                expenseDataFile = new GsonBuilder().create().fromJson(params[0], ExpenseDataFile.class);
                generateTimeMillis();
                Collections.sort(expenseDataFile.getExpense(), Expense.ExpenseComparator);
                OfflineDataStorage.writeObject(getBaseContext(), expenseDataFile, EXPENSE_APPROVE_DATA_FILE);
            } else {
                expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(ApproveExpensesActivity.this, Constants.EXPENSE_APPROVE_DATA_FILE);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            expenseSheetApproveAdapter.changeData(expenseDataFile);
            ShowDialog.hideDialog();
            isToLoadFromLocal = false;
        }
    }

    class MyOfflineSaveTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ShowDialog.showDialog(ApproveExpensesActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            OfflineDataStorage.writeObject(ApproveExpensesActivity.this, expenseDataFile, Constants.EXPENSE_APPROVE_DATA_FILE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ShowDialog.hideDialog();
        }
    }

}