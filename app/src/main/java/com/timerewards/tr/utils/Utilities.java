package com.timerewards.tr.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.timerewards.tr.DataFiles.ProjectsModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Utilities {

    public static final int CAMERA_PERMISSION_REQUEST = 100;
    public static final int STORAGE_PERMISSION_REQUEST = 101;

    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static String dateToServerFormat(String date) {

        SimpleDateFormat simpleDateFormatOLD = new SimpleDateFormat("dd-MMM-yyyy");
        SimpleDateFormat simpleDateFormatNEW = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;

        try {
            date1 = simpleDateFormatOLD.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return simpleDateFormatNEW.format(date1) + "T00:00:00";
    }

    public static String dateToLocalFormat(String date) {
        SimpleDateFormat simpleDateFormatOLD = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simpleDateFormatNEW = new SimpleDateFormat("dd-MMM-yyyy");

        String dateServerFormat = date.substring(0, date.indexOf("T"));

        Date date1 = null;

        try {
            date1 = simpleDateFormatOLD.parse(dateServerFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return simpleDateFormatNEW.format(date1);
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkStoragePermission(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            return true;
//            Toast.makeText(context, "lower version", Toast.LENGTH_SHORT).show();
        } else {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        || ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CAMERA)) {
                    ShowDialofAndRequestPermission(context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA});
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, STORAGE_PERMISSION_REQUEST);
                }
            }
        }
        return false;
    }

    private static void ShowDialofAndRequestPermission(final Context context, final String[] permission) {
        AlertDialog.Builder permissionDialog = new AlertDialog.Builder(context);
        permissionDialog.setMessage("Please grant Permission, to add the receipt");
        permissionDialog.setNegativeButton("Allow", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityCompat.requestPermissions((Activity) context, permission, STORAGE_PERMISSION_REQUEST);
            }
        });

        permissionDialog.setPositiveButton("Deny", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        permissionDialog.show();

    }

    public static String intToStatus(int statusCode) {
        switch (statusCode) {
            case 0:
                return "On Hold";
            case 1:
                return "Rejected";
            case 2:
                return "Submitted";
            case 3:
                return "Approved";
            case 4:
                return "Submitted";
            case 5:
                return "Downloaded";
            default:
                return "";
        }
    }

    public static void showTimeOutToast(Context context) {
        Toast.makeText(context, "Timeout.. Please try again", Toast.LENGTH_SHORT).show();
    }

    public static void showNoConnectionToast(Context context) {
        Toast.makeText(context, "No Internet connection", Toast.LENGTH_SHORT).show();
    }

    public static void showSomethingWrongToast(Context context) {
        Toast.makeText(context, "Something went wrong.\nPlease try again.", Toast.LENGTH_SHORT).show();
    }

    public static void handleVolleyError(Context context, VolleyError volleyError) {
        if (volleyError.networkResponse == null) {
            if (volleyError.getClass().equals(NoConnectionError.class)) {
                Utilities.showNoConnectionToast(context);
            }
        }
        if (volleyError.networkResponse == null) {
            if (volleyError.getClass().equals(TimeoutError.class)) {
                Utilities.showTimeOutToast(context);
            }
        } else {
            Utilities.showSomethingWrongToast(context);
        }
    }

    public static void showImportantToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showTemporaryToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static int dpToPx(Context context, float px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, displayMetrics);
    }

    public static void showErrorLog(String msg) {
        Log.e("timerewards", msg);
    }

    /**
     * This function gives the filtered list of projects. This function returns the projects having ExpPrj = false
     *
     * @param projectsModelArrayList
     * @return
     */
    public static ArrayList<ProjectsModel> filterBasedOnExpPrj(ArrayList<ProjectsModel> projectsModelArrayList) {
        ArrayList<ProjectsModel> projectsModelArrayListNew = new ArrayList<>();
        for (int i = 0; i < projectsModelArrayList.size(); i++) {
            ProjectsModel projectsModel = projectsModelArrayList.get(i);
            if (projectsModel.isExpPrj()) {
                projectsModelArrayListNew.add(projectsModel);
            }
        }
        return projectsModelArrayListNew;
    }

}
