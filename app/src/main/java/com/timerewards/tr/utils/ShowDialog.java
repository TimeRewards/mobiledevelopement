package com.timerewards.tr.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

public class ShowDialog {

    private static ProgressDialog pDialog = null;

    public static void showDialog(Context context) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please Wait");
            pDialog.setCancelable(false);
            if (!pDialog.isShowing()) {
                pDialog.show();
            }
        }
    }

    public static void showDialog(Context context, String msg) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(context);
            pDialog.setProgressDrawable(new ColorDrawable(Color.BLUE));
            pDialog.setMessage(msg);
            pDialog.setCancelable(false);
            if (!pDialog.isShowing()) {
                try {
                    pDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void hideDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            try {
                pDialog.dismiss();
                pDialog = null;
            } catch (Exception e) {
                pDialog = null;
                e.printStackTrace();
            }
        }
    }

}
