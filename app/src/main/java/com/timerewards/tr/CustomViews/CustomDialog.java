package com.timerewards.tr.CustomViews;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by machine2 on 7/4/15.
 */
public class CustomDialog {

    public static Dialog showDialog(Context context, View view) {
        final Dialog dialog = new Dialog(context);
        // hide to default title for Dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();

        wmlp.gravity = Gravity.CENTER;
//        wmlp.x = 100;   //x position
        wmlp.y = 100;   //y position
        // inflate the layout dialog_layout.xml and set it as contentView

        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(view);
        dialog.show();
        return dialog;
    }
}
