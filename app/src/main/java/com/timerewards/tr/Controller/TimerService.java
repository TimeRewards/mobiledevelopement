package com.timerewards.tr.Controller;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.timerewards.tr.DataFiles.TimerDataFile;
import com.timerewards.tr.DataFiles.TimerModel;
import com.timerewards.tr.OfflineDataStorage;

import java.util.ArrayList;

public class TimerService extends IntentService {

    public static Boolean isRunning = false;
    public static ArrayList<TimerModel> timerModelArrayList = new ArrayList<>();
    public static TimerDataFile timerDataFile = new TimerDataFile();
    public long time = 0;
    private PowerManager.WakeLock wakeLock;

    public TimerService() {
        super("Timer");
    }

    public static void addTimer() {
        TimerModel timerModel = new TimerModel();
        timerModel.setStartTime(System.currentTimeMillis());
        timerModelArrayList.add(timerModel);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        timerDataFile = (TimerDataFile) OfflineDataStorage.readObject(getApplicationContext(), Constants.TIMER_DATA_FILE);
        if (timerDataFile == null) {
            timerDataFile = new TimerDataFile();
        }
        timerModelArrayList = timerDataFile.getTimerModelArrayList();
        for (TimerModel timerModel : timerModelArrayList) {
            timerModel.setRunning(false);
        }
        while (isRunning == true) {
            try {
                Thread.sleep(1000 * 1);
//                time++;
                increaseTime();
//                Log.e("i ma running", "i ma running");
                sendBroadcast(new Intent("TimerService"));
//                if (time == 10) {
//                    time = 0;
////                   saveOffline();
//                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveOffline() {
        timerDataFile.setTimerModelArrayList(timerModelArrayList);
        OfflineDataStorage.writeObject(getApplicationContext(), timerDataFile, Constants.TIMER_DATA_FILE);
        Log.e("arsh", String.valueOf(timerDataFile.getTimerModelArrayList().size()));
        TimerDataFile timerDataFile1 = new TimerDataFile();
        timerDataFile1.setTimerModelArrayList(timerModelArrayList);
        Log.e("arsh1", String.valueOf(timerDataFile1.getTimerModelArrayList().size()));
    }



    private void increaseTime() {
        for (TimerModel timerModel : timerModelArrayList) {

            if(timerModel.isRunning()) {

                timerModel.setMillis(timerModel.getCalEndTime()+System.currentTimeMillis()-timerModel.getCalStartTime());
            }

//            if (timerModel.isRunning()) {
//                timerModel.setMillis(timerModel.getMillis()+1000);
//            }

        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        PowerManager mgr = (PowerManager)getApplicationContext().getSystemService(Context.POWER_SERVICE);
        wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
        wakeLock.acquire();


//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
//
//        builder.setTicker("App info string");
//        builder.setOngoing(true);
//        builder.setOnlyAlertOnce(true);
//
//        Notification notification = builder.build();
//
//// optionally set a custom view
//
//        startForeground(1, notification);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        wakeLock.release();
    }
}
