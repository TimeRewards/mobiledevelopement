package com.timerewards.tr.Controller;

public class Constants {

    public static final String DOMAIN = "https://www.timerewards.com/";
    public static final String LOGIN_API = DOMAIN + "api/AccountApi/GetLoginForMobileJson";
    public static final String PROJECTS_API = DOMAIN + "api/commonapi/getProjectsforMobileJson";
    public static final String CLASSES_API = DOMAIN + "api/commonapi/getClassesForMobileJson";
    public static final String TASKS_API = DOMAIN + "api/commonapi/gettasksforMobileJson";
    public static final String ACCOUNTS_API = DOMAIN + "api/commonapi/GetAccountsForMobileJson";
    public static final String EXPENSES_API = DOMAIN + "api/Expenseapi/GetExpListForMobileJson";
    public static final String GET_OPTIONS_API = DOMAIN + "api/CommonApi/GetOptionsForMobileJson";
    public static final String TIMESHEETS_API = DOMAIN + "api/TimeActivityApi/GetTAListForMobileJson";
    public static final String SUBMIT_NEW_TIMESHEET_API = DOMAIN + "api/TimeActivityApi/SubmitTSForMobileJson";
    public static final String DELETE_TIMESHEET_API = DOMAIN + "API/TimeActivityAPI/DeleteTimesheetForMobileJson";
    public static final String UPLOAD_EXPENSE_RECEIPTS_API = DOMAIN + "api/ExpenseAPI/UploadExpenseReceiptsForMobileJson";
    public static final String SUBMIT_EXPENSE_HEADER_API = DOMAIN + "API/ExpenseAPI/SubmitExpHeaderForMobileJson";
    public static final String SUBMIT_EXPENSE_ITEM_API = DOMAIN + "API/ExpenseAPI/SubmitExpItemForMobileJson";
    public static final String DELETE_EXPENSE_ITEM_API = DOMAIN + "API/ExpenseAPI/DeleteExpenseForMobileJson";
    public static final String TIMESHEETS_TO_APPROVE_API = DOMAIN + "api/ApproverAPI/GetTimeApproverListForMobileJson";
    public static final String EXPENSES_TO_APPROVE_API = DOMAIN + "API/ApproverAPI/GetExpApproverListForMobileJson";
//    public static final String ACCEPT_REJECT_TIMESHEET_API = DOMAIN + "API/ApproverAPI/ApproveOrRejectTimesheetForMobileJson";
    public static final String ACCEPT_REJECT_TIMESHEET_API = DOMAIN + "api/approverapi/approveorrejecttimesheetformobilejson";
//    public static final String ACCEPT_REJECT_EXPENSE_API = DOMAIN + "api/ApproverAPI/ApproveOrRejectExpenseForMobileJson";
    public static final String ACCEPT_REJECT_EXPENSE_API = DOMAIN + "api/approverapi/approveorrejectexpenseformobilejson";

    public static final String UPDATE_TOKEN_API = DOMAIN + "api/commonapi/UpdateTokenforMobileJson";

    public static final String GETOPTIONS_FILE = "GetOptionsDataFile";
    public static final String TIMESHEETS_FILE = "timesheet";
    public static final String TIMESHEETS_APPROVE_FILE = "TimesheetApprove";
    public static final String CLASSES_DATA_FILE = "classes";
    public static final String PROJECTS_DATA_FILE = "projects";
    public static final String TASKS_DATA_FILE = "tasks";
    public static final String EXPENSE_DATA_FILE = "ExpenseDataFile";
    public static final String EXPENSE_APPROVE_DATA_FILE = "ExpenseApproveDataFile";
    public static final String EMPLOYEE_LOGIN_FILE = "employee_login_file";
    public static final String ACCOUNTS_DATA_FILE = "accounts_data_file";
    public static final String SHARED_PREFERENCES = "shared_preferences";
    public static final String EZStatusID_0 = "Onhold";
    public static final String EZStatusID_1 = "Rejected";
    public static final String EZStatusID_2 = "Submitted";
    public static final String EZStatusID_3 = "Approved";
    public static final String EZStatusID_4 = "Submitted";
    public static final String EZStatusID_5 = "Downloaded";
    public static final String ASSEST_TYPE_EXPENSE = "Expense";
    public static final String ASSEST_TYPE_CREDIT_CARD = "CreditCard";
    public static final String HEADER_POSITION = "header_position";
    public static final String EXPENSE_ITEM_POSITION = "expense_item_position";
    public static final String PROJECT_ID = "project_id";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String ACCOUNT_ID = "account_id";
    public static final String CURRENT_DATE = "current_date";
    public static final String RELOAD_LIST = "reload_list";

    public static final String TIMESHEET_POSITION = "timesheet_position";

    public static final String IMAGE_COUNTER = "image_counter";
    public static final String SHARED_PREFERENCES_FILE = "shared_preferences_file";

    public static final String TIMER_DATA_FILE = "timer_data_file";
    public static final String TIMER_COUNT = "timer_count";
    public static final String TIMER_POSITION = "timer_position";
    public static final String TIMESLOT_POSITION = "timeslot_position";
    public static final String PROJECT_OR_TASK = "project_or_task";
    public static final String PROJECT_OR_TASK_POSITION = "project_or_task_position";
    public static final String ALL_TIME_SLOTS = "all_time_slots";
    public static final String NOTIFICATION_ID = "NotificationID";
    public static final String TIMESHEET_LAST_SYNC_DATE = "timesheet_last_sync_date";
    public static final String EXPENSE_LAST_SYNC_DATE = "expense_last_sync_date";
    public static final String NEW_ASSIGNMENTS = "new_assignments";
    public static final String FCM_TOKEN = "fcm_token";
    public static final String START_MILLIS = "start_millis";
    public static final String END_MILLIS = "end_millis";
    public static final String EMPLOYEE_NAME = "employee_name";
    public static final String PROJECT_OR_TASK_NAME = "project_or_task_name";
    public static final String DIALOUGE_IDEN = "dialog_ident";
}
