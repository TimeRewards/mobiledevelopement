package com.timerewards.tr.Controller;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    String refreshedToken;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (refreshedToken == null) {
            refreshedToken = FirebaseInstanceId.getInstance().getToken();
        }
        Log.e("FCM_DUMMY", "token refreshed " + FirebaseInstanceId.getInstance().getToken());
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
        sharedPreferences.edit().putString(Constants.FCM_TOKEN, FirebaseInstanceId.getInstance().getToken()).apply();
    }
}
