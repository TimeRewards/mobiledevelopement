package com.timerewards.tr.Controller;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.timerewards.tr.Activities.ApproveExpensesActivity;
import com.timerewards.tr.Activities.ApproveTimesheetLayer;
import com.timerewards.tr.Activities.ExpenseSheetActivity;
import com.timerewards.tr.Activities.MainActivity;
import com.timerewards.tr.Activities.MyTimeSheetTransactions;
import com.timerewards.tr.R;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TIMESHEET_APPROVED = "1";   // Take the user to Timesheet activity and sync Timesheets according to last sync date saved
    public static final String TIMESHEET_REJECTED = "2";   // Take the user to Timesheet activity and sync Timesheets according to last sync date saved
    public static final String EXPENSE_APPROVED = "3";   // Take the user to Expenses activity and sync Expenses according to last sync date saved
    public static final String EXPENSE_REJECTED = "4";   // Take the user to Expenses activity and sync Expenses according to last sync date saved
    public static final String NEW_ASSIGNMENTS = "5";    // Sync Customer/Projects and Service items and let user be in the main screen
    public static final String TIMESHEETS_TO_APPROVE = "6"; // Take the user to “Timesheets for Approval” screen
    public static final String EXPENSES_TO_APPROVE = "7"; // Take the user to “Expenses for Approval” screen
    public static final String TIMESHEET_DUE_REMINDER = "8"; // Take the user to the MyTimesheetTransactions
    public static final String MESSAGE_FROM_ADMIN = "9"; //only show the message. No other action required.

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("arsh", "onMessageReceived: " + "message received FCM");
        SharedPreferences preferences = getSharedPreferences(Constants.SHARED_PREFERENCES, MODE_PRIVATE);
        if ("yes".equalsIgnoreCase(preferences.getString("session", "na"))) {
            displayTimesheetNotification(remoteMessage);
        } else {
            //logout state. No need to handle the notifications.
        }
    }

    protected void displayTimesheetNotification(RemoteMessage remoteMessage) {
        // Invoking the default notification service
        Notification.Builder mBuilder = new Notification.Builder(this);

        mBuilder.setContentTitle(remoteMessage.getData().get("title"));
//        mBuilder.setContentTitle("title");
        mBuilder.setContentText(remoteMessage.getData().get("description"));
//        mBuilder.setContentText("description description description description description description description " +
//                "description description description description description description description " +
//                "description description description description description description description ");
        mBuilder.setTicker(remoteMessage.getData().get("description"));
//        mBuilder.setTicker("description description description description description description description " +
//                "description description description description description description description " +
//                "description description description description description description description ");
        mBuilder.setSmallIcon(R.drawable.ic_notif_small);
        Bitmap icon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.launcher_icon);
        mBuilder.setLargeIcon(icon);
        mBuilder.setStyle(new Notification.BigTextStyle().bigText(remoteMessage.getData().get("description")));
        int defaults = 0;
        defaults = defaults | Notification.DEFAULT_LIGHTS;
        defaults = defaults | Notification.DEFAULT_VIBRATE;
        defaults = defaults | Notification.DEFAULT_SOUND;
        mBuilder.setDefaults(defaults);
        mBuilder.setAutoCancel(true);

        // Creates an implicit intent

        Intent resultIntent = null;
        String notificationID = remoteMessage.getData().get(Constants.NOTIFICATION_ID);
        //if notification id = 1;
        if (notificationID.equalsIgnoreCase(TIMESHEET_APPROVED)) {
            resultIntent = new Intent(this, MyTimeSheetTransactions.class);
            resultIntent.putExtra(Constants.RELOAD_LIST, true);
            resultIntent.putExtra("date", remoteMessage.getData().get("date"));
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            myNotificationManager.notify(Integer.parseInt(TIMESHEET_APPROVED), mBuilder.build());
        }
        //if notification id = 2;
        if (notificationID.equalsIgnoreCase(TIMESHEET_REJECTED)) {
            resultIntent = new Intent(this, MyTimeSheetTransactions.class);
            resultIntent.putExtra(Constants.RELOAD_LIST, true);
            resultIntent.putExtra("date", remoteMessage.getData().get("date"));
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            myNotificationManager.notify(Integer.parseInt(TIMESHEET_REJECTED), mBuilder.build());
        }
        //if notification id = 3;
        if (notificationID.equalsIgnoreCase(EXPENSE_APPROVED)) {
            resultIntent = new Intent(this, ExpenseSheetActivity.class);
            resultIntent.putExtra(Constants.RELOAD_LIST, true);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            myNotificationManager.notify(Integer.parseInt(EXPENSE_APPROVED), mBuilder.build());
        }
        //if notification id = 4;
        if (notificationID.equalsIgnoreCase(EXPENSE_REJECTED)) {
            resultIntent = new Intent(this, ExpenseSheetActivity.class);
            resultIntent.putExtra(Constants.RELOAD_LIST, true);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            myNotificationManager.notify(Integer.parseInt(EXPENSE_REJECTED), mBuilder.build());
        }
        //if notification id = 5;
        if (notificationID.equalsIgnoreCase(NEW_ASSIGNMENTS)) {
            resultIntent = new Intent(this, MainActivity.class);
            resultIntent.putExtra(Constants.NEW_ASSIGNMENTS, true);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            myNotificationManager.notify(Integer.parseInt(NEW_ASSIGNMENTS), mBuilder.build());
        }
        //if notification id = 6;
        if (notificationID.equalsIgnoreCase(TIMESHEETS_TO_APPROVE)) {
            resultIntent = new Intent(this, ApproveTimesheetLayer.class);
            ApproveTimesheetLayer.isToLoadFromServer = true;
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            myNotificationManager.notify(Integer.parseInt(TIMESHEETS_TO_APPROVE), mBuilder.build());
        }
        //if notification id = 7;
        if (notificationID.equalsIgnoreCase(EXPENSES_TO_APPROVE)) {
            resultIntent = new Intent(this, ApproveExpensesActivity.class);
            resultIntent.putExtra(Constants.RELOAD_LIST, true);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            myNotificationManager.notify(Integer.parseInt(EXPENSES_TO_APPROVE), mBuilder.build());
        }
        //if notification id = 8;
        if (notificationID.equalsIgnoreCase(TIMESHEET_DUE_REMINDER)) {
            resultIntent = new Intent(this, MyTimeSheetTransactions.class);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            myNotificationManager.notify(Integer.parseInt(TIMESHEET_DUE_REMINDER), mBuilder.build());
        }
        //if notification id = 9;
        if (notificationID.equalsIgnoreCase(MESSAGE_FROM_ADMIN)) {
            resultIntent = new Intent();
            PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0,
                    resultIntent, PendingIntent.FLAG_ONE_SHOT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            myNotificationManager.notify(Integer.parseInt(MESSAGE_FROM_ADMIN), mBuilder.build());
        }

    }
}
