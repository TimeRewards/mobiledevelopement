package com.timerewards.tr.Controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by machine on 9/2/17.
 */

public class AlarmReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Intent myService = new Intent(context, TimerService.class);
        context.startService(myService);
    }
}
