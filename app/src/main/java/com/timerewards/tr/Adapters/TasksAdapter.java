package com.timerewards.tr.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.TaskDataFile;
import com.timerewards.tr.DataFiles.TasksModel;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;

import java.util.ArrayList;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.MyViewHolder> {

    public int positionTask;
    LayoutInflater inflater;
    ArrayList<TasksModel> tasksModelArrayList = new ArrayList<>();
    TaskDataFile taskDataFile;
    TasksSelection tasksSelection;
    private Context context;

    public TasksAdapter(Context context, int position, ArrayList<TasksModel> tasksModelArrayList) {
        taskDataFile = (TaskDataFile) OfflineDataStorage.readObject(context, Constants.TASKS_DATA_FILE);
//        this.leaveTaskModelArrayList = taskDataFile.getTasksModelList();
        this.tasksModelArrayList = tasksModelArrayList;
        tasksSelection = (TasksSelection) context;
        inflater = LayoutInflater.from(context);
        this.context = context;
        positionTask = position;
    }

    public void changeData(ArrayList<TasksModel> tasksModelArrayList){
        this.tasksModelArrayList = tasksModelArrayList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_project, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;

    }

    public void changePosition(int position) {
        this.positionTask = position;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
//        if (position == positionTask) {
//            holder.radioButton.setChecked(true);
//        } else {
//            holder.radioButton.setChecked(false);
//        }

        if (tasksModelArrayList.get(position).getTaskID() == positionTask) {
            holder.radioButton.setChecked(true);
        } else {
            holder.radioButton.setChecked(false);
        }



        switch (tasksModelArrayList.get(position).getSubLevel()){
            case 0:
                holder.textView.setText("" + tasksModelArrayList.get(position).getTaskName());
                break;
            case 1:
                holder.textView.setText("   " + tasksModelArrayList.get(position).getTaskName());
                break;
            case 2:
                holder.textView.setText("      " + tasksModelArrayList.get(position).getTaskName());
                break;
            case 3:
                holder.textView.setText("         " + tasksModelArrayList.get(position).getTaskName());
                break;
            case 4:
                holder.textView.setText("            " + tasksModelArrayList.get(position).getTaskName());
                break;
            case 5:
                holder.textView.setText("               " + tasksModelArrayList.get(position).getTaskName());
                break;
        }

        if (!tasksModelArrayList.get(position).isClickable()) {
            holder.linearLayout.setEnabled(false);
            holder.radioButton.setChecked(false);
            holder.textView.setTextColor(Color.GRAY);
        } else {
            holder.linearLayout.setEnabled(true);
            holder.textView.setTextColor(Color.BLACK);
        }
//
//        myViewHolder.radioButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                positionTask = position;
//                myViewHolder.radioButton.toggle();
//            }
//        });
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tasksSelection.setTask(tasksModelArrayList.get(position).getTaskID());
            }
        });
        holder.radioButton.setEnabled(false);
    }

    @Override
    public int getItemCount() {
        return tasksModelArrayList.size();
    }

    public interface TasksSelection {
        public void setTask(int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        RadioButton radioButton;
        TextView textView;
        RelativeLayout linearLayout;

        public MyViewHolder(View item) {
            super(item);
            radioButton = (RadioButton) item.findViewById(R.id.radio_button);
            textView = (TextView) item.findViewById(R.id.textView);
            linearLayout = (RelativeLayout) item.findViewById(R.id.ll_parent);
        }
    }
}
