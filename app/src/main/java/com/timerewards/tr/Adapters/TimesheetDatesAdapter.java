package com.timerewards.tr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.timerewards.tr.Activities.TimeSheetActivity;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TimesheetDatesAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> dates;
    TableObject tableObject;
    ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();
    ArrayList<String> hoursArrayList = new ArrayList<>();
    ArrayList<String> datesArrayList = new ArrayList<>();
    float totalHours = 0.0f;

    public TimesheetDatesAdapter(Context context, ArrayList<String> dates) {
        this.context = context;
        this.dates = dates;
        tableObject = (TableObject) OfflineDataStorage.readObject(context, Constants.TIMESHEETS_FILE);
        timesheetArrayList = tableObject.getTimesheetList();
        datesArrayList = new ArrayList<>();
        for (int i = 0; i < dates.size(); i++) {
            datesArrayList.add(Utilities.dateToServerFormat(dates.get(i)));
        }

        for (int i = 0; i < datesArrayList.size(); i++) {
            float hours = 0.0f;
            for (int j = 0; j < timesheetArrayList.size(); j++) {
                if (timesheetArrayList.get(j).isMarkedForDeletion()) {
                    continue;
                } else {
                    if (timesheetArrayList.get(j).getActivityDate().equalsIgnoreCase(datesArrayList.get(i))) {
                        hours = hours + timesheetArrayList.get(j).getActualHours();
                    }
                }
            }
            hoursArrayList.add(String.valueOf(hours));
        }
        for (int i = 0; i < hoursArrayList.size(); i++) {
            totalHours = totalHours + Float.parseFloat(hoursArrayList.get(i));
        }

    }

    public float getTotalHours() {
        return totalHours;
    }

    public void changeData(ArrayList<String> dates) {
        this.dates = dates;
        tableObject = (TableObject) OfflineDataStorage.readObject(context, Constants.TIMESHEETS_FILE);
        timesheetArrayList = tableObject.getTimesheetList();
        hoursArrayList = new ArrayList<>();
        datesArrayList = new ArrayList<>();
        totalHours = 0.0f;
        for (int i = 0; i < dates.size(); i++) {
            datesArrayList.add(Utilities.dateToServerFormat(dates.get(i)));
        }

        for (int i = 0; i < datesArrayList.size(); i++) {
            float hours = 0.0f;
            for (int j = 0; j < timesheetArrayList.size(); j++) {
                if (timesheetArrayList.get(j).isMarkedForDeletion()) {
                    continue;
                } else {
                    if (timesheetArrayList.get(j).getActivityDate().equalsIgnoreCase(datesArrayList.get(i))) {
                        hours = hours + timesheetArrayList.get(j).getActualHours();
                    }
                }
            }
            hoursArrayList.add(String.valueOf(hours));
        }
        for (int i = 0; i < hoursArrayList.size(); i++) {
            totalHours = totalHours + Float.parseFloat(hoursArrayList.get(i));
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        MyViewHolder mViewHolder;

        final int temp = i;

        if (convertView == null) {
            convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_row_dates, viewGroup, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        if (i % 2 == 0) {
            mViewHolder.relativeLayout.setBackgroundColor(context.getResources().getColor(R.color.white_color));
            mViewHolder.relativeLayout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.selector_textview_white_row));
        } else {
            mViewHolder.relativeLayout.setBackgroundColor(context.getResources().getColor(R.color.gray_color_light));
            mViewHolder.relativeLayout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.selector_textview_gray_row));
        }


        mViewHolder.number.setText(String.format("%.2f", (double) Math.round(Double.parseDouble(hoursArrayList.get(i)) * 100) / 100));

        mViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TimeSheetActivity.class);
                String tempDate = dates.get(temp);
                SimpleDateFormat sdfmt1 = new SimpleDateFormat("dd-MMM-yyyy");
                SimpleDateFormat sdfmt2 = new SimpleDateFormat("yyyy-MM-dd");
                Date dDate = null;
                try {
                    dDate = sdfmt1.parse(tempDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String strOutput = sdfmt2.format(dDate);
                intent.putExtra("date", strOutput);
                context.startActivity(intent);
            }
        });

        if (i % 7 == 0) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.green_color_light));
        } else if (i % 7 == 1) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.red_color_light));
        } else if (i % 7 == 2) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.yellow_color_light));
        } else if (i % 7 == 3) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.cyan_color_light));
        } else if (i % 7 == 4) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.green_color_light));
        } else if (i % 7 == 5) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.red_color_light));
        } else if (i % 7 == 6) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.yellow_color_light));
        }

        mViewHolder.date.setText(dates.get(i));

        return convertView;
    }

    private class MyViewHolder {
        View view;
        TextView date, number;
        ImageView open_details;
        RelativeLayout relativeLayout;

        public MyViewHolder(View item) {
            view = item.findViewById(R.id.view);
            date = (TextView) item.findViewById(R.id.tv_date);
            number = (TextView) item.findViewById(R.id.tv_float_hours);
            open_details = (ImageView) item.findViewById(R.id.iv_open_details);
            relativeLayout = (RelativeLayout) item.findViewById(R.id.relative_layout);
        }
    }

}