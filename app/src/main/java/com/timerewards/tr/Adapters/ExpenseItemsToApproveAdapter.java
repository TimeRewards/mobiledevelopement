package com.timerewards.tr.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Activities.EditExpenseItemInApprovals;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.ApproveRejectExpenseResponseModel;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.ExpenseHeaderModel;
import com.timerewards.tr.DataFiles.ExpenseItemModel;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ExpenseItemsToApproveAdapter extends BaseAdapter {

    public ArrayList<ExpenseItemModel> expenseItemsOnDate = new ArrayList<>();
    public int[] positionInList;
    Context context;
    int expenseSheetPosition;
    ExpenseHeaderModel expenseHeaderModel;
    ArrayList<ExpenseItemModel> expenseItemModels;
    ExpenseDataFile expenseDataFile;
    Expense expense;
    int tempPosition = 0;
    EmployeeLogin employeeLogin;
    String rejectReason;
    GetOptionsDataFile getOptionsDataFile;
    Refresh refresh;

    public ExpenseItemsToApproveAdapter(Context context, int expenseSheetPosition) {
        this.context = context;
        refresh = (Refresh) context;
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(context, Constants.GETOPTIONS_FILE);
        this.expenseSheetPosition = expenseSheetPosition;
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
//        expense = expenseDataFile.getExpense().get(expenseSheetPosition);
//        expenseHeaderModel = expense.getExpHeader();
//        expenseItemModels = expense.getExpenseItemList();
//        positionInList = new int[expenseItemModels.size()];
//        for (int i = 0; i < expenseItemModels.size(); i++) {
//            if (expenseItemModels.get(i).isAcceptedByUser() || expenseItemModels.get(i).isRejectedByUser()) {
//
//            } else {
//                expenseItemsOnDate.add(expenseItemModels.get(i));
//                positionInList[tempPosition] = i;
//                tempPosition++;
//            }
//        }
    }

    public void changeData(ExpenseDataFile expenseDataFile) {
        expenseItemsOnDate.clear();
        tempPosition = 0;
        this.expenseDataFile = expenseDataFile;
        expense = expenseDataFile.getExpense().get(expenseSheetPosition);
        expenseHeaderModel = expense.getExpHeader();
        expenseItemModels = expense.getExpenseItemList();
        positionInList = new int[expenseItemModels.size()];
        for (int i = 0; i < expenseItemModels.size(); i++) {
            if (expenseItemModels.get(i).isAcceptedByUser() || expenseItemModels.get(i).isRejectedByUser()) {

            } else {
                expenseItemsOnDate.add(expenseItemModels.get(i));
                positionInList[tempPosition] = i;
                tempPosition++;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return expenseItemsOnDate.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        MyViewHolder mViewHolder;
        final int temp = i;
        if (convertView == null) {
            convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_row_expense_items_to_approve, viewGroup, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        if (getOptionsDataFile.isCanApproverEditExp()) {
            mViewHolder.iv_edit_expense.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.iv_edit_expense.setVisibility(View.GONE);
        }
        float amount = expenseItemsOnDate.get(i).getExpenseAmount();
        boolean billable = expenseItemsOnDate.get(i).isBillable();
        int paidby = expenseItemsOnDate.get(i).getPaidBy();

        mViewHolder.tvDate.setText(Utilities.dateToLocalFormat(expenseItemsOnDate.get(i).getExpenseDate()));
        mViewHolder.tvCustomer.setText(expenseItemsOnDate.get(i).getClassFullName());
        mViewHolder.tvAccount.setText(expenseItemsOnDate.get(i).getAccountFullName());
        mViewHolder.tvMerchant.setText(expenseItemsOnDate.get(i).getMerchant());
        mViewHolder.tvMerchant.setText(expenseItemsOnDate.get(i).getMerchant());
        mViewHolder.tvAmount.setText(String.format("%.2f", (double) Math.round(amount * 100) / 100));
        if (paidby == 1) {
            mViewHolder.tvPaidBy.setText("Paid By Me");
        } else {
            mViewHolder.tvPaidBy.setText("Paid By Company");
        }

        if (billable == true) {
            mViewHolder.tvBillable.setText("Billable");
        } else {
            mViewHolder.tvBillable.setText("Not Billable");
        }
        switch (expenseItemModels.get(i).getEZStatusID()) {
            case 0:
                mViewHolder.tvStatus.setText(Constants.EZStatusID_0);
                break;
            case 1:
                mViewHolder.tvStatus.setText(Constants.EZStatusID_1);
                break;
            case 2:
                mViewHolder.tvStatus.setText(Constants.EZStatusID_2);
                break;
            case 3:
                mViewHolder.tvStatus.setText(Constants.EZStatusID_3);
                break;
            case 4:
                mViewHolder.tvStatus.setText(Constants.EZStatusID_4);
                break;
            case 5:
                mViewHolder.tvStatus.setText(Constants.EZStatusID_5);
                break;
        }

        mViewHolder.tvDetails.setText(expenseItemsOnDate.get(i).getDescription());
        mViewHolder.tvAcceptExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                approveExpenseOnServer(expenseItemsOnDate.get(temp).getEmployeeExpenseItemID(), positionInList[temp], true);
            }
        });
        mViewHolder.tvRejectExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_reject_reason);
                dialog.setTitle("Please Enter Reject Reason");
                final EditText etReason = (EditText) dialog.findViewById(R.id.et_dialog_reject_reason);
                Button buttonReject = (Button) dialog.findViewById(R.id.button_dialog_ok);
                Button buttonCancel = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                dialog.setCancelable(false);
                buttonReject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (etReason.getText().toString().length() > 0) {
                            rejectReason = etReason.getText().toString();
                            approveExpenseOnServer(expenseItemsOnDate.get(temp).getEmployeeExpenseItemID(), positionInList[temp], false);
                            dialog.dismiss();
                        } else {
                            etReason.setError("Can't be left Blank");
                        }
                    }
                });
                buttonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        mViewHolder.iv_edit_expense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditExpenseItemInApprovals.class);
                intent.putExtra(Constants.HEADER_POSITION, expenseSheetPosition);
                intent.putExtra(Constants.EXPENSE_ITEM_POSITION, positionInList[temp]);
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    private void approveExpenseOnServer(final int expenseItemID, final int itemPositionInList, final boolean accept) {

        String url = "";
        url = Constants.ACCEPT_REJECT_EXPENSE_API;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("EmployeeExpenseID", "0");
            jsonObject.put("EmployeeExpenseItemID", String.valueOf(expenseItemID));
            jsonObject.put("ApproveOrReject", accept);
            jsonObject.put("AppSignature", employeeLogin.getApproverSignature());
            if (accept == false) {
                jsonObject.put("AdminNotes", rejectReason);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("arsh", "url=" + url);

        if (Utilities.isOnline(context)) {
            JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    ShowDialog.hideDialog();
                    ApproveRejectExpenseResponseModel responseModel = new GsonBuilder().create().fromJson(s.toString(), ApproveRejectExpenseResponseModel.class);
                    if (responseModel.getEmployeeExpenseItems().size() > 0) {
                        expenseItemModels.get(itemPositionInList).setEmployeeExpenseItemID(responseModel.getEmployeeExpenseItems().get(0).getEZStatusID());
                        if (accept == true) {
                            expenseItemModels.get(itemPositionInList).setAcceptedByUser(true);
                        } else {
                            expenseItemModels.get(itemPositionInList).setAcceptedByUser(false);
                        }
                        expenseItemModels.get(itemPositionInList).setMarkedApprovedRejectedOnServer(true);
                        expenseItemModels.get(itemPositionInList).setAcceptedByUser(true);
                        expenseItemModels.get(itemPositionInList).setMarkedForAcceptRejectByUser(true);
                        expenseDataFile.getExpense().get(expenseSheetPosition).setExpenseItemList(expenseItemModels);
                        if (accept == true) {
                            Toast.makeText(context, "Expense Approved on Server", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Expense Rejected on Server", Toast.LENGTH_SHORT).show();
                        }
                        double amount = 0.0d;
                        for (int i = 0; i < expenseItemModels.size(); i++) {
                            if (expenseItemModels.get(i).isMarkedForAcceptRejectByUser() == false) {
                                amount = amount + expenseItemModels.get(i).getExpenseAmount();
                            }
                        }

                        refresh.refresh(itemPositionInList);

//                        if (amount > 0) {
//                            restartActivity();
//                        } else {
//                            Intent intent = new Intent(context, ApproveExpensesActivity.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            context.startActivity(intent);
//                            ((ApproveExpensesDetails) context).finish();
//                        }
                    } else {
                        Toast.makeText(context, "No Expense Approved", Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    ShowDialog.hideDialog();
                    Utilities.handleVolleyError(context, volleyError);
                }
            });
            ShowDialog.showDialog(context);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public interface Refresh {
        public void refresh(int expenseItemPosition);
    }

    private class MyViewHolder {
        TextView tvDate, tvAccount, tvMerchant, tvCustomer, tvAmount, tvPaidBy, tvBillable, tvStatus, tvDetails, tvAcceptExpense, tvRejectExpense;
        ImageView iv_edit_expense;

        public MyViewHolder(View item) {
            tvDate = (TextView) item.findViewById(R.id.tv_expense_date);
            tvAccount = (TextView) item.findViewById(R.id.tv_account);
            tvMerchant = (TextView) item.findViewById(R.id.tv_merchant);
            tvCustomer = (TextView) item.findViewById(R.id.tv_customer);
            tvAmount = (TextView) item.findViewById(R.id.tv_amount);
            tvPaidBy = (TextView) item.findViewById(R.id.tv_paid_by);
            tvBillable = (TextView) item.findViewById(R.id.tv_billable);
            tvStatus = (TextView) item.findViewById(R.id.tv_status);
            tvDetails = (TextView) item.findViewById(R.id.tv_details);
            tvAcceptExpense = (TextView) item.findViewById(R.id.tv_accept_expense);
            tvRejectExpense = (TextView) item.findViewById(R.id.tv_reject_expense);
            iv_edit_expense = (ImageView) item.findViewById(R.id.iv_edit_expense);
        }
    }
}