package com.timerewards.tr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.timerewards.tr.Activities.ExpenseSheetActivity;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ExpenseSheetDatesAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> dates;
    ArrayList<String> amountArrayList = new ArrayList<>();
    ArrayList<String> datesArrayList = new ArrayList<>();
    ArrayList<Expense> expenseArrayList = new ArrayList<>();
    ExpenseDataFile expenseDataFile;

    public ExpenseSheetDatesAdapter(Context context, ArrayList<String> dates) {
        this.context = context;
        this.dates = dates;
        expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(context, Constants.EXPENSE_DATA_FILE);
        expenseArrayList = expenseDataFile.getExpense();
        for (int i = 0; i < dates.size(); i++) {
            datesArrayList.add(Utilities.dateToServerFormat(dates.get(i)));
        }
        for (int i = 0; i < datesArrayList.size(); i++) {
            float amount = 0.0f;
            for (int j = 0; j < expenseArrayList.size(); j++) {
                for (int k = 0; k < expenseArrayList.get(j).getExpenseItemList().size(); k++) {
                    if (expenseArrayList.get(j).getExpenseItemList().get(k).isMarkedForDeletion()) {
                        continue;
                    } else {
                        if (expenseArrayList.get(j).getExpenseItemList().get(k).getExpenseDate().equalsIgnoreCase(datesArrayList.get(i))) {
                            amount = amount + expenseArrayList.get(j).getExpenseItemList().get(k).getExpenseAmount();
                        }
                    }
                }
            }
            amountArrayList.add(String.valueOf(amount));
        }
    }

    public void changeData(ArrayList<String> dates) {
        this.dates = dates;
        notifyDataSetChanged();
        amountArrayList = new ArrayList<>();
        datesArrayList = new ArrayList<>();
        for (int i = 0; i < dates.size(); i++) {
            datesArrayList.add(Utilities.dateToServerFormat(dates.get(i)));
        }
        for (int i = 0; i < datesArrayList.size(); i++) {
            float amount = 0.0f;
            for (int j = 0; j < expenseArrayList.size(); j++) {
                for (int k = 0; k < expenseArrayList.get(j).getExpenseItemList().size(); k++) {
                    if (expenseArrayList.get(j).getExpenseItemList().get(k).isMarkedForDeletion()) {
                        continue;
                    } else {
                        if (expenseArrayList.get(j).getExpHeader().getEntryDate().equalsIgnoreCase(datesArrayList.get(i))) {
                            amount = amount + expenseArrayList.get(j).getExpenseItemList().get(k).getExpenseAmount();
                        }
                    }
                }
            }
            amountArrayList.add(String.format("%.2f", (double) Math.round(amount * 100) / 100));
        }
    }

    @Override
    public int getCount() {
        return dates.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        MyViewHolder mViewHolder;

        final int temp = i;

        if (convertView == null) {
            convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_row_expense_dates, viewGroup, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        if (i % 2 == 0) {
            mViewHolder.relativeLayout.setBackgroundColor(context.getResources().getColor(R.color.white_color));
            mViewHolder.relativeLayout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.selector_textview_white_row));
        } else {
            mViewHolder.relativeLayout.setBackgroundColor(context.getResources().getColor(R.color.gray_color_light));
            mViewHolder.relativeLayout.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.selector_textview_gray_row));
        }
        mViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ExpenseSheetActivity.class);
                String tempDate = dates.get(temp);
                SimpleDateFormat sdfmt1 = new SimpleDateFormat("dd-MMM-yyyy");
                SimpleDateFormat sdfmt2 = new SimpleDateFormat("yyyy-MM-dd");
                java.util.Date dDate = null;
                try {
                    dDate = sdfmt1.parse(tempDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String strOutput = sdfmt2.format(dDate);
                intent.putExtra("date", strOutput);
                context.startActivity(intent);
            }
        });

        if (i % 7 == 0) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.green_color_light));
        } else if (i % 7 == 1) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.red_color_light));
        } else if (i % 7 == 2) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.yellow_color_light));
        } else if (i % 7 == 3) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.cyan_color_light));
        } else if (i % 7 == 4) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.green_color_light));
        } else if (i % 7 == 5) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.red_color_light));
        } else if (i % 7 == 6) {
            mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.yellow_color_light));
        }

        mViewHolder.date.setText(dates.get(i));
        mViewHolder.tvAmount.setText(amountArrayList.get(i));

        return convertView;
    }

    private class MyViewHolder {
        View view;
        TextView date, tvAmount;
        ImageView open_details;
        RelativeLayout relativeLayout;

        public MyViewHolder(View item) {
            view = item.findViewById(R.id.view);
            date = (TextView) item.findViewById(R.id.tv_date);
            tvAmount = (TextView) item.findViewById(R.id.tv_float_amount);
            open_details = (ImageView) item.findViewById(R.id.iv_open_details);
            relativeLayout = (RelativeLayout) item.findViewById(R.id.relative_layout);
        }
    }
}