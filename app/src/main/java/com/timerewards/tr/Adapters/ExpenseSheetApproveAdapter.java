package com.timerewards.tr.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Activities.ApproveExpensesDetails;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.ApproveRejectExpenseResponseModel;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ExpenseSheetApproveAdapter extends BaseAdapter {

    public ArrayList<Expense> expensesNotAcceptedRejected = new ArrayList<>();
    public int[] positionInList;
    int tempPosition = 0;
    SimpleDateFormat simpleDateFormatOLD = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat simpleDateFormatNEW = new SimpleDateFormat("dd-MMM-yyyy");
    EmployeeLogin employeeLogin;
    TextView tvAccept, tvReject;
    String rejectReason;
    Refresh refresh;
    private Context context;
    private ArrayList<Expense> expenseArrayList = new ArrayList<>();
    private ExpenseDataFile expenseDataFile;
    private int numOfRequests = 0;

    public ExpenseSheetApproveAdapter(Context context) {
        this.context = context;
        refresh = (Refresh) context;
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
//        expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(context, Constants.EXPENSE_APPROVE_DATA_FILE);
//        if (expenseDataFile == null) {
//            expenseDataFile = new ExpenseDataFile();
//            OfflineDataStorage.writeObject(context, expenseDataFile, Constants.EXPENSE_APPROVE_DATA_FILE);
//        }
//        expenseArrayList = expenseDataFile.getExpense();
//        positionInList = new int[expenseArrayList.size() + 1];
//        for (int i = 0; i < expenseArrayList.size(); i++) {
//            if (!expenseArrayList.get(i).isMarkedForAcceptRejectByUser()) {
//                Expense expense = expenseArrayList.get(i);
//                boolean hasItemToApproveReject = false;
//                for (int j = 0; j < expense.getExpenseItemList().size(); j++) {
//                    if (hasItemToApproveReject == false) {
//                        if (expense.getExpenseItemList().get(j).isMarkedApprovedRejectedOnServer() == false) {
//                            if (!(expense.getExpenseItemList().get(j).isAcceptedByUser() || expense.getExpenseItemList().get(j).isRejectedByUser())) {
//                                hasItemToApproveReject = true;
//                            }
//                        }
//                    }
//                }
//                if (hasItemToApproveReject == true) {
//                    expensesNotAcceptedRejected.add(expenseArrayList.get(i));
//                    positionInList[tempPosition] = i;
//                    tempPosition++;
//                }
//            }
//        }

    }

    public void changeData(ExpenseDataFile expenseDataFile) {
        expensesNotAcceptedRejected = new ArrayList<>();
        tempPosition = 0;
        this.expenseDataFile = expenseDataFile;
        this.expenseArrayList = expenseDataFile.getExpense();
        positionInList = new int[expenseArrayList.size() + 1];
        for (int i = 0; i < expenseArrayList.size(); i++) {
            if (!expenseArrayList.get(i).isMarkedForAcceptRejectByUser()) {
                Expense expense = expenseArrayList.get(i);
                boolean hasItemToApproveReject = false;
                for (int j = 0; j < expense.getExpenseItemList().size(); j++) {
                    if (hasItemToApproveReject == false) {
                        if (expense.getExpenseItemList().get(j).isMarkedApprovedRejectedOnServer() == false) {
                            if (!(expense.getExpenseItemList().get(j).isAcceptedByUser() || expense.getExpenseItemList().get(j).isRejectedByUser())) {
                                hasItemToApproveReject = true;
                            }
                        }
                    }
                }
                if (hasItemToApproveReject == true) {
                    expensesNotAcceptedRejected.add(expenseArrayList.get(i));
                    positionInList[tempPosition] = i;
                    tempPosition++;
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return expensesNotAcceptedRejected.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        MyViewHolder mViewHolder;
        final int temp = i;
        if (convertView == null) {
            convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_row_expenses_to_approve, viewGroup, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        Expense expense = expensesNotAcceptedRejected.get(i);
        mViewHolder.tvEmployeeName.setText(expense.getExpenseItemList().get(0).getEmployeeFullName());
        mViewHolder.tvExpenseReason.setText(expense.getExpHeader().getDescription());

        String date = expensesNotAcceptedRejected.get(i).getExpHeader().getEntryDate();
        Date date1 = null;
        try {
            date1 = simpleDateFormatOLD.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mViewHolder.tvExpenseDate.setText(simpleDateFormatNEW.format(date1));

        float amount = 0.0f;

        for (int a = 0; a < expense.getExpenseItemList().size(); a++) {
            if (expense.getExpenseItemList().get(a).isMarkedForAcceptRejectByUser() == false) {
                amount = amount + expense.getExpenseItemList().get(a).getExpenseAmount();
            }
        }

        mViewHolder.tvExpenseAmount.setText(String.format("%.2f", (double) Math.round(amount * 100) / 100));

        mViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ApproveExpensesDetails.class);
                intent.putExtra("id", positionInList[temp]);
                context.startActivity(intent);
            }
        });
        mViewHolder.relativeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_expense_sheet_options);
                dialog.show();

                tvAccept = (TextView) dialog.findViewById(R.id.tv_accept);
                tvAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Utilities.isOnline(context)) {
                            approveExpenseOnServer(positionInList[temp], true);
                        } else {
                            Utilities.showNoConnectionToast(context);
                        }
                    }
                });
                tvReject = (TextView) dialog.findViewById(R.id.tv_reject);
                tvReject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Utilities.isOnline(context)) {
                            final Dialog dialog = new Dialog(context);
                            dialog.setContentView(R.layout.dialog_reject_reason);
                            dialog.setTitle("Please Enter Reject Reason");
                            final EditText etReason = (EditText) dialog.findViewById(R.id.et_dialog_reject_reason);
                            Button buttonReject = (Button) dialog.findViewById(R.id.button_dialog_ok);
                            Button buttonCancel = (Button) dialog.findViewById(R.id.button_dialog_cancel);
                            dialog.setCancelable(false);
                            buttonReject.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (etReason.getText().toString().length() > 0) {
                                        rejectReason = etReason.getText().toString();
                                        approveExpenseOnServer(positionInList[temp], false);
                                        dialog.dismiss();
                                    } else {
                                        etReason.setError("Can't be left Blank");
                                    }
                                }
                            });
                            buttonCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        } else {
                            Utilities.showNoConnectionToast(context);
                        }

                    }
                });
                return false;
            }
        });
        return convertView;
    }

    private void approveExpenseOnServer(final int expenseSheetPosition, final boolean accept) {

        String url = "";
        url = Constants.ACCEPT_REJECT_EXPENSE_API;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("EmployeeExpenseID", expenseArrayList.get(expenseSheetPosition).getExpHeader().getEmployeeExpenseID());
            jsonObject.put("EmployeeExpenseItemID", "");
            jsonObject.put("ApproveOrReject", accept);
            jsonObject.put("AppSignature", employeeLogin.getApproverSignature());
            if (accept == false) {
                jsonObject.put("AdminNotes", rejectReason);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("arsh", "url=" + url);

        if (Utilities.isOnline(context)) {
            JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    ApproveRejectExpenseResponseModel responseModel = new GsonBuilder().create().fromJson(s.toString(), ApproveRejectExpenseResponseModel.class);
                    if (responseModel.getEmployeeExpenseItems().size() > 0) {
                        notifyDataSetChanged();
                        refresh.refresh(expenseSheetPosition);
                    } else {
                        Toast.makeText(context, "No Expense Approved or Rejected", Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    ShowDialog.hideDialog();
                    Utilities.handleVolleyError(context, volleyError);
                }
            });
            ShowDialog.showDialog(context);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 4, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        } else {
            Utilities.showNoConnectionToast(context);
        }
    }

    public interface Refresh {
        public void refresh(int expenseSheetPosition);
    }

    private class MyViewHolder {
        View view, view2, view3;
        TextView tvEmployeeName, tvExpenseReason, tvExpenseDate, tvExpenseAmount;
        ImageView ivDetails;
        RelativeLayout relativeLayout;

        public MyViewHolder(View item) {
            view = item.findViewById(R.id.view);
            view2 = item.findViewById(R.id.view2);
            view3 = item.findViewById(R.id.view3);
            tvEmployeeName = (TextView) item.findViewById(R.id.tv_employee_name);
            tvExpenseReason = (TextView) item.findViewById(R.id.tv_expense_reason);
            ivDetails = (ImageView) item.findViewById(R.id.iv_open_details);
            relativeLayout = (RelativeLayout) item.findViewById(R.id.relative_layout);
            tvExpenseDate = (TextView) item.findViewById(R.id.tv_expense_date);
            tvExpenseAmount = (TextView) item.findViewById(R.id.tv_timesheet_hours);
        }
    }

}