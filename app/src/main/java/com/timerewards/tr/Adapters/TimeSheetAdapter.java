package com.timerewards.tr.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Activities.DuplicateTimesheetActivity;
import com.timerewards.tr.Activities.MyTimeSheetTransactions;
import com.timerewards.tr.Activities.TimeSheetActivity;
import com.timerewards.tr.Activities.TimeSheetDetails;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.DataModels.TimeSheetResponse;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class TimeSheetAdapter extends BaseAdapter {

    public ArrayList<Timesheet> timesheetsOnDate;
    public int[] positionInList;
    Context context;
    String date;
    TableObject tableObject;
    ArrayList<Timesheet> timesheets;
    Timesheet timesheet;
    String tempDate;
    int timeSheetIdInTimeSheets;
    int tempPosition = 0;
    TextView tvDelete, tvDuplicate, tvSubmit;
    EmployeeLogin employeeLogin;

    public TimeSheetAdapter(Context context, String date) {
        this.context = context;
        this.date = date;
        timesheetsOnDate = new ArrayList<>();
        tableObject = (TableObject) OfflineDataStorage.readObject(context, Constants.TIMESHEETS_FILE);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
        timesheets = tableObject.getTimesheetList();
        positionInList = new int[timesheets.size()];
        for (int i = 0; i < timesheets.size(); i++) {
            timesheet = timesheets.get(i);
            tempDate = timesheet.getActivityDate();
            String tempDate2 = tempDate.substring(0, tempDate.indexOf("T"));
            if (date.equals(tempDate.substring(0, tempDate.indexOf("T")))) {
                if (timesheet.isMarkedForDeletion() == false) {
                    timesheetsOnDate.add(timesheets.get(i));
                    timeSheetIdInTimeSheets = i;
                    positionInList[tempPosition] = i;
                    tempPosition++;
                }
            }
        }
    }

    public void changeData(String date) {
        this.date = date;
        tempPosition = 0;
        timesheetsOnDate = new ArrayList<>();
        tableObject = (TableObject) OfflineDataStorage.readObject(context, Constants.TIMESHEETS_FILE);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
        timesheets = tableObject.getTimesheetList();
        positionInList = new int[timesheets.size()];
        for (int i = 0; i < timesheets.size(); i++) {
            timesheet = timesheets.get(i);
            tempDate = timesheet.getActivityDate();
            String tempDate2 = tempDate.substring(0, tempDate.indexOf("T"));
            if (date.equals(tempDate.substring(0, tempDate.indexOf("T")))) {
                if (timesheet.isMarkedForDeletion() == false) {
                    timesheetsOnDate.add(timesheets.get(i));
                    timeSheetIdInTimeSheets = i;
                    positionInList[tempPosition] = i;
                    tempPosition++;
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return timesheetsOnDate.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        MyViewHolder mViewHolder;
        final int temp = i;
        if (convertView == null) {
            convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_row_timesheet, viewGroup, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        mViewHolder.tvCustomer.setText(timesheetsOnDate.get(i).getProjectFullName());
        if(timesheetsOnDate.get(i).getProjectFullName()!=null)
        {
            if (timesheetsOnDate.get(i).getProjectFullName().equalsIgnoreCase("None")) {
                mViewHolder.tvCustomer.setText("None");
            }
        }else
        {
            mViewHolder.tvCustomer.setText("None");
        }

        mViewHolder.tvTask.setText(timesheetsOnDate.get(i).getTaskFullName());
        Log.e("getActualHours",timesheetsOnDate.get(i).getActualHours()+"");
        mViewHolder.tvHours.setText(String.format("%.2f", (double) Math.round(timesheetsOnDate.get(i).getActualHours() * 100) / 100));
        mViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TimeSheetDetails.class);
                intent.putExtra("id", positionInList[temp]);
                intent.putExtra("date", date);
                context.startActivity(intent);
            }
        });

        mViewHolder.relativeLayout.setLongClickable(true);

        switch (timesheetsOnDate.get(i).getEZStatusID()) {
            case 0:
                mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.onhold_local_color));
                break;
            case 1:
                mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.rejected_color));
                break;
            default:
                mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.other_status_color));
                break;
        }

        mViewHolder.relativeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_timesheet_options);
                dialog.show();
                tvDelete = (TextView) dialog.findViewById(R.id.tv_delete_timesheet);
                tvDuplicate = (TextView) dialog.findViewById(R.id.tv_duplicate_timesheet);
                tvSubmit = (TextView) dialog.findViewById(R.id.tv_submit_timesheet);
                tvSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        if (Utilities.isOnline(context)) {
                            submitTimeSheetOnServer(positionInList[temp]);
                        } else {
                            Utilities.showNoConnectionToast(context);
                        }
                    }
                });
                tvDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        if (timesheetsOnDate.get(temp).getEZStatusID() == 0 || timesheetsOnDate.get(temp).getEZStatusID() == 1) {
                            new AlertDialog.Builder(context)
                                    .setTitle("Confirm Delete!")
                                    .setMessage("Are you sure you want to Delete?")
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {
                                            deleteTimesheet(positionInList[temp]);
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, null).show();
                        } else {
                            Toast.makeText(context, "Can't be Deleted", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                tvDuplicate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent intent2 = new Intent(context, DuplicateTimesheetActivity.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent2.putExtra(Constants.TIMESHEET_POSITION, positionInList[temp]);
                        context.startActivity(intent2);
                    }
                });
                return true;
            }
        });
        return convertView;
    }

    private void submitTimeSheetOnServer(final int timesheetPosition) {

        String submitDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").format(Calendar.getInstance().getTimeInMillis());

        if (tableObject.getTimesheetList().get(timesheetPosition).getEZStatusID() == 0
                || tableObject.getTimesheetList().get(timesheetPosition).getEZStatusID() == 1) {
            if (Utilities.isOnline(context)) {
                timesheet = tableObject.getTimesheetList().get(timesheetPosition);
                String displayname = null;
                String activityDate = null;
                String projectname = null;
                String taskname = null;
                String classname = null;
                String starttime = null;
                String endtime = null;
                String actualhours = null;
                String descrip = null;
                String adminnotes = null;
                String taskid = null;
                String projectid = null;
                String classrowid = null;
                String billable = null;
                String ezstatusid = null;
                String istime = null;
                String empsubdate = submitDate;
                String empsignature = null;
                String status = null;
                String timeactivityid = null;
                if (tableObject.getDisplayName() != null) {
                    displayname = employeeLogin.getDisplayName();
                }
                if (timesheet.getActivityDate() != null) {
                    activityDate = timesheet.getActivityDate();
                }
                if (timesheet.getProjectFullName() != null) {
                    projectname = timesheet.getProjectFullName();
                }
                if (timesheet.getTaskFullName() != null) {
                    taskname = timesheet.getTaskFullName();
                }
                if (timesheet.getClassFullName() != null) {
                    classname = timesheet.getClassFullName();
                }
                if (timesheet.getStartTime() != null) {
                    starttime = timesheet.getStartTime();
                }
                if (timesheet.getEndTime() != null) {
                    endtime = timesheet.getEndTime();
                }

                actualhours = "" + timesheet.getActualHours();

                if (timesheet.getDescription() != null) {
                    descrip = timesheet.getDescription();
                }
                if (timesheet.getAdminNotes() != null) {
                    adminnotes = timesheet.getAdminNotes();
                }
                taskid = "" + timesheet.getTaskID();
                projectid = "" + timesheet.getProjectID();
                classrowid = "" + timesheet.getClassRowID();
                billable = "" + timesheet.isBillable();
                ezstatusid = "" + timesheet.getEZStatusID();
                istime = "" + timesheet.isTime();
                if (timesheet.getEmpSubDate() != null) {
                    empsubdate = timesheet.getEmpSubDate();
                }
                if (timesheet.getEmpSignature() != null) {
                    empsignature = timesheet.getEmpSignature();
                }
                if (timesheet.getStatus() != null) {
                    status = timesheet.getStatus();
                }
                timeactivityid = "" + timesheet.getTimeActivityID();

                String url = Constants.SUBMIT_NEW_TIMESHEET_API;
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("userid", employeeLogin.getUserid());
                    jsonObject.put("sitecode", employeeLogin.getSiteCode());
                    jsonObject.put("EmployeeFullName", employeeLogin.getDisplayName());
                    jsonObject.put("ActivityDate", timesheet.getActivityDate());
                    jsonObject.put("ProjectFullName", timesheet.getProjectFullName());
                    jsonObject.put("TaskFullName", taskname);
                    jsonObject.put("ClassFullName", classname);
                    jsonObject.put("StartTime", starttime);
                    jsonObject.put("EndTime", endtime);
                    jsonObject.put("ActualHours", actualhours);
                    jsonObject.put("Description", descrip);
                    jsonObject.put("AdminNotes", adminnotes);
                    jsonObject.put("TaskID", taskid);
                    jsonObject.put("ProjectID", projectid);
                    jsonObject.put("ClassRowID", classrowid);
                    jsonObject.put("Billable", billable);
                    jsonObject.put("EZStatusID", ezstatusid);
                    jsonObject.put("IsTime", istime);
                    jsonObject.put("EmpSignature", empsignature);
                    jsonObject.put("EmpSubDate", empsubdate);
                    jsonObject.put("Status", status);
                    jsonObject.put("SourceRefID", 18);
                    jsonObject.put("timeActivityID", timeactivityid);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i("arsh", "url = " + url);
                Log.i("arsh", "TimeSheet Position = " + timesheetPosition);
                Log.i("arsh", "Display Name = " + displayname);
                Log.i("arsh", "UserID = " + tableObject.getUserid());
                Log.i("arsh", "Activity Date = " + activityDate);
                Log.i("arsh", "task Name = " + taskname);
                Log.i("arsh", "class Name = " + classname);
                Log.i("arsh", "Project Name" + projectname);
                Log.i("arsh", "start time = " + starttime);
                Log.i("arsh", "end time = " + endtime);
                Log.i("arsh", "Actual hours = " + actualhours);
                Log.i("arsh", "Description = " + descrip);
                Log.i("arsh", "Admin Notes = " + adminnotes);
                Log.i("arsh", "task id = " + taskid);
                Log.i("arsh", "Project id = " + projectid);
                Log.i("arsh", "class row id = " + classrowid);
                Log.i("arsh", "billable = " + billable);
                Log.i("arsh", "EZ Status id = " + ezstatusid);
                Log.i("arsh", "Is time = " + istime);
                Log.i("arsh", "Emp sub date = " + empsubdate);
                Log.i("arsh", "Emp Signature = " + empsignature);
                Log.i("arsh", "status = " + status);
                Log.i("arsh", "time activity id = " + timeactivityid);
                JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject
                        , new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        ShowDialog.hideDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.has("TimeActivityID")) {
                                Log.i("arsh", "response = " + s);
                                TimeSheetResponse timeSheetResponse = new GsonBuilder().create().fromJson(s.toString(), TimeSheetResponse.class);
                                tableObject.getTimesheetList().get(timesheetPosition).setTimeActivityID(timeSheetResponse.getTimeActivityID());
                                tableObject.getTimesheetList().get(timesheetPosition).setEZStatusID(timeSheetResponse.getEZStatusID());
                                tableObject.getTimesheetList().get(timesheetPosition).set$id("");
                                Log.i("arsh", "" + timeSheetResponse.getTimeActivityID());
                                Toast.makeText(context, "Submitted Successfully", Toast.LENGTH_SHORT).show();
                                OfflineDataStorage.writeObject(context, tableObject, Constants.TIMESHEETS_FILE);
                                startActivityAfterDelete();
                            } else {
                                if (jsonObject.has("ErrorMessage")) {
                                    Toast.makeText(context, jsonObject.getString("ErrorMessage"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, "Request Failed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            Toast.makeText(context, "Exception occured.", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        ShowDialog.hideDialog();
                        Utilities.handleVolleyError(context, volleyError);
                    }
                });
                ShowDialog.showDialog(context);
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);
            } else {
                Utilities.showNoConnectionToast(context);
            }
        } else {
            Toast.makeText(context, "Already Submitted", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteTimesheet(final int timesheetPosition) {
        if (timesheet.getEZStatusID() == 0 && timesheet.get$id().equalsIgnoreCase("new")) {
            tableObject.getTimesheetList().remove(timesheetPosition);
            OfflineDataStorage.writeObject(context, tableObject, Constants.TIMESHEETS_FILE);
            startActivityAfterDelete();

        } else {
            if (timesheet.getEZStatusID() == 0 || timesheet.getEZStatusID() == 1) {
                if (Utilities.isOnline(context)) {
                    String url = Constants.DELETE_TIMESHEET_API;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("userid", employeeLogin.getUserid());
                        jsonObject.put("sitecode", employeeLogin.getSiteCode());
                        jsonObject.put("timeActivityID", timesheet.getTimeActivityID());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                            + "?timeActivityID=" + timesheet.getTimeActivityID()
//                            + "&UserID=" + employeeLogin.getUserid()
//                            + "&SiteCode=" + employeeLogin.getSiteCode();
                    Log.i("arsh", "url=" + url);
                    JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject
                            , new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            ShowDialog.hideDialog();
                            Log.i("arsh", "response=" + s);
                            if (s.equalsIgnoreCase("true")) {
                                tableObject.getTimesheetList().remove(timesheetPosition);
                                Toast.makeText(context, "Timesheet Delete failed", Toast.LENGTH_SHORT).show();
                                OfflineDataStorage.writeObject(context, tableObject, Constants.TIMESHEETS_FILE);
                                startActivityAfterDelete();
                            } else {
                                Toast.makeText(context, "Timesheet Delete failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            ShowDialog.hideDialog();
                            Utilities.handleVolleyError(context, volleyError);
                        }
                    });
                    ShowDialog.showDialog(context);
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                } else {
                    Utilities.showNoConnectionToast(context);
                }
            }
        }
    }

    private void startActivityAfterDelete() {
        Intent intent = new Intent(context, MyTimeSheetTransactions.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        ((TimeSheetActivity) context).finish();
    }

    private class MyViewHolder {
        View view, view2;
        TextView tvCustomer, tvTask, tvHours;
        ImageView ivDetails;
        RelativeLayout relativeLayout;

        public MyViewHolder(View item) {
            view = item.findViewById(R.id.view);
            view2 = item.findViewById(R.id.view2);
            tvCustomer = (TextView) item.findViewById(R.id.tv_customer);
            tvTask = (TextView) item.findViewById(R.id.tv_task);
            ivDetails = (ImageView) item.findViewById(R.id.iv_open_details);
            relativeLayout = (RelativeLayout) item.findViewById(R.id.relative_layout);
            tvHours = (TextView) item.findViewById(R.id.tv_timesheet_hours);
        }
    }

}