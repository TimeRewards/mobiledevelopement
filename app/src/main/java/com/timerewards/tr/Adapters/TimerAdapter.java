package com.timerewards.tr.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.timerewards.tr.Activities.NewTimeSheetFromTimer;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.Controller.TimerService;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.TableObject;
import com.timerewards.tr.DataFiles.TimerModel;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.Utilities;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimerAdapter extends RecyclerView.Adapter<TimerAdapter.MyViewHolder> {

    private GetOptionsDataFile getOptionsDataFile;
    private ArrayList<TimerModel> timerModelArrayList = new ArrayList<>();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
    private Context context;
    private EmployeeLogin employeeLogin;


    public TimerAdapter(Context context, ArrayList<TimerModel> timerModelArrayList) {
        this.context = context;
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(context, Constants.GETOPTIONS_FILE);
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
        this.timerModelArrayList = timerModelArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_timer, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    public void changeData(ArrayList<TimerModel> timerModelArrayList) {
        this.timerModelArrayList = timerModelArrayList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final TimerModel timerModel = timerModelArrayList.get(position);
        Calendar calendar = Calendar.getInstance();
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTime(new Date(timerModel.getMillis()));
        holder.tvTimerName.setText("Start Timer " + String.valueOf(position + 1));
        holder.tvTime.setText(simpleDateFormat.format(calendar.getTime()));
        if (timerModel.getTimesheet() != null) {
            holder.tvProjectName.setVisibility(View.VISIBLE);
            holder.tvTaskName.setVisibility(View.VISIBLE);
            holder.tvProjectName.setText(timerModel.getTimesheet().getProjectFullName());
            holder.tvTaskName.setText(timerModel.getTimesheet().getTaskFullName());
        } else {
            holder.tvProjectName.setVisibility(View.GONE);
            holder.tvTaskName.setVisibility(View.GONE);
        }
        if (timerModel.isRunning()) {
            holder.tvPlayPause.setText("Pause");
            holder.tvPlayPause.setCompoundDrawables(null, null, context.getResources().getDrawable(R.drawable.ic_action_play_green), null);
        } else {
            holder.tvPlayPause.setText("Start");
            holder.tvPlayPause.setCompoundDrawables(null, null, context.getResources().getDrawable(R.drawable.ic_action_pause_green), null);
//            holder.tvPlayPause.setCompoundDrawableTintMode();
        }
        holder.tvPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TimerService.timerModelArrayList.get(position).isRunning()) {
                    //
                    long balance=System.currentTimeMillis()-TimerService.timerModelArrayList.get(position).getCalStartTime();
                    TimerService.timerModelArrayList.get(position).setCalEndTime( TimerService.timerModelArrayList.get(position).getCalEndTime()+balance);
                    //
                    TimerService.timerModelArrayList.get(position).setRunning(false);
                    holder.tvPlayPause.setText("Start");
                    holder.tvPlayPause.setCompoundDrawables(null, null, context.getResources().getDrawable(R.drawable.ic_action_pause_green), null);
                } else {
                    //
                    TimerService.timerModelArrayList.get(position).setCalStartTime(System.currentTimeMillis());
                    //

                    if (timerModelArrayList.get(position).getMillis() == 0) {
                        timerModelArrayList.get(position).setStartTime(System.currentTimeMillis());
                    }
                    TimerService.timerModelArrayList.get(position).setRunning(true);
                    holder.tvPlayPause.setText("Pause");
                    holder.tvPlayPause.setCompoundDrawables(null, null, context.getResources().getDrawable(R.drawable.ic_action_play_green), null);
                }
            }
        });
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Confirm Delete!");
                builder.setMessage("Are you sure you want to delete?");
                builder.setIcon(android.R.drawable.ic_dialog_alert);
                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TimerService.timerModelArrayList.remove(position);
                        notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
        holder.iv_open_timesheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NewTimeSheetFromTimer.class);
                intent.putExtra(Constants.TIMER_COUNT, timerModelArrayList.get(position).getMillis());
                intent.putExtra(Constants.TIMER_POSITION, position);
                context.startActivity(intent);
            }
        });
        holder.button_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTimeSheet(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return timerModelArrayList.size();
    }

    private void saveTimeSheet(int position) {
        Timesheet timesheet = null;
        if (TimerService.timerModelArrayList.get(position).getTimesheet() == null) {
            //save empty timesheet
            timesheet = new Timesheet();
            timesheet.setEZStatusID(0);
            timesheet.set$id("new");
            timesheet.setEmployeeFullName(employeeLogin.getDisplayName());
            timesheet.setEmployeeID(employeeLogin.getUserid());
            timesheet.setTimeActivityID(0);
            timesheet.setTaskID(0);
            timesheet.setProjectID(0);
            timesheet.setClassRowID(0);
            timesheet.setDescription("");
            timesheet.setProjectFullName("None");
            timesheet.setTaskFullName("None");
            timesheet.setClassFullName("None");
            switch (getOptionsDataFile.getShowBillable()) {
                case 0:
                    timesheet.setBillable(false);
                    break;
                case 1:
                    timesheet.setBillable(false);
                    break;
                case 2:
                    timesheet.setBillable(true);
                    break;
                case 3:
                    timesheet.setBillable(false);
                    break;
            }
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormatOLD = new SimpleDateFormat("dd-MMM-yyyy");
            timesheet.setActivityDate(Utilities.dateToServerFormat(simpleDateFormatOLD.format(new Date(calendar.getTimeInMillis()))));
            Log.e("getTimerInterval",getOptionsDataFile.getTimerInterval()+"");
            Log.e("timer",TimerService.timerModelArrayList.get(position).getMillis()+"");

//            if (TimerService.timerModelArrayList.get(position).getMillis() < 1 * 60 * 60 * 1000) {
//                timesheet.setActualHours(((float) getOptionsDataFile.getTimerInterval()) / 60);
//            } else {
//                timesheet.setActualHours(TimerService.timerModelArrayList.get(position).getMillis() / (1000 * 60 * 60));
//            }


            //rajeev start



//            Double d = Double.valueOf(gettimefromtimer);
//            double finalValue=0.0;
//            d=d/(1000*60);
//            if(d<getOptionsDataFile.getTimerInterval())
//            {
//                finalValue= ((double) getOptionsDataFile.getTimerInterval()) / 60;
//                timesheet.setActualHours(((float) getOptionsDataFile.getTimerInterval()) / 60);
//            }else
//            {
//              Double ceilingvalue=Math.ceil(d/getOptionsDataFile.getTimerInterval());
//              timesheet.setActualHours((float)(ceilingvalue*getOptionsDataFile.getTimerInterval())/60);
//                finalValue=(double)(ceilingvalue*getOptionsDataFile.getTimerInterval())/60;
//            }
//
//            // rajeev work end
//
//
//            Date date = new Date(TimerService.timerModelArrayList.get(position).getStartTime());
//            timesheet.setStartTime(new SimpleDateFormat("HH:mm").format(date));
//
//            //rajeev start
//            long valueincurrentmuli=((long)(finalValue*1000l*60l*60l));
//            Log.e("end time",TimerService.timerModelArrayList.get(position).getStartTime()+valueincurrentmuli+"");
//            Date end=new Date(TimerService.timerModelArrayList.get(position).getStartTime()+valueincurrentmuli);
//            timesheet.setEndTime(new SimpleDateFormat("HH:mm").format(end));

            // rajeev work end

        } else {
            //save timesheet
            timesheet = TimerService.timerModelArrayList.get(position).getTimesheet();
        }
        long gettimefromtimer=TimerService.timerModelArrayList.get(position).getMillis();
        if (gettimefromtimer == 0){
            gettimefromtimer = 1000;
        }

        int timeInterval = getOptionsDataFile.getTimerInterval();
        double timerRan = Double.valueOf(gettimefromtimer/1000);
        double x = timerRan;
        double y = x/60;
        double z = y/timeInterval;
        double a= Math.ceil(z);
        double hours = (timeInterval*a)/60;
        String rounded_hours = String.format("%.2f", hours);
        String textToShow = "Timer ran for " + x + " seconds.\n";
        textToShow += "Timer ran for " + y + " minutes.\n";
        textToShow += "It is " + z + " times of timer value.\n";
        textToShow += "but we have to consider " + a + " times of timer value.\n";
        textToShow += "hours to count is " + hours + ".\n";
        textToShow += "after rounding off hours are " + rounded_hours + ".\n";
        timesheet.setActualHours((float) hours);
        Date startDate = new Date(TimerService.timerModelArrayList.get(position).getStartTime());
        timesheet.setStartTime(new SimpleDateFormat("HH:mm").format(startDate));
        Calendar calendar1  = Calendar.getInstance();
        calendar1.setTimeInMillis((long) (startDate.getTime() + hours*60*60*1000));
        Date endDate = new Date(calendar1.getTimeInMillis());
        timesheet.setEndTime(new SimpleDateFormat("HH:mm").format(endDate));
        for (int l = 0; l < 4; l++) {
            Toast.makeText(context, textToShow, Toast.LENGTH_LONG).show();
        }
        //saving timesheet
        TableObject tableObject = (TableObject) OfflineDataStorage.readObject(context, Constants.TIMESHEETS_FILE);
        tableObject.getTimesheetList().add(timesheet);
        OfflineDataStorage.writeObject(context, tableObject, Constants.TIMESHEETS_FILE);
        TimerService.timerModelArrayList.remove(position);
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTimerName, tvTime, tvPlayPause, tvProjectName, tvTaskName, button_done;
        ImageView ivDelete, iv_open_timesheet;
        RelativeLayout parentLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTimerName = (TextView) itemView.findViewById(R.id.tv_timer_name);
            tvProjectName = (TextView) itemView.findViewById(R.id.tv_project_name);
            tvTaskName = (TextView) itemView.findViewById(R.id.tv_task_name);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvPlayPause = (TextView) itemView.findViewById(R.id.iv_play_pause);
            ivDelete = (ImageView) itemView.findViewById(R.id.iv_delete);
            button_done = (TextView) itemView.findViewById(R.id.button_done);
            iv_open_timesheet = (ImageView) itemView.findViewById(R.id.iv_open_timesheet);
            parentLayout = (RelativeLayout) itemView.findViewById(R.id.parent_layout);
        }
    }
}
