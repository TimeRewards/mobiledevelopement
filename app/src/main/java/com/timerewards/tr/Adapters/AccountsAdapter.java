package com.timerewards.tr.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.timerewards.tr.DataFiles.AccountModel;
import com.timerewards.tr.DataFiles.ProjectsModel;
import com.timerewards.tr.R;

import java.util.ArrayList;

public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.MyViewHolder> {

    public int positionAccount;
    LayoutInflater inflater;
    ArrayList<AccountModel> accountModelArrayList = new ArrayList<>();
    AccoutSelection accoutSelection;
    private Context context;

    public AccountsAdapter(Context context, int position, ArrayList<AccountModel> accountModelArrayList) {
//        this.classesModelArrayList = classesDataFile.getClassesModelList();
        this.accountModelArrayList = accountModelArrayList;
        accoutSelection = (AccoutSelection) context;
        inflater = LayoutInflater.from(context);
        this.context = context;
        positionAccount = position;
    }

    public void changePosition(int position) {
        this.positionAccount = position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_project, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;

    }

    public void changeData(ArrayList<AccountModel> accountModelArrayList) {
        this.accountModelArrayList = accountModelArrayList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (accountModelArrayList.get(position).getAccountid() == positionAccount) {
            holder.radioButton.setChecked(true);
        } else {
            holder.radioButton.setChecked(false);
        }
        holder.textView.setText("" + accountModelArrayList.get(position).getAccountname());

        switch (accountModelArrayList.get(position).getSubLevelLocal()) {
            case 0:
                holder.textView.setText("" + accountModelArrayList.get(position).getAccountname());
                break;
            case 1:
                holder.textView.setText("   " + accountModelArrayList.get(position).getAccountname());
                break;
            case 2:
                holder.textView.setText("      " + accountModelArrayList.get(position).getAccountname());
                break;
            case 3:
                holder.textView.setText("         " + accountModelArrayList.get(position).getAccountname());
                break;
            case 4:
                holder.textView.setText("            " + accountModelArrayList.get(position).getAccountname());
                break;
            case 5:
                holder.textView.setText("               " + accountModelArrayList.get(position).getAccountname());
                break;
        }

        if (!accountModelArrayList.get(position).isClickable()) {
            holder.linearLayout.setEnabled(false);
            holder.radioButton.setChecked(false);
            holder.textView.setTextColor(Color.GRAY);
        } else {
            holder.linearLayout.setEnabled(true);
            holder.textView.setTextColor(Color.BLACK);
        }

        holder.textView.setTextColor(Color.BLACK);

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accoutSelection.setAccount(accountModelArrayList.get(position).getAccountid());
            }
        });
        holder.radioButton.setEnabled(false);
    }

    @Override
    public int getItemCount() {
        return accountModelArrayList.size();
    }

    public interface AccoutSelection {
        public void setAccount(int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        RadioButton radioButton;
        TextView textView;
        RelativeLayout linearLayout;

        public MyViewHolder(View item) {
            super(item);
            radioButton = (RadioButton) item.findViewById(R.id.radio_button);
            textView = (TextView) item.findViewById(R.id.textView);
            linearLayout = (RelativeLayout) item.findViewById(R.id.ll_parent);
        }
    }
}
