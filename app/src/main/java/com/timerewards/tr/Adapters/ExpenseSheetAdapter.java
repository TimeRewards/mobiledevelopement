package com.timerewards.tr.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Activities.ExpenseSheetActivity;
import com.timerewards.tr.Activities.ExpenseSheetDetails;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.ExpenseHeaderModel;
import com.timerewards.tr.DataFiles.ExpenseHeaderSubmitResponse;
import com.timerewards.tr.DataFiles.ExpenseItemModel;
import com.timerewards.tr.DataFiles.ExpenseItemSubmitResponse;
import com.timerewards.tr.DataFiles.PhotoUploadResponse;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.PhotoMultipartRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class ExpenseSheetAdapter extends BaseAdapter {

    public int[] positionInList;
    Context context;
    String date;
    ExpenseDataFile expenseDataFile;
    ArrayList<Expense> expenseSheets;
    ArrayList<Expense> expenseSheetsOnDate;
    Expense expense;
    ExpenseHeaderModel expenseHeaderModel;
    String tempDate;
    int timeSheetIdInTimeSheets;
    int tempPosition = 0;
    TextView tvDelete, tvDuplicate, tvSubmit;
    int numOfRequests = 0;
    int numOfItems = 0;

    public ExpenseSheetAdapter(Context context) {
        this.context = context;
        expenseSheetsOnDate = new ArrayList<>();
        expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(context, Constants.EXPENSE_DATA_FILE);
        expenseSheets = expenseDataFile.getExpense();
        positionInList = new int[expenseSheets.size()];
        for (int i = 0; i < expenseSheets.size(); i++) {
            expense = expenseSheets.get(i);
            expenseHeaderModel = expense.getExpHeader();
            tempDate = expenseHeaderModel.getEntryDate();
            if (tempDate != null) {
                boolean hasChildToShow = false;
                ArrayList<ExpenseItemModel> expenseItemModelArrayList = new ArrayList<>();
                expenseItemModelArrayList = expense.getExpenseItemList();
                for (int j = 0; j < expenseItemModelArrayList.size(); j++) {
                    if (!expenseItemModelArrayList.get(j).isHasDeletedInDevice()
                            && !expenseItemModelArrayList.get(j).isMarkedForDeletion()) {
                        hasChildToShow = true;
                    }
                }

                if (expense.isMarkedForDeletion() == false
                        && expense.getExpenseItemList().size() > 0
                        && hasChildToShow) {
                    expenseSheetsOnDate.add(expenseSheets.get(i));
                    timeSheetIdInTimeSheets = i;
                    positionInList[tempPosition] = i;
                    tempPosition++;
                }
//                }
            }
        }
    }

    @Override
    public int getCount() {
        return expenseSheetsOnDate.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        MyViewHolder mViewHolder;
        final int temp = position;
        if (convertView == null) {
            convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_row_expenses, viewGroup, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        mViewHolder.tvCustomer.setText(Utilities.dateToLocalFormat(expenseSheetsOnDate.get(position).getExpHeader().getEntryDate()));
        mViewHolder.tvTask.setText(expenseSheetsOnDate.get(position).getExpHeader().getDescription());
        float amount = 0.0f;
        for (int z = 0; z < expenseSheetsOnDate.get(position).getExpenseItemList().size(); z++) {
            if (expenseSheetsOnDate.get(position).getExpenseItemList().get(z).isMarkedForDeletion() != true) {
                amount += expenseSheetsOnDate.get(position).getExpenseItemList().get(z).getExpenseAmount();
            }
        }
        mViewHolder.tvAmount.setText(String.format("%.2f", (double) Math.round(amount * 100) / 100));

        mViewHolder.relativeLayout.setLongClickable(true);

        mViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ExpenseSheetDetails.class);
                intent.putExtra(Constants.HEADER_POSITION, positionInList[temp]);
                intent.putExtra("date", date);
                context.startActivity(intent);
            }
        });

        switch (expenseSheetsOnDate.get(position).getOverAllStatus()) {
            case 0:
                mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.onhold_local_color));
                break;
            case 1:
                mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.rejected_color));
                break;
            default:
                mViewHolder.view.setBackgroundColor(context.getResources().getColor(R.color.other_status_color));
                break;
        }

        mViewHolder.relativeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_timesheet_options);
                dialog.show();

                tvDelete = (TextView) dialog.findViewById(R.id.tv_delete_timesheet);
                tvDuplicate = (TextView) dialog.findViewById(R.id.tv_duplicate_timesheet);
                tvDuplicate.setVisibility(View.GONE);
                tvSubmit = (TextView) dialog.findViewById(R.id.tv_submit_timesheet);
                tvSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        numOfRequests = 0;
                        if (expenseDataFile.getExpense().get(positionInList[temp]).getExpHeader().getEmployeeExpenseID() == 0) {
                            if (Utilities.isOnline(context)) {
                                submitOnServer(positionInList[temp]);
                            } else {
                                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            submitAllExpenseItems(positionInList[temp]);
                        }
                    }
                });
                tvDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        numOfRequests = 0;
                        ArrayList<ExpenseItemModel> expenseItemModelArrayList = expenseDataFile.getExpense().get(positionInList[temp]).getExpenseItemList();
                        for (int i = 0; i < expenseItemModelArrayList.size(); i++) {
                            if (!(expenseItemModelArrayList.get(i).isHasDeletedInDevice() || expenseItemModelArrayList.get(i).isMarkedForDeletion())) {
                                if (expenseItemModelArrayList.get(i).getEZStatusID() == 0 || expenseItemModelArrayList.get(i).getEZStatusID() == 1) {
                                    if (expenseItemModelArrayList.get(i).getEmployeeExpenseItemID() == 0) {
                                        expenseDataFile.getExpense().get(positionInList[temp]).getExpenseItemList().get(i).setHasDeletedInDevice(true);
                                        OfflineDataStorage.writeObject(context, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                                        continue;
                                    } else {
                                        deleteExpenseItem(positionInList[temp], i);
                                    }
                                }
                            }
                        }
                    }
                });

                return true;
            }
        });
        return convertView;
    }

    private void submitOnServer(final int expenseSheetPosition) {

        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);

        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().getEmployeeExpenseID() == 0) {
            Expense expense = expenseDataFile.getExpense().get(expenseSheetPosition);
            if (expense.getExpHeader().getEmployeeExpenseID() == 0) {
                String url = null;
                if (Utilities.isOnline(context)) {
                    url = Constants.SUBMIT_EXPENSE_HEADER_API;
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("UserId", employeeLogin.getUserid());
                        jsonObject.put("SiteCode", employeeLogin.getSiteCode());
                        jsonObject.put("EmployeeExpenseID", 0);
                        jsonObject.put("ExpenseReason", expense.getExpHeader().getDescription());
                        jsonObject.put("ExpenseDate", expense.getExpHeader().getEntryDate());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            Log.i("arsh", "response=" + s);
                            try {
                                JSONObject jsonObject = new JSONObject(s);
                                if (jsonObject.has("EmployeeExpenseID")) {
                                    ExpenseHeaderSubmitResponse expenseHeaderSubmitResponse = new GsonBuilder().create().fromJson(s.toString(), ExpenseHeaderSubmitResponse.class);
                                    expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().setEmployeeExpenseID(expenseHeaderSubmitResponse.getEmployeeExpenseID());
                                    for (int i = 0; i < expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().size(); i++) {
                                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(i).setEmployeeExpenseID(expenseHeaderSubmitResponse.getEmployeeExpenseID());
                                    }
                                    expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().set$id("");
                                    OfflineDataStorage.writeObject(context, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                                    submitAllExpenseItems(expenseSheetPosition);
                                } else {
                                    Toast.makeText(context, "Request Failed", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(context, "Request Failed", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                            hideProgressDialog();

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            hideProgressDialog();
                            Utilities.handleVolleyError(context, volleyError);
                        }
                    });
                    showProgressDialog();
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                } else {
                    Utilities.showNoConnectionToast(context);
                }
            }
        }
    }

    private void submitAllExpenseItems(final int expenseSheetPosition) {
        ArrayList<ExpenseItemModel> expenseItemModelArrayList = expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList();
        for (int i = 0; i < expenseItemModelArrayList.size(); i++) {
            if (Utilities.isOnline(context)) {
                if (!(expenseItemModelArrayList.get(i).isHasDeletedInDevice() || expenseItemModelArrayList.get(i).isMarkedForDeletion())) {
                    if (expenseItemModelArrayList.get(i).getEZStatusID() == 0 || expenseItemModelArrayList.get(i).getEZStatusID() == 1) {
                        submitExpenseItemOnServer(expenseSheetPosition, i);
                    }
                }
            } else {
                Utilities.showNoConnectionToast(context);
            }
        }
    }

    private void submitExpenseItemOnServer(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
        String submitDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").format(Calendar.getInstance().getTimeInMillis());

//        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).get$id().equalsIgnoreCase("new")) {
        Expense expense = expenseDataFile.getExpense().get(expenseSheetPosition);
        String url = null;
        JSONObject jsonObject = new JSONObject();
        url = Constants.SUBMIT_EXPENSE_ITEM_API;
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("EmployeeExpenseID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseID());
            jsonObject.put("EmployeeExpenseItemID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID());
            jsonObject.put("ExpenseDate", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getExpenseDate());
            jsonObject.put("ExpenseAmount", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getExpenseAmount());
            jsonObject.put("PaidBy", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getPaidBy());
            jsonObject.put("IsReimbursable", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isReimbursable());
            jsonObject.put("Billable", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isBillable());
            jsonObject.put("Merchant", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getMerchant());
            jsonObject.put("Description", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getDescription());
            jsonObject.put("ProjectID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getProjectID());
            jsonObject.put("AccountID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getAccountID());
            jsonObject.put("ClassRowID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getClassRowID());
            jsonObject.put("EmployeeFullNme", employeeLogin.getDisplayName());
            jsonObject.put("CCAccountName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getCCAccountName());
            jsonObject.put("ProjectFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getProjectFullName());
            jsonObject.put("AccountFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getAccountFullName());
            jsonObject.put("ClassFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getClassFullName());
            jsonObject.put("SourceRefID", "18");
            jsonObject.put("EmpSubDate", submitDate);
            jsonObject.put("Qty", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getQty());
            jsonObject.put("Price", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getPrice());
            jsonObject.put("EmpSignature", employeeLogin.getEmployeeSignature());
            jsonObject.put("IsDefaultGLAnItem", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isDefaultGLAnItem());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("arsh", "url = " + url);

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("arsh", "response=" + s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has("EmployeeExpenseItemID")) {
                        numOfItems++;
                        ExpenseItemSubmitResponse expenseItemSubmitResponse = new GsonBuilder().create().fromJson(s.toString(), ExpenseItemSubmitResponse.class);
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setEmployeeExpenseItemID(expenseItemSubmitResponse.getEmployeeExpenseItemID());
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setEZStatusID(expenseItemSubmitResponse.getEZStatusID());
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).set$id("");
                        OfflineDataStorage.writeObject(context, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getReceiptName().length() > 0
                                && expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isHasReceiptUploadedOnServer() == false) {
                            uploadImage(expenseSheetPosition, expenseItemPosition);
                        } else {
//                                Toast.makeText(context, "Submitted successfully", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (jsonObject.has("ErrorMessage")) {
//                                Toast.makeText(context, jsonObject.getString("ErrorMessage"), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    Toast.makeText(context, "Exception Occured", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Utilities.handleVolleyError(context, volleyError);
                hideProgressDialog();
            }
        });
        showProgressDialog();
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
//        }
    }

    private void uploadImage(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
        String url = "";

        url = Constants.UPLOAD_EXPENSE_RECEIPTS_API;

        String filename = expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getReceiptName();
        File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(f, filename);

        if (file.exists()) {

            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
            entityBuilder.addPart("file", new FileBody(file));
            entityBuilder.addTextBody("userid", String.valueOf(employeeLogin.getUserid()));
            entityBuilder.addTextBody("sitecode", String.valueOf(employeeLogin.getSiteCode()));
            entityBuilder.addTextBody("employeeexpenseitemid", String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID()));
            entityBuilder.setLaxMode().setBoundary("xx").setCharset(null);

            Log.e("file", file.getName());
            PhotoMultipartRequest multipartRequest = new PhotoMultipartRequest(url, entityBuilder, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    hideProgressDialog();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("uploaded")) {
                            PhotoUploadResponse photoUploadResponse = new GsonBuilder().create().fromJson(response, PhotoUploadResponse.class);
                            if (photoUploadResponse.getUploaded().equalsIgnoreCase("true")) {
//                                Toast.makeText(context, "Submitted successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Receipt not uploaded", Toast.LENGTH_SHORT).show();
                            }
                            Log.e("response", response);
                        }
                    } catch (JSONException e) {
                        Toast.makeText(context, "Exception Occured", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    hideProgressDialog();
                    Utilities.handleVolleyError(context, error);
                }
            });
            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    100000000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            try {
                Log.e("sdf", multipartRequest.getBodyContentType());
                Log.e("gbh", multipartRequest.getUrl());
                Log.e("fd", multipartRequest.getBody()[0] + "");

                for (Map.Entry e : multipartRequest.getHeaders().entrySet()) {
                    Log.e("key", e.getKey() + "");
                    Log.e("value", e.getValue() + "");
                }

            } catch (AuthFailureError authFailureError) {
                authFailureError.printStackTrace();
            }
            showProgressDialog();
            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getRequestQueue().add(multipartRequest);
        } else {
            Toast.makeText(context, "Receipt Image Not found", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteExpenseItem(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
        if (Utilities.isOnline(context)) {
            String url = null;
            url = Constants.DELETE_EXPENSE_ITEM_API;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("userid", employeeLogin.getUserid());
                jsonObject.put("sitecode", employeeLogin.getSiteCode());
                jsonObject.put("EmployeeExpenseID", "0");
                jsonObject.put("EmployeeExpenseItemID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    Log.i("arsh", "response=" + s);
                    if (s.equalsIgnoreCase("true")) {
//                        Toast.makeText(context, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setHasDeletedInDevice(true);
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setIsMarkedForDeletion(true);
                        OfflineDataStorage.writeObject(context, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                        numOfItems++;
                    } else {
                    }
                    hideProgressDialog();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Utilities.handleVolleyError(context, volleyError);
                    hideProgressDialog();
                }
            });

            showProgressDialog();
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        } else {
            Utilities.showNoConnectionToast(context);
        }
    }

    private void restartActivity() {
        Intent intent = new Intent(context, ExpenseSheetActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
        ((ExpenseSheetActivity) context).finish();
    }

    private void showProgressDialog() {

        numOfRequests++;
        ShowDialog.showDialog(context);

    }

    private void hideProgressDialog() {

        numOfRequests--;

        if (numOfRequests < 1) {
            Toast.makeText(context, "Request processed for " + numOfItems + " Items", Toast.LENGTH_SHORT).show();
            numOfRequests = 0;
            numOfItems = 0;
            ShowDialog.hideDialog();
            restartActivity();
        }

    }

    private class MyViewHolder {
        View view, view2;
        TextView tvCustomer, tvTask, tvAmount;
        ImageView ivDetails;
        RelativeLayout relativeLayout;

        public MyViewHolder(View item) {
            view = item.findViewById(R.id.view);
            view2 = item.findViewById(R.id.view2);
            tvCustomer = (TextView) item.findViewById(R.id.tv_customer);
            tvTask = (TextView) item.findViewById(R.id.tv_task);
            ivDetails = (ImageView) item.findViewById(R.id.iv_open_details);
            relativeLayout = (RelativeLayout) item.findViewById(R.id.relative_layout);
            tvAmount = (TextView) item.findViewById(R.id.tv_timesheet_hours);
        }
    }

}