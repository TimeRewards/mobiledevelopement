package com.timerewards.tr.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.ClassesDataFile;
import com.timerewards.tr.DataFiles.ClassesModel;
import com.timerewards.tr.DataFiles.ProjectsModel;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;

import java.util.ArrayList;

public class ClassesAdapter extends RecyclerView.Adapter<ClassesAdapter.MyViewHolder> {

    public int positionClass;
    LayoutInflater inflater;
    ArrayList<ClassesModel> classesModelArrayList = new ArrayList<>();
    ClassesDataFile classesDataFile;
    ClassSelection classSelection;
    private Context context;

    public ClassesAdapter(Context context, int position, ArrayList<ClassesModel> classesModelArrayList) {
        classesDataFile = (ClassesDataFile) OfflineDataStorage.readObject(context, Constants.CLASSES_DATA_FILE);
//        this.classesModelArrayList = classesDataFile.getClassesModelList();
        this.classesModelArrayList = classesModelArrayList;
        classSelection = (ClassSelection) context;
        inflater = LayoutInflater.from(context);
        this.context = context;
        positionClass = position;
    }

    public void changePosition(int position) {
        this.positionClass = position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_project, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;

    }
    public void changeData(ArrayList<ClassesModel> classesModelArrayList) {
        this.classesModelArrayList = classesModelArrayList;
        notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
//        if (position == positionClass) {
//            holder.radioButton.setChecked(true);
//        } else {
//            holder.radioButton.setChecked(false);
//        }

        if (classesModelArrayList.get(position).getClassRowID() == positionClass) {
            holder.radioButton.setChecked(true);
        } else {
            holder.radioButton.setChecked(false);
        }

        switch (classesModelArrayList.get(position).getSubLevel()) {
            case 0:
                holder.textView.setText("" + classesModelArrayList.get(position).getClassName());
                break;
            case 1:
                holder.textView.setText("   " + classesModelArrayList.get(position).getClassName());
                break;
            case 2:
                holder.textView.setText("      " + classesModelArrayList.get(position).getClassName());
                break;
            case 3:
                holder.textView.setText("         " + classesModelArrayList.get(position).getClassName());
                break;
            case 4:
                holder.textView.setText("            " + classesModelArrayList.get(position).getClassName());
                break;
            case 5:
                holder.textView.setText("               " + classesModelArrayList.get(position).getClassName());
                break;
        }

        if (!classesModelArrayList.get(position).isClickable()) {
            holder.linearLayout.setEnabled(false);
            holder.radioButton.setChecked(false);
            holder.textView.setTextColor(Color.GRAY);
        } else {
            holder.linearLayout.setEnabled(true);
            holder.textView.setTextColor(Color.BLACK);
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                classSelection.setClass(classesModelArrayList.get(position).getClassRowID());
            }
        });
//        holder.radioButton.setEnabled(false);
    }

    @Override
    public int getItemCount() {
        return classesModelArrayList.size();
    }

    public interface ClassSelection {
        public void setClass(int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        RadioButton radioButton;
        TextView textView;
        RelativeLayout linearLayout;

        public MyViewHolder(View item) {
            super(item);
            radioButton = (RadioButton) item.findViewById(R.id.radio_button);
            textView = (TextView) item.findViewById(R.id.textView);
            linearLayout = (RelativeLayout) item.findViewById(R.id.ll_parent);
        }
    }
}
