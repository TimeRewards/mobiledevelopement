package com.timerewards.tr.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.GsonBuilder;
import com.timerewards.tr.Activities.DuplicateExpenseItemActivity;
import com.timerewards.tr.Activities.EditExpenseItemActivity;
import com.timerewards.tr.Activities.ExpenseSheetActivity;
import com.timerewards.tr.Activities.ExpenseSheetDetails;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.Expense;
import com.timerewards.tr.DataFiles.ExpenseDataFile;
import com.timerewards.tr.DataFiles.ExpenseHeaderModel;
import com.timerewards.tr.DataFiles.ExpenseHeaderSubmitResponse;
import com.timerewards.tr.DataFiles.ExpenseItemModel;
import com.timerewards.tr.DataFiles.ExpenseItemSubmitResponse;
import com.timerewards.tr.DataFiles.GetOptionsDataFile;
import com.timerewards.tr.DataFiles.PhotoUploadResponse;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.PhotoMultipartRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class ExpenseItemsAdapter extends BaseAdapter {

    private Context context;
    private int expenseSheetPosition;
    private ExpenseHeaderModel expenseHeaderModel;
    private ArrayList<ExpenseItemModel> expenseItemModels;
    private ExpenseDataFile expenseDataFile;
    private int[] positionInList;
    private ArrayList<ExpenseItemModel> expenseItemModelsOnDate = new ArrayList<>();
    private int tempPosition = 0;
    private Expense expense;
    private GetOptionsDataFile getOptionsDataFile;

    public ExpenseItemsAdapter(Context context, int expenseSheetPosition) {
        this.context = context;
        this.expenseSheetPosition = expenseSheetPosition;
        expenseDataFile = (ExpenseDataFile) OfflineDataStorage.readObject(context, Constants.EXPENSE_DATA_FILE);
        getOptionsDataFile = (GetOptionsDataFile) OfflineDataStorage.readObject(context, Constants.GETOPTIONS_FILE);
        expense = expenseDataFile.getExpense().get(expenseSheetPosition);
        expenseHeaderModel = expense.getExpHeader();
        expenseItemModels = expense.getExpenseItemList();
        positionInList = new int[expenseItemModels.size()];
        for (int i = 0; i < expenseItemModels.size(); i++) {
            ExpenseItemModel expenseItemModel = expenseItemModels.get(i);
            if (!(expenseItemModel.isHasDeletedInDevice() == true || expenseItemModel.isMarkedForDeletion())) {
                expenseItemModelsOnDate.add(expenseItemModel);
                positionInList[tempPosition] = i;
                tempPosition++;
            }
        }
    }

    @Override
    public int getCount() {
        return expenseItemModelsOnDate.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        MyViewHolder mViewHolder;
        final int temp = i;
        if (convertView == null) {
            convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_row_expense_items, viewGroup, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        float amount = expenseItemModelsOnDate.get(i).getExpenseAmount();
        boolean billable = expenseItemModelsOnDate.get(i).isBillable();
        int paidby = expenseItemModelsOnDate.get(i).getPaidBy();

        mViewHolder.tvDate.setText(Utilities.dateToLocalFormat(expenseItemModelsOnDate.get(i).getExpenseDate()));
        mViewHolder.tvCustomer.setText(expenseItemModelsOnDate.get(i).getClassFullName());
        if (getOptionsDataFile.isUseClassesInExp()) {
            mViewHolder.llClass.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.llClass.setVisibility(View.GONE);
        }
        mViewHolder.tvAccount.setText(expenseItemModelsOnDate.get(i).getAccountFullName());
        mViewHolder.tvMerchant.setText(expenseItemModelsOnDate.get(i).getMerchant());
        mViewHolder.tvCreditCard.setText(expenseItemModelsOnDate.get(i).getCCAccountName());
        mViewHolder.tvProject.setText(expenseItemModelsOnDate.get(i).getProjectFullName());
        mViewHolder.tvAmount.setText(String.format("%.2f", (double) Math.round(amount * 100) / 100));
        if (paidby == 1) {
            mViewHolder.tvPaidBy.setText("Paid By Me");
        } else {
            mViewHolder.tvPaidBy.setText("Paid By Company");
        }

        if (billable == true) {
            mViewHolder.tvBillable.setText("Billable");
        } else {
            mViewHolder.tvBillable.setText("Not Billable");
        }

        mViewHolder.tvStatus.setText(Utilities.intToStatus(expenseItemModelsOnDate.get(i).getEZStatusID()));
        if (expenseItemModelsOnDate.get(i).getEmployeeExpenseItemID() == 0 && expenseItemModelsOnDate.get(i).get$id().equalsIgnoreCase("new")) {
            mViewHolder.tvStatus.setText("Saved Locally");
        }

        mViewHolder.tvDetails.setText(expenseItemModelsOnDate.get(i).getDescription());

        String filename = expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(temp).getReceiptName();
        File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(f, filename);
        Bitmap bitmap = null;
        if (filename.length() > 0 && file.exists()) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            try {
                bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options);
                mViewHolder.ivReceipt.setImageBitmap(bitmap);
                mViewHolder.llReceipt.setVisibility(View.VISIBLE);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            mViewHolder.llReceipt.setVisibility(View.GONE);
        }

        mViewHolder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(positionInList[temp]).getEZStatusID() == 0
                        || expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(positionInList[temp]).getEZStatusID() == 1) {
                    Intent intent = new Intent(context, EditExpenseItemActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra(Constants.HEADER_POSITION, expenseSheetPosition);
                    intent.putExtra(Constants.EXPENSE_ITEM_POSITION, temp);
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "Can't be Edited", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mViewHolder.tvDuplicate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DuplicateExpenseItemActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(Constants.HEADER_POSITION, expenseSheetPosition);
                intent.putExtra(Constants.EXPENSE_ITEM_POSITION, temp);
                context.startActivity(intent);
            }
        });

        mViewHolder.tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().getEmployeeExpenseID() == 0)
                        && (expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(positionInList[temp]).getEmployeeExpenseItemID() == 0)) {
                    submitOnServer(expenseSheetPosition, positionInList[temp]);
                } else {
                    if (expenseItemModelsOnDate.get(positionInList[temp]).getEZStatusID() == 0 ||
                            expenseItemModelsOnDate.get(positionInList[temp]).getEZStatusID() == 1) {
                        submitExpenseItemOnServer(expenseSheetPosition, positionInList[temp]);
                    } else {
                        Toast.makeText(context, "Already Submitted.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        mViewHolder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);

                alertDialogBuilder.setTitle("Are you sure want to Delete?");

                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                                if (expenseItemModelsOnDate.get(positionInList[temp]).getEmployeeExpenseItemID() == 0) {
                                    expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().remove(positionInList[temp]);
                                    OfflineDataStorage.writeObject(context, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                                    restartActivity();
                                } else {
                                    if (expenseItemModelsOnDate.get(temp).getEZStatusID() == 0 || expenseItemModelsOnDate.get(temp).getEZStatusID() == 1) {
                                        deleteExpenseItem(expenseSheetPosition, positionInList[temp]);
                                    } else {
                                        Toast.makeText(context, "Can't be Deleted", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        });
                android.app.AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();

            }
        });

        return convertView;
    }

    private void submitOnServer(final int expenseSheetPosition, final int expenseItemPosition) {

        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);

        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().getEmployeeExpenseID() == 0) {
            Expense expense = expenseDataFile.getExpense().get(expenseSheetPosition);
            if (expense.getExpHeader().getEmployeeExpenseID() == 0) {
                String url = null;
                if (Utilities.isOnline(context)) {
//                    try {
                    url = Constants.SUBMIT_EXPENSE_HEADER_API;
//                                + "?UserID=" + URLEncoder.encode(String.valueOf(employeeLogin.getUserid()), "UTF-8")
//                                + "&SiteCode=" + URLEncoder.encode(employeeLogin.getSiteCode(), "UTF-8")
//                                + "&EmployeeExpenseID=" + URLEncoder.encode("0", "UTF-8")
//                                + "&ExpenseReason=" + URLEncoder.encode(expense.getExpHeader().getDescription(), "UTF-8")
//                                + "&ExpenseDate=" + URLEncoder.encode(expense.getExpHeader().getEntryDate(), "UTF-8");
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                        Toast.makeText(context, "Exception encoding url", Toast.LENGTH_SHORT).show();
//                    }
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("UserId", employeeLogin.getUserid());
                        jsonObject.put("SiteCode", employeeLogin.getSiteCode());
                        jsonObject.put("EmployeeExpenseID", 0);
                        jsonObject.put("ExpenseReason", expense.getExpHeader().getDescription());
                        jsonObject.put("ExpenseDate", expense.getExpHeader().getEntryDate());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            Log.i("arsh", "response=" + s);
                            ExpenseHeaderSubmitResponse expenseHeaderSubmitResponse = new GsonBuilder().create().fromJson(s.toString(), ExpenseHeaderSubmitResponse.class);
                            expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().setEmployeeExpenseID(expenseHeaderSubmitResponse.getEmployeeExpenseID());
                            for (int i = 0; i < expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().size(); i++) {
                                expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(i).setEmployeeExpenseID(expenseHeaderSubmitResponse.getEmployeeExpenseID());
                            }
                            expenseDataFile.getExpense().get(expenseSheetPosition).getExpHeader().set$id("");
                            OfflineDataStorage.writeObject(context, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                            submitExpenseItemOnServer(expenseSheetPosition, expenseItemPosition);

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            ShowDialog.hideDialog();
                            Utilities.handleVolleyError(context, volleyError);
                        }
                    });
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                    ShowDialog.showDialog(context);
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void submitExpenseItemOnServer(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
        String submitDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss").format(Calendar.getInstance().getTimeInMillis());

//        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).get$id().equalsIgnoreCase("new")) {
        Expense expense = expenseDataFile.getExpense().get(expenseSheetPosition);
        String url = null;
//        try {
        url = Constants.SUBMIT_EXPENSE_ITEM_API;
//                    + "?UserID=" + URLEncoder.encode(String.valueOf(employeeLogin.getUserid()), "UTF-8")
//                    + "&SiteCode=" + URLEncoder.encode(employeeLogin.getSiteCode(), "UTF-8")
//                    + "&EmployeeExpenseID=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseID()), "UTF-8")
//                    + "&EmployeeExpenseItemID=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID()), "UTF-8")
//                    + "&ExpenseDate=" + URLEncoder.encode(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getExpenseDate(), "UTF-8")
//                    + "&ExpenseAmount=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getExpenseAmount()), "UTF-8")
//                    + "&PaidBy=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getPaidBy()), "UTF-8")
//                    + "&IsReimbursable=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isReimbursable()), "UTF-8")
//                    + "&Billable=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isBillable()), "UTF-8")
//                    + "&Merchant=" + URLEncoder.encode(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getMerchant(), "UTF-8")
//                    + "&Description=" + URLEncoder.encode(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getDescription(), "UTF-8")
//                    + "&ProjectId=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getProjectID()), "UTF-8")
//                    + "&AccountId=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getAccountID()), "UTF-8")
//                    + "&ClassRowID=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getClassRowID()), "UTF-8")
//                    + "&EmployeeFullNme=" + URLEncoder.encode(employeeLogin.getDisplayName(), "UTF-8")
//                    + "&CCAccountName=" + URLEncoder.encode(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getCCAccountName())
//                    + "&ProjectfullName=" + URLEncoder.encode(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getProjectFullName(), "UTF-8")
//                    + "&AccountFullName=" + URLEncoder.encode(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getAccountFullName(), "UTF-8")
//                    + "&ClassFullName=" + URLEncoder.encode(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getClassFullName(), "UTF-8")
//                    + "&SourceRefID=18"
//                    + "&EmpSubDate=" + URLEncoder.encode(submitDate, "UTF-8")
//                    + "&Qty=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getQty()), "UTF-8")
//                    + "&Price=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getPrice()), "UTF-8")
//                    + "&EmpSignature=" + URLEncoder.encode(employeeLogin.getEmployeeSignature(), "UTF-8")
//                    + "&IsDefaultGLAnItem=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isDefaultGLAnItem()), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//            Toast.makeText(context, "Exception encoding url", Toast.LENGTH_SHORT).show();
//        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("EmployeeExpenseID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseID());
            jsonObject.put("EmployeeExpenseItemID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID());
            jsonObject.put("ExpenseDate", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getExpenseDate());
            jsonObject.put("ExpenseAmount", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getExpenseAmount());
            jsonObject.put("PaidBy", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getPaidBy());
            jsonObject.put("IsReimbursable", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isReimbursable());
            jsonObject.put("Billable", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isBillable());
            jsonObject.put("Merchant", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getMerchant());
            jsonObject.put("Description", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getDescription());
            jsonObject.put("ProjectID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getProjectID());
            jsonObject.put("AccountID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getAccountID());
            jsonObject.put("ClassRowID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getClassRowID());
            jsonObject.put("EmployeeFullNme", employeeLogin.getDisplayName());
            jsonObject.put("CCAccountName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getCCAccountName());
            jsonObject.put("ProjectFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getProjectFullName());
            jsonObject.put("AccountFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getAccountFullName());
            jsonObject.put("ClassFullName", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getClassFullName());
            jsonObject.put("SourceRefID", "18");
            jsonObject.put("EmpSubDate", submitDate);
            jsonObject.put("Qty", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getQty());
            jsonObject.put("Price", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getPrice());
            jsonObject.put("EmpSignature", employeeLogin.getEmployeeSignature());
            jsonObject.put("IsDefaultGLAnItem", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isDefaultGLAnItem());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("arsh", "url = " + url);

        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.i("arsh", "response=" + s);
                ShowDialog.hideDialog();
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    if (jsonObject.has("EmployeeExpenseItemID")) {
                        ExpenseItemSubmitResponse expenseItemSubmitResponse = new GsonBuilder().create().fromJson(s.toString(), ExpenseItemSubmitResponse.class);
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setEmployeeExpenseItemID(expenseItemSubmitResponse.getEmployeeExpenseItemID());
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).setEZStatusID(expenseItemSubmitResponse.getEZStatusID());
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).set$id("");
                        OfflineDataStorage.writeObject(context, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                        if (expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getReceiptName().length() > 0
                                && expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).isHasReceiptUploadedOnServer() == false) {
                            uploadImage(expenseSheetPosition, expenseItemPosition);
                        } else {
                            Toast.makeText(context, "Submitted successfully", Toast.LENGTH_SHORT).show();
                            restartActivity();
                        }
                    } else {
                        Toast.makeText(context, jsonObject.getString("ErrorMessage"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(context, "Exception Occured", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Utilities.handleVolleyError(context, volleyError);
                ShowDialog.hideDialog();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
        ShowDialog.showDialog(context);
//        }
    }

    private void deleteExpenseItem(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
        if (Utilities.isOnline(context)) {
            String url = null;
            url = Constants.DELETE_EXPENSE_ITEM_API;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("userid", employeeLogin.getUserid());
                jsonObject.put("sitecode", employeeLogin.getSiteCode());
                jsonObject.put("EmployeeExpenseID", "0");
                jsonObject.put("EmployeeExpenseItemID", expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID());
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            try {
//                url = Constants.DELETE_EXPENSE_ITEM_API
//                        + "?UserID=" + URLEncoder.encode(String.valueOf(employeeLogin.getUserid()), "UTF-8")
//                        + "&SiteCode=" + URLEncoder.encode(employeeLogin.getSiteCode(), "UTF-8")
//                        + "&EmployeeExpenseID=" + URLEncoder.encode("0", "UTF-8")
//                        + "&EmployeeExpenseItemID=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID()), "UTF-8");
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//                Toast.makeText(context, "Exception encoding url", Toast.LENGTH_SHORT).show();
//            }

            JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
                @Override
                public void onResponse(String s) {
                    ShowDialog.hideDialog();
                    Log.i("arsh", "response=" + s);
                    if (s.equalsIgnoreCase("true")) {
                        Toast.makeText(context, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                        expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().remove(expenseItemPosition);
                        OfflineDataStorage.writeObject(context, expenseDataFile, Constants.EXPENSE_DATA_FILE);
                        restartActivity();
                    } else {
                        Toast.makeText(context, "Delete Failed", Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Utilities.handleVolleyError(context, volleyError);
                    ShowDialog.hideDialog();
                }
            });

            ShowDialog.showDialog(context);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        } else {
            Toast.makeText(context, "No Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadImage(final int expenseSheetPosition, final int expenseItemPosition) {
        EmployeeLogin employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
        String url = "";

        try {
            url = Constants.UPLOAD_EXPENSE_RECEIPTS_API;
            JSONObject jsonObject = new JSONObject();
//            try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("EmployeeExpenseItemID", employeeLogin.getSiteCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }

//            + "?UserID=" + URLEncoder.encode(String.valueOf(employeeLogin.getUserid()), "UTF-8")
//                    + "&SiteCode=" + URLEncoder.encode(employeeLogin.getSiteCode(), "UTF-8")
//                    + "&EmployeeExpenseItemID=" + URLEncoder.encode(String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID()), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }

        String filename = expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getReceiptName();
        File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(f, filename);

        if (file.exists()) {

            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
            entityBuilder.addPart("file", new FileBody(file));
            entityBuilder.addTextBody("userid", String.valueOf(employeeLogin.getUserid()));
            entityBuilder.addTextBody("sitecode", String.valueOf(employeeLogin.getSiteCode()));
            entityBuilder.addTextBody("employeeexpenseitemid", String.valueOf(expenseDataFile.getExpense().get(expenseSheetPosition).getExpenseItemList().get(expenseItemPosition).getEmployeeExpenseItemID()));
            entityBuilder.setLaxMode().setBoundary("xx").setCharset(null);

            Log.e("file", file.getName());
            PhotoMultipartRequest multipartRequest = new PhotoMultipartRequest(url, entityBuilder, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    ShowDialog.hideDialog();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.has("uploaded")) {
                            PhotoUploadResponse photoUploadResponse = new GsonBuilder().create().fromJson(response, PhotoUploadResponse.class);
                            if (photoUploadResponse.getUploaded().equalsIgnoreCase("true")) {
                                Toast.makeText(context, "Submitted successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Receipt not uploaded", Toast.LENGTH_SHORT).show();
                            }
                            Log.e("response", response);
                        }
                    } catch (JSONException e) {
                        Toast.makeText(context, "Exception Occured", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    restartActivity();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ShowDialog.hideDialog();
                    Utilities.handleVolleyError(context, error);
                }
            });
            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    100000000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            try {
                Log.e("sdf", multipartRequest.getBodyContentType());
                Log.e("gbh", multipartRequest.getUrl());
                Log.e("fd", multipartRequest.getBody()[0] + "");

                for (Map.Entry e : multipartRequest.getHeaders().entrySet()) {
                    Log.e("key", e.getKey() + "");
                    Log.e("value", e.getValue() + "");
                }

            } catch (AuthFailureError authFailureError) {
                authFailureError.printStackTrace();
            }
            ShowDialog.showDialog(context);
            AppController.getRequestQueue().add(multipartRequest);
        } else {
            Toast.makeText(context, "Receipt Image Not found", Toast.LENGTH_SHORT).show();
        }
    }

    private void restartActivity() {
        Intent intent = new Intent(context, ExpenseSheetActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("id", expenseSheetPosition);
        context.startActivity(intent);
        ((ExpenseSheetDetails) context).finish();
    }

    private class MyViewHolder {
        TextView tvDate, tvAccount, tvMerchant, tvCustomer, tvAmount, tvCreditCard, tvProject, tvPaidBy, tvBillable, tvStatus, tvDetails, tvDuplicate, tvDelete, tvSubmit;
        ImageView ivEdit, ivReceipt;
        LinearLayout llClass, llReceipt;

        public MyViewHolder(View item) {
            tvDate = (TextView) item.findViewById(R.id.tv_expense_date);
            tvAccount = (TextView) item.findViewById(R.id.tv_account);
            tvMerchant = (TextView) item.findViewById(R.id.tv_merchant);
            tvCustomer = (TextView) item.findViewById(R.id.tv_customer);
            tvAmount = (TextView) item.findViewById(R.id.tv_amount);
            tvCreditCard = (TextView) item.findViewById(R.id.tv_credit_card);
            tvProject = (TextView) item.findViewById(R.id.tv_project);
            tvPaidBy = (TextView) item.findViewById(R.id.tv_paid_by);
            tvBillable = (TextView) item.findViewById(R.id.tv_billable);
            tvStatus = (TextView) item.findViewById(R.id.tv_status);
            tvDetails = (TextView) item.findViewById(R.id.tv_details);
            tvDuplicate = (TextView) item.findViewById(R.id.tv_duplicate_expense);
            tvDelete = (TextView) item.findViewById(R.id.tv_delete_expense);
            tvSubmit = (TextView) item.findViewById(R.id.tv_submit_expense);
            ivEdit = (ImageView) item.findViewById(R.id.iv_edit_expense);
            llClass = (LinearLayout) item.findViewById(R.id.ll_class);
            llReceipt = (LinearLayout) item.findViewById(R.id.ll_receipt);
            ivReceipt = (ImageView) item.findViewById(R.id.iv_receipt_image);
        }
    }

}