package com.timerewards.tr.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.timerewards.tr.Activities.ApproveTimesheetActivity;
import com.timerewards.tr.Controller.AppController;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.ProjectsInTimeSlotModel;
import com.timerewards.tr.DataFiles.TimeSlotModel;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.DataModels.EmployeeLogin;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;
import com.timerewards.tr.utils.JsonStringRequest;
import com.timerewards.tr.utils.ShowDialog;
import com.timerewards.tr.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class TimeSheetProjApproveLayerAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<TimeSlotModel> timeSlotModelArrayList = new ArrayList<>();
    private SimpleDateFormat simpleDateFormatOLD = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat simpleDateFormatNEW = new SimpleDateFormat("dd-MMM-yyyy");
    private EmployeeLogin employeeLogin;
    private String rejectReason;
    private ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();
    private TextView tvAccept, tvReject;
    private Refresh refresh;

    public TimeSheetProjApproveLayerAdapter(Context context, ArrayList<TimeSlotModel> timeSlotModelArrayList) {
        this.context = context;
        refresh = (Refresh) context;
        employeeLogin = (EmployeeLogin) OfflineDataStorage.readObject(context, Constants.EMPLOYEE_LOGIN_FILE);
        this.timeSlotModelArrayList = timeSlotModelArrayList;
        simpleDateFormatNEW.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public void changeData(ArrayList<TimeSlotModel> timeSlotModelArrayList, ArrayList<Timesheet> timesheetArrayList) {
        this.timeSlotModelArrayList = timeSlotModelArrayList;
        this.timesheetArrayList = timesheetArrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return timeSlotModelArrayList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return timeSlotModelArrayList.get(i).getProjectsList().size();
    }

    @Override
    public Object getGroup(int i) {
        return i;
    }

    @Override
    public Object getChild(int i, int i1) {
        return i;
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int i, boolean b, View convertView, ViewGroup viewGroup) {
        MyGroupHolder myGroupHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) viewGroup.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.single_row_approve_layer_header_new, viewGroup, false);
            myGroupHolder = new MyGroupHolder(convertView);
            convertView.setTag(myGroupHolder);
        } else {
            myGroupHolder = (MyGroupHolder) convertView.getTag();
        }
        if (timeSlotModelArrayList == null || timeSlotModelArrayList.size() == 0) {
            return convertView;
        }
        final TimeSlotModel timeSlotModel = timeSlotModelArrayList.get(i);
        if (timeSlotModel.isToShowPeriod()) {
            myGroupHolder.llTimeSlot.setVisibility(View.VISIBLE);
            myGroupHolder.tvTimeSlotHours.setText(String.format("%.2f", (double) Math.round(timeSlotModel.getCompleteTimeSlotHours() * 100) / 100));
        } else {
            myGroupHolder.llTimeSlot.setVisibility(View.GONE);
        }
        myGroupHolder.tvCustomer.setText(timeSlotModel.getEmpName());
        String date = simpleDateFormatNEW.format(new Date(timeSlotModel.getStartMillis()))
                + " - "
                + simpleDateFormatNEW.format(new Date(timeSlotModel.getEndMillis()));
        myGroupHolder.tvTimePeriod.setText(date);
        myGroupHolder.tvHours.setText(String.format("%.2f", (double) Math.round(timeSlotModel.getHours() * 100) / 100));
        myGroupHolder.rlTimeSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ApproveTimesheetActivity.class);
                intent.putExtra(Constants.START_MILLIS, timeSlotModel.getStartMillis());
                intent.putExtra(Constants.END_MILLIS, timeSlotModel.getEndMillis());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
        myGroupHolder.rlTimeSlot.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_expense_sheet_options);
                dialog.show();
                tvAccept = (TextView) dialog.findViewById(R.id.tv_accept);
                tvAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Utilities.isOnline(context)) {
                            for (int i = 0; i < timeSlotModelArrayList.size(); i++) {
                                if (timeSlotModel.getStartMillis() == timeSlotModelArrayList.get(i).getStartMillis()) {
                                    timesheetArrayList.addAll(timeSlotModelArrayList.get(i).getTimesheetArrayList());
                                }
                            }
                            acceptRejectTimeSheet(true, timeSlotModel.getStartMillis(), timeSlotModel.getEndMillis(), null, null);
                        } else {
                            Utilities.showNoConnectionToast(context);
                        }
                    }
                });
                tvReject = (TextView) dialog.findViewById(R.id.tv_reject);
                tvReject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Utilities.isOnline(context)) {
                            final Dialog dialogReason = new Dialog(context);
                            dialogReason.setContentView(R.layout.dialog_reject_reason);
                            dialogReason.setTitle("Please Enter Reject Reason");
                            final EditText etReason = (EditText) dialogReason.findViewById(R.id.et_dialog_reject_reason);
                            Button buttonReject = (Button) dialogReason.findViewById(R.id.button_dialog_ok);
                            Button buttonCancel = (Button) dialogReason.findViewById(R.id.button_dialog_cancel);
                            dialogReason.setCancelable(false);
                            buttonReject.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (etReason.getText().toString().length() > 0) {
                                        dialogReason.dismiss();
                                        rejectReason = etReason.getText().toString();
                                        for (int i = 0; i < timeSlotModelArrayList.size(); i++) {
                                            if (timeSlotModel.getStartMillis() == timeSlotModelArrayList.get(i).getStartMillis()) {
                                                timesheetArrayList.addAll(timeSlotModelArrayList.get(i).getTimesheetArrayList());
                                            }
                                        }
                                        acceptRejectTimeSheet(false, timeSlotModel.getStartMillis(), timeSlotModel.getEndMillis(), null, null);
                                    } else {
                                        etReason.setError("Can't be left Blank");
                                    }
                                }
                            });
                            buttonCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogReason.dismiss();
                                }
                            });
                            dialogReason.show();
                        } else {
                            Utilities.showNoConnectionToast(context);
                        }

                    }
                });
                dialog.show();
                return false;
            }
        });
        myGroupHolder.rlEmployeeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ApproveTimesheetActivity.class);
                intent.putExtra(Constants.EMPLOYEE_NAME, timeSlotModel.getEmpName());
                intent.putExtra(Constants.START_MILLIS, timeSlotModel.getStartMillis());
                intent.putExtra(Constants.END_MILLIS, timeSlotModel.getEndMillis());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
        myGroupHolder.rlEmployeeName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_expense_sheet_options);
                dialog.show();

                tvAccept = (TextView) dialog.findViewById(R.id.tv_accept);
                tvAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Utilities.isOnline(context)) {
                            timesheetArrayList = timeSlotModel.getTimesheetArrayList();
                            acceptRejectTimeSheet(true, timeSlotModel.getStartMillis(), timeSlotModel.getEndMillis(), timeSlotModel.getEmpName(), null);
                        } else {
                            Utilities.showNoConnectionToast(context);
                        }
                    }
                });
                tvReject = (TextView) dialog.findViewById(R.id.tv_reject);
                tvReject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Utilities.isOnline(context)) {
                            final Dialog dialogReason = new Dialog(context);
                            dialogReason.setContentView(R.layout.dialog_reject_reason);
                            dialogReason.setTitle("Please Enter Reject Reason");
                            final EditText etReason = (EditText) dialogReason.findViewById(R.id.et_dialog_reject_reason);
                            Button buttonReject = (Button) dialogReason.findViewById(R.id.button_dialog_ok);
                            Button buttonCancel = (Button) dialogReason.findViewById(R.id.button_dialog_cancel);
                            dialogReason.setCancelable(false);
                            buttonReject.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (etReason.getText().toString().length() > 0) {
                                        dialogReason.dismiss();
                                        rejectReason = etReason.getText().toString();
                                        timesheetArrayList = timeSlotModel.getTimesheetArrayList();
                                        acceptRejectTimeSheet(false, timeSlotModel.getStartMillis(), timeSlotModel.getEndMillis(), timeSlotModel.getEmpName(), null);
                                    } else {
                                        etReason.setError("Can't be left Blank");
                                    }
                                }
                            });
                            buttonCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogReason.dismiss();
                                }
                            });
                            dialogReason.show();
                        } else {
                            Utilities.showNoConnectionToast(context);
                        }

                    }
                });
                dialog.show();
                return false;
            }
        });
        ExpandableListView eLV = (ExpandableListView) viewGroup;
        eLV.expandGroup(i);
        return convertView;
    }

    @Override
    public View getChildView(final int i, final int i1, boolean b, View convertView, ViewGroup viewGroup) {
        MyChildHolder myChildHolder;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) viewGroup.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.single_row_approve_layer_child, viewGroup, false);
            myChildHolder = new MyChildHolder(convertView);
            convertView.setTag(myChildHolder);
        } else {
            myChildHolder = (MyChildHolder) convertView.getTag();
        }
        if (timeSlotModelArrayList == null || timeSlotModelArrayList.size() == 0) {
            return convertView;
        }
        final ProjectsInTimeSlotModel projectsInTimeSlotModel = timeSlotModelArrayList.get(i).getProjectsList().get(i1);
        myChildHolder.tvProjectTaskName.setText(projectsInTimeSlotModel.getProjectName());
        myChildHolder.tvHours.setText(String.format("%.2f", (double) Math.round(projectsInTimeSlotModel.getHours() * 100) / 100));
        myChildHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ApproveTimesheetActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Constants.EMPLOYEE_NAME, timeSlotModelArrayList.get(i).getEmpName());
                intent.putExtra(Constants.START_MILLIS, timeSlotModelArrayList.get(i).getStartMillis());
                intent.putExtra(Constants.END_MILLIS, timeSlotModelArrayList.get(i).getEndMillis());
                intent.putExtra(Constants.PROJECT_OR_TASK_NAME, projectsInTimeSlotModel.getProjectName());
                intent.putExtra(Constants.PROJECT_OR_TASK, "project");
                context.startActivity(intent);
            }
        });
        myChildHolder.parentLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_expense_sheet_options);
                dialog.show();

                tvAccept = (TextView) dialog.findViewById(R.id.tv_accept);
                tvAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Utilities.isOnline(context)) {
                            timesheetArrayList = projectsInTimeSlotModel.getTimesheetArrayList();
                            acceptRejectTimeSheet(true, timeSlotModelArrayList.get(i).getStartMillis(), timeSlotModelArrayList.get(i).getEndMillis(), timeSlotModelArrayList.get(i).getEmpName(), projectsInTimeSlotModel.getProjectName());
                        } else {
                            Utilities.showNoConnectionToast(context);
                        }
                    }
                });
                tvReject = (TextView) dialog.findViewById(R.id.tv_reject);
                tvReject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if (Utilities.isOnline(context)) {
                            final Dialog dialogReason = new Dialog(context);
                            dialogReason.setContentView(R.layout.dialog_reject_reason);
                            dialogReason.setTitle("Please Enter Reject Reason");
                            final EditText etReason = (EditText) dialogReason.findViewById(R.id.et_dialog_reject_reason);
                            Button buttonReject = (Button) dialogReason.findViewById(R.id.button_dialog_ok);
                            Button buttonCancel = (Button) dialogReason.findViewById(R.id.button_dialog_cancel);
                            dialogReason.setCancelable(false);
                            buttonReject.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (etReason.getText().toString().length() > 0) {
                                        rejectReason = etReason.getText().toString();
                                        timesheetArrayList = projectsInTimeSlotModel.getTimesheetArrayList();
                                        acceptRejectTimeSheet(false, timeSlotModelArrayList.get(i).getStartMillis(), timeSlotModelArrayList.get(i).getEndMillis(), timeSlotModelArrayList.get(i).getEmpName(), projectsInTimeSlotModel.getProjectName());
                                        dialogReason.dismiss();
                                    } else {
                                        etReason.setError("Can't be left Blank");
                                    }
                                }
                            });
                            buttonCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialogReason.dismiss();
                                }
                            });
                            dialogReason.show();
                        } else {
                            Utilities.showNoConnectionToast(context);
                        }

                    }
                });
                dialog.show();
                return false;
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    private void acceptRejectTimeSheet(final boolean accept, long startMillis, long endMillis, String empName, String projName) {

        ArrayList<Timesheet> arrayListTimesheetStored = refresh.getTimeSheetList();

        extractTimeSheetPositions(arrayListTimesheetStored, startMillis, endMillis, empName, projName);

        String IDs = "";

        for (int i = 0; i < timesheetArrayList.size(); i++) {
            IDs = IDs + String.valueOf(timesheetArrayList.get(i).getTimeActivityID());
            if (i < timesheetArrayList.size() - 1) {
                IDs = IDs + ",";
            }

            if (i > timesheetArrayList.size() - 5) {
                Log.e("", "");
                continue;
            }
        }
        Log.i("arsh", IDs);
        String url = "";
        url = Constants.ACCEPT_REJECT_TIMESHEET_API;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", employeeLogin.getUserid());
            jsonObject.put("sitecode", employeeLogin.getSiteCode());
            jsonObject.put("TimeActivityID", IDs);
            jsonObject.put("ApproveOrReject", accept);
            jsonObject.put("AppSignature", employeeLogin.getApproverSignature());
            if (accept == false) {
                jsonObject.put("AdminNotes", rejectReason);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("arsh", "url=" + url);
        JsonStringRequest stringRequest = new JsonStringRequest(Request.Method.POST, url, jsonObject, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                ShowDialog.hideDialog();
                Log.i("arsh", "response=" + s);
                ArrayList<Timesheet> arrayListTimesheet = refresh.getTimeSheetList();
                for (int i = timesheetArrayList.size() - 1; i >= 0; i--) {
                    arrayListTimesheet.remove(timesheetArrayList.get(i).getPositionInList());
                }
                refresh.setTimesheetList(arrayListTimesheet);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ShowDialog.hideDialog();
                Utilities.handleVolleyError(context, volleyError);
            }
        });
        ShowDialog.showDialog(context);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void extractTimeSheetPositions(ArrayList<Timesheet> arrayListTimesheetStored, long startMillis, long endMillis, String empName, String projName) {
        if (startMillis != 0 && endMillis != 0) {
            //getting all the timesheets in this timeslot
            ArrayList<Timesheet> timesheetArrayList1 = new ArrayList<>();
            for (int i = 0; i < arrayListTimesheetStored.size(); i++) {
                Timesheet timesheet = arrayListTimesheetStored.get(i);
                if (timesheet.getTimeInMillis() >= startMillis && timesheet.getTimeInMillis() <= endMillis) {
                    timesheet.setPositionInList(i);
                    timesheetArrayList1.add(timesheet);
                }
            }
            timesheetArrayList = new ArrayList<>();
            timesheetArrayList.addAll(timesheetArrayList1);
            timesheetArrayList1.clear();
            //if employee name is clicked
            if (empName != null) {
                for (int i = 0; i < timesheetArrayList.size(); i++) {
                    Timesheet timesheet = timesheetArrayList.get(i);
                    if (empName.equalsIgnoreCase(timesheet.getEmployeeFullName())) {
                        timesheetArrayList1.add(timesheet);
                    }
                }
                timesheetArrayList = new ArrayList<>();
                timesheetArrayList.addAll(timesheetArrayList1);
                timesheetArrayList1.clear();
                //if project is selected
                if (projName != null) {
                    //checking if project is selected
                    for (int i = 0; i < timesheetArrayList.size(); i++) {
                        if (projName.equalsIgnoreCase("timeoff")) {
                            //in case if timeoff selected
                            if (timesheetArrayList.get(i).isTime() == false) {
                                Timesheet timesheet = timesheetArrayList.get(i);
                                timesheetArrayList1.add(timesheet);
                            }
                        } else {
                            Timesheet timesheet = timesheetArrayList.get(i);
                            if (projName.equalsIgnoreCase(timesheet.getProjectFullName())) {
                                timesheetArrayList1.add(timesheet);
                            }
                        }
                    }
                    timesheetArrayList = new ArrayList<>();
                    timesheetArrayList.addAll(timesheetArrayList1);
                    timesheetArrayList1.clear();
                }
            }
        }
    }

    public interface Refresh {
        public void refresh();

        public ArrayList<Timesheet> getTimeSheetList();

        public void setTimesheetList(ArrayList<Timesheet> arrayListTimesheet);
    }

    public class MyGroupHolder {

        TextView tvCustomer, tvTimePeriod, tvHours, tvTimeSlotHours;
        LinearLayout parentLayout;
        LinearLayout llTimeSlot;
        RelativeLayout rlEmployeeName, rlTimeSlot;

        public MyGroupHolder(View item) {
            tvCustomer = (TextView) item.findViewById(R.id.tv_customer);
            tvTimePeriod = (TextView) item.findViewById(R.id.tv_time_period);
            tvHours = (TextView) item.findViewById(R.id.tv_employee_hours);
            tvTimeSlotHours = (TextView) item.findViewById(R.id.tv_timeslot_hours);
            parentLayout = (LinearLayout) item.findViewById(R.id.parent_layout);
            llTimeSlot = (LinearLayout) item.findViewById(R.id.ll_time_slot);
            rlTimeSlot = (RelativeLayout) item.findViewById(R.id.rl_time_slot);
            rlEmployeeName = (RelativeLayout) item.findViewById(R.id.rl_employee_name);
        }
    }

    public class MyChildHolder {

        TextView tvProjectTaskName, tvHours;
        RelativeLayout parentLayout;

        public MyChildHolder(View item) {
            tvProjectTaskName = (TextView) item.findViewById(R.id.tv_project_task_name);
            tvHours = (TextView) item.findViewById(R.id.tv_timesheet_hours);
            parentLayout = (RelativeLayout) item.findViewById(R.id.parent_layout);
        }
    }
}