package com.timerewards.tr.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.LeaveTaskModel;
import com.timerewards.tr.DataFiles.TaskDataFile;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;

import java.util.ArrayList;

public class LeaveTaskAdapter extends RecyclerView.Adapter<LeaveTaskAdapter.MyViewHolder> {

    public int positionLeaveTask;
    LayoutInflater inflater;
    ArrayList<LeaveTaskModel> leaveTaskModelArrayList = new ArrayList<>();
    TaskDataFile taskDataFile;
    LeaveTasksSelection leaveTasksSelection;
    private Context context;

    public LeaveTaskAdapter(Context context, int position, ArrayList<LeaveTaskModel> leaveTaskModelArrayList) {
        taskDataFile = (TaskDataFile) OfflineDataStorage.readObject(context, Constants.TASKS_DATA_FILE);
//        this.leaveTaskModelArrayList = taskDataFile.getTasksModelList();
        this.leaveTaskModelArrayList = leaveTaskModelArrayList;
        leaveTasksSelection = (LeaveTasksSelection) context;
        inflater = LayoutInflater.from(context);
        this.context = context;
        positionLeaveTask = position;
    }

    public void changePosition(int position) {
        positionLeaveTask = position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_project, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position == positionLeaveTask) {
            holder.radioButton.setChecked(true);
        } else {
            holder.radioButton.setChecked(false);
        }

        switch (leaveTaskModelArrayList.get(position).getSubLevel()) {
            case 0:
                holder.textView.setText("" + leaveTaskModelArrayList.get(position).getTaskName());
                break;
            case 1:
                holder.textView.setText("   " + leaveTaskModelArrayList.get(position).getTaskName());
                break;
            case 2:
                holder.textView.setText("      " + leaveTaskModelArrayList.get(position).getTaskName());
                break;
            case 3:
                holder.textView.setText("         " + leaveTaskModelArrayList.get(position).getTaskName());
                break;
            case 4:
                holder.textView.setText("            " + leaveTaskModelArrayList.get(position).getTaskName());
                break;
            case 5:
                holder.textView.setText("               " + leaveTaskModelArrayList.get(position).getTaskName());
                break;
        }

        if (!leaveTaskModelArrayList.get(position).isClickable()) {
            holder.linearLayout.setEnabled(false);
            holder.radioButton.setChecked(false);
            holder.textView.setTextColor(Color.GRAY);
        } else {
            holder.linearLayout.setEnabled(true);
            holder.textView.setTextColor(Color.BLACK);
        }
//
//        myViewHolder.radioButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                positionLeaveTask = position;
//                myViewHolder.radioButton.toggle();
//            }
//        });
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leaveTasksSelection.setLeave(position);
            }
        });
        holder.radioButton.setEnabled(false);
    }

    @Override
    public int getItemCount() {
        return leaveTaskModelArrayList.size();
    }

    public interface LeaveTasksSelection {
        public void setLeave(int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        RadioButton radioButton;
        TextView textView;
        RelativeLayout linearLayout;

        public MyViewHolder(View item) {
            super(item);
            radioButton = (RadioButton) item.findViewById(R.id.radio_button);
            textView = (TextView) item.findViewById(R.id.textView);
            linearLayout = (RelativeLayout) item.findViewById(R.id.ll_parent);
        }
    }
}
