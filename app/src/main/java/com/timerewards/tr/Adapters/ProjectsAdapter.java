package com.timerewards.tr.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.ProjectsDataFile;
import com.timerewards.tr.DataFiles.ProjectsModel;
import com.timerewards.tr.OfflineDataStorage;
import com.timerewards.tr.R;

import java.util.ArrayList;

public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.MyViewHolder> {

    public int positionProject;
    LayoutInflater inflater;
    ArrayList<ProjectsModel> projectsModelArrayList = new ArrayList<>();
    ProjectsDataFile projectsDataFile;
    ProjectSelection projectSelection;
    private Context context;

    public ProjectsAdapter(Context context, int position, ArrayList<ProjectsModel> projectsModelArrayList) {
        projectsDataFile = (ProjectsDataFile) OfflineDataStorage.readObject(context, Constants.PROJECTS_DATA_FILE);
        this.projectsModelArrayList = projectsModelArrayList;
        projectSelection = (ProjectSelection) context;
        inflater = LayoutInflater.from(context);
        this.context = context;
        positionProject = position;
    }

    public void changePosition(int position) {
        this.positionProject = position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_project, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (projectsModelArrayList.get(position).getProjectID() == positionProject) {
            holder.radioButton.setChecked(true);
        } else {
            holder.radioButton.setChecked(false);
        }

        switch (projectsModelArrayList.get(position).getSubLevel()) {
            case 0:
                holder.textView.setText("" + projectsModelArrayList.get(position).getProjectName());
                break;
            case 1:
                holder.textView.setText("   " + projectsModelArrayList.get(position).getProjectName());
                break;
            case 2:
                holder.textView.setText("      " + projectsModelArrayList.get(position).getProjectName());
                break;
            case 3:
                holder.textView.setText("         " + projectsModelArrayList.get(position).getProjectName());
                break;
            case 4:
                holder.textView.setText("            " + projectsModelArrayList.get(position).getProjectName());
                break;
            case 5:
                holder.textView.setText("               " + projectsModelArrayList.get(position).getProjectName());
                break;
        }

        if (!projectsModelArrayList.get(position).isClickable() || !projectsModelArrayList.get(position).isEnabled()) {
            holder.linearLayout.setEnabled(false);
            holder.radioButton.setChecked(false);
            holder.textView.setTextColor(Color.GRAY);
        } else {
            holder.linearLayout.setEnabled(true);
            holder.textView.setTextColor(Color.BLACK);
        }
//
//        myViewHolder.radioButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                positionProject = position;
//                myViewHolder.radioButton.toggle();
//            }
//        });
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                projectSelection.setProject(projectsModelArrayList.get(position).getProjectID());
            }
        });
        holder.radioButton.setEnabled(false);
    }

    @Override
    public int getItemCount() {
        return projectsModelArrayList.size();
    }

    public void changeData(ArrayList<ProjectsModel> projectsModelArrayList) {
        this.projectsModelArrayList = projectsModelArrayList;
        notifyDataSetChanged();
    }

    public interface ProjectSelection {
        public void setProject(int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        RadioButton radioButton;
        TextView textView;
        RelativeLayout linearLayout;

        public MyViewHolder(View item) {
            super(item);
            radioButton = (RadioButton) item.findViewById(R.id.radio_button);
            textView = (TextView) item.findViewById(R.id.textView);
            linearLayout = (RelativeLayout) item.findViewById(R.id.ll_parent);
        }
    }
}
