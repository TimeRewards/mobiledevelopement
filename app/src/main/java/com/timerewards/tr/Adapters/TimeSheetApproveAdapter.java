package com.timerewards.tr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.timerewards.tr.Activities.ApproveTimesheetActivity;
import com.timerewards.tr.Activities.ApproveTimesheetDetails;
import com.timerewards.tr.Controller.Constants;
import com.timerewards.tr.DataFiles.Timesheet;
import com.timerewards.tr.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TimeSheetApproveAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();
    private SimpleDateFormat simpleDateFormatOLD = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat simpleDateFormatNEW = new SimpleDateFormat("dd-MMM-yyyy");

    public TimeSheetApproveAdapter(Context context) {
        this.context = context;
    }

    public void changeData(ArrayList<Timesheet> timesheetArrayList) {
        this.timesheetArrayList = timesheetArrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return timesheetArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, final ViewGroup viewGroup) {
        MyViewHolder mViewHolder;
        final int temp = i;
        if (convertView == null) {
            convertView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_row_timesheet_to_approve, viewGroup, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        mViewHolder.tvEmployeeName.setText(timesheetArrayList.get(i).getEmployeeFullName());
        mViewHolder.tvCustomerName.setText(timesheetArrayList.get(i).getClassFullName());

        String date = timesheetArrayList.get(i).getActivityDate().substring(0, timesheetArrayList.get(i).getActivityDate().indexOf("T"));
        Date date1 = null;
        try {
            date1 = simpleDateFormatOLD.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        mViewHolder.tvTimesheetDate.setText(simpleDateFormatNEW.format(date1));
        mViewHolder.tvTimesheetHours.setText(String.format("%.2f", (double) Math.round(timesheetArrayList.get(i).getActualHours() * 100) / 100));
        mViewHolder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ApproveTimesheetDetails.class);
                intent.putExtra(Constants.TIMESHEET_POSITION, timesheetArrayList.get(temp).getPositionInList());
                ((ApproveTimesheetActivity) viewGroup.getContext()).startActivityForResult(intent, ApproveTimesheetActivity.REQUEST_CODE);
            }
        });
        return convertView;
    }

    private class MyViewHolder {
        View view, view2, view3;
        TextView tvEmployeeName, tvCustomerName, tvTimesheetDate, tvTimesheetHours;
        RelativeLayout relativeLayout;

        public MyViewHolder(View item) {
            view = item.findViewById(R.id.view);
            view2 = item.findViewById(R.id.view2);
            view3 = item.findViewById(R.id.view3);
            tvEmployeeName = (TextView) item.findViewById(R.id.tv_employee_name);
            tvCustomerName = (TextView) item.findViewById(R.id.tv_customer_name);
            tvTimesheetDate = (TextView) item.findViewById(R.id.tv_timesheet_date);
            tvTimesheetHours = (TextView) item.findViewById(R.id.tv_timesheet_hours);
            relativeLayout = (RelativeLayout) item.findViewById(R.id.relative_layout);
        }
    }
}