package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class TaskDataFile implements Serializable {

    private ArrayList<TasksModel> tasksModelList;
    private ArrayList<AssignmentModel> assignmentModels;
    private ArrayList<LeaveTaskModel> leaveTaskModels;

    public TaskDataFile() {

    }

    public ArrayList<LeaveTaskModel> getLeaveTaskModels() {
        return leaveTaskModels;
    }

    public void setLeaveTaskModels(ArrayList<LeaveTaskModel> leaveTaskModels) {
        this.leaveTaskModels = leaveTaskModels;
    }

    public ArrayList<AssignmentModel> getAssignmentModels() {

        return assignmentModels;
    }

    public void setAssignmentModels(ArrayList<AssignmentModel> assignmentModels) {
        this.assignmentModels = assignmentModels;
    }

    public ArrayList<TasksModel> getTasksModelList() {
        return tasksModelList;
    }

    public void setTasksModelList(ArrayList<TasksModel> tasksModelList) {
        this.tasksModelList = tasksModelList;
    }

}
