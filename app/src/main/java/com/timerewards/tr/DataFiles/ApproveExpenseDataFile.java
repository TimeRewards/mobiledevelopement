package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class ApproveExpenseDataFile implements Serializable {

    private ArrayList<ExpenseHeaderModel> expenseHeaderModelList;
    private ArrayList<ExpenseItemModel> expenseItemModelList;

    public ApproveExpenseDataFile() {
    }

    public ArrayList<ExpenseHeaderModel> getExpenseHeaderModelList() {
        return expenseHeaderModelList;
    }

    public void setExpenseHeaderModelList(ArrayList<ExpenseHeaderModel> expenseHeaderModelList) {
        this.expenseHeaderModelList = expenseHeaderModelList;
    }

    public ArrayList<ExpenseItemModel> getExpenseItemModelList() {
        return expenseItemModelList;
    }

    public void setExpenseItemModelList(ArrayList<ExpenseItemModel> expenseItemModelList) {
        this.expenseItemModelList = expenseItemModelList;
    }
}
