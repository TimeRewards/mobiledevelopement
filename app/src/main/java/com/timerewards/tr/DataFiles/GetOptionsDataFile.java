package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class GetOptionsDataFile implements Serializable {

    private String $id;
    private int Product;
    private String EmployeeLex;
    private String CustomerLex;
    private String ProjectLex;
    private String TaskLex;
    private String ClassLex;
    private int ShowBillable;
    private int ProjectOrTaskBillable;
    private boolean UseClassesInTS;
    private boolean UseClassesInExp;
    private int ShowLeave;
    private int canEnterFutureTime;
    private String StartTime;
    private String EndTime;
    private int IsApprover;
    private int IsClassMandatory;
    private int IsTaskMandatory;
    private int IsProjectMandatory;
    private int TimeSheetFrequency;
    private int LastDayOfWeek;
    private String BiWeeklyEndDate;
    private boolean IsAppSigRequiredforTS;
    private boolean IsAppSigRequiredforExp;
    private boolean IsEmpSigRequiredforTS;
    private boolean IsEmpSigRequiredforExp;
    private boolean CanApproverEditTS;
    private boolean CanApproverEditExp;
    private boolean ShowStartEndTime;
    private int MaxSizeofReceipt;
    private boolean DefaultBillableStatusExp;
    private int TimerInterval;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getProduct() {
        return Product;
    }

    public void setProduct(int product) {
        Product = product;
    }

    public String getEmployeeLex() {
        return EmployeeLex;
    }

    public void setEmployeeLex(String employeeLex) {
        EmployeeLex = employeeLex;
    }

    public String getCustomerLex() {
        return CustomerLex;
    }

    public void setCustomerLex(String customerLex) {
        CustomerLex = customerLex;
    }

    public String getProjectLex() {
        return ProjectLex;
    }

    public void setProjectLex(String projectLex) {
        ProjectLex = projectLex;
    }

    public String getTaskLex() {
        return TaskLex;
    }

    public void setTaskLex(String taskLex) {
        TaskLex = taskLex;
    }

    public String getClassLex() {
        return ClassLex;
    }

    public void setClassLex(String classLex) {
        ClassLex = classLex;
    }

    public int getShowBillable() {
        return ShowBillable;
    }

    public void setShowBillable(int showBillable) {
        ShowBillable = showBillable;
    }

    public int getProjectOrTaskBillable() {
        return ProjectOrTaskBillable;
    }

    public void setProjectOrTaskBillable(int projectOrTaskBillable) {
        ProjectOrTaskBillable = projectOrTaskBillable;
    }

    public boolean isUseClassesInTS() {
        return UseClassesInTS;
    }

    public void setUseClassesInTS(boolean useClassesInTS) {
        UseClassesInTS = useClassesInTS;
    }

    public boolean isUseClassesInExp() {
        return UseClassesInExp;
    }

    public void setUseClassesInExp(boolean useClassesInExp) {
        UseClassesInExp = useClassesInExp;
    }

    public int getShowLeave() {
        return ShowLeave;
    }

    public void setShowLeave(int showLeave) {
        ShowLeave = showLeave;
    }

    public int getCanEnterFutureTime() {
        return canEnterFutureTime;
    }

    public void setCanEnterFutureTime(int canEnterFutureTime) {
        this.canEnterFutureTime = canEnterFutureTime;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public int getIsApprover() {
        return IsApprover;
    }

    public void setIsApprover(int isApprover) {
        IsApprover = isApprover;
    }

    public int getIsClassMandatory() {
        return IsClassMandatory;
    }

    public void setIsClassMandatory(int isClassMandatory) {
        IsClassMandatory = isClassMandatory;
    }

    public int getIsTaskMandatory() {
        return IsTaskMandatory;
    }

    public void setIsTaskMandatory(int isTaskMandatory) {
        IsTaskMandatory = isTaskMandatory;
    }

    public int getIsProjectMandatory() {
        return IsProjectMandatory;
    }

    public void setIsProjectMandatory(int isProjectMandatory) {
        IsProjectMandatory = isProjectMandatory;
    }

    public int getTimeSheetFrequency() {
        return TimeSheetFrequency;
    }

    public void setTimeSheetFrequency(int timeSheetFrequency) {
        TimeSheetFrequency = timeSheetFrequency;
    }

    public int getLastDayOfWeek() {
        return LastDayOfWeek;
    }

    public void setLastDayOfWeek(int lastDayOfWeek) {
        LastDayOfWeek = lastDayOfWeek;
    }

    public String getBiWeeklyEndDate() {
        return BiWeeklyEndDate;
    }

    public void setBiWeeklyEndDate(String biWeeklyEndDate) {
        BiWeeklyEndDate = biWeeklyEndDate;
    }

    public boolean isAppSigRequiredforTS() {
        return IsAppSigRequiredforTS;
    }

    public void setAppSigRequiredforTS(boolean appSigRequiredforTS) {
        IsAppSigRequiredforTS = appSigRequiredforTS;
    }

    public void setIsAppSigRequiredforTS(boolean isAppSigRequiredforTS) {
        IsAppSigRequiredforTS = isAppSigRequiredforTS;
    }

    public boolean isAppSigRequiredforExp() {
        return IsAppSigRequiredforExp;
    }

    public void setAppSigRequiredforExp(boolean appSigRequiredforExp) {
        IsAppSigRequiredforExp = appSigRequiredforExp;
    }

    public void setIsAppSigRequiredforExp(boolean isAppSigRequiredforExp) {
        IsAppSigRequiredforExp = isAppSigRequiredforExp;
    }

    public boolean isEmpSigRequiredforTS() {
        return IsEmpSigRequiredforTS;
    }

    public void setEmpSigRequiredforTS(boolean empSigRequiredforTS) {
        IsEmpSigRequiredforTS = empSigRequiredforTS;
    }

    public void setIsEmpSigRequiredforTS(boolean isEmpSigRequiredforTS) {
        IsEmpSigRequiredforTS = isEmpSigRequiredforTS;
    }

    public boolean isEmpSigRequiredforExp() {
        return IsEmpSigRequiredforExp;
    }

    public void setEmpSigRequiredforExp(boolean empSigRequiredforExp) {
        IsEmpSigRequiredforExp = empSigRequiredforExp;
    }

    public void setIsEmpSigRequiredforExp(boolean isEmpSigRequiredforExp) {
        IsEmpSigRequiredforExp = isEmpSigRequiredforExp;
    }

    public boolean isCanApproverEditTS() {
        return CanApproverEditTS;
    }

    public void setCanApproverEditTS(boolean canApproverEditTS) {
        CanApproverEditTS = canApproverEditTS;
    }

    public boolean isCanApproverEditExp() {
        return CanApproverEditExp;
    }

    public void setCanApproverEditExp(boolean canApproverEditExp) {
        CanApproverEditExp = canApproverEditExp;
    }

    public boolean isShowStartEndTime() {
        return ShowStartEndTime;
    }

    public void setShowStartEndTime(boolean showStartEndTime) {
        ShowStartEndTime = showStartEndTime;
    }

    public int getMaxSizeofReceipt() {
        return MaxSizeofReceipt;
    }

    public void setMaxSizeofReceipt(int maxSizeofReceipt) {
        MaxSizeofReceipt = maxSizeofReceipt;
    }

    public boolean isDefaultBillableStatusExp() {
        return DefaultBillableStatusExp;
    }

    public void setDefaultBillableStatusExp(boolean defaultBillableStatusExp) {
        DefaultBillableStatusExp = defaultBillableStatusExp;
    }

    public int getTimerInterval() {
        return TimerInterval;
    }

    public void setTimerInterval(int timerInterval) {
        TimerInterval = timerInterval;
    }
}
