package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class ApproveRejectTimeSheetResponseModel implements Serializable {

    private String $id;
    private int UserID;
    private ArrayList<Timesheet> Timesheets = new ArrayList<>();

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public ArrayList<Timesheet> getTimesheets() {
        return Timesheets;
    }

    public void setTimesheets(ArrayList<Timesheet> timesheets) {
        Timesheets = timesheets;
    }
}
