package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.Comparator;

public class Timesheet implements Serializable, Comparable<Timesheet> {

    public static Comparator<Timesheet> TimeSheetComparator = new Comparator<Timesheet>() {
        @Override
        public int compare(Timesheet timesheet, Timesheet t1) {
            return timesheet.compareTo(t1);
        }
    };
    private String $id;
    private String ActivityDate;
    private float ActualHours;
    private String AdminNotes;
    private String ApproverID;
    //    private String AppSignature;
//    private String AppSubDate;
    private boolean Billable;
    //    private String BillableHours;
    private String ClassFullName;
    private int ClassRowID;
    //    private String CreatedBy;
//    private String CreateDate;
    private String Description;
    private String EmployeeFullName;
    private int EmployeeID;
    private int EZStatusID;
    private boolean IsTime;
    private String ProjectFullName;
    private int ProjectID;
    private String TaskFullName;
    private int TaskID;
    private int TimeActivityID;
    private String EmpSignature;
    private String EmpSubDate;
    private String Status;
    private String StartTime;
    private String EndTime;
    private String UpdatedDate;
    private boolean markedForAcceptRejectByUser = false;
    private boolean acceptedByUser = false;
    private boolean rejectedByUser = false;
    private boolean markedOnServer = false;
    private boolean isMarkedForDeletion = false;
    private boolean isMarkedForSubmit = false;
    private boolean hasDeletedOnServer = false;
    private Long timeInMillis;
    private String UpdatedBy;
    private int positionInList = -1;

    public Timesheet() {
    }

    public boolean isMarkedForDeletion() {
        return isMarkedForDeletion;
    }

    public void setIsMarkedForDeletion(boolean isMarkedForDeletion) {
        this.isMarkedForDeletion = isMarkedForDeletion;
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getProjectID() {
        return ProjectID;
    }

    public void setProjectID(int projectID) {
        ProjectID = projectID;
    }

    public String getActivityDate() {
        return ActivityDate;
    }

    public void setActivityDate(String activityDate) {
        ActivityDate = activityDate;
    }

    public float getActualHours() {
        return ActualHours;
    }

    public void setActualHours(float actualHours) {
        this.ActualHours = actualHours;
    }

    public String getAdminNotes() {
        return AdminNotes;
    }

    public void setAdminNotes(String adminNotes) {
        AdminNotes = adminNotes;
    }

    public boolean isBillable() {
        return Billable;
    }

    public void setBillable(boolean billable) {
        Billable = billable;
    }

    public String getClassFullName() {
        return ClassFullName;
    }

    public void setClassFullName(String classFullName) {
        ClassFullName = classFullName;
    }

    public int getClassRowID() {
        return ClassRowID;
    }

    public void setClassRowID(int classRowID) {
        ClassRowID = classRowID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEmployeeFullName() {
        return EmployeeFullName;
    }

    public void setEmployeeFullName(String employeeFullName) {
        EmployeeFullName = employeeFullName;
    }

    public int getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(int employeeID) {
        EmployeeID = employeeID;
    }

    public int getEZStatusID() {
        return EZStatusID;
    }

    public void setEZStatusID(int EZStatusID) {
        this.EZStatusID = EZStatusID;
    }

    public boolean isTime() {
        return IsTime;
    }

    public void setIsTime(boolean isTime) {
        IsTime = isTime;
    }

    public String getProjectFullName() {
        return ProjectFullName;
    }

    public void setProjectFullName(String projectFullName) {
        ProjectFullName = projectFullName;
    }

    public String getTaskFullName() {
        return TaskFullName;
    }

    public void setTaskFullName(String taskFullName) {
        TaskFullName = taskFullName;
    }

    public int getTaskID() {
        return TaskID;
    }

    public void setTaskID(int taskID) {
        TaskID = taskID;
    }

    public int getTimeActivityID() {
        return TimeActivityID;
    }

    public void setTimeActivityID(int timeActivityID) {
        TimeActivityID = timeActivityID;
    }

    public String getEmpSignature() {
        return EmpSignature;
    }

    public void setEmpSignature(String empSignature) {
        EmpSignature = empSignature;
    }

    public String getEmpSubDate() {
        return EmpSubDate;
    }

    public void setEmpSubDate(String empSubDate) {
        EmpSubDate = empSubDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        EndTime = endTime;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }

    public boolean isMarkedForAcceptRejectByUser() {
        return markedForAcceptRejectByUser;
    }

    public void setMarkedForAcceptRejectByUser(boolean markedForAcceptRejectByUser) {
        this.markedForAcceptRejectByUser = markedForAcceptRejectByUser;
    }

    public boolean isAcceptedByUser() {
        return acceptedByUser;
    }

    public void setAcceptedByUser(boolean acceptedByUser) {
        this.acceptedByUser = acceptedByUser;
    }

    public boolean isRejectedByUser() {
        return rejectedByUser;
    }

    public void setRejectedByUser(boolean rejectedByUser) {
        this.rejectedByUser = rejectedByUser;
    }

    public boolean isMarkedOnServer() {
        return markedOnServer;
    }

    public void setMarkedOnServer(boolean markedOnServer) {
        this.markedOnServer = markedOnServer;
    }

    public boolean isMarkedForSubmit() {
        return isMarkedForSubmit;
    }

    public void setIsMarkedForSubmit(boolean isMarkedForSubmit) {
        this.isMarkedForSubmit = isMarkedForSubmit;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        UpdatedBy = updatedBy;
    }

    public boolean isHasDeletedOnServer() {
        return hasDeletedOnServer;
    }

    public void setHasDeletedOnServer(boolean hasDeletedOnServer) {
        this.hasDeletedOnServer = hasDeletedOnServer;
    }

    public Long getTimeInMillis() {
        return timeInMillis;
    }

    public void setTimeInMillis(Long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }

    @Override
    public int compareTo(Timesheet timesheet) {
        Long time1 = timesheet.getTimeInMillis();
        Long time2 = this.getTimeInMillis();

        //ascending order
        long diff = time2 - time1;
        int difference = (int) diff;
        if (time2 > time1) return -1;
        if (time1 > time2) return 1;
        return 0;
    }

    public int getPositionInList() {
        return positionInList;
    }

    public void setPositionInList(int positionInList) {
        this.positionInList = positionInList;
    }
}