package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class AccountsDataFile implements Serializable {

    private String $id;
    private ArrayList<AccountModel> AccountsList;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public ArrayList<AccountModel> getAccountsList() {
        return AccountsList;
    }

    public void setAccountsList(ArrayList<AccountModel> accountsList) {
        AccountsList = accountsList;
    }
}
