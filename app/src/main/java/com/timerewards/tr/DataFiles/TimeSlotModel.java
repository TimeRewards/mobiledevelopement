package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class TimeSlotModel implements Serializable {

    private long startMillis;
    private long endMillis;
    private String startDate;
    private String endDate;
    private ArrayList<ProjectsInTimeSlotModel> ProjectsList = new ArrayList<>();
    private ArrayList<TasksInTimeSlotModel> TasksList = new ArrayList<>();
    private double hours;
    private String empName;
    private boolean toShowPeriod;
    private double completeTimeSlotHours;

    private ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();

    public long getStartMillis() {
        return startMillis;
    }

    public void setStartMillis(long startMillis) {
        this.startMillis = startMillis;
    }

    public long getEndMillis() {
        return endMillis;
    }

    public void setEndMillis(long endMillis) {
        this.endMillis = endMillis;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ArrayList<Timesheet> getTimesheetArrayList() {
        return timesheetArrayList;
    }

    public void setTimesheetArrayList(ArrayList<Timesheet> timesheetArrayList) {
        this.timesheetArrayList = timesheetArrayList;
    }

    public ArrayList<ProjectsInTimeSlotModel> getProjectsList() {
        return ProjectsList;
    }

    public void setProjectsList(ArrayList<ProjectsInTimeSlotModel> projectsList) {
        ProjectsList = projectsList;
    }

    public ArrayList<TasksInTimeSlotModel> getTasksList() {
        return TasksList;
    }

    public void setTasksList(ArrayList<TasksInTimeSlotModel> tasksList) {
        TasksList = tasksList;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public boolean isToShowPeriod() {
        return toShowPeriod;
    }

    public void setToShowPeriod(boolean toShowPeriod) {
        this.toShowPeriod = toShowPeriod;
    }

    public double getCompleteTimeSlotHours() {
        return completeTimeSlotHours;
    }

    public void setCompleteTimeSlotHours(double completeTimeSlotHours) {
        this.completeTimeSlotHours = completeTimeSlotHours;
    }
}
