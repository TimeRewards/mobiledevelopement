package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class AccountModel implements Serializable {

    private String $id;
    private int accountid;
    private String accountname;
    private String FullName;
    private String assettype;
    private int IsDefaultGLAnItem;
    private boolean MustEnterQtyRate = false;
    private Double DefaultRate = 0.0d;
    private boolean SubLevel;
    private int subLevelLocal;
    private boolean isClickable = true;
    private boolean LockDefaultRate = false;
    private boolean hasAdded;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getAccountid() {
        return accountid;
    }

    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        if (fullName != null) {
            int counter = 0;
            for (int i = 0; i < fullName.length(); i++) {
                if (fullName.charAt(i) == ':') {
                    counter++;
                }
            }
        }
        FullName = fullName;
    }

    public String getAssettype() {
        return assettype;
    }

    public void setAssettype(String assettype) {
        this.assettype = assettype;
    }

    public int getIsDefaultGLAnItem() {
        return IsDefaultGLAnItem;
    }

    public void setIsDefaultGLAnItem(int isDefaultGLAnItem) {
        IsDefaultGLAnItem = isDefaultGLAnItem;
    }

    public boolean isMustEnterQtyRate() {
        return MustEnterQtyRate;
    }

    public void setMustEnterQtyRate(boolean mustEnterQtyRate) {
        MustEnterQtyRate = mustEnterQtyRate;
    }

    public Double getDefaultRate() {
        return DefaultRate;
    }

    public void setDefaultRate(Double defaultRate) {
        DefaultRate = defaultRate;
    }

    public boolean isSubLevel() {
        return SubLevel;
    }

    public void setSubLevel(boolean subLevel) {
        SubLevel = subLevel;
    }

    public int getSubLevelLocal() {
        return subLevelLocal;
    }

    public void setSubLevelLocal(int subLevelLocal) {
        this.subLevelLocal = subLevelLocal;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setClickable(boolean clickable) {
        isClickable = clickable;
    }

    public void setIsClickable(boolean isClickable) {
        this.isClickable = isClickable;
    }

    public boolean isLockDefaultRate() {
        return LockDefaultRate;
    }

    public void setLockDefaultRate(boolean lockDefaultRate) {
        LockDefaultRate = lockDefaultRate;
    }

    public boolean isHasAdded() {
        return hasAdded;
    }

    public void setHasAdded(boolean hasAdded) {
        this.hasAdded = hasAdded;
    }
}
