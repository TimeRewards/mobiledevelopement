package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class ClassesModel implements Serializable {

    private String $id;
    private int ClassRowID;
    private String ClassName;
    private String FullName;
    private int SubLevel;
    private boolean isClickable = true;
    private boolean hasAdded;

    public ClassesModel() {

    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getClassRowID() {
        return ClassRowID;
    }

    public void setClassRowID(int classRowID) {
        ClassRowID = classRowID;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public int getSubLevel() {
        return SubLevel;
    }

    public void setSubLevel(int subLevel) {
        SubLevel = subLevel;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setIsClickable(boolean isClickable) {
        this.isClickable = isClickable;
    }

    public void setClickable(boolean clickable) {
        isClickable = clickable;
    }

    public boolean isHasAdded() {
        return hasAdded;
    }

    public void setHasAdded(boolean hasAdded) {
        this.hasAdded = hasAdded;
    }
}
