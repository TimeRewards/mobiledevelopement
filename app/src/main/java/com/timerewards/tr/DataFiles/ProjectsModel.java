package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class ProjectsModel implements Serializable {

    private String $id;
    private int ProjectID;
    private boolean AssignmentOptional;
    private String FullName;
    private int SubLevel;
    private String ProjectName;
    private boolean Billable;
    private int ParentID;
    private boolean Enabled;
    private boolean ExpPrj;
    private boolean isClickable = true;
    private boolean hasAdded;

    public ProjectsModel() {

    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getProjectID() {
        return ProjectID;
    }

    public void setProjectID(int projectID) {
        ProjectID = projectID;
    }

    public boolean isAssignmentOptional() {
        return AssignmentOptional;
    }

    public void setAssignmentOptional(boolean assignmentOptional) {
        AssignmentOptional = assignmentOptional;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public int getSubLevel() {
        return SubLevel;
    }

    public void setSubLevel(int subLevel) {
        SubLevel = subLevel;
    }

    public String getProjectName() {
        return ProjectName;
    }

    public void setProjectName(String projectName) {
        ProjectName = projectName;
    }

    public boolean isBillable() {
        return Billable;
    }

    public void setBillable(boolean billable) {
        Billable = billable;
    }

    public int getParentID() {
        return ParentID;
    }

    public void setParentID(int parentID) {
        ParentID = parentID;
    }

    public boolean isEnabled() {
        return Enabled;
    }

    public void setEnabled(boolean enabled) {
        Enabled = enabled;
    }

    public boolean isExpPrj() {
        return ExpPrj;
    }

    public void setExpPrj(boolean expPrj) {
        ExpPrj = expPrj;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setIsClickable(boolean isClickable) {
        this.isClickable = isClickable;
    }

    public boolean isHasAdded() {
        return hasAdded;
    }

    public void setHasAdded(boolean hasAdded) {
        this.hasAdded = hasAdded;
    }
}
