package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class Expense implements Serializable, Comparable<Expense> {

    public static Comparator<Expense> ExpenseComparator = new Comparator<Expense>() {
        @Override
        public int compare(Expense expense, Expense t1) {
            return expense.compareTo(t1);
        }
    };
    private ExpenseHeaderModel ExpHeader;
    private ArrayList<ExpenseItemModel> ExpenseItemList = new ArrayList<>();
    private boolean isMarkedForDeletion = false;
    private boolean markedForAcceptRejectByUser = false;
    private boolean acceptedByUser = false;
    private boolean rejectedByUser = false;
    private boolean markedOnServer = false;

    public int getOverAllStatus() {
        for (int i = 0; i < ExpenseItemList.size(); i++) {
            if (!ExpenseItemList.get(i).isHasDeletedInDevice()) {
                if (ExpenseItemList.get(i).getEZStatusID() == 0) {
                    return 0;
                }
            }
        }
        for (int i = 0; i < ExpenseItemList.size(); i++) {
            if (!ExpenseItemList.get(i).isHasDeletedInDevice()) {
                if (ExpenseItemList.get(i).getEZStatusID() == 1) {
                    return 1;
                }
            }
        }

        return 2;

    }

    public ExpenseHeaderModel getExpHeader() {
        return ExpHeader;
    }

    public void setExpHeader(ExpenseHeaderModel expHeader) {
        ExpHeader = expHeader;
    }

    public ArrayList<ExpenseItemModel> getExpenseItemList() {
        return ExpenseItemList;
    }

    public void setExpenseItemList(ArrayList<ExpenseItemModel> expenseItemList) {
        ExpenseItemList = expenseItemList;
    }

    public boolean isMarkedForDeletion() {
        return isMarkedForDeletion;
    }

    public void setIsMarkedForDeletion(boolean isMarkedForDeletion) {
        this.isMarkedForDeletion = isMarkedForDeletion;
    }

    public boolean isMarkedOnServer() {
        return markedOnServer;
    }

    public void setMarkedOnServer(boolean markedOnServer) {
        this.markedOnServer = markedOnServer;
    }

    public boolean isMarkedForAcceptRejectByUser() {
        return markedForAcceptRejectByUser;
    }

    public void setMarkedForAcceptRejectByUser(boolean markedForAcceptRejectByUser) {
        this.markedForAcceptRejectByUser = markedForAcceptRejectByUser;
    }

    public boolean isAcceptedByUser() {
        return acceptedByUser;
    }

    public void setAcceptedByUser(boolean acceptedByUser) {
        this.acceptedByUser = acceptedByUser;
    }

    public boolean isRejectedByUser() {
        return rejectedByUser;
    }

    public void setRejectedByUser(boolean rejectedByUser) {
        this.rejectedByUser = rejectedByUser;
    }

    @Override
    public int compareTo(Expense expense) {
        Long time1 = expense.getExpHeader().getTimeInMillis();
        Long time2 = this.getExpHeader().getTimeInMillis();

        //ascending order
        long diff = time2 - time1;
        int difference = (int) diff;
        if (time2 > time1) return -1;
        if (time1 > time2) return 1;
        return 0;
    }
}
