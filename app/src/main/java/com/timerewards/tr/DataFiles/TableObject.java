package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class TableObject implements Serializable {

    private String $id;
    private int Userid;
    private boolean IsValid;
    private String DisplayName;
    private ArrayList<Timesheet> timesheetList;
    private String SiteCode;

    public TableObject() {
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getUserid() {
        return Userid;
    }

    public void setUserid(int userid) {
        Userid = userid;
    }

    public boolean isValid() {
        return IsValid;
    }

    public void setIsValid(boolean isValid) {
        IsValid = isValid;
    }

    public String getDisplayName() {
        return DisplayName;
    }

    public void setDisplayName(String displayName) {
        DisplayName = displayName;
    }

    public ArrayList<Timesheet> getTimesheetList() {
        return timesheetList;
    }

    public void setTimesheetList(ArrayList<Timesheet> timesheetList) {
        this.timesheetList = timesheetList;
    }

    public String getSiteCode() {
        return SiteCode;
    }

    public void setSiteCode(String siteCode) {
        SiteCode = siteCode;
    }
}