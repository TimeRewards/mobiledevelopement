package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class ExpenseItemSubmitResponse implements Serializable {

    private String $id;
    private int UserID;
    private int EmployeeExpenseID;
    private int EmployeeExpenseItemID;
    private int EZStatusID;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public int getEmployeeExpenseID() {
        return EmployeeExpenseID;
    }

    public void setEmployeeExpenseID(int employeeExpenseID) {
        EmployeeExpenseID = employeeExpenseID;
    }

    public int getEmployeeExpenseItemID() {
        return EmployeeExpenseItemID;
    }

    public void setEmployeeExpenseItemID(int employeeExpenseItemID) {
        EmployeeExpenseItemID = employeeExpenseItemID;
    }

    public int getEZStatusID() {
        return EZStatusID;
    }

    public void setEZStatusID(int EZStatusID) {
        this.EZStatusID = EZStatusID;
    }
}
