package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class ExpenseHeaderSubmitResponse implements Serializable {

    private String $id;
    private int UserID;
    private int EmployeeExpenseID;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public int getEmployeeExpenseID() {
        return EmployeeExpenseID;
    }

    public void setEmployeeExpenseID(int employeeExpenseID) {
        EmployeeExpenseID = employeeExpenseID;
    }
}
