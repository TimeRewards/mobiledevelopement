package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class PhotoUploadResponse implements Serializable {

    private String uploaded;

    public String getUploaded() {
        return uploaded;
    }

    public void setUploaded(String uploaded) {
        this.uploaded = uploaded;
    }
}
