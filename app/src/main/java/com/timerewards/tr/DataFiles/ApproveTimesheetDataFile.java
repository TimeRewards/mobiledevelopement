package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class ApproveTimesheetDataFile implements Serializable {

    private ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();
    private ArrayList<TimeSlotModel> timeSlotModelArrayList = new ArrayList<>();

    public ApproveTimesheetDataFile() {
    }

    public ArrayList<Timesheet> getTimesheetArrayList() {
        return timesheetArrayList;
    }

    public void setTimesheetArrayList(ArrayList<Timesheet> timesheetArrayList) {
        this.timesheetArrayList = timesheetArrayList;
    }

    public ArrayList<TimeSlotModel> getTimeSlotModelArrayList() {
        return timeSlotModelArrayList;
    }

    public void setTimeSlotModelArrayList(ArrayList<TimeSlotModel> timeSlotModelArrayList) {
        this.timeSlotModelArrayList = timeSlotModelArrayList;
    }
}
