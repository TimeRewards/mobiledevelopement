package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class ExpenseToApproveDataFile implements Serializable {

    public String $id;
    public int userid;

    public ArrayList<Expense> ApproverExpense = new ArrayList<>();

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public ArrayList<com.timerewards.tr.DataFiles.Expense> getExpense() {
        return ApproverExpense;
    }

    public void setExpense(ArrayList<com.timerewards.tr.DataFiles.Expense> expense) {
        ApproverExpense = expense;
    }
}
