package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class ExpenseHeaderModel implements Serializable {

    private String $id;
    private int EmployeeExpenseID;
    private int userID;
    private String EntryDate;
    private String Description;
    private String CreatedBy;
    private String CreatedDate;
    private String UpdatedBy;
    private String UpdatedDate;
    private boolean hasMarkedForDeletion = false;
    private boolean hasDeletedOnServer = false;
    private boolean markedForSubmit = false;
    private Long timeInMillis;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getEmployeeExpenseID() {
        return EmployeeExpenseID;
    }

    public void setEmployeeExpenseID(int employeeExpenseID) {
        EmployeeExpenseID = employeeExpenseID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getEntryDate() {
        return EntryDate;
    }

    public void setEntryDate(String entryDate) {
        this.EntryDate = entryDate;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        this.CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        this.CreatedDate = createdDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.UpdatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.UpdatedDate = updatedDate;
    }

    public boolean isHasMarkedForDeletion() {
        return hasMarkedForDeletion;
    }

    public void setHasMarkedForDeletion(boolean hasMarkedForDeletion) {
        this.hasMarkedForDeletion = hasMarkedForDeletion;
    }

    public boolean isHasDeletedOnServer() {
        return hasDeletedOnServer;
    }

    public void setHasDeletedOnServer(boolean hasDeletedOnServer) {
        this.hasDeletedOnServer = hasDeletedOnServer;
    }

    public boolean isMarkedForSubmit() {
        return markedForSubmit;
    }

    public void setMarkedForSubmit(boolean markedForSubmit) {
        this.markedForSubmit = markedForSubmit;
    }

    public Long getTimeInMillis() {
        return timeInMillis;
    }

    public void setTimeInMillis(Long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }
}
