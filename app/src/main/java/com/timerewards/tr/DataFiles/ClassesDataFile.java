package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class ClassesDataFile implements Serializable {

    private ArrayList<ClassesModel> classesModelList;

    public ClassesDataFile() {
    }

    public ArrayList<ClassesModel> getClassesModelList() {
        return classesModelList;
    }

    public void setClassesModelList(ArrayList<ClassesModel> classesModelList) {
        this.classesModelList = classesModelList;
    }
}
