package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class TimerModel implements Serializable {

    private String TimerName;
    private boolean running = false;
    private Long millis = 0l;
    private Long startTime;
    private Timesheet timesheet;

    private long calStartTime;
    private long calEndTime;
    private long balance=0l;

    public String getTimerName() {
        return TimerName;
    }

    public void setTimerName(String timerName) {
        TimerName = timerName;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public Long getMillis() {
        return millis;
    }

    public void setMillis(Long millis) {
        this.millis = millis;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Timesheet getTimesheet() {
        return timesheet;
    }

    public void setTimesheet(Timesheet timesheet) {
        this.timesheet = timesheet;
    }

    public long getCalStartTime() {
        return calStartTime;
    }

    public void setCalStartTime(long calStartTime) {
        this.calStartTime = calStartTime;
    }

    public long getCalEndTime() {
        return calEndTime;
    }

    public void setCalEndTime(long calEndTime) {
        this.calEndTime = calEndTime;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }
}
