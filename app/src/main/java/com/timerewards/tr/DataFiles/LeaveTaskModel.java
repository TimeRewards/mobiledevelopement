package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class LeaveTaskModel implements Serializable {

    private String $id;
    private int TaskID;
    private String FullName;
    private String TaskName;
    private int SubLevel;

    private boolean isClickable = true;

    public int getSubLevel() {
        return SubLevel;
    }

    public void setSubLevel(int subLevel) {
        SubLevel = subLevel;
    }

    public String getTaskName() {
        return TaskName;
    }

    public void setTaskName(String taskName) {
        TaskName = taskName;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public int getTaskID() {
        return TaskID;
    }

    public void setTaskID(int taskID) {
        TaskID = taskID;
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setIsClickable(boolean isClickable) {
        this.isClickable = isClickable;
    }
}
