package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class ProjectsInTimeSlotModel implements Serializable {

    private String projectName;
    private double hours;
    private ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    public ArrayList<Timesheet> getTimesheetArrayList() {
        return timesheetArrayList;
    }

    public void setTimesheetArrayList(ArrayList<Timesheet> timesheetArrayList) {
        this.timesheetArrayList = timesheetArrayList;
    }
}
