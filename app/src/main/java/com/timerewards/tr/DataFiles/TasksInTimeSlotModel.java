package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class TasksInTimeSlotModel implements Serializable {

    private String TaskName;
    private double hours;
    private ArrayList<Timesheet> timesheetArrayList = new ArrayList<>();

    public String getTaskName() {
        return TaskName;
    }

    public void setTaskName(String taskName) {
        TaskName = taskName;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    public ArrayList<Timesheet> getTimesheetArrayList() {
        return timesheetArrayList;
    }

    public void setTimesheetArrayList(ArrayList<Timesheet> timesheetArrayList) {
        this.timesheetArrayList = timesheetArrayList;
    }
}
