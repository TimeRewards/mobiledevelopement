package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class ProjectsDataFile implements Serializable {
    private ArrayList<ProjectsModel> projectsModelList;

    public ProjectsDataFile() {

    }

    public ArrayList<ProjectsModel> getProjectsModelList() {
        return projectsModelList;
    }

    public void setProjectsModelList(ArrayList<ProjectsModel> projectsModelList) {
        this.projectsModelList = projectsModelList;
    }
}
