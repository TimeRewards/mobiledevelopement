package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class ApproveRejectExpenseResponseModel implements Serializable {

    private String $id;
    private int UserID;
    private ArrayList<ExpenseItemModel> EmployeeExpenseItems = new ArrayList<>();

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public ArrayList<ExpenseItemModel> getEmployeeExpenseItems() {
        return EmployeeExpenseItems;
    }

    public void setEmployeeExpenseItems(ArrayList<ExpenseItemModel> employeeExpenseItems) {
        EmployeeExpenseItems = employeeExpenseItems;
    }
}
