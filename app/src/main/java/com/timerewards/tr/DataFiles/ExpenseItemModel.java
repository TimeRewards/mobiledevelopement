package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class ExpenseItemModel implements Serializable {

    private String $id;
    private int EmployeeExpenseItemID;
    private int EmployeeExpenseID;
    private String ExpenseDate;
    private float ExpenseAmount;
    private int PaidBy;
    private boolean IsReimbursable;
    private String Merchant = "";
    private String Description = "";
    private String CCAccountName;
    private int ApproverID;
    private int ProjectID;
    private int AccountID;
    private int ClassRowID;
    private String EmployeeFullName;
    private String ProjectFullName;
    private String AccountFullName;
    private String ClassFullName;
    private boolean Billable;
    private boolean IsDefaultGLAnItem;
    private int EZStatusID;
    private int userID;
    private String EmpSignature;
    private String EmpSubDate;
    private String AppSignature;
    private String AppSubDate;
    private String CreatedBy;
    private String CreatedDate;
    private String UpdatedBy;
    private String UpdatedDate;
    private String receiptName = "";
    private String AdminNotes;
    private boolean markedForAcceptRejectByUser = false;
    private boolean acceptedByUser = false;
    private boolean rejectedByUser = false;
    private boolean markedForSubmit = false;
    private boolean markedApprovedRejectedOnServer = false;
    private boolean isMarkedForDeletion = false;
    private boolean hasDeletedOnServer = false;
    private boolean hasReceiptUploadedOnServer = false;
    private boolean hasDeletedInDevice = false;
    private Double Qty = 0.0d;
    private Double Price = 0.0d;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public int getEmployeeExpenseItemID() {
        return EmployeeExpenseItemID;
    }

    public void setEmployeeExpenseItemID(int employeeExpenseItemID) {
        EmployeeExpenseItemID = employeeExpenseItemID;
    }

    public int getEmployeeExpenseID() {
        return EmployeeExpenseID;
    }

    public void setEmployeeExpenseID(int employeeExpenseID) {
        EmployeeExpenseID = employeeExpenseID;
    }

    public String getExpenseDate() {
        return ExpenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        ExpenseDate = expenseDate;
    }

    public float getExpenseAmount() {
        return ExpenseAmount;
    }

    public void setExpenseAmount(float expenseAmount) {
        ExpenseAmount = expenseAmount;
    }

    public int getPaidBy() {
        return PaidBy;
    }

    public void setPaidBy(int paidBy) {
        PaidBy = paidBy;
    }

    public boolean isReimbursable() {
        return IsReimbursable;
    }

    public void setReimbursable(boolean reimbursable) {
        IsReimbursable = reimbursable;
    }

    public void setIsReimbursable(boolean isReimbursable) {
        IsReimbursable = isReimbursable;
    }

    public String getMerchant() {
        if (Merchant == null) {
            return "";
        }
        return Merchant;
    }

    public void setMerchant(String merchant) {
        Merchant = merchant;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCCAccountName() {
        return CCAccountName;
    }

    public void setCCAccountName(String CCAccountName) {
        this.CCAccountName = CCAccountName;
    }

    public int getApproverID() {
        return ApproverID;
    }

    public void setApproverID(int approverID) {
        ApproverID = approverID;
    }

    public int getProjectID() {
        return ProjectID;
    }

    public void setProjectID(int projectID) {
        ProjectID = projectID;
    }

    public int getAccountID() {
        return AccountID;
    }

    public void setAccountID(int accountID) {
        AccountID = accountID;
    }

    public int getClassRowID() {
        return ClassRowID;
    }

    public void setClassRowID(int classRowID) {
        ClassRowID = classRowID;
    }

    public String getEmployeeFullName() {
        return EmployeeFullName;
    }

    public void setEmployeeFullName(String employeeFullName) {
        EmployeeFullName = employeeFullName;
    }

    public String getProjectFullName() {
        return ProjectFullName;
    }

    public void setProjectFullName(String projectFullName) {
        ProjectFullName = projectFullName;
    }

    public String getAccountFullName() {
        return AccountFullName;
    }

    public void setAccountFullName(String accountFullName) {
        AccountFullName = accountFullName;
    }

    public String getClassFullName() {
        return ClassFullName;
    }

    public void setClassFullName(String classFullName) {
        ClassFullName = classFullName;
    }

    public boolean isBillable() {
        return Billable;
    }

    public void setBillable(boolean billable) {
        Billable = billable;
    }

    public boolean isDefaultGLAnItem() {
        return IsDefaultGLAnItem;
    }

    public void setDefaultGLAnItem(boolean defaultGLAnItem) {
        IsDefaultGLAnItem = defaultGLAnItem;
    }

    public void setIsDefaultGLAnItem(boolean isDefaultGLAnItem) {
        IsDefaultGLAnItem = isDefaultGLAnItem;
    }

    public int getEZStatusID() {
        return EZStatusID;
    }

    public void setEZStatusID(int EZStatusID) {
        this.EZStatusID = EZStatusID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getEmpSignature() {
        return EmpSignature;
    }

    public void setEmpSignature(String empSignature) {
        EmpSignature = empSignature;
    }

    public String getEmpSubDate() {
        return EmpSubDate;
    }

    public void setEmpSubDate(String empSubDate) {
        EmpSubDate = empSubDate;
    }

    public String getAppSignature() {
        return AppSignature;
    }

    public void setAppSignature(String appSignature) {
        AppSignature = appSignature;
    }

    public String getAppSubDate() {
        return AppSubDate;
    }

    public void setAppSubDate(String appSubDate) {
        AppSubDate = appSubDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getUpdatedBy() {
        return UpdatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        UpdatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }

    public String getReceiptName() {
        return receiptName;
    }

    public void setReceiptName(String receiptName) {
        this.receiptName = receiptName;
    }

    public boolean isMarkedForAcceptRejectByUser() {
        return markedForAcceptRejectByUser;
    }

    public void setMarkedForAcceptRejectByUser(boolean markedForAcceptRejectByUser) {
        this.markedForAcceptRejectByUser = markedForAcceptRejectByUser;
    }

    public boolean isAcceptedByUser() {
        return acceptedByUser;
    }

    public void setAcceptedByUser(boolean acceptedByUser) {
        this.acceptedByUser = acceptedByUser;
    }

    public boolean isRejectedByUser() {
        return rejectedByUser;
    }

    public void setRejectedByUser(boolean rejectedByUser) {
        this.rejectedByUser = rejectedByUser;
    }

    public boolean isMarkedForSubmit() {
        return markedForSubmit;
    }

    public void setMarkedForSubmit(boolean markedForSubmit) {
        this.markedForSubmit = markedForSubmit;
    }

    public boolean isMarkedApprovedRejectedOnServer() {
        return markedApprovedRejectedOnServer;
    }

    public void setMarkedApprovedRejectedOnServer(boolean markedApprovedRejectedOnServer) {
        this.markedApprovedRejectedOnServer = markedApprovedRejectedOnServer;
    }

    public boolean isMarkedForDeletion() {
        return isMarkedForDeletion;
    }

    public void setMarkedForDeletion(boolean markedForDeletion) {
        isMarkedForDeletion = markedForDeletion;
    }

    public void setIsMarkedForDeletion(boolean isMarkedForDeletion) {
        this.isMarkedForDeletion = isMarkedForDeletion;
    }

    public boolean isHasDeletedOnServer() {
        return hasDeletedOnServer;
    }

    public void setHasDeletedOnServer(boolean hasDeletedOnServer) {
        this.hasDeletedOnServer = hasDeletedOnServer;
    }

    public boolean isHasReceiptUploadedOnServer() {
        return hasReceiptUploadedOnServer;
    }

    public void setHasReceiptUploadedOnServer(boolean hasReceiptUploadedOnServer) {
        this.hasReceiptUploadedOnServer = hasReceiptUploadedOnServer;
    }

    public boolean isHasDeletedInDevice() {
        return hasDeletedInDevice;
    }

    public void setHasDeletedInDevice(boolean hasDeletedInDevice) {
        this.hasDeletedInDevice = hasDeletedInDevice;
    }

    public Double getQty() {
        return Qty;
    }

    public void setQty(Double qty) {
        Qty = qty;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double price) {
        Price = price;
    }

    public String getAdminNotes() {
        return AdminNotes;
    }

    public void setAdminNotes(String adminNotes) {
        AdminNotes = adminNotes;
    }
}