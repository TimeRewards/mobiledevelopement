package com.timerewards.tr.DataFiles;

import java.io.Serializable;
import java.util.ArrayList;

public class TimerDataFile implements Serializable {

    private ArrayList<TimerModel> timerModelArrayList = new ArrayList<>();

    public ArrayList<TimerModel> getTimerModelArrayList() {
        return timerModelArrayList;
    }

    public void setTimerModelArrayList(ArrayList<TimerModel> timerModelArrayList) {
        this.timerModelArrayList = timerModelArrayList;
    }
}
