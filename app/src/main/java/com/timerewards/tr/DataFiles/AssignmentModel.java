package com.timerewards.tr.DataFiles;

import java.io.Serializable;

public class AssignmentModel implements Serializable {

    private String $id;
    private int ProjectID;
    private int TaskID;
    private int ParentID;
    private int SubLevel;
    private boolean isClickable = true;
    private int UserID;

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public int getSubLevel() {
        return SubLevel;
    }

    public void setSubLevel(int subLevel) {
        SubLevel = subLevel;
    }

    public int getParentID() {
        return ParentID;
    }

    public void setParentID(int parentID) {
        ParentID = parentID;
    }

    public int getTaskID() {
        return TaskID;
    }

    public void setTaskID(int taskID) {
        TaskID = taskID;
    }

    public int getProjectID() {
        return ProjectID;
    }

    public void setProjectID(int projectID) {
        ProjectID = projectID;
    }

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setIsClickable(boolean isClickable) {
        this.isClickable = isClickable;
    }
}
